package com.skilledanswers.picnart.custom;

import com.skilledanswers.picnart.R;

public enum CustomPagerEnum {

    RED(R.string.app_name, R.layout.banner_image_layout),
    BLUE(R.string.app_name, R.layout.banner_image_layout),
    ORANGE(R.string.app_name, R.layout.banner_image_layout),
    GREEN(R.string.app_name, R.layout.banner_image_layout),
    RED1(R.string.app_name, R.layout.banner_image_layout),
    BLUE1(R.string.app_name, R.layout.banner_image_layout),
    ORANGE1(R.string.app_name, R.layout.banner_image_layout);

    private int mTitleResId;
    private int mLayoutResId;

    CustomPagerEnum(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}