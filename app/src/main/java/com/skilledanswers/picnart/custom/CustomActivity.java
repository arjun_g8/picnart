package com.skilledanswers.picnart.custom;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skilledanswers.picnart.LoginActivity;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.PrefUtil;

import java.util.ArrayList;
import java.util.HashMap;


public class CustomActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener {


    private HashMap<String, String> action;

    private RelativeLayout container;

    protected Toolbar toolbar;
    private DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    private int count;
    private AlertDialog myalertDialog = null;
    FragmentManager fragmentManager;
    private TextView cart_count_text, notification_count_text;

    //protected static ArrayList<AvailableTrip> availableTripsFromBase = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
//        }
        setContentView(R.layout.activity_custom);


        container = findViewById(R.id.relative);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert toolbar != null;
        toolbar.findViewById(R.id.home_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

        toolbar.setNavigationIcon(null);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);


        setToggle(true);
    }

    public void setToggle(
            boolean isEnabled) {
//
//        if (isEnabled) {
//
//            toggle = new ActionBarDrawerToggle(
//                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//            drawer.addDrawerListener(toggle);
//            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//            toggle.setDrawerIndicatorEnabled(true);
//            toggle.syncState();
//
//        } else {
//
//
//            toggle = new ActionBarDrawerToggle(
//                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//            drawer.addDrawerListener(toggle);
//            toggle.setDrawerIndicatorEnabled(false);
//            toggle.syncState();
//        }
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
///*            if (getSupportFragmentManager().getBackStackEntryCount()==0){
//                setToggle(true);
//            }*/
//
//        }
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom, menu);
        MenuItem notification = menu.findItem(R.id.action_notification);
        MenuItem cart_count = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(notification, R.layout.action_bar_notification);
        MenuItemCompat.setActionView(cart_count, R.layout.cart_count);
        cart_count.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        notification.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        View cart = cart_count.getActionView();
        cart_count_text = cart.findViewById(R.id.cart_count_text);
        View notification_count = notification.getActionView();
        notification_count_text = notification_count.findViewById(R.id.hotlist_notificationtext);
        MenuItem menuItem = menu.findItem(R.id.action_myaccount);
        MenuItem menuItemaction_myorders = menu.findItem(R.id.action_myorders);
        MenuItem menuItemaction_wishlist = menu.findItem(R.id.action_wishlist);
        MenuItem menuItemaction_rateapp = menu.findItem(R.id.action_rateapp);
        MenuItem menuItemaction_help_center = menu.findItem(R.id.action_help_center);
        MenuItem menuItemaction_legal = menu.findItem(R.id.action_legal);


       // SharedPreferences sharedPreferences = getSharedPreferences(AllConstants.SESSION_PREFERENCE_NAME, MODE_PRIVATE);

       /* if (sharedPreferences != null) {
            if (sharedPreferences.getString(AllConstants.SESSION_PREF_KEY_SESSION_TOKEN, null) == null) {
                menuItem.setTitle("Login");
                menuItemaction_myorders.setVisible(false);
                menuItemaction_wishlist.setVisible(false);
                menuItemaction_rateapp.setVisible(false);
                menuItemaction_help_center.setVisible(false);
                menuItemaction_legal.setVisible(false);
            }
        }*/


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_myaccount) {
            // Handle the camera action
            PrefUtil prefUtil = new PrefUtil(CustomActivity.this);
            prefUtil.clearPref();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finishAffinity();
            return true;

        }
        if (id != R.id.action_myaccount && id != R.id.main) {
            Toast.makeText(this, "Coming Soon...", Toast.LENGTH_SHORT).show();
            return true;

        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        //MainFragment fragment = new MainFragment();
        // fragmentTransition(MainFragment.newInstance());


        //setToggle(false);
        if (id == R.id.nav_account) {
            // Handle the camera action


        } else if (id == R.id.nav_home) {
            //getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);


        } else if (id == R.id.nav_orders) {


        } else if (id == R.id.nav_wishlist) {
            //  fragmentTransition();

        } else {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub


    }

    /**
     * Sets the touch and click listeners for a view..
     *
     * @param id the id of View
     * @return the view
     */


    /**
     * Sets the click listener for a view.
     *
     * @param id the id of View
     * @return the view
     */
    public View setClick(int id) {

        View v = findViewById(id);
        v.setOnClickListener(this);
        return v;
    }


    private void displayDatas(Bundle resultData) {

    }

    public void fragmentTransition(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // transaction.setCustomAnimations(R.animator.in,R.animator.out);
        transaction.replace(R.id.relative, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    public void addCart() {
        count = count + 1;
        cart_count_text.setText("" + count);
    }


}
