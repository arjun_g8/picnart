package com.skilledanswers.picnart.firebase;


import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import bluepaymax.skilledanswers.picnart.bus.utils.AllConstants;

public class MyFirebaseIDService extends FirebaseInstanceIdService {

    public MyFirebaseIDService() {
    }

    private static final String TAG = "MyFirebaseIDService";


    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String token) {
        //You can implement this method to store the token on your server
        //Not required for current project
       /* SharedPreferences preferences = this.getSharedPreferences(AllConstants.FCM_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(AllConstants.FCM_PREFERENCE_KEY_VALUE,token);
        editor.apply();*/

    }
}
