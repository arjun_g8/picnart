package com.skilledanswers.picnart.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.skilledanswers.picnart.R;

import java.util.concurrent.ExecutionException;

import bluepaymax.skilledanswers.picnart.R;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            final String appPackageName = getPackageName();
            System.out.println("------- PACKAGE NAME: " + appPackageName);
            System.out.println("jjjjjjjjjjjjjjjjjjjjjj user... remote message----------------" + remoteMessage.getData());
            // Toast.makeText(this, "" + remoteMessage.getData().toString(), Toast.LENGTH_SHORT).show();
            if (remoteMessage != null) {
                switch (remoteMessage.getData().get("type")) {
                    case "text":
                        try {
                            createNewNotification("Bluepaymax", remoteMessage.getData().get("contentText"));
                            //dropNotificationTypeText(remoteMessage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "image":
                        try {
                            createNewNotification("Bluepaymax", remoteMessage.getData().get("contentText"));
                            //dropImageNotification(remoteMessage);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("----------- exc: " + e.toString());
        }


    }


    private void dropNotificationTypeText(RemoteMessage message) throws Exception {
        //  Intent intent = new Intent(this, SplashScreen.class);
        //  PendingIntent pendingIntent = null;

        Intent playStoreIntent;
        PendingIntent pendingIntent;

        final String appPackageName = getPackageName();

        System.out.println("------- PACKAGE NAME: " + appPackageName);
        // getPackageName() from Context or Activity object
        try {
            playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        } catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            System.out.println("----------- EXCEPTION: " + anfe.toString());
            playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }

        pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, playStoreIntent, 0);

        //pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        Bitmap bitmap;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_only_logo_blue);
        } else
            bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher_web);

        int notificationIcon;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationIcon = R.drawable.ic_only_logo_blue;
        } else
            notificationIcon = R.drawable.ic_launcher_web;
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, "1")
                        .setLargeIcon(bitmap)
                        .setSmallIcon(notificationIcon)
                        .setContentTitle("BluepayMax app has new update.")
                        .setTicker("BluepayMax app has new update.")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(message.getData().get("contentText")))
                        .setOngoing(false)
                        .setContentIntent(pendingIntent)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setPriority(Notification.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentText(message.getData().get("contentText"));

        assert notificationManager != null;
        notificationManager.notify(0, mBuilder.build());
    }

    private void dropImageNotification(RemoteMessage message) throws Exception {
        Bitmap bitmapImage = null;
        try {
            bitmapImage = Glide.with(this)
                    .load(message.getData().get("url"))
                    .asBitmap()
                    .into(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        if (bitmapImage != null) {
            // Intent intent = new Intent(this, SplashScreen.class);
            Intent playStoreIntent;
            PendingIntent pendingIntent = null;

            final String appPackageName = getPackageName();

            System.out.println("------- PACKAGE NAME: " + appPackageName);
            // getPackageName() from Context or Activity object
            try {
                playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
            } catch (android.content.ActivityNotFoundException anfe) {
                anfe.printStackTrace();
                System.out.println("----------- EXCEPTION: " + anfe.toString());
                playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
            }

            pendingIntent = PendingIntent.getActivity(this, 0, playStoreIntent, 0);

            Bitmap bitmap;
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_only_logo_blue);
            } else
                bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher_web);

            int notificationIcon;

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationIcon = R.drawable.ic_only_logo_blue;
            } else
                notificationIcon = R.drawable.ic_launcher_web;

            NotificationManager notificationManager = (NotificationManager)
                    this.getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this, "1")
                            .setLargeIcon(bitmap)
                            .setSmallIcon(notificationIcon)
                            .setContentTitle("BluepayMax app has new update.")
                            .setContentText(message.getData().get("contentText"))
                            .setTicker("BluepayMax")
                            .setContentIntent(pendingIntent)
                            .setStyle(new NotificationCompat.BigPictureStyle()
                                    .bigPicture(bitmap)
                                    .setBigContentTitle("BluepayMax app has new update.")
                                    .setSummaryText(message.getData().get("contentText")))
                            .setOngoing(false)
                            .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setAutoCancel(true);

            // builder.setContentIntent(pendingIntent);
            assert notificationManager != null;
            notificationManager.notify(0, builder.build());
        }

    }

    private void createNewNotification(String title, String msg) {
        Intent playStoreIntent;
        final String appPackageName = "bluepaymax.bluepayventures.bluepaymaxapp";
        // Intent intent = new Intent(remote.getNotification().getClickAction());

        try {
            playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        } catch (android.content.ActivityNotFoundException anfe) {
            anfe.printStackTrace();
            System.out.println("----------- EXCEPTION: " + anfe.toString());
            playStoreIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }


        NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notifBulider = new NotificationCompat.Builder(this, "1");
        notifBulider.setSmallIcon(R.drawable.ic_launcher_web);
        notifBulider.setContentTitle(title);
        notifBulider.setContentText(msg);
        notifBulider.setShowWhen(true);

        notifBulider.setContentIntent(PendingIntent.getActivity(this, 0, playStoreIntent, 0));
        Uri notificationS = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notifBulider.setSound(notificationS);
        assert mNotificationManager != null;
        mNotificationManager.notify(1, notifBulider.build());
    }
}



