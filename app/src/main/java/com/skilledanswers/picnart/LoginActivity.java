package com.skilledanswers.picnart;

import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    /////////////////////////////////////////////////////////////////////////////////////////////////// global widgits's start

    // TextInputLayout
    private TextInputLayout textInputLayoutEmail = null;
    private TextInputLayout textInputLayoutPwd = null;
    // AppCompatEditText
    private AppCompatEditText editTextEmail = null;
    private AppCompatEditText editTextPwd = null;
    // AppCompatButton
    private AppCompatButton buttonSubmit = null;
    private AppCompatButton signup = null;
    // AppCompatTextView
    private AppCompatTextView textViewForgotPwd = null;
    private ProgressDialog progressDialog = null;
    private EcommerceAPIInterface apiService = null;
    ////////////////////////////////////////////////////////////////////////////////////////////////////// global widgits's end

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /////////////////////////////////////////////////////////////////////////////////////// hiding the window icon bar..
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setAllowEnterTransitionOverlap(false);
            getWindow().setAllowReturnTransitionOverlap(false);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////// end
        setContentView(R.layout.login);
        //////////////////////////////////////////////////////////////////////////////// setting the toolbar

        //getSupportActionBar().setTitle("Login");
        ////////////////////////////////////////////////////////////////////////////////////////////end
        ///////////////////////// connecting widgits to java

        //
        textInputLayoutEmail = findViewById(R.id.login_activity_emailTextInputLayoutID);
        textInputLayoutPwd = findViewById(R.id.login_activity_pwdTextInputLayoutID);
        //
        editTextEmail = findViewById(R.id.login_activity_emailEdittextID);

        editTextEmail.setText("BVPL");
        Selection.setSelection(editTextEmail.getText(), editTextEmail.getText().length());

        editTextEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("BVPL")){
                    editTextEmail.setText("BVPL");
                    Selection.setSelection(editTextEmail.getText(), editTextEmail.getText().length());

                }
            }
        });

        editTextPwd = findViewById(R.id.login_activity_pwdEdittextID);
        //
        buttonSubmit = findViewById(R.id.login_activity_loginButtonID);

        //   signup = findViewById(R.id.login_activity_signup);
        //
       /* textViewForgotPwd = findViewById(R.id.login_activity_forgotPwdTextviewButtonID);
        textViewForgotPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*Intent intent = new Intent(LoginActivity.this, LandingPage.class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    //getWindow().setEnterTransition(new Explode().setDuration(500));
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this).toBundle());
                } else
                    startActivity(intent);*//*
            }
        });

        ///////////////////////// end
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
            }
        });*/

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isThereInternet(LoginActivity.this)) {
                    if (validateBVPL() && validatePassword())
                        postLoginData();
                } else
                    DialogUtils.showNoInternetDialog(LoginActivity.this);

            }
        });
        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (textInputLayoutEmail.isErrorEnabled())
                    textInputLayoutEmail.setErrorEnabled(false);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editTextPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (textInputLayoutPwd.isErrorEnabled())
                    textInputLayoutPwd.setErrorEnabled(false);

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void postLoginData() {
        showProgressDialog();
        EcommerceAPIInterface apiService = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiService.performLogin("memberRemoteLogin", editTextEmail.getText().toString(), editTextPwd.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        dismissPdialog();
                        System.out.println("dddddddddd----log raw------" + response.raw());
                        if (response.isSuccessful()) {
                            try {
                                String s = new String(response.body().bytes());
                                System.out.println("dddddddddd----log s---" + s);
                                JSONObject object1 = new JSONObject(s);

                                if (object1.getBoolean("status")) {
                                    Toast.makeText(LoginActivity.this, "Login Success", Toast.LENGTH_SHORT).show();

                                 /*   Intent intent = new Intent(LoginActivity.this, LandingScreenActivity.class);
                                    startActivity(intent);
                                    finish();*/
                                } else {
                                    Toast.makeText(LoginActivity.this, "" + object1.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            Toast.makeText(LoginActivity.this, "" + response.message(), Toast.LENGTH_SHORT).show();
                            PrefUtil prefUtil = new PrefUtil(LoginActivity.this);
                            prefUtil.setIsLoggedIn(false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissPdialog();
                        Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        PrefUtil prefUtil = new PrefUtil(LoginActivity.this);
                        prefUtil.setIsLoggedIn(false);
                    }
                });
    }

    private boolean validateBVPL() {
        if (editTextEmail.getText().toString().isEmpty()) {
            textInputLayoutEmail.setError("BVPL ID required!");
            editTextEmail.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean validatePassword() {
        if (editTextPwd.getText().toString().isEmpty()) {
            textInputLayoutPwd.setError("Password required!");
            editTextPwd.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private void showProgressDialog() {

        if (progressDialog == null)
            progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    private void dismissPdialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (apiService != null)
            apiService.performLogin("", "", "").cancel();
    }
}
