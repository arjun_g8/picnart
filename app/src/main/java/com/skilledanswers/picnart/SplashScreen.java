package com.skilledanswers.picnart;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.skilledanswers.picnart.Utils.PrefUtil;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class SplashScreen extends AppCompatActivity {
    /**
     * Check if the app is running.
     */
    private boolean isRunning;

    private AppCompatImageView logoIV;

    private LinearLayout mainLayout = null;
    /*    private RelativeLayout whiteLayout = null;
        private RelativeLayout redLayout = null;*/
    private AppCompatImageView logo = null, belowLogo = null;
    private Animation animation = null, animation1 = null, animation2 = null, animation3 = null, animation4 = null;
    ObjectAnimator objectanimator1 = null;
    int distance;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);

        FirebaseMessaging.getInstance().subscribeToTopic("app_updates");
        String notificationToken = FirebaseInstanceId.getInstance().getToken();

        final String appPackageName = getPackageName();

        System.out.println("------- PACKAGE NAME: " + appPackageName);

        System.out.println("jjjjjjjjjj- NOTIFICATION TOKEN --------------: " + notificationToken);

        mainLayout = findViewById(R.id.mainLayout);
        /*whiteLayout = (RelativeLayout) findViewById(R.id.whiteLayout);
        redLayout = (RelativeLayout) findViewById(R.id.redLayout);*/
        logo = findViewById(R.id.logoText);
        belowLogo = findViewById(R.id.belowLogo);

        animation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animation1 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animation3 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animation2 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.left_to_right);
        animation4 = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.center_to_top);

        doSplash();
    }

    private void doSplash() {
        Thread timer = new Thread() {
            public void run() {
                try {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // TODO Auto-generated method stub

                            new Handler().postDelayed(new Runnable() {
                                                          @Override
                                                          public void run() {
                                                              logo.setVisibility(View.VISIBLE);
                                                              logo.startAnimation(animation2);
                                                          }
                                                      },
                                    500L);

                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    belowLogo.setVisibility(View.VISIBLE);
                                    belowLogo.startAnimation(animation2);
                                }
                            }, 2000L);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mainLayout.startAnimation(animation1);
                                }
                            }, 3500L);

                        }
                    });
                    sleep(3500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startTheApplication();
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void startTheApplication() {
        PrefUtil prefUtil = new PrefUtil(SplashScreen.this);
        Intent intent;

        if (prefUtil.getLoginStatus()) {
           /* intent = new Intent(SplashScreen.this, LandingScreenActivity.class);
            startActivity(intent);
            finish();*/
        } else {
            intent = new Intent(SplashScreen.this, LoginActivity.class);
            startActivity(intent);
            finish();
            //  }
        }
    }
}
