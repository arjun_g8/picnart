package com.skilledanswers.picnart.Utils;

/**
 * Created by SkilledAnswers-D1 on 02-01-2018.
 */

public interface OnLoadMoreListener {
    void onLoadingMoreItems();
}
