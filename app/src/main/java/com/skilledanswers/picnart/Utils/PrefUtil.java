package com.skilledanswers.picnart.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import bluepaymax.skilledanswers.picnart.scan_n_pay.retrofit.response_pojo.WalletData;

/**
 * Created by SkilledAnswers-D1 on 21-09-2017.
 */

public class PrefUtil {
    private static final String APP_PREF = "BLUEPAYMAX_PREF";
    private static final String EMAIL = "EMAIL";
    private static final String REG_NO = "REG_NO";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String MOB = "MOB";
    private static final String isLoggedIn = "LOG_IN";
    private static final String TOKEN = "TOKEN";

    private static final String IS_FOR_SEARCH = "SEARCH";
    private SharedPreferences mSharedPreferences;

    public PrefUtil(Context context) {
        mSharedPreferences = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
    }

    public void putToken(String token) {
        mSharedPreferences.edit().putString(TOKEN, token).apply();
    }

    public String getToken() {
        return mSharedPreferences.getString(TOKEN, null);
    }

    public void putIsForSearch(boolean status) {
        mSharedPreferences.edit().putBoolean(IS_FOR_SEARCH, status).apply();
    }

    public boolean getSearchStatus() {
        return mSharedPreferences.getBoolean(IS_FOR_SEARCH, false);
    }

    public void putRegNum(String RegNo) {
        mSharedPreferences.edit().putString(REG_NO, RegNo).apply();
    }

    public void putEmail(String email) {
        mSharedPreferences.edit().putString(EMAIL, email).apply();
    }

    public void putFirstName(String firstName) {
        mSharedPreferences.edit().putString(FIRST_NAME, firstName).apply();
    }

    public void putMob(String mob) {
        mSharedPreferences.edit().putString(MOB, mob).apply();
    }

    public void setIsLoggedIn(boolean status) {
        mSharedPreferences.edit().putBoolean(isLoggedIn, status).apply();
    }

    public void setBvpl(String bvpl) {
        mSharedPreferences.edit().putString("BVPL_SAVED", bvpl).apply();
    }

    public boolean isJustLoggedInToWallet() {
        return mSharedPreferences.getBoolean("IS_JUST_LOGGED_INTO_WALLET", true);
    }

    public void setLoggedInToWallet(boolean status) {
        mSharedPreferences.edit().putBoolean("IS_JUST_LOGGED_INTO_WALLET", status).apply();
    }

    public String getBvpl() {
        return mSharedPreferences.getString("BVPL_SAVED", null);
    }

    public boolean getLoginStatus() {
        return mSharedPreferences.getBoolean(isLoggedIn, false);
    }

    public String getFirstName() {
        return mSharedPreferences.getString(FIRST_NAME, null);
    }

    public String getMob() {
        return mSharedPreferences.getString(MOB, null);
    }

    public String getEmail() {
        return mSharedPreferences.getString(EMAIL, null);
    }

    public String getRegNo() {
        return mSharedPreferences.getString(REG_NO, null);
    }

    public void clearPref() {
        mSharedPreferences.edit().clear().apply();
    }

    public void setWalletData(WalletData walletData) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        Gson gson = new Gson();
        String loginDataFromGson = gson.toJson(walletData);
        editor.putString("WALLET_DATA", loginDataFromGson);
        editor.apply();
    }

    public WalletData getWalletData() {
        return new Gson().fromJson(mSharedPreferences.getString("WALLET_DATA", null), WalletData.class);
    }

}
