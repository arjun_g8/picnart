package com.skilledanswers.picnart.Utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SkilledAnswers-D1 on 24-10-2017.
 */

public class CommonUtilsEcommerce {
    //**** SOME CONSTANTS HERE ***

    public static boolean isThereInternet(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isGPSEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isLocationAccessPermissionGiven(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

       /* String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;*/
    }

    public static String getDiscountInPercent(double msp, double mrp) {
      /*  Discount = Marked Price - Selling Price

        Discount% = Discount/Marked Price × 100%.*/

        double dis = mrp - msp;
        double discount = (dis / mrp) * 100;

        return String.valueOf(discount);
    }

    public static String formatToSingleDecimalFraction(String actual) {
        return String.format("%.1f", new BigDecimal(actual));
    }

    public static int roundNumberToNearestHundred(int number, int multiple) {

        int result = number;

        //If not already multiple of given number

        if (number % multiple != 0) {

            int division = (number / multiple) + 1;

            result = division * multiple;

        }

        return result;

    }

}
