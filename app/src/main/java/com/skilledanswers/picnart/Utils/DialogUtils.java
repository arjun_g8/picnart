package com.skilledanswers.picnart.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import bluepaymax.skilledanswers.picnart.LoginActivity;
import bluepaymax.skilledanswers.picnart.R;

/**
 * Created by SkilledAnswers-D1 on 15-09-2017.
 */

public class DialogUtils {
    public static void showNoInternetDialog(final Context context) {
        final Dialog dialog = new Dialog(context, R.style.comm_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet_dialog);
        dialog.findViewById(R.id.go_to_settings_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showCustomPDialog(Dialog customPDialog, String msg) {

        //customPDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        customPDialog.setContentView(R.layout.custom_progress_dialog);
        customPDialog.setTitle(msg);

        AppCompatTextView text = customPDialog.findViewById(R.id.pd_text);
        text.setText(msg);
        ProgressBar prog = customPDialog.findViewById(R.id.pd_p_bar);
        prog.setIndeterminate(true);
        customPDialog.setCancelable(false);
        customPDialog.show();

    }

    public static void dismissCustomPDialog(Dialog progressDialog) {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public static void showRechargeSuccessPopUp(final Context context) {
        final Dialog dialog = new Dialog(context, R.style.comm_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.recharge_success_dialog);
        dialog.findViewById(R.id.ok_btn_recharge_success_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showReLoginPopUp(final Context context) {
        final Dialog dialog = new Dialog(context);
        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.re_login_popup);
        dialog.setCancelable(true);
        final AppCompatEditText reviewEd = dialog.findViewById(R.id.review_ed_dialog);
        final TextInputLayout reviewTxtIp = dialog.findViewById(R.id.review_txt_ip_dialog);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.login_btn_in_popup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Toast.makeText(context, "Please Login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void showCustomToast(Context context, String msg) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View toastView = layoutInflater.inflate(R.layout.custom_toast_view, null);

        AppCompatTextView toastTV = toastView.findViewById(R.id.toast_tv);
        toastTV.setText(msg);

        Toast toast = new Toast(context.getApplicationContext());
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(toastView);
        toast.cancel();
        toast.show();
    }


}
