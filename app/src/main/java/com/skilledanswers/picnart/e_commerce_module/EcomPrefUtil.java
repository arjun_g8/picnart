package com.skilledanswers.picnart.e_commerce_module;

import android.content.Context;
import android.content.SharedPreferences;

import bluepaymax.skilledanswers.picnart.e_commerce_module.model.User;


/**
 * Created by SkilledAnswers-D1 on 08-11-2017.
 */

public class EcomPrefUtil {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public EcomPrefUtil(Context context) {
        sharedPreferences = context.getSharedPreferences("BLUEPAYMAX_MOB_APP_PREF", Context.MODE_PRIVATE);
    }

    public boolean isUserLoggedIn() {
        return sharedPreferences.getBoolean("LOGGED_IN", false);
    }

    public void setUserSession() {
        editor = sharedPreferences.edit();
        editor.putBoolean("LOGGED_IN", true);
        editor.apply();
    }

    public void saveUserDetails(User userDetails) {
        editor = sharedPreferences.edit();
        editor.putString("USER_ID", userDetails.getUserId());
        editor.putString("F_NAME", userDetails.getfName());
        editor.putString("L_NAME", userDetails.getlName());
        editor.putString("MOB", userDetails.getMobNum());
        editor.putString("EMAIL", userDetails.getEmail());
        editor.apply();
    }

    public void setIsClosedFromPayment(boolean status){
        editor = sharedPreferences.edit();
        editor.putBoolean("IS_CLOSED", status);
        editor.apply();
    }

    public boolean getCloseStatus(){
        return sharedPreferences.getBoolean("IS_CLOSED", false);
    } //Closed option

    public User getUserDetails() {
        User userDetails = new User(sharedPreferences.getString("USER_ID", null),
                sharedPreferences.getString("F_NAME", null),
                sharedPreferences.getString("L_NAME", null),
                sharedPreferences.getString("MOB", null), sharedPreferences.getString("EMAIL", null));
        return userDetails;
    }

    public void setCartCount(int count) {
        editor = sharedPreferences.edit();
        editor.putInt("CART_COUNT", count);
        editor.apply();
    }

    public int getCartCount() {
        return sharedPreferences.getInt("CART_COUNT", 0);
    }

    public void savePincodeForGrocey(String pincode) {
        editor = sharedPreferences.edit();
        editor.putString("PINCODE_SAVED", pincode);
        editor.apply();
    }

    public String getSavedPincode() {
        return sharedPreferences.getString("PINCODE_SAVED", null);
    }
}

