package com.skilledanswers.picnart.e_commerce_module;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.skilledanswers.picnart.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;
import bluepaymax.skilledanswers.picnart.bus.utils.AllConstants;
import bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities.EcomMainActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.PaymentStatusFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retrofit_response_models.PostPaymentObject;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OzonePayActivity extends AppCompatActivity {
    private ProgressDialog pDialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ozone_pay);

        Intent intent = getIntent();
//        Log.e("intent", intent.getDataString());

        // SaveAddressForOrderPlaceResponse saveAddressForOrderPlaceResponse = intent.getParcelableExtra("DATA_FOR_PAYMENT");

        PrefUtil prefUtil = new PrefUtil(this);

        if (intent != null) {

            System.out.println("uuuuuuuuuu----SAVED--- " + intent.getStringExtra("NAME_FOR_PAYMENT"));

            String salt = AllConstants.TRACKNPAY_SALT_KEY; // put your salt
            String api_key = intent.getStringExtra("API_KEY_FOR_PAYMENT"); // put your api_key

            System.out.println("------------- API KEY: " + api_key);

            String return_url = intent.getStringExtra("RETURN_URL_FOR_PAYMENT");
            String mode = intent.getStringExtra("MODE");  ///// make LIVE after testing
            String order_id = intent.getStringExtra("ORDER_ID_FOR_PAYMENT");
            System.out.println("uuuuuuuuuu----order_id---" + order_id);
            String amount = intent.getStringExtra("AMOUNT_FOR_PAYMENT");
            String currency = intent.getStringExtra("CURRENCY_FOR_PAYMENT");
            String description = intent.getStringExtra("DESC_FOR_PAYMENT");
            String name = intent.getStringExtra("NAME_FOR_PAYMENT");
            String email = prefUtil.getEmail();
            String phone = intent.getStringExtra("MOB_FOR_PAYMENT");
            String address_line_1 = intent.getStringExtra("ADDRESS_LINE_1_FOR_PAYMENT");
            String address_line_2 = intent.getStringExtra("ADDRESS_LINE_2_FOR_PAYMENT");
            String city = intent.getStringExtra("CITY_FOR_PAYMENT");
            String state = intent.getStringExtra("STATE_FOR_PAYMENT");
            String zip_code = intent.getStringExtra("ZIP_CODE_FOR_PAYMENT");
            String country = intent.getStringExtra("COUNTRY_FOR_PAYMENT");
            String udf1 = intent.getStringExtra("UDF1_FOR_PAYMENT");
            String udf2 = intent.getStringExtra("UDF2_FOR_PAYMENT");
            String udf3 = intent.getStringExtra("UDF3_FOR_PAYMENT");
            String udf4 = intent.getStringExtra("UDF4_FOR_PAYMENT");
            String udf5 = intent.getStringExtra("UDF5_FOR_PAYMENT");
            String formURL = intent.getStringExtra("FORM_URL");

            Map<String, String> map = new HashMap<String, String>();
            map.put("api_key", api_key);

            map.put("mode", mode);
            map.put("order_id", order_id);
            map.put("amount", amount);
            map.put("currency", currency);
            map.put("description", description);
            map.put("name", name);
            map.put("email", email);
            map.put("phone", phone);
            map.put("address_line_1", address_line_1);
            map.put("address_line_2", address_line_2);
            map.put("city", city);
            map.put("state", state);
            map.put("zip_code", zip_code);
            map.put("country", country);
            // intent.putExtra("BLUEPAYID",((AppCompatEditText)findViewById(R.id.booker_setails_bluepayEditextId)).getText().toString());
            //intent.putExtra("BLOCKKEY",blockkey);
            map.put("udf1", udf1);
            map.put("udf2", udf2);
            map.put("udf3", udf3);
            map.put("udf4", udf4);
            map.put("udf5", udf5);
            map.put("return_url", return_url);
            String hashData = intent.getStringExtra("HASH_FOR_PAYMENT");
            //map.put("hash",hashData);
            String postData = "";
            Log.d("", "HashData: " + hashData);

            // Sort the map by key and create the hashData and postData
            for (String key : new TreeSet<String>(map.keySet())) {
//                if (map.get(key).equals("")) {
                //hashData = hashData + "|" + map.get(key);
                postData = postData + key + "=" + map.get(key) + "&";
                //   }
            }
            // Generate the hash key using hashdata and append the has to postData query string
//            String hash = generateSha512Hash(hashData).toUpperCase();

            Log.e("", "PostData Before: " + postData);
            System.out.println("----------PostData before: " + postData);

            postData = postData + "hash=" + hashData;


            //   Log.d("", "Hash: " + hash);

            Log.e("", "PostData: " + postData);
            System.out.println("----------PostData after: " + postData);
            System.out.println("----------Form URL : " + formURL);

            WebView webView = findViewById(R.id.webView_ozone);
            WebSettings webSettings = webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.getSettings().setDisplayZoomControls(false);
            webView.getSettings().setSupportZoom(true);
//        webView.setWebChromeClient(new WebChromeClient());


            if (Build.VERSION.SDK_INT >= 21) {
                webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }

            if (Build.VERSION.SDK_INT >= 21) {
                webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }

            webView.setWebViewClient(new WebViewClient());
            webView.postUrl("https://biz.ozonepay.in/v1/paymentrequest", postData.getBytes());

            webView.addJavascriptInterface(new OzonePayActivity.MyJavaScriptInterface(this), "Android");
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    showProgressDialog();
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    super.onPageFinished(view, url);
                    dismissProgressDialog();
                }
            });
        }
    }

    /**
     * Interface to bind Javascript from WebView with Android
     */
    public class MyJavaScriptInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        MyJavaScriptInterface(Context c) {
            mContext = c;

        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void getPaymentResponse(String jsonResponse) {
            try {
                //  showProgressDialog();
                JSONObject resposeData = new JSONObject(jsonResponse);
                Log.d("", "ResponseJson: " + resposeData.toString());
                System.out.println("ddddddddd-----getPaymentResponse------------" + resposeData.toString());

                try {
                    //String postPayJsonString = new String(response.body().bytes());

                   /* System.out.println("---------------------- POST PAY RESPONSE STRING: " + postPayJsonString);

                    JSONObject jsonObject = new JSONObject(postPayJsonString);*/

                    boolean status = resposeData.getBoolean("status");
                    String msg = resposeData.getString("message");
                    String paymentStatus = resposeData.getString("paymentStatus");
                    String transactionNum = resposeData.getString("transactionNo");
                    String orderNum = resposeData.getString("orderNo");

                    Fragment fragment = new PaymentStatusFragment();
                    Bundle bundle = new Bundle();

                    bundle.putString("MESSAGE", msg);
                    bundle.putString("TRANS_NUM", transactionNum);
                    bundle.putString("ORDER_NUM", orderNum);
                    bundle.putString("PAYMENT_STATUS", paymentStatus);
                    bundle.putBoolean("STATUS", status);
                    fragment.setArguments(bundle);

                    //dismissProgressDialog();

                    moveToFragment(fragment, "PAY_STATUS_FRAG");

                } catch (JSONException e) {
                    // dismissProgressDialog();
                    e.printStackTrace();
                }

                Intent intent = new Intent();
                intent.putExtra("resp", resposeData.toString());

                //  Toast.makeText(mContext, "" + resposeData.getString("response_message"), Toast.LENGTH_SHORT).show();
                if (resposeData.getString("response_code").equals("0")) {

                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(jsonResponse);
                    Gson gson = new Gson();
                    PostPaymentObject object = gson.fromJson(mJson, PostPaymentObject.class);

                    //handlePayment(object, resposeData.getString("response_code"));
                    // success
                    //setResult(RESULT_OK, intent);
                } else {
                    JsonParser parser = new JsonParser();
                    JsonElement mJson = parser.parse(jsonResponse);
                    Gson gson = new Gson();
                    PostPaymentObject object = gson.fromJson(mJson, PostPaymentObject.class);

                    // handlePayment(object, resposeData.getString("response_code"));

                    // failed
                    //setResult(RESULT_CANCELED, intent);
                }
                //  finish();

            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("--------------- PAy ex: " + ex.toString());
            }
        }
    }

    private void handlePayment(PostPaymentObject postPaymentData, String responseCode) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        final String json = gson.toJson(postPaymentData);
        JSONObject postJson = null;
        try {
            postJson = new JSONObject(json);
            System.out.println("---------------------- POST PAY JSON: " + postJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        PrefUtil prefUtil = new PrefUtil(OzonePayActivity.this);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.handlePostPayment("handleOzonePostPayment", prefUtil.getToken(), prefUtil.getRegNo(),
                postJson, responseCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("OZONE POST: ", response.raw().toString());
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String postPayJsonString = new String(response.body().bytes());

                            System.out.println("---------------------- POST PAY RESPONSE STRING: " + postPayJsonString);

                            JSONObject jsonObject = new JSONObject(postPayJsonString);

                            boolean status = jsonObject.getBoolean("status");
                            String msg = jsonObject.getString("message");
                            String paymentStatus = jsonObject.getString("paymentStatus");
                            String transactionNum = jsonObject.getString("transactionNo");
                            String orderNum = jsonObject.getString("orderNo");

                            Fragment fragment = new PaymentStatusFragment();
                            Bundle bundle = new Bundle();

                            bundle.putString("MESSAGE", msg);
                            bundle.putString("TRANS_NUM", transactionNum);
                            bundle.putString("ORDER_NUM", orderNum);
                            bundle.putString("PAYMENT_STATUS", paymentStatus);
                            bundle.putBoolean("STATUS", status);
                            fragment.setArguments(bundle);

                            //dismissProgressDialog();

                            moveToFragment(fragment, "PAY_STATUS_FRAG");

                        } catch (IOException e) {
                            //dismissProgressDialog();
                            e.printStackTrace();
                        } catch (JSONException e) {
                            // dismissProgressDialog();
                            e.printStackTrace();
                        }
                    }
                } else {
                    // dismissProgressDialog();
                    //Toast.makeText(OzonePayActivity.this, "", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }


    /**
     * Generates the SHA-512 hash (same as PHP) for the given string
     *
     * @param toHash string to be hashed
     * @return return hashed string
     */
    public String generateSha512Hash(String toHash) {
        MessageDigest md = null;
        byte[] hash = null;
        try {
            md = MessageDigest.getInstance("SHA-512");
            hash = md.digest(toHash.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return convertToHex(hash);
    }

    /**
     * Converts the given byte[] to a hex string.
     *
     * @param raw the byte[] to convert
     * @return the string the given byte[] represents
     */
    private String convertToHex(byte[] raw) {
        StringBuilder sb = new StringBuilder();
        for (byte aRaw : raw) {
            sb.append(Integer.toString((aRaw & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();
    }

    private void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(OzonePayActivity.this);
            pDialog.setMessage("Please wait... .");
            pDialog.setCancelable(false);

        }
        pDialog.show();
    }

    private void dismissProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setTitle("")
                .setMessage("Pressing back will cancel payment and closes all previous activities")
                .setCancelable(true)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(OzonePayActivity.this, EcomMainActivity.class);
                        startActivity(intent);
                        finishAffinity();

                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void moveToFragment(Fragment toFragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            replaceFragment(toFragment);
            System.out.println("*** ALREADY there ****");
        } else {
           /* if (TAG.equals("APPLY_FILTER_FRAGMENT")) {
                transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                transaction.replace(R.id.relative, toFragment, TAG);
                transaction.commit();
            } else {*/

//           , R.anim.enter_from_right, R.anim.exit_to_left

            /*transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);*/
            transaction.replace(R.id.ozone_pay_main, toFragment, TAG);
            transaction.addToBackStack(null).commit();
            //  }
        }

    }

    public void replaceFragment(Fragment frag) {
        FragmentManager manager = getSupportFragmentManager();
        if (manager != null) {
            FragmentTransaction t = manager.beginTransaction();
            Fragment currentFrag = manager.findFragmentById(R.id.relative);

            //Check if the new Fragment is the same
            //If it is, don't add to the back stack
            if (currentFrag != null && currentFrag.getClass().equals(frag.getClass())) {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            } else {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            }
        }
    }
}
