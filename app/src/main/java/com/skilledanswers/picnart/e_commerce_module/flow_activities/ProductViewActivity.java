package bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.custom.TouchImageView;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.PreviewImgModel;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.BasicAuthorization;


public class ProductViewActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private LinearLayout linearLayout;
    private ArrayList<String> prevImgs = new ArrayList<>();
    private boolean isChecked = false;
    private int selectedItemPos = 0;
    private ArrayList<PreviewImgModel> previewImgModelArrayList = new ArrayList<>();
    private RecyclerView previewImgRV;

    int curImgPos = 0;

    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_view);

        actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("");

        previewImgRV = findViewById(R.id.item_previews_rec_view);
        previewImgRV.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        previewImgRV.setHasFixedSize(true);
        previewImgRV.setAdapter(new PreviewImgAdapter());
        previewImgRV.getAdapter().notifyDataSetChanged();

        //isShowMenuItem = false;

        prevImgs.clear();
        prevImgs = getIntent().getStringArrayListExtra("PREVIEW_IMAGES_FULL_LIST_FROM_OVERVIEW");
        System.out.println("------------ PREV IMG LIST FROM PREV: " + prevImgs.size());

        previewImgModelArrayList.clear();

        if (prevImgs != null) {
            if (prevImgs.size() > 0) {
                for (String previewImage : prevImgs) {
                    previewImgModelArrayList.add(new PreviewImgModel(previewImage, false));
                }
            }
        }

        curImgPos = getIntent().getIntExtra("PREV_IMG_POS", 0);

        // int positionChanged = (previewImgModelArrayList.size() - 1) - curImgPos;

        Log.e("CUR_IMG_POS: ", "" + curImgPos);
        // Log.e("POS CHANGED: ", "" + positionChanged);

        if (previewImgModelArrayList.size() > 0)
            previewImgModelArrayList.get(curImgPos).setSelected(true);

        linearLayout = findViewById(R.id.previews_layout_full_screen);

        viewPager = findViewById(R.id.prod_full_iv_view_pager);
        viewPager.setAdapter(new ImgPreviewAdapter(this, previewImgModelArrayList));
        viewPager.setRotationY(180);
        //previewImgRV.setRotationY(180);
        viewPager.setCurrentItem(curImgPos);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                clearAllChecks();
                previewImgModelArrayList.get(position).setSelected(true);
                previewImgRV.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linearLayout.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ImgPreviewAdapter extends PagerAdapter {
        private Context context = null;
        private ArrayList<PreviewImgModel> imgUrlList = null;

        ImgPreviewAdapter(Context context, ArrayList<PreviewImgModel> imgUrlList) {
            this.context = context;
            this.imgUrlList = imgUrlList;
        }

        @Override
        public int getCount() {
            return imgUrlList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TouchImageView imageView = new TouchImageView(context);
            imageView.setScaleType(TouchImageView.ScaleType.FIT_CENTER);
            imageView.setRotationY(180);

            LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                    .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                    .build();
            String url;

            url = previewImgModelArrayList.get(position).getImgUrl();

            System.out.println("--------URL FOR FULL IMG VIEW SCREEN: " + url);

            Glide.with(ProductViewActivity.this)
                    .load(new GlideUrl(url, auth))
                    .placeholder(ContextCompat.getDrawable(ProductViewActivity.this, R.drawable.ic_def_pic))
                    .error(ContextCompat.getDrawable(ProductViewActivity.this, R.drawable.ic_def_pic))
                    .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .dontAnimate()
                    .into(imageView);

            imageView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        toggleViewOnTouch();
                        return true;
                    }

                    return false;
                }
            });

            container.addView(imageView, 0);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    private void toggleViewOnTouch() {
        if (linearLayout.isShown())
            linearLayout.setVisibility(View.GONE);
        else
            linearLayout.setVisibility(View.VISIBLE);
    }


    private class PreviewImgAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ProductViewActivity.this).inflate(R.layout.preview_img_item, parent, false);
            return new PreviewImageHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final PreviewImageHolder previewImageHolder = (PreviewImageHolder) holder;

            LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                    .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                    .build();

            String url = previewImgModelArrayList.get(position).getImgUrl();

            System.out.println("------------FULL PREVIEW MAIN IMAGE URL: " + url);

            Glide.with(ProductViewActivity.this)
                    .load(new GlideUrl(url, auth))
                    .placeholder(ContextCompat.getDrawable(ProductViewActivity.this, R.drawable.ic_def_pic))
                    .error(ContextCompat.getDrawable(ProductViewActivity.this, R.drawable.ic_def_pic))
                    .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .dontAnimate()
                    .into(previewImageHolder.previewIV);

            selectedItemPos = position;
            final Drawable drawable = ContextCompat.getDrawable(ProductViewActivity.this, R.drawable.rect_corner_blue_bg);
            if (previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).isSelected()) {
                previewImageHolder.previewLayout.setBackgroundDrawable(drawable);
            } else {
                previewImageHolder.previewLayout.setBackground(null);
            }
            previewImageHolder.previewIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewPager.setCurrentItem(position);
                    //   clearAllChecks();
                    if (isChecked) {
                        clearAllChecks();
                        previewImageHolder.previewLayout.setBackground(null);
                        previewImgModelArrayList.get(position).setSelected(false);
                    }

                    if (!previewImgModelArrayList.get(position).isSelected()) {
                        clearAllChecks();
                        isChecked = false;
                        previewImageHolder.previewLayout.setBackgroundDrawable(drawable);
                        previewImgModelArrayList.get(position).setSelected(true);
                    }
                    /*else {
                        isChecked = true;
                        previewImageHolder.previewIV.setBackground(null);
                        previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).setSelected(false);
                    }*/
                    notifyDataSetChanged();
                }
            });
        }

        @Override
        public int getItemCount() {
            return previewImgModelArrayList.size();
        }
    }

    private class PreviewImageHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView previewIV;
        private RelativeLayout previewLayout;

        PreviewImageHolder(View itemView) {
            super(itemView);
            previewIV = itemView.findViewById(R.id.list_item_prev_iv);
            previewLayout = itemView.findViewById(R.id.prev_img_parent_view_id);
        }

    }

    private void clearAllChecks() {
        for (int i = 0; i < previewImgModelArrayList.size(); i++) {
            previewImgModelArrayList.get(i).setSelected(false);
        }
    }
}
