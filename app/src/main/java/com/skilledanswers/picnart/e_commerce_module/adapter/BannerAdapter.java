package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.AppCompatImageView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import bluepaymax.skilledanswers.picnart.R;


/**
 * Created by subh on 5/21/2016.
 */
public class BannerAdapter extends PagerAdapter {
    private Context mContext;
    private String[] bannerImages;

    public BannerAdapter(Context context) {
        mContext = context;
    }

    public BannerAdapter(Context context, String[] bannerImages) {
        mContext = context;
        this.bannerImages = bannerImages;
    }

    @Override
    public Object instantiateItem(final ViewGroup collection, int position) {

        //  CustomPagerEnum customPagerEnum = CustomPagerEnum.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.banner_image_layout, collection, false);
        AppCompatImageView imageView = layout.findViewById(R.id.banner);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Glide.with(mContext)
                        .load(bannerImages[position])
                        // optional
                        .into(imageView);
            } else {
                Glide.with(mContext)
                        .load(bannerImages[position])
                        // optional
                        .into(imageView);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        collection.addView(layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // v.getContext().startActivity(new Intent(v.getContext(), SubCategory.class));
            }
        });

        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return bannerImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

}
