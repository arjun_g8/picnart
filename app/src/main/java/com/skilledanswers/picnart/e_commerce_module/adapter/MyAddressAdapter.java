package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.ShippingAndBillingAddressData;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.model.BillingDataPost;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.ShippingAddressData;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 10-05-2016.
 */
public class MyAddressAdapter extends RecyclerView.Adapter {

    public static int passPositionToEdit = 0;
    private List<ShippingAndBillingAddressData> myAddressModels;
    private Context context = null;
    private Fragment fragment;
    private EcomPrefUtil prefUtil;
    private PrefUtil appUtil;
    private EcommerceCustomActivity activity;
    private boolean isForOrderPlacing;
    private OnAddressSelected onAddressSelected;

    public interface OnAddressSelected {
        void setAddressData(ShippingAddressData addressData, BillingDataPost billingDataPost);
    }

    public MyAddressAdapter(List<ShippingAndBillingAddressData> myAddressModels, Context context, boolean isForPlacingOrder, OnAddressSelected onAddressSelected) {
        this.myAddressModels = myAddressModels;
        this.context = context;
        activity = (EcommerceCustomActivity) context;
        prefUtil = new EcomPrefUtil(activity);
        appUtil = new PrefUtil(activity);
        this.onAddressSelected = onAddressSelected;
        isForOrderPlacing = isForPlacingOrder;
        initSelections();
    }

    @Override
    public AddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.address_list_item, parent, false);
        return new AddressHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        AddressHolder myAddressAdapter = (AddressHolder) holder;
        final ShippingAndBillingAddressData model = myAddressModels.get(position);

        String name = model.getName();
        String address = model.getAddress() + ", " + model.getLandmark() + ", " + model.getCity() + "-" + model.getPin() + ", "
                + model.getState();
        String mob = model.getMobile();

        System.out.println("---- NAME FOR ADDRESS ADAPTER: " + name);
        System.out.println("---- MOB FOR ADDRESS ADAPTER: " + mob);

        myAddressAdapter.nameTV.setText(name);
        myAddressAdapter.addressTV.setText(address);
        myAddressAdapter.mobTV.setText(mob);

        if (isForOrderPlacing) {
            myAddressAdapter.checkedBtn.setVisibility(View.VISIBLE);

            myAddressAdapter.addressItemCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clearSelections();
                    model.setSelected(true);
                    ShippingAddressData shippingAddressData = new ShippingAddressData();
                    shippingAddressData.setName(model.getName());
                    shippingAddressData.setMobile(model.getMobile());
                    shippingAddressData.setAddress(model.getAddress());
                    shippingAddressData.setCity(model.getCity());
                    shippingAddressData.setLandmark(model.getLandmark());
                    shippingAddressData.setPin(String.valueOf(model.getPin()));
                    shippingAddressData.setShippingId(String.valueOf(model.getId()));
                    shippingAddressData.setState(model.getState());
                    shippingAddressData.setCompany(model.getCompany());
                    shippingAddressData.setEmail(model.getEmail());
                    shippingAddressData.setDistrict(model.getDistrict());
                    shippingAddressData.setSelected(true);

                    BillingDataPost billingAddressData = new BillingDataPost();
                    billingAddressData.setName(model.getBillingName());
                    billingAddressData.setMobile(model.getBillingMobile());
                    billingAddressData.setAddress(model.getBillingAddress());
                    billingAddressData.setCity(model.getBillingCity());
                    billingAddressData.setLandmark(model.getBillingLandmark());
                    billingAddressData.setPin(String.valueOf(model.getBillingPin()));
                    billingAddressData.setDistrict(model.getBillingDistrict());
                    billingAddressData.setState(model.getBillingState());
                    billingAddressData.setCompany(model.getBillingCompany());
                    billingAddressData.setEmail(model.getBillingEmail());

                    onAddressSelected.setAddressData(shippingAddressData, billingAddressData);
                    notifyDataSetChanged();
                }
            });

            //myAddressAdapter.addressItemCV.setBackgroundResource(0);

            myAddressAdapter.nameTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_circular_symbol, 0, 0, 0);

            if (model.isSelected()) {
                // myAddressAdapter.addressItemCV.setBackgroundResource(R.drawable.rect_corner_blue_bg);
                // myAddressAdapter.addressItemCV.setBackgroundColor(ContextCompat.getColor(activity, R.color.gray_plus));
                myAddressAdapter.nameTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_success_check, 0, 0, 0);
                // myAddressAdapter.checkedBtn.setImageResource(R.drawable.ic_success_check);
            } else {
                // myAddressAdapter.addressItemCV.setBackgroundResource(0);
                myAddressAdapter.nameTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_circular_symbol, 0, 0, 0);
                // myAddressAdapter.checkedBtn.setImageResource(R.drawable.ic_circular_symbol);
            }
        }

        myAddressAdapter.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Perform delete here
                if (CommonUtils.isThereInternet(activity))
                    removeAddress(String.valueOf(model.getId()), position);
                else
                    DialogUtils.showNoInternetDialog(activity);
            }
        });
    }

    private ProgressDialog progressDialog;

    private void removeAddress(String addressId, final int pos) {
        progressDialog = new ProgressDialog(activity);
        DialogUtils.showCustomPDialog(progressDialog, "Removing...");
        Log.e("ADDRESS ID: ", addressId);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.removeUserAddress("removeUserShippingAddress", appUtil.getToken(), addressId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String jsonResponseString = new String(response.body().bytes());
                            System.out.println("-----------REMOVE ADDRESS STRING JSON: " + jsonResponseString);
                            JSONObject jsonObject = new JSONObject(jsonResponseString);

                            try {
                                boolean status = jsonObject.getBoolean("status");
                                String msg = jsonObject.getString("message");

                                if (status) {
                                    myAddressModels.remove(pos);
                                    notifyItemRemoved(pos);
                                    notifyItemRangeChanged(pos, myAddressModels.size());
                                    DialogUtils.dismissCustomPDialog(progressDialog);
                                } else
                                    DialogUtils.dismissCustomPDialog(progressDialog);
                                Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                                DialogUtils.dismissCustomPDialog(progressDialog);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            DialogUtils.dismissCustomPDialog(progressDialog);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            DialogUtils.dismissCustomPDialog(progressDialog);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return myAddressModels.size();
    }

    private class AddressHolder extends RecyclerView.ViewHolder {
        AppCompatTextView nameTV, addressTV, mobTV;
        AppCompatButton deliverAddressBtn;
        AppCompatImageButton deleteBtn, editBtn, menuBtn, checkedBtn;
        private CardView addressItemCV;

        AddressHolder(final View itemView) {
            super(itemView);
            addressItemCV = itemView.findViewById(R.id.address_item_main_cv);
            nameTV = itemView.findViewById(R.id.name_in_address_list_item);
            addressTV = itemView.findViewById(R.id.address_in_address_list_item);
            mobTV = itemView.findViewById(R.id.mob_in_address_list_item);
            checkedBtn = itemView.findViewById(R.id.address_item_check_btn);
            // deliverAddressBtn = (AppCompatButton) itemView.findViewById(R.id.deliver_to_address_btn);
            deleteBtn = itemView.findViewById(R.id.remove_address_btn);
            editBtn = itemView.findViewById(R.id.edit_icon_addr_item);
            //  menuBtn = itemView.findViewById(R.id.address_item_menu_btn);
        }
    }

    private void clearSelections() {
        for (int i = 0; i < myAddressModels.size(); i++) {
            myAddressModels.get(i).setSelected(false);
        }
    }

    private void initSelections() {
        for (int i = 0; i < myAddressModels.size(); i++) {
            myAddressModels.get(i).setSelected(false);
        }
    }

}
