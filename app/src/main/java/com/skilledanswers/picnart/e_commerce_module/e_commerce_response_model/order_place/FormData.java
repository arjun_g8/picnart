package bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FormData implements Parcelable{

    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("bank_code")
    @Expose
    private String bankCode;
    @SerializedName("card_brand")
    @Expose
    private String cardBrand;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("display_currency")
    @Expose
    private String displayCurrency;
    @SerializedName("display_currency_rates")
    @Expose
    private String displayCurrencyRates;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("emi")
    @Expose
    private String emi;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("page_id")
    @Expose
    private String pageId;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("payment_option")
    @Expose
    private String paymentOption;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("reference_no")
    @Expose
    private String referenceNo;
    @SerializedName("return_url")
    @Expose
    private String returnUrl;
    @SerializedName("ship_address")
    @Expose
    private String shipAddress;
    @SerializedName("ship_city")
    @Expose
    private String shipCity;
    @SerializedName("ship_country")
    @Expose
    private String shipCountry;
    @SerializedName("ship_name")
    @Expose
    private String shipName;
    @SerializedName("ship_phone")
    @Expose
    private String shipPhone;
    @SerializedName("ship_postal_code")
    @Expose
    private String shipPostalCode;
    @SerializedName("ship_state")
    @Expose
    private String shipState;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("secure_hash")
    @Expose
    private String secureHash;

    protected FormData(Parcel in) {
        accountId = in.readString();
        address = in.readString();
        amount = in.readString();
        bankCode = in.readString();
        cardBrand = in.readString();
        channel = in.readString();
        city = in.readString();
        country = in.readString();
        currency = in.readString();
        description = in.readString();
        displayCurrency = in.readString();
        displayCurrencyRates = in.readString();
        email = in.readString();
        emi = in.readString();
        mode = in.readString();
        name = in.readString();
        pageId = in.readString();
        paymentMode = in.readString();
        paymentOption = in.readString();
        phone = in.readString();
        postalCode = in.readString();
        referenceNo = in.readString();
        returnUrl = in.readString();
        shipAddress = in.readString();
        shipCity = in.readString();
        shipCountry = in.readString();
        shipName = in.readString();
        shipPhone = in.readString();
        shipPostalCode = in.readString();
        shipState = in.readString();
        state = in.readString();
        secureHash = in.readString();
    }

    public static final Creator<FormData> CREATOR = new Creator<FormData>() {
        @Override
        public FormData createFromParcel(Parcel in) {
            return new FormData(in);
        }

        @Override
        public FormData[] newArray(int size) {
            return new FormData[size];
        }
    };

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getCardBrand() {
        return cardBrand;
    }

    public void setCardBrand(String cardBrand) {
        this.cardBrand = cardBrand;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayCurrency() {
        return displayCurrency;
    }

    public void setDisplayCurrency(String displayCurrency) {
        this.displayCurrency = displayCurrency;
    }

    public String getDisplayCurrencyRates() {
        return displayCurrencyRates;
    }

    public void setDisplayCurrencyRates(String displayCurrencyRates) {
        this.displayCurrencyRates = displayCurrencyRates;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmi() {
        return emi;
    }

    public void setEmi(String emi) {
        this.emi = emi;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getShipAddress() {
        return shipAddress;
    }

    public void setShipAddress(String shipAddress) {
        this.shipAddress = shipAddress;
    }

    public String getShipCity() {
        return shipCity;
    }

    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }

    public String getShipCountry() {
        return shipCountry;
    }

    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    public String getShipPhone() {
        return shipPhone;
    }

    public void setShipPhone(String shipPhone) {
        this.shipPhone = shipPhone;
    }

    public String getShipPostalCode() {
        return shipPostalCode;
    }

    public void setShipPostalCode(String shipPostalCode) {
        this.shipPostalCode = shipPostalCode;
    }

    public String getShipState() {
        return shipState;
    }

    public void setShipState(String shipState) {
        this.shipState = shipState;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSecureHash() {
        return secureHash;
    }

    public void setSecureHash(String secureHash) {
        this.secureHash = secureHash;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accountId);
        dest.writeString(address);
        dest.writeString(amount);
        dest.writeString(bankCode);
        dest.writeString(cardBrand);
        dest.writeString(channel);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(currency);
        dest.writeString(description);
        dest.writeString(displayCurrency);
        dest.writeString(displayCurrencyRates);
        dest.writeString(email);
        dest.writeString(emi);
        dest.writeString(mode);
        dest.writeString(name);
        dest.writeString(pageId);
        dest.writeString(paymentMode);
        dest.writeString(paymentOption);
        dest.writeString(phone);
        dest.writeString(postalCode);
        dest.writeString(referenceNo);
        dest.writeString(returnUrl);
        dest.writeString(shipAddress);
        dest.writeString(shipCity);
        dest.writeString(shipCountry);
        dest.writeString(shipName);
        dest.writeString(shipPhone);
        dest.writeString(shipPostalCode);
        dest.writeString(shipState);
        dest.writeString(state);
        dest.writeString(secureHash);
    }
}
