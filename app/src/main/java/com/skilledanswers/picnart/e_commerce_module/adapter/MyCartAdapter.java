package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.fragments.MyCart;
import com.skilledanswers.picnart.e_commerce_module.retofit.BasicAuthorization;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.LoadCartItemsModel;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.UpdateCartPostResponse;
import com.travijuu.numberpicker.library.Enums.ActionEnum;
import com.travijuu.numberpicker.library.Interface.ValueChangedListener;
import com.travijuu.numberpicker.library.NumberPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by SkilledAnswers-D1 on 18-05-2016.
 */
public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.Holder> {
    private Context context = null;
    //  private ArrayList<MyCartModel> myCartModels = null;
    private List<LoadCartItemsModel.CartItemData> cartItemsList;
    private EcommerceCustomActivity customActivity;
    private PrefUtil prefUtil;
    private String[] qtyStringArray = new String[]{
            "1", "2", "3", "4", "5"
    };
    private NoItemsInCartInterface noItemsInCartInterface;
    private CartUpdatable cartUpdatable;
    private boolean isForUpdate;

    public interface NoItemsInCartInterface {
        void noItems(boolean status);
    }

    public interface CartUpdatable {
        void onCartUpdate();

        void onCartRemove();
    }

    public MyCartAdapter(Context context, List<LoadCartItemsModel.CartItemData> cartItemsList, NoItemsInCartInterface noItemsInCartInterface, CartUpdatable cartUpdatable) {
        this.context = context;
        this.cartItemsList = cartItemsList;
        customActivity = (EcommerceCustomActivity) context;
        this.noItemsInCartInterface = noItemsInCartInterface;
        prefUtil = new PrefUtil(customActivity);
        isForUpdate = false;
        this.cartUpdatable = cartUpdatable;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cart_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        LoadCartItemsModel.CartItemData model = cartItemsList.get(position);
        // holder.imageView.setImageResource(model.get_image());
        String productName = model.getProduct().getTitle();
        String productQty = String.valueOf(model.getQuantity());
        String productPrice = model.getStock().getMsp().replace(".00", "");

        String shipping = model.getStock().getShippingCharge();

        int someNumber = Integer.parseInt(productPrice);
        NumberFormat nf = NumberFormat.getInstance();
        String mspFormatted = nf.format(someNumber);

        /*int mrpNum = Integer.parseInt(mrp);
        String mrpFormatted = nf.format(mrpNum);*/

        holder.name.setText(productName);
        if (shipping != null)
            holder.shippingChargeTV.setText("Rs. " + shipping);
//        holder.qty.setText(productQty);
        holder.price.setText("Rs. " + mspFormatted);
        Log.e("CART_PRODUCT_NAME", productName);
        Log.e("CART_PRODUCT_PRICE", productPrice);
        Log.e("CART_PRODUCT_NUM", productQty);

//        holder.delevery.setText("" + model.get_delevery());
//        holder.ratingBar.setRating(model.get_rating());

        LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                .build();
        String urlForProductImg = "www.google.com";
        if (cartItemsList != null)
            if (cartItemsList.size() > 0) {
                if (cartItemsList.get(position).getProduct().getImage() != null)
                    urlForProductImg = cartItemsList.get(position).getProduct().getImage();

            }
        Glide.with(context)
                .load(new GlideUrl(urlForProductImg, auth))
                .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                .error(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .dontAnimate()
                .into(holder.imageView);

        ArrayAdapter arrayAdapter = new ArrayAdapter(customActivity, android.R.layout.simple_spinner_item, qtyStringArray);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        holder.numberPicker.setValue(Integer.parseInt(cartItemsList.get(position).getQuantity()));

        holder.numberPicker.setValueChangedListener(new ValueChangedListener() {
            @Override
            public void valueChanged(int value, ActionEnum action) {
                updateCart(cartItemsList.get(position).getId(), String.valueOf(value));
            }

        });



        holder.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isThereInternet(context)) {
                    try {
                        removeFromCart(cartItemsList.get(position).getId(), position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //removeItemFromCart(position);
                } else
                    DialogUtils.showNoInternetDialog(context);
            }
        });

    }

    @Override
    public int getItemCount() {
        return cartItemsList.size();
    }

    class Holder extends RecyclerView.ViewHolder {
        ImageView imageView = null;
        TextView name = null, desc = null;
        EditText qty;
        TextView changeQty = null;
        LinearLayout linearLayoutPrice = null;
        TextView price = null;
        TextView delevery = null;
        TextView deleveryDetails = null;
        TextView sellar = null;
        RatingBar ratingBar = null;
        RelativeLayout removeBtn = null;
        private NumberPicker numberPicker;
        AppCompatTextView shippingChargeTV;

        @SuppressLint("WrongViewCast")
        Holder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.img);
            this.name = itemView.findViewById(R.id.product_name_tv_in_overview);
            this.price = itemView.findViewById(R.id.wish_list_item_price);
            numberPicker = itemView.findViewById(R.id.cart_update_number_picker);
            //qtySpinner = itemView.findViewById(R.id.cart_qty_spinner);
            /*this.delevery=(TextView)itemView.findViewById(R.id.cart_row_delevery);
            this.ratingBar=(RatingBar)itemView.findViewById(R.id.cart_rowratingbar);*/
            shippingChargeTV = itemView.findViewById(R.id.shipping_charge_tv_in_cart);

            removeBtn = (RelativeLayout) itemView.findViewById(R.id.cart_item_cancel_iv);

        }
    }

    private void removeFromCart(String cartId, final int cartItemPos) {
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiInterface.removeProductFromCart("removeItemFromCart", cartId, prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String cartRemoveResponseString = new String(response.body().bytes());

                        JSONObject cartRemoveJsonObject = new JSONObject(cartRemoveResponseString);
                        String msg = cartRemoveJsonObject.getString("message");
                        boolean status = cartRemoveJsonObject.getBoolean("status");

                        if (status) {
                            cartItemsList.remove(cartItemPos);
                            notifyItemRemoved(cartItemPos);
                            notifyItemRangeChanged(cartItemPos, cartItemsList.size());
                            customActivity.updateCartCount(cartItemsList.size());

                            cartUpdatable.onCartRemove();

                            int amt = 0;
                            if (cartItemsList.size() > 0)
                                for (int i = 0; i < cartItemsList.size(); i++) {
                                    String msp = cartItemsList.get(i).getStock().getMsp().replace(".00", "");
                                    amt += Integer.parseInt(msp);
                                }

                            //getCartTotal();

                            if (cartItemsList.size() == 0) {
                                noItemsInCartInterface.noItems(true);
                            }

                            // Toast.makeText(context, "Item removed from cart", Toast.LENGTH_SHORT).show();
                            DialogUtils.showCustomToast(customActivity, msg);
                            // Toast.makeText(customActivity, "" + msg, Toast.LENGTH_SHORT).show();
                        } else
                            DialogUtils.showCustomToast(customActivity, msg);
                        //  Toast.makeText(customActivity, "" + msg, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void updateCart(String cartId, String qty) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.updateCart("updateQuantityInCart", cartId, qty, prefUtil.getRegNo(), prefUtil.getToken()).enqueue(new Callback<UpdateCartPostResponse>() {
            @Override
            public void onResponse(Call<UpdateCartPostResponse> call, Response<UpdateCartPostResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        UpdateCartPostResponse.Totals totals = response.body().getTotals();
                        int totalAmt = totals.getCartTotal();
                        getCartTotal();
                        cartUpdatable.onCartUpdate();
                        DialogUtils.showCustomToast(customActivity, response.body().getMessage());
                        // Toast.makeText(context, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    } else {
                        DialogUtils.showCustomToast(customActivity, "Something went wrong! Login Required");
                        // Toast.makeText(context, "Something went wrong! Login Required", Toast.LENGTH_SHORT).show();
                        DialogUtils.showReLoginPopUp(customActivity);
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateCartPostResponse> call, Throwable t) {

            }
        });
    }

    private void getCartTotal() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getCartTotal("calculateCartTotal", prefUtil.getRegNo()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.e("CART", response.raw().toString());
                    if (response.body() != null) {
                        if (response.body() != null) {
                            try {
                                String jsonResponseString = new String(response.body().bytes());
                                System.out.println("-----------CART TOTAL JSON: " + jsonResponseString);
                                JSONObject jsonObject = new JSONObject(jsonResponseString);

                                try {
                                    String cartTotalPrice = jsonObject.getString("cartTotal");
                                    String shippingCharge = jsonObject.getString("shippingPrice");
                                    String itemPrice = jsonObject.getString("itemPrice");
                                    if (cartTotalPrice == null)
                                        DialogUtils.showCustomToast(customActivity, "Session Expired..Please login again!");
                                        // Toast.makeText(customActivity, "Session Expired..Please login again!", Toast.LENGTH_SHORT).show();
                                    else {
                                        MyCart.setTotalAmount(Integer.parseInt(itemPrice), Integer.parseInt(shippingCharge));
                                        MyCart.setShippingAmount(shippingCharge);
                                        System.out.println("------------CART TOTAL API: " + cartTotalPrice);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

}
