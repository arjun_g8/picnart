package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

/**
 * Created by SkilledAnswers-D1 on 30-12-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<SearchData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<SearchData> getData() {
        return data;
    }

    public void setData(List<SearchData> data) {
        this.data = data;
    }

    public class SearchData {

        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("product_code")
        @Expose
        private String productCode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("isGrocery")
        @Expose
        private Boolean isGrocery;

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getIsGrocery() {
            return isGrocery;
        }

        public void setIsGrocery(Boolean isGrocery) {
            this.isGrocery = isGrocery;
        }

    }

}