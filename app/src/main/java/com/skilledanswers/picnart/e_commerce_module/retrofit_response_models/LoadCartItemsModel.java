package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 27-12-2017.
 */

public class LoadCartItemsModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    @Expose
    private List<CartItemData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<CartItemData> getData() {
        return data;
    }

    public void setData(List<CartItemData> data) {
        this.data = data;
    }

    public class Product {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("categoryId")
        @Expose
        private String categoryId;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("isLive")
        @Expose
        private String isLive;
        @SerializedName("adminId")
        @Expose
        private String adminId;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("rejection")
        @Expose
        private String rejection;

        @SerializedName("parentCatId")
        @Expose
        private Integer parentCategoryID;

        public Integer getParentCategoryID() {
            return parentCategoryID;
        }

        public void setParentCategoryID(Integer parentCategoryID) {
            this.parentCategoryID = parentCategoryID;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsLive() {
            return isLive;
        }

        public void setIsLive(String isLive) {
            this.isLive = isLive;
        }

        public String getAdminId() {
            return adminId;
        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getRejection() {
            return rejection;
        }

        public void setRejection(String rejection) {
            this.rejection = rejection;
        }

    }



    public class Stock {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("sellerId")
        @Expose
        private String sellerId;
        @SerializedName("sku")
        @Expose
        private String sku;
        @SerializedName("productId")
        @Expose
        private String productId;
        @SerializedName("msp")
        @Expose
        private String msp;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("shippingCharge")
        @Expose
        private String shippingCharge;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("parentCatId")
        @Expose
        private Integer parentCategoryID;

        public Integer getParentCategoryID() {
            return parentCategoryID;
        }

        public void setParentCategoryID(Integer parentCategoryID) {
            this.parentCategoryID = parentCategoryID;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getMsp() {
            return msp;
        }

        public void setMsp(String msp) {
            this.msp = msp;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getShippingCharge() {
            return shippingCharge;
        }

        public void setShippingCharge(String shippingCharge) {
            this.shippingCharge = shippingCharge;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

    public class CartItemData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("regNo")
        @Expose
        private String regNo;
        @SerializedName("productId")
        @Expose
        private String productId;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("product")
        @Expose
        private Product product;
        @SerializedName("stock")
        @Expose
        private Stock stock;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRegNo() {
            return regNo;
        }

        public void setRegNo(String regNo) {
            this.regNo = regNo;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Stock getStock() {
            return stock;
        }

        public void setStock(Stock stock) {
            this.stock = stock;
        }

    }
}
