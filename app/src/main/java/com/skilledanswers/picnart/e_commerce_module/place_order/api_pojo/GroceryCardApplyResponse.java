package com.skilledanswers.picnart.e_commerce_module.place_order.api_pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroceryCardApplyResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("cartTotal")
    @Expose
    private Integer cartTotal;
    @SerializedName("discount")
    @Expose
    private Integer discount;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("cardId")
        @Expose
        private Integer cardId;
        @SerializedName("orderId")
        @Expose
        private Integer orderId;
        @SerializedName("cardMonth")
        @Expose
        private String cardMonth;
        @SerializedName("isUsed")
        @Expose
        private Integer isUsed;
        @SerializedName("redeemDate")
        @Expose
        private String redeemDate;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("card")
        @Expose
        private Card card;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getCardId() {
            return cardId;
        }

        public void setCardId(Integer cardId) {
            this.cardId = cardId;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public String getCardMonth() {
            return cardMonth;
        }

        public void setCardMonth(String cardMonth) {
            this.cardMonth = cardMonth;
        }

        public Integer getIsUsed() {
            return isUsed;
        }

        public void setIsUsed(Integer isUsed) {
            this.isUsed = isUsed;
        }

        public String getRedeemDate() {
            return redeemDate;
        }

        public void setRedeemDate(String redeemDate) {
            this.redeemDate = redeemDate;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public Card getCard() {
            return card;
        }

        public void setCard(Card card) {
            this.card = card;
        }

    }

    public class Card {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("regNo")
        @Expose
        private Integer regNo;
        @SerializedName("orderId")
        @Expose
        private Integer orderId;
        @SerializedName("productId")
        @Expose
        private Integer productId;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("cardAmount")
        @Expose
        private String cardAmount;
        @SerializedName("balance")
        @Expose
        private String balance;
        @SerializedName("redeemAllowance")
        @Expose
        private String redeemAllowance;
        @SerializedName("activation")
        @Expose
        private String activation;
        @SerializedName("status")
        @Expose
        private Integer status;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRegNo() {
            return regNo;
        }

        public void setRegNo(Integer regNo) {
            this.regNo = regNo;
        }

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getCardAmount() {
            return cardAmount;
        }

        public void setCardAmount(String cardAmount) {
            this.cardAmount = cardAmount;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getRedeemAllowance() {
            return redeemAllowance;
        }

        public void setRedeemAllowance(String redeemAllowance) {
            this.redeemAllowance = redeemAllowance;
        }

        public String getActivation() {
            return activation;
        }

        public void setActivation(String activation) {
            this.activation = activation;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

}