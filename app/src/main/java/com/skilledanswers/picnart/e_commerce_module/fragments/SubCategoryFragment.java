package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.EcommerceCategoriesResponse;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 22-12-2017.
 */

public class SubCategoryFragment extends Fragment {
    private View subCategoryView;
    private ProgressBar loadingBar;
    private RecyclerView subCategoryRV;
    private Bundle bundle;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private List<EcommerceCategoriesResponse.SubCategory> subCategoryList = new ArrayList<>();
    private AppCompatTextView noSubCatTV;
    private String categoryName = "";
    private int categoryID;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        subCategoryView = inflater.inflate(R.layout.sub_category_fragment, container, false);

        setHasOptionsMenu(true);
        ecommerceCustomActivity.setToggle(false);
        bundle = getArguments();
        int categoryId = bundle.getInt("ECOM_CATEGORY_ID");
        categoryID = categoryId;

        System.out.println("---------------- cat id sub cat: " + categoryID);

        categoryName = bundle.getString("ECOM_CATEGORY_NAME", "");

        loadingBar = subCategoryView.findViewById(R.id.loading_p_bar_sub_category);
        subCategoryRV = subCategoryView.findViewById(R.id.sub_category_rv);
        noSubCatTV = subCategoryView.findViewById(R.id.no_sub_cat_tv);
        subCategoryRV.setLayoutManager(new LinearLayoutManager(ecommerceCustomActivity));
        subCategoryRV.setHasFixedSize(true);

        if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
            subCategoryRV.setVisibility(View.GONE);
            loadingBar.setVisibility(View.VISIBLE);
            getAllSubCategories(categoryId);
        } else
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);

        return subCategoryView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle(categoryName);

       /* ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle(categoryName);*/
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(false);
        //menu.findItem(R.id.action_settings).setVisible(false);
//        menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void getAllSubCategories(final int categoryId) {
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiInterface.getEcommerceCategories("loadAllCategories").enqueue(new Callback<EcommerceCategoriesResponse>() {
            @Override
            public void onResponse(Call<EcommerceCategoriesResponse> call, Response<EcommerceCategoriesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        subCategoryList.clear();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (Integer.parseInt(response.body().getData().get(i).getId()) == categoryId) {
                                subCategoryList = response.body().getData().get(i).getSubCategories();
                                break;
                            }
                        }

                        System.out.println("-------- sub categories size: " + subCategoryList.size());
                        if (subCategoryList.size() > 0) {
                            subCategoryRV.setAdapter(new EcommerceSubCategoriesAdapter(subCategoryList));
                            subCategoryRV.getAdapter().notifyDataSetChanged();
                        } else if (subCategoryList.size() == 0) {
                            noSubCatTV.setVisibility(View.VISIBLE);
                            noSubCatTV.setText("No Sub Categories and Products to show");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EcommerceCategoriesResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("-------- categories ex: " + t.toString());
            }
        });
        loadingBar.setVisibility(View.GONE);
        subCategoryRV.setVisibility(View.VISIBLE);
    }

    private class EcommerceSubCategoriesAdapter extends RecyclerView.Adapter {

        EcommerceSubCategoriesAdapter(List<EcommerceCategoriesResponse.SubCategory> subList) {

            for (int i = 0; i < subList.size(); i++) {
                if (subList.get(i).getImageURL() != null) {
                    subList.get(i).setHasImage(true);
                } else
                    subList.get(i).setHasImage(false);
            }
            subCategoryList = subList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.new_categories_list_item_view, parent, false);
            return new EcommerceSubCategoriesHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            EcommerceSubCategoriesHolder ecommerceCategoriesHolder = (EcommerceSubCategoriesHolder) holder;

            if (subCategoryList.get(position).isHasImage()) {
                String urlForImg = subCategoryList.get(position).getImageURL();
                Log.e("IMAGE URL", " SUB CATEGORY: " + urlForImg);
                Glide.with(ecommerceCustomActivity)
                        .load(new GlideUrl(urlForImg))
                       /* .placeholder(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))*/
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(ecommerceCategoriesHolder.categoriesImgV);
            }

            ecommerceCategoriesHolder.categoriesTV.setText(subCategoryList.get(position).getName());
            ecommerceCategoriesHolder.categoryMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment fragment;
                    if (categoryID == 41)
                        fragment = new SubLayerFragment();
                    else
                        fragment = new AllProductsFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("CATEGORY_ID_FOR_ALL_PRODUCTS", categoryID);
                    bundle.putInt("SUB_CATEGORY_ID_FOR_ALL_PRODUCTS", Integer.parseInt(subCategoryList.get(position).getId()));
                    bundle.putString("SUB_CATEGORY_NAME_FOR_ALL_PRODUCTS", subCategoryList.get(position).getName());
                    fragment.setArguments(bundle);
                    ecommerceCustomActivity.moveToFragment(fragment, "ALL_PRODUCTS_FRAGMENT");
                }
            });
        }

        @Override
        public int getItemCount() {
            return subCategoryList.size();
        }
    }

    private class EcommerceSubCategoriesHolder extends RecyclerView.ViewHolder {
        private CircleImageView categoriesImgV;
        private AppCompatTextView categoriesTV;
        LinearLayout categoryMainView;

        EcommerceSubCategoriesHolder(View itemView) {
            super(itemView);
            categoriesImgV = itemView.findViewById(R.id.category_item_iv);
            categoriesTV = itemView.findViewById(R.id.category_item_tv);
            categoryMainView = itemView.findViewById(R.id.category_view);
        }
    }

}
