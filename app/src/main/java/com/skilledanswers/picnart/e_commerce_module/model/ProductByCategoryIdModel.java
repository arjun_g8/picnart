package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 08-11-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductByCategoryIdModel {

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public class ProductAttribut {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("attributeName")
        @Expose
        private String attributeName;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getAttributeName() {
            return attributeName;
        }

        public void setAttributeName(String attributeName) {
            this.attributeName = attributeName;
        }

    }

    public class Datum {

        @SerializedName("productId")
        @Expose
        private Integer productId;
       /* @SerializedName("created")
        @Expose
        private String created;*/
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("purchasePrice")
        @Expose
        private Integer purchasePrice;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("salePrice")
        @Expose
        private Integer salePrice;
       /* @SerializedName("updated")
        @Expose
        private BigInteger updated;*/
        @SerializedName("offers")
        @Expose
        private List<Object> offers = null;
        @SerializedName("productAttributs")
        @Expose
        private List<ProductAttribut> productAttributs = null;

        private boolean isAddedToWishList;
        private boolean hasImage;

        public boolean isHasImage() {
            return hasImage;
        }

        public void setHasImage(boolean hasImage) {
            this.hasImage = hasImage;
        }

        public boolean isAddedToWishList() {
            return isAddedToWishList;
        }

        public void setAddedToWishList(boolean addedToWishList) {
            isAddedToWishList = addedToWishList;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

     /*   public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
*/
        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getPurchasePrice() {
            return purchasePrice;
        }

        public void setPurchasePrice(Integer purchasePrice) {
            this.purchasePrice = purchasePrice;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Integer getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(Integer salePrice) {
            this.salePrice = salePrice;
        }

      /*  public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

        public List<Object> getOffers() {
            return offers;
        }

        public void setOffers(List<Object> offers) {
            this.offers = offers;
        }

        public List<ProductAttribut> getProductAttributs() {
            return productAttributs;
        }

        public void setProductAttributs(List<ProductAttribut> productAttributs) {
            this.productAttributs = productAttributs;
        }

    }

}