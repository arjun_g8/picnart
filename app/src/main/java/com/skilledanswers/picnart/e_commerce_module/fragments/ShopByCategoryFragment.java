package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.EcommerceCategoriesResponse;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.BrandsModel;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 15-11-2017.
 */

public class ShopByCategoryFragment extends Fragment {
    private EcomPrefUtil prefUtil;
    private EcommerceCustomActivity activity;
    private View shopByCategoryView;
    private RecyclerView shopByCategoryRV;
    List<EcommerceCategoriesResponse.CategoryData> categoriesList = new ArrayList<>();
    ArrayList<BrandsModel> brandsList = new ArrayList<>();
    private String currentViewFor;
    private List<EcommerceCategoriesResponse.SubCategory> subCategoryList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        shopByCategoryView = inflater.inflate(R.layout.shop_by_category_fragment, container, false);
        activity = (EcommerceCustomActivity) getActivity();
        if (getArguments() != null) {
            if (getArguments().getString("CLICKED_FOR") != null)
                currentViewFor = getArguments().getString("CLICKED_FOR", "");
        }
        activity.setToggle(false);
        setHasOptionsMenu(true);
        initShopByCategoryView();
        return shopByCategoryView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity = (EcommerceCustomActivity) getActivity();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("Shop By Category");
        activity.setToggle(false);
        //activity.hideToolBarLogo();
        // activity.setAppToolBarTitle("Shop By Category");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(false);
        //menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void initShopByCategoryView() {
        shopByCategoryRV = shopByCategoryView.findViewById(R.id.shop_by_category_rv);
        shopByCategoryRV.setLayoutManager(new LinearLayoutManager(activity));

        if (CommonUtils.isThereInternet(activity)) {
            if (currentViewFor.equals("CATEGORY"))
                loadAllCategories();
           /* else
                getAllBrands();*/

        } else
            DialogUtils.showNoInternetDialog(activity);
    }

    private void loadAllCategories() {
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiInterface.getEcommerceCategories("loadAllCategories").enqueue(new Callback<EcommerceCategoriesResponse>() {
            @Override
            public void onResponse(Call<EcommerceCategoriesResponse> call, Response<EcommerceCategoriesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        categoriesList.clear();
                        categoriesList = response.body().getData();
                        if (categoriesList.size() > 0) {
                            shopByCategoryRV.setAdapter(new EcommerceCategoriesAdapter(categoriesList));
                            shopByCategoryRV.getAdapter().notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<EcommerceCategoriesResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("-------- categories ex: " + t.toString());
            }
        });
    }

    private class EcommerceCategoriesAdapter extends RecyclerView.Adapter {
        private boolean isExpanded;

        EcommerceCategoriesAdapter(List<EcommerceCategoriesResponse.CategoryData> catList) {
            for (int i = 0; i < catList.size(); i++) {
                if (catList.get(i).getImage() != null) {
                    catList.get(i).setHasImage(true);
                } else
                    catList.get(i).setHasImage(false);

                if (catList.get(i).getSubCategories() != null && catList.get(i).getSubCategories().size() > 0) {
                    catList.get(i).setDoesItHasSubCategory(true);
                } else
                    catList.get(i).setDoesItHasSubCategory(false);

                if (catList.get(i).getName().equalsIgnoreCase("Apparels"))
                    catList.remove(i);
            }

            EcommerceCategoriesResponse.CategoryData groceryCategory = catList.get(catList.size() - 1);
            EcommerceCategoriesResponse.CategoryData defCategory = catList.get(2);
            catList.set(2, groceryCategory);
            catList.set(catList.size() - 1, defCategory);

            categoriesList = catList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.new_categories_list_item_view, parent, false);
            return new EcommerceCategoriesHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final EcommerceCategoriesHolder ecommerceCategoriesHolder = (EcommerceCategoriesHolder) holder;

            System.out.println("----------- POSITION: " + position + "\t" + "Adapter POS: " + ecommerceCategoriesHolder.getAdapterPosition());

            ecommerceCategoriesHolder.categoriesTV.setText(categoriesList.get(ecommerceCategoriesHolder.getAdapterPosition()).getName());

            if (categoriesList.get(position).isDoesItHasSubCategory()) {
                ecommerceCategoriesHolder.hasImageIV.setImageResource(R.drawable.ic_right_arrow);
                ecommerceCategoriesHolder.categoryMainView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Fragment fragment = new SubCategoryFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("ECOM_CATEGORY_ID", Integer.parseInt(categoriesList.get(position).getId()));
                        bundle.putString("ECOM_CATEGORY_NAME", categoriesList.get(position).getName());
                        fragment.setArguments(bundle);
                        activity.moveToFragment(fragment, "ECOMMERCE_SUB_CATEGORY_FRAGMENT");
                    }

                });

            }

            if (categoriesList.get(position).isHasImage()) {
                String urlForImg = categoriesList.get(position).getImageURL();
                Log.e("IMAGE URL", " SUB CATEGORY: " + urlForImg);
                Glide.with(activity)
                        .load(new GlideUrl(urlForImg))
                        /* .placeholder(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                         .error(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))*/
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(ecommerceCategoriesHolder.categoriesImgV);
            }

            // ecommerceCategoriesHolder.categoriesImgV.setImageResource(R.drawable.selfie_stick);
        }

        private void getAllSubCategories(final int categoryId, final RecyclerView subCategoryRV) {
            EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
            apiInterface.getEcommerceCategories("loadAllCategories").enqueue(new Callback<EcommerceCategoriesResponse>() {
                @Override
                public void onResponse(Call<EcommerceCategoriesResponse> call, Response<EcommerceCategoriesResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            subCategoryList.clear();
                            subCategoryList = response.body().getData().get(categoryId).getSubCategories();
                            System.out.println("-------- sub categories size: " + subCategoryList.size());
                            if (subCategoryList.size() > 0) {
                                subCategoryRV.setVisibility(View.VISIBLE);
                                subCategoryRV.setAdapter(new EcommerceSubCategoriesAdapter(subCategoryList));
                                subCategoryRV.getAdapter().notifyDataSetChanged();
                            } else if (subCategoryList.size() == 0) {
                                subCategoryRV.setVisibility(View.GONE);
                               /* noSubCatTV.setVisibility(View.VISIBLE);
                                noSubCatTV.setText("No Sub Categories and Products to show");*/
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<EcommerceCategoriesResponse> call, Throwable t) {
                    t.printStackTrace();
                    System.out.println("-------- categories ex: " + t.toString());
                }
            });
           /* loadingBar.setVisibility(View.GONE);
            subCategoryRV.setVisibility(View.VISIBLE);*/
        }

        @Override
        public int getItemCount() {
            return categoriesList.size();
        }
    }

    private class EcommerceCategoriesHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView hasImageIV;
        private AppCompatTextView categoriesTV;
        LinearLayout categoryMainView;
        private RecyclerView subCategoryRV;
        private CircleImageView categoriesImgV;

        EcommerceCategoriesHolder(View itemView) {
            super(itemView);
            categoriesImgV = itemView.findViewById(R.id.category_item_iv);
            categoriesTV = itemView.findViewById(R.id.category_item_tv);
            categoryMainView = itemView.findViewById(R.id.category_view);
            subCategoryRV = itemView.findViewById(R.id.sub_cat_rv);
            subCategoryRV.setLayoutManager(new GridLayoutManager(activity, 3));
            subCategoryRV.setHasFixedSize(true);
            hasImageIV = itemView.findViewById(R.id.categories_has_sub_cat_iv);
        }
    }

    private class EcommerceSubCategoriesAdapter extends RecyclerView.Adapter {

        EcommerceSubCategoriesAdapter(List<EcommerceCategoriesResponse.SubCategory> subList) {

            for (int i = 0; i < subList.size(); i++) {
                if (subList.get(i).getImageURL() != null) {
                    subList.get(i).setHasImage(true);
                } else
                    subList.get(i).setHasImage(false);
            }
            subCategoryList = subList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.sub_cat_rv_list_item, parent, false);
            return new EcommerceSubCategoriesHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            EcommerceSubCategoriesHolder ecommerceCategoriesHolder = (EcommerceSubCategoriesHolder) holder;

            if (subCategoryList.get(position).isHasImage()) {
                String urlForImg = subCategoryList.get(position).getImageURL();
                Glide.with(activity)
                        .load(new GlideUrl(urlForImg))
                        .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {

                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(ecommerceCategoriesHolder.categoriesImgV);
            }

            ecommerceCategoriesHolder.categoriesTV.setText(subCategoryList.get(position).getName());
            ecommerceCategoriesHolder.categoryMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }

        @Override
        public int getItemCount() {
            return subCategoryList.size();
        }
    }

    private class EcommerceSubCategoriesHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView categoriesImgV;
        private AppCompatTextView categoriesTV;
        LinearLayout categoryMainView;

        EcommerceSubCategoriesHolder(View itemView) {
            super(itemView);
            categoriesImgV = itemView.findViewById(R.id.sub_cat_list_iv);
            categoriesTV = itemView.findViewById(R.id.sub_cat_list_tv);
            categoryMainView = itemView.findViewById(R.id.sub_cat_list_main_view);
        }
    }

}
