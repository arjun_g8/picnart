package com.skilledanswers.picnart.e_commerce_module.payment;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PaymentGatewayResponse {


    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("message2")
    @Expose
    private String message2;
    @SerializedName("transactionNo")
    @Expose
    private String transactionNo;
    @SerializedName("orderNo")
    @Expose
    private Integer orderNo;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("productDetails")
    @Expose
    private ProductDetails productDetails;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage2() {
        return message2;
    }

    public void setMessage2(String message2) {
        this.message2 = message2;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Integer getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Integer orderNo) {
        this.orderNo = orderNo;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

public class ProductDetails {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private List<Result> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}

public class Result {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("order_no")
    @Expose
    private String orderNo;
    @SerializedName("product_id")
    @Expose
    private Integer productId;
    @SerializedName("seller_id")
    @Expose
    private Integer sellerId;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("bpm_payment")
    @Expose
    private String bpmPayment;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("pv")
    @Expose
    private String pv;
    @SerializedName("limit_flag")
    @Expose
    private Integer limitFlag;
    @SerializedName("is_live")
    @Expose
    private Integer isLive;
    @SerializedName("coupon_enable")
    @Expose
    private Integer couponEnable;
    @SerializedName("card_flag")
    @Expose
    private Integer cardFlag;
    @SerializedName("admin_id")
    @Expose
    private Integer adminId;
    @SerializedName("rejection")
    @Expose
    private String rejection;
    @SerializedName("reg_no")
    @Expose
    private String regNo;
    @SerializedName("txn_id")
    @Expose
    private Integer txnId;
    @SerializedName("shipping_address_id")
    @Expose
    private Integer shippingAddressId;
    @SerializedName("shipping_charge")
    @Expose
    private String shippingCharge;
    @SerializedName("shipping_name")
    @Expose
    private String shippingName;
    @SerializedName("shipping_company")
    @Expose
    private String shippingCompany;
    @SerializedName("shipping_email")
    @Expose
    private String shippingEmail;
    @SerializedName("shipping_mobile")
    @Expose
    private String shippingMobile;
    @SerializedName("shipping_address")
    @Expose
    private String shippingAddress;
    @SerializedName("shipping_city")
    @Expose
    private String shippingCity;
    @SerializedName("shipping_district")
    @Expose
    private String shippingDistrict;
    @SerializedName("shipping_state")
    @Expose
    private String shippingState;
    @SerializedName("shipping_pin")
    @Expose
    private Integer shippingPin;
    @SerializedName("shipping_landmark")
    @Expose
    private String shippingLandmark;
    @SerializedName("billing_name")
    @Expose
    private String billingName;
    @SerializedName("billing_company")
    @Expose
    private String billingCompany;
    @SerializedName("billing_email")
    @Expose
    private String billingEmail;
    @SerializedName("billing_mobile")
    @Expose
    private String billingMobile;
    @SerializedName("billing_address")
    @Expose
    private String billingAddress;
    @SerializedName("billing_city")
    @Expose
    private String billingCity;
    @SerializedName("billing_district")
    @Expose
    private String billingDistrict;
    @SerializedName("billing_state")
    @Expose
    private String billingState;
    @SerializedName("billing_pin")
    @Expose
    private Integer billingPin;
    @SerializedName("billing_landmark")
    @Expose
    private String billingLandmark;
    @SerializedName("total_amount")
    @Expose
    private String totalAmount;
    @SerializedName("cart_type")
    @Expose
    private String cartType;
    @SerializedName("redeem_amount")
    @Expose
    private String redeemAmount;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("pin_no")
    @Expose
    private String pinNo;
    @SerializedName("history_id")
    @Expose
    private String historyId;
    @SerializedName("mpoid")
    @Expose
    private String mpoid;
    @SerializedName("order_unique_id")
    @Expose
    private String orderUniqueId;
    @SerializedName("udf1")
    @Expose
    private String udf1;

    @SerializedName("image")
    @Expose
    List<String> image;

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSellerId() {
        return sellerId;
    }

    public void setSellerId(Integer sellerId) {
        this.sellerId = sellerId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBpmPayment() {
        return bpmPayment;
    }

    public void setBpmPayment(String bpmPayment) {
        this.bpmPayment = bpmPayment;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPv() {
        return pv;
    }

    public void setPv(String pv) {
        this.pv = pv;
    }

    public Integer getLimitFlag() {
        return limitFlag;
    }

    public void setLimitFlag(Integer limitFlag) {
        this.limitFlag = limitFlag;
    }

    public Integer getIsLive() {
        return isLive;
    }

    public void setIsLive(Integer isLive) {
        this.isLive = isLive;
    }

    public Integer getCouponEnable() {
        return couponEnable;
    }

    public void setCouponEnable(Integer couponEnable) {
        this.couponEnable = couponEnable;
    }

    public Integer getCardFlag() {
        return cardFlag;
    }

    public void setCardFlag(Integer cardFlag) {
        this.cardFlag = cardFlag;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getRejection() {
        return rejection;
    }

    public void setRejection(String rejection) {
        this.rejection = rejection;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public Integer getTxnId() {
        return txnId;
    }

    public void setTxnId(Integer txnId) {
        this.txnId = txnId;
    }

    public Integer getShippingAddressId() {
        return shippingAddressId;
    }

    public void setShippingAddressId(Integer shippingAddressId) {
        this.shippingAddressId = shippingAddressId;
    }

    public String getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(String shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingCompany() {
        return shippingCompany;
    }

    public void setShippingCompany(String shippingCompany) {
        this.shippingCompany = shippingCompany;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingMobile() {
        return shippingMobile;
    }

    public void setShippingMobile(String shippingMobile) {
        this.shippingMobile = shippingMobile;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingDistrict() {
        return shippingDistrict;
    }

    public void setShippingDistrict(String shippingDistrict) {
        this.shippingDistrict = shippingDistrict;
    }

    public String getShippingState() {
        return shippingState;
    }

    public void setShippingState(String shippingState) {
        this.shippingState = shippingState;
    }

    public Integer getShippingPin() {
        return shippingPin;
    }

    public void setShippingPin(Integer shippingPin) {
        this.shippingPin = shippingPin;
    }

    public String getShippingLandmark() {
        return shippingLandmark;
    }

    public void setShippingLandmark(String shippingLandmark) {
        this.shippingLandmark = shippingLandmark;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(String billingMobile) {
        this.billingMobile = billingMobile;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingDistrict() {
        return billingDistrict;
    }

    public void setBillingDistrict(String billingDistrict) {
        this.billingDistrict = billingDistrict;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public Integer getBillingPin() {
        return billingPin;
    }

    public void setBillingPin(Integer billingPin) {
        this.billingPin = billingPin;
    }

    public String getBillingLandmark() {
        return billingLandmark;
    }

    public void setBillingLandmark(String billingLandmark) {
        this.billingLandmark = billingLandmark;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getCartType() {
        return cartType;
    }

    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public String getRedeemAmount() {
        return redeemAmount;
    }

    public void setRedeemAmount(String redeemAmount) {
        this.redeemAmount = redeemAmount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPinNo() {
        return pinNo;
    }

    public void setPinNo(String pinNo) {
        this.pinNo = pinNo;
    }

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getMpoid() {
        return mpoid;
    }

    public void setMpoid(String mpoid) {
        this.mpoid = mpoid;
    }

    public String getOrderUniqueId() {
        return orderUniqueId;
    }

    public void setOrderUniqueId(String orderUniqueId) {
        this.orderUniqueId = orderUniqueId;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

}
}
