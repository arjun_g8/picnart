package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.api_pojo.GroceryCardData;

public class GroceryCardAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<GroceryCardData> groceryCards;
    private String rupeesSymbol;

    private GroceryCardSelectable groceryCardSelectable;

    public interface GroceryCardSelectable {
        void onGroceryCardSelected(GroceryCardData groceryCardData);
    }

    public interface GrocerCardAppliable{

    }

    public GroceryCardAdapter(Context context, List<GroceryCardData> groceryCards, GroceryCardSelectable groceryCardSelectable) {
        this.context = context;
        this.groceryCards = groceryCards;
        this.groceryCardSelectable = groceryCardSelectable;

        rupeesSymbol = context.getString(R.string.rs_symbol) + " ";
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.grocery_card_wallet_item, parent, false);
        return new GroceryCardHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        final GroceryCardHolder groceryCardHolder = (GroceryCardHolder) holder;
        final int pos = groceryCardHolder.getAdapterPosition();

        final GroceryCardData groceryCardData = groceryCards.get(pos);

        groceryCardHolder.availBalTV.setText(rupeesSymbol + Math.round(Float.parseFloat(groceryCardData.getBalance())));
        groceryCardHolder.perTransTV.setText(rupeesSymbol + Math.round(Float.parseFloat(groceryCardData.getCurrentAvailable())));
        groceryCardHolder.redeemBalTV.setText(rupeesSymbol + Math.round(Float.parseFloat(groceryCardData.getRedeemAllowance())));

        groceryCardHolder.useBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!groceryCardData.isSelected()) {
                    clearSelection(pos);
                    groceryCardData.setSelected(true);
                } else
                    groceryCardData.setSelected(false);

                notifyDataSetChanged();

                if (groceryCardData.isSelected() &&  groceryCardHolder.useBtn.getText().toString().equalsIgnoreCase("Apply"))
                    groceryCardSelectable.onGroceryCardSelected(groceryCardData);
                else
                    groceryCardSelectable.onGroceryCardSelected(null);
            }
        });

        if (groceryCardData.isSelected()) {
            groceryCardHolder.useBtn.setText("Applied");
        } else {
            groceryCardHolder.useBtn.setText("Apply");
        }
    }

    private void clearSelection(int pos) {

        for (int i = 0; i < groceryCards.size(); i++) {
            if (i != pos)
                groceryCards.get(i).setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return groceryCards.size();
    }

    private class GroceryCardHolder extends RecyclerView.ViewHolder {
        private TextView availBalTV, perTransTV, redeemBalTV;
        private AppCompatButton useBtn;

        private GroceryCardHolder(View itemView) {
            super(itemView);

            availBalTV = itemView.findViewById(R.id.avail_bal_tv_grocery_card);
            perTransTV = itemView.findViewById(R.id.per_trans_tv_grocery_card);
            redeemBalTV = itemView.findViewById(R.id.redeem_bal_tv_grocery_card);

            useBtn = itemView.findViewById(R.id.use_grocery_card_btn);
        }
    }
}
