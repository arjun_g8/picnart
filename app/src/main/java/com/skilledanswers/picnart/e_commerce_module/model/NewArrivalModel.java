package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 17-11-2017.
 */

public class NewArrivalModel {
    private int productImg;
    private String productName, price;

    public NewArrivalModel(int productImg, String productName, String price) {
        this.productImg = productImg;
        this.productName = productName;
        this.price = price;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getProductImg() {
        return productImg;
    }

    public void setProductImg(int productImg) {
        this.productImg = productImg;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}

