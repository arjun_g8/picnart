package com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 22-12-2017.
 */

public class EcommerceCategoriesResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<CategoryData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<CategoryData> getData() {
        return data;
    }

    public void setData(List<CategoryData> data) {
        this.data = data;
    }

    public class SubCategory {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("parentCategoryId")
        @Expose
        private String parentCategoryId;
        @SerializedName("hasSubcategory")
        @Expose
        private String hasSubcategory;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("subLayer")
        @Expose
        private List<SubLayer> subLayer = null;
        @SerializedName("imageURL")
        @Expose
        private String imageURL;

        private boolean isHasImage;

        public boolean isHasImage() {
            return isHasImage;
        }

        public void setHasImage(boolean hasImage) {
            isHasImage = hasImage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getParentCategoryId() {
            return parentCategoryId;
        }

        public void setParentCategoryId(String parentCategoryId) {
            this.parentCategoryId = parentCategoryId;
        }

        public String getHasSubcategory() {
            return hasSubcategory;
        }

        public void setHasSubcategory(String hasSubcategory) {
            this.hasSubcategory = hasSubcategory;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public List<SubLayer> getSubLayer() {
            return subLayer;
        }

        public void setSubLayer(List<SubLayer> subLayer) {
            this.subLayer = subLayer;
        }

        public String getImageURL() {
            return imageURL;
        }

        public void setImageURL(String imageURL) {
            this.imageURL = imageURL;
        }

    }

    public class SubLayer {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("parentCategoryId")
        @Expose
        private String parentCategoryId;
        @SerializedName("hasSubcategory")
        @Expose
        private String hasSubcategory;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("imageURL")
        @Expose
        private String imageURL;

        private boolean isHasImage;

        public boolean isHasImage() {
            return isHasImage;
        }

        public void setHasImage(boolean hasImage) {
            isHasImage = hasImage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getParentCategoryId() {
            return parentCategoryId;
        }

        public void setParentCategoryId(String parentCategoryId) {
            this.parentCategoryId = parentCategoryId;
        }

        public String getHasSubcategory() {
            return hasSubcategory;
        }

        public void setHasSubcategory(String hasSubcategory) {
            this.hasSubcategory = hasSubcategory;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getImageURL() {
            return imageURL;
        }

        public void setImageURL(String imageURL) {
            this.imageURL = imageURL;
        }

    }

    public class CategoryData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("imageURL")
        @Expose
        private String imageURL;
        @SerializedName("parentCategoryId")
        @Expose
        private Object parentCategoryId;
        @SerializedName("hasSubcategory")
        @Expose
        private String hasSubcategory;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("subCategories")
        @Expose
        private List<SubCategory> subCategories = null;

        private boolean hasImage;

        public String getImageURL() {
            return imageURL;
        }

        public void setImageURL(String imageURL) {
            this.imageURL = imageURL;
        }

        private boolean doesItHasSubCategory;
        private boolean isExpanded;

        public boolean isExpanded() {
            return isExpanded;
        }

        public void setExpanded(boolean expanded) {
            isExpanded = expanded;
        }

        public boolean isDoesItHasSubCategory() {
            return doesItHasSubCategory;
        }

        public void setDoesItHasSubCategory(boolean doesItHasSubCategory) {
            this.doesItHasSubCategory = doesItHasSubCategory;
        }

        public boolean isHasImage() {
            return hasImage;
        }

        public void setHasImage(boolean hasImage) {
            this.hasImage = hasImage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Object getParentCategoryId() {
            return parentCategoryId;
        }

        public void setParentCategoryId(Object parentCategoryId) {
            this.parentCategoryId = parentCategoryId;
        }

        public String getHasSubcategory() {
            return hasSubcategory;
        }

        public void setHasSubcategory(String hasSubcategory) {
            this.hasSubcategory = hasSubcategory;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public List<SubCategory> getSubCategories() {
            return subCategories;
        }

        public void setSubCategories(List<SubCategory> subCategories) {
            this.subCategories = subCategories;
        }

    }

}