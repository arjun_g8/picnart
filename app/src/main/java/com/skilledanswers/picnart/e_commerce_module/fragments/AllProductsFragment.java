package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.CommonUtilsEcommerce;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.EndlessRecyclerViewScrollListener;
import com.skilledanswers.picnart.Utils.OnLoadMoreListener;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.AllProductsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.FilterArrayPostAllProducts;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 23-12-2017.
 */

public class AllProductsFragment extends Fragment implements View.OnClickListener, EcommerceCustomActivity.OnBackPressedListener {
    private View allProductsView;
    private RecyclerView allProductsRV;
    private AppCompatTextView sortTV, filterTV, noProductsTV;
    private ProgressBar loadingBar;
    private LinearLayout allProductsMainView;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private List<AllProductsResponse.AllProductsData> allProducts = new ArrayList<>();
    private String subCategoryName = "", categoryId = "", subCategoryId = "", subLayerId = "", CAT_ID = "";
    private HashMap<String, ArrayList<String>> filteredHashMap = new HashMap<>();
    private String minPrice, maxPrice, minPV, maxPV;
    private EndlessRecyclerViewScrollListener scrollListener;
    private boolean isForList = false;
    private AppCompatImageButton listGridImgBtn;
    JSONObject filterPostJson;
    private RecyclerView.LayoutManager layoutManager;
    private AllProductsAdapter allProductsAdapter;

    private static final int REQ_CODE_FILTER = 777;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        allProductsView = inflater.inflate(R.layout.all_products_fragment, container, false);

        setHasOptionsMenu(true);
        ecommerceCustomActivity.setToggle(false);
        FilterArrayPostAllProducts filterArrayPostAllProducts;

        allProductsRV = allProductsView.findViewById(R.id.all_products_rv);
        layoutManager = new LinearLayoutManager(ecommerceCustomActivity);
        allProductsRV.setLayoutManager(layoutManager);
        allProductsRV.setHasFixedSize(true);

        loadingBar = allProductsView.findViewById(R.id.loading_p_bar_all_products);
        allProductsMainView = allProductsView.findViewById(R.id.load_products_view_id);
        noProductsTV = allProductsView.findViewById(R.id.no_products_tv_all_products);
        sortTV = allProductsView.findViewById(R.id.sort_all_products_tv);
        filterTV = allProductsView.findViewById(R.id.filter_all_products_tv);

        sortTV.setOnClickListener(this);
        filterTV.setOnClickListener(this);

        sortTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sort_new, 0, 0, 0);
        filterTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_filter_new, 0, 0, 0);

        listGridImgBtn = allProductsView.findViewById(R.id.list_grid_i_btn);
        listGridImgBtn.setImageResource(R.drawable.ic_list);
        listGridImgBtn.setOnClickListener(this);

        toggleView(0);


        if (getArguments() != null) {
            if (getArguments().getSerializable("FILTER_HASHMAP") != null)
                filteredHashMap = (HashMap<String, ArrayList<String>>) getArguments().getSerializable("FILTER_HASHMAP");

            categoryId = String.valueOf(getArguments().getInt("CATEGORY_ID_FOR_ALL_PRODUCTS"));
            subCategoryId = String.valueOf(getArguments().getInt("SUB_CATEGORY_ID_FOR_ALL_PRODUCTS"));
            subCategoryName = getArguments().getString("SUB_CATEGORY_NAME_FOR_ALL_PRODUCTS");

            CAT_ID = categoryId;

            minPrice = getArguments().getString("MIN_PRICE_FILTER", null);
            maxPrice = getArguments().getString("MAX_PRICE_FILTER", null);
            minPV = getArguments().getString("MIN_PV_FILTER", null);
            maxPV = getArguments().getString("MAX_PV_FILTER", null);

            System.out.println("---------- CATEGORY ID: " + categoryId);

            System.out.println("------------ MIN PV: " + minPV + "\t MAX PV: " + maxPV);
            System.out.println("------------ MIN PRICE: " + minPrice + "\t MAX PRICE: " + maxPrice);

            ArrayList<String> selectedAttributesList;

            if (filteredHashMap != null && filteredHashMap.size() > 0) {

                ArrayList<FilterArrayPostAllProducts.Filter> filterList = new ArrayList<>();

                for (Map.Entry m : filteredHashMap.entrySet()) {
                    FilterArrayPostAllProducts.Filter filter = new FilterArrayPostAllProducts.Filter();
                    filter.setAttributeId(m.getKey().toString());
                    selectedAttributesList = filteredHashMap.get(m.getKey().toString());

                    filter.setValue(selectedAttributesList);
                    filterList.add(filter);
                    System.out.println("-------- ALL PRODUCTS SEL filters ---------- " + m.getKey() + "\t" + m.getValue());
                }

                System.out.println("-------- FILTER LIST APPLIED: " + filterList.toString());

                filterArrayPostAllProducts = new FilterArrayPostAllProducts();
                filterArrayPostAllProducts.setFilter(filterList);
                FilterArrayPostAllProducts.PriceRange priceRange = new FilterArrayPostAllProducts.PriceRange();

                if (minPrice != null && maxPrice != null) {
                   /* minPrice = minPrice.replace(".00", "");
                    maxPrice = maxPrice.replace(".00", "");*/
                    priceRange.setMin(minPrice);
                    priceRange.setMax(maxPrice);
                    filterArrayPostAllProducts.setPriceRange(priceRange);
                } else {
                    filterArrayPostAllProducts.setPriceRange(null);
                }

                if (minPV != null && maxPV != null) {
                    FilterArrayPostAllProducts.PvRange pvRange = new FilterArrayPostAllProducts.PvRange();
                   /* minPV = minPV.replace(".00", "");
                    maxPV = maxPV.replace(".00", "");*/

                    pvRange.setMin(minPV);
                    pvRange.setMax(maxPV);

                    filterArrayPostAllProducts.setPvRange(pvRange);
                } else {
                    filterArrayPostAllProducts.setPvRange(null);
                }

                Gson gson = new GsonBuilder().create();
                //gson.setLenient()
                String json = gson.toJson(filterArrayPostAllProducts);

                System.out.println("---------- jSOn filter: " + json);

                try {
                    filterPostJson = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                filterArrayPostAllProducts = new FilterArrayPostAllProducts();
                filterArrayPostAllProducts.setFilter(new ArrayList<FilterArrayPostAllProducts.Filter>());
                FilterArrayPostAllProducts.PriceRange priceRange = new FilterArrayPostAllProducts.PriceRange();

                if (minPrice != null && maxPrice != null) {
                  /*  minPrice = minPrice.replace(".00", "");
                    maxPrice = maxPrice.replace(".00", "");*/
                    priceRange.setMin(minPrice);
                    priceRange.setMax(maxPrice);
                    filterArrayPostAllProducts.setPriceRange(priceRange);
                } else {
                    filterArrayPostAllProducts.setPriceRange(null);
                }

                if (minPV != null && maxPV != null) {
                    FilterArrayPostAllProducts.PvRange pvRange = new FilterArrayPostAllProducts.PvRange();
                    minPV = minPV.replace(".00", "");
                    maxPV = maxPV.replace(".00", "");
                    pvRange.setMin(minPV);
                    pvRange.setMax(maxPV);

                    filterArrayPostAllProducts.setPvRange(pvRange);
                } else {
                    filterArrayPostAllProducts.setPvRange(null);
                }

                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(filterArrayPostAllProducts);

                try {
                    filterPostJson = new JSONObject(json);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            checkConnectionAndLoadProducts();
        } else {
            checkConnectionAndLoadProducts();
        }
        // applyLazyLoading();

        return allProductsView;
    }

    private void checkConnectionAndLoadProducts() {
        if (CommonUtils.isThereInternet(ecommerceCustomActivity))

        {
            //CAT_ID = subCategoryId;
            loadAllProducts(subCategoryId,
                    0, 100000, filterPostJson);
        } else

        {
            toggleView(2);
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
        }
    }

    private void showSortBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_for_sort, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);

        final RadioGroup timeRadioGroup = sheetView.findViewById(R.id.radio_group_for_sort);

        timeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.low_to_high:
                        sortProducts("LOW TO HIGH");
                        Toast.makeText(getActivity(), "Low to high", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.high_to_low:
                        sortProducts("HIGH TO LOW");
                        Toast.makeText(getActivity(), "High to Low", Toast.LENGTH_SHORT).show();
                        break;
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBottomSheetDialog.dismiss();

                    }
                }, 250);

            }
        });

        mBottomSheetDialog.show();
    }

    private void sortProducts(String sortBy) {
        System.out.println("**** PRICE BEFORE SORT: ");

        for (int i = 0; i < allProducts.size(); i++) {
            System.out.println("**** Rs. " + allProducts.get(i).getMsp());
        }

        switch (sortBy) {
            case "HIGH TO LOW":
                Collections.sort(allProducts, new Comparator<AllProductsResponse.AllProductsData>() {
                    @Override
                    public int compare(final AllProductsResponse.AllProductsData object1, final AllProductsResponse.AllProductsData object2) {
                        int obj1, obj2;
                        String msp1 = object1.getMsp().replace(".00", "");
                        String msp2 = object2.getMsp().replace(".00", "");
                        obj1 = Integer.parseInt(msp1);
                        obj2 = Integer.parseInt(msp2);
                        return obj2 - obj1;
                    }
                });

                System.out.println("\n\n**** PRICE AFTER SORT: ");

                for (int i = 0; i < allProducts.size(); i++) {
                    System.out.println("**** Rs. " + allProducts.get(i).getMsp());
                }

                break;
            case "LOW TO HIGH":
                Collections.sort(allProducts, new Comparator<AllProductsResponse.AllProductsData>() {
                    @Override
                    public int compare(final AllProductsResponse.AllProductsData object1, final AllProductsResponse.AllProductsData object2) {
                        int obj1, obj2;
                        String msp1 = object1.getMsp().replace(".00", "");
                        String msp2 = object2.getMsp().replace(".00", "");
                        obj1 = Integer.parseInt(msp1);
                        obj2 = Integer.parseInt(msp2);
                        return obj1 - obj2;
                    }
                });
                break;
        }

        if (allProducts.size() > 0) {
            //Toast.makeText(ecommerceCustomActivity, allProducts.size() + " Products found", Toast.LENGTH_SHORT).show();
            if (isForList)
                allProductsRV.setAdapter(new AllProductsAdapter(1));
            else
                allProductsRV.setAdapter(new AllProductsAdapter(0));
            allProductsRV.getAdapter().notifyDataSetChanged();

            toggleView(1);
        } else {
            System.out.println("-------- Un Success: ");
            toggleView(2);
            noProductsTV.setText("No Products found");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle(subCategoryName);

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

      /*  ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle(subCategoryName);*/
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(true);
        //menu.findItem(R.id.action_settings).setVisible(false);
//        menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void toggleView(int loadStatus) {
        if (loadStatus == 0) {
            allProductsMainView.setVisibility(View.GONE);
            loadingBar.setVisibility(View.VISIBLE);
            noProductsTV.setVisibility(View.GONE);
        } else if (loadStatus == 1) {
            loadingBar.setVisibility(View.GONE);
            allProductsMainView.setVisibility(View.VISIBLE);
            noProductsTV.setVisibility(View.GONE);
        } else if (loadStatus == 2) {
            loadingBar.setVisibility(View.GONE);
            allProductsMainView.setVisibility(View.GONE);
            noProductsTV.setVisibility(View.VISIBLE);
            noProductsTV.setText("No Products");
        }
    }

    private void loadAllProducts(String categoryId, int offSet, int limit, JSONObject filterArray) {
        if (isForList) {
            layoutManager = new LinearLayoutManager(ecommerceCustomActivity);
            allProductsRV.setLayoutManager(layoutManager);
            allProductsRV.setHasFixedSize(true);
        } else {
            layoutManager = new GridLayoutManager(ecommerceCustomActivity, 2);
            allProductsRV.setLayoutManager(layoutManager);
            allProductsRV.setHasFixedSize(true);
        }

        System.out.println("-------- FILTER LIST APPLIED FOR LOADING: " + filterArray.toString());
        System.out.println("-------- All products POST PARAM: " + categoryId);
        System.out.println("-------- SUB cat id all products: " + CAT_ID);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getAllProducts("loadProducts", Integer.parseInt(categoryId),
                offSet, limit, filterArray).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
//                Log.e("RESPONSE", response.body().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        allProducts.clear();
                        allProducts = response.body().getData();

                        for (int i = 0; i < allProducts.size(); i++) {
                            if (allProducts.get(i).getImage() != null && !TextUtils.isEmpty(allProducts.get(i).getImage()))
                                allProducts.get(i).setHasImage(true);
                            else
                                allProducts.get(i).setHasImage(false);
                        }

                        minPrice = response.body().getPriceFilter().getMinPrice();
                        maxPrice = response.body().getPriceFilter().getMaxPrice();

                        minPV = response.body().getPvFilter().getMinpv();
                        maxPV = response.body().getPvFilter().getMaxpv();

                        if (allProducts.size() > 0) {
                            /*if (allProducts.size() > 5) {
                                System.out.println("-------- All products size: " + allProducts.size());
                                if (isForList) */

                           // Toast.makeText(ecommerceCustomActivity, allProducts.size() + " Products", Toast.LENGTH_SHORT).show();

                            if (isForList)
                                allProductsRV.setAdapter(new AllProductsAdapter(1));
                            else
                                allProductsRV.setAdapter(new AllProductsAdapter(0));

                            toggleView(1);

                          /*  allProductsAdapter = new AllProductsAdapter(1);
                            allProductsRV.setAdapter(allProductsAdapter);*/
                                  /*  allProductsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                                        @Override
                                        public void onLoadingMoreItems() {
                                            //add null , so the adapter will check view_type and show progress bar at bottom
                                            allProducts.add(null);
                                            allProductsAdapter.notifyItemInserted(allProducts.size() - 1);

                                            handler.postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //   remove progress item
                                                    allProducts.remove(allProducts.size() - 1);
                                                    allProductsAdapter.notifyItemRemoved(allProducts.size());
                                                    //add items one by one

                                                    int start = allProducts.size();
                                                    int end;
                                                    if (start < 5) {
                                                        end = start;
                                                    } else {
                                                        end = start + 5;
                                                    }

                                                    for (int i = start + 1; i <= end; i++) {
                                                        allProducts.add(allProducts.get(i));
                                                        allProductsAdapter.notifyItemInserted(allProducts.size());
                                                    }
                                                    allProductsAdapter.setLoaded();
                                                    //or you can add all at once but do not forget to call mAdapter.notifyDataSetChanged();
                                                }
                                            }, 2000);

                                        }
                                    });
                                } else
                                    allProductsRV.setAdapter(new AllProductsAdapter(0));
                                allProductsRV.getAdapter().notifyDataSetChanged();
                                toggleView(1);
                            }
                        } else {
                            if (isForList) {
                                allProductsAdapter = new AllProductsAdapter(1);
                                allProductsRV.setAdapter(allProductsAdapter);
                                allProductsAdapter.notifyDataSetChanged();
                            }
                        }*/
                        }

                    } else {
                        System.out.println("-------- Un Success: " + response.body().toString());
                        toggleView(2);
                        noProductsTV.setText("No Products found");
                    }

                } else {
                    System.out.println("-------- Un Success exc: " + response.message());
                    toggleView(2);
                }
            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("----- " + t.toString());
                toggleView(2);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sort_all_products_tv:
                showSortBottomSheetDialog();
                break;
            case R.id.filter_all_products_tv:
                filteredHashMap.clear();
              /*  minPV = null;
                maxPV = null;
                minPrice = null;
                maxPrice = null;*/
                Bundle bundle = new Bundle();
                bundle.putInt("CATEGORY_ID_FILTER", Integer.parseInt(categoryId));
                bundle.putInt("SUB_CATEGORY_ID_FILTER", Integer.parseInt(subCategoryId));
                bundle.putString("SUB_CATEGORY_NAME_FILTER", subCategoryName);
                bundle.putString("MIN_PV_FILTER", minPV);
                bundle.putString("MAX_PV_FILTER", maxPV);
                bundle.putString("MIN_PRICE_FILTER", minPrice);
                bundle.putString("MAX_PRICE_FILTER", maxPrice);
                Fragment fragment = new ApplyFilterFragment();
                fragment.setArguments(bundle);
                // moveToFragmentNoStack(fragment, "APPLY_FILTER_FRAGMENT");
                ecommerceCustomActivity.moveToFragment(fragment, "APPLY_FILTER_FRAGMENT");
                break;

            case R.id.list_grid_i_btn:
                if (isForList) {
                    listGridImgBtn.setImageResource(R.drawable.ic_list);
                    isForList = false;
                    // Toast.makeText(ecommerceCustomActivity, "List", Toast.LENGTH_SHORT).show();
                } else {
                    listGridImgBtn.setImageResource(R.drawable.ic_grid);
                    isForList = true;
                    // Toast.makeText(ecommerceCustomActivity, "Grid", Toast.LENGTH_SHORT).show();
                }

                // applyLazyLoading();

                if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
                    loadAllProducts(subCategoryId,
                            0, 100000, filterPostJson);
                } else {
                    toggleView(2);
                    DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                }
                break;
        }
    }

    public void moveToFragmentNoStack(Fragment toFragment, String TAG) {
        ecommerceCustomActivity.hideSearchBar();
        System.out.println("------- INSIDE MOVE TO FRAGMENT");
        FragmentTransaction transaction = ecommerceCustomActivity.getSupportFragmentManager().beginTransaction();
        Fragment fragment = ecommerceCustomActivity.getSupportFragmentManager().findFragmentByTag(TAG);

        if (fragment != null && fragment.isVisible()) {
            System.out.println("*** ALREADY there ****");
            // ecommerceCustomActivity.replaceFragment(toFragment);

        } else {
            transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
            //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.replace(R.id.relative, toFragment, TAG);
            transaction.commit();
        }
        System.out.println("-------- DONE MOVE TO FRAGMENT");
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }


    private class AllProductsAdapter extends RecyclerView.Adapter {
        int view;

        private final int VIEW_ITEM = 1;
        private final int VIEW_PROG = 0;

        // The minimum amount of items to have below your current scroll position
// before loading more.
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;
        private boolean loading;
        private OnLoadMoreListener onLoadMoreListener;

        AllProductsAdapter(int view) {
            this.view = view;

           /* if (allProductsRV.getLayoutManager() instanceof LinearLayoutManager) {

                final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) allProductsRV
                        .getLayoutManager();


                allProductsRV
                        .addOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrolled(RecyclerView recyclerView,
                                                   int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);

                                totalItemCount = linearLayoutManager.getItemCount();
                                lastVisibleItem = linearLayoutManager
                                        .findLastVisibleItemPosition();
                                if (!loading
                                        && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                    // End has been reached
                                    // Do something
                                    if (onLoadMoreListener != null) {
                                        onLoadMoreListener.onLoadingMoreItems();
                                    }
                                    loading = true;
                                }
                            }
                        });
            }*/
        }

       /* @Override
        public int getItemViewType(int position) {
            return allProducts.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }*/

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder vh;
            //   if (viewType == VIEW_ITEM) {
            View v = null;
            if (view == 0)
                v = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.index_item, parent, false);
            else if (view == 1)
                v = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.product_view_like_list, parent, false);
            vh = new AllProductsHolder(v);
        /*    } else {
                View v = LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.loading_progress_view, parent, false);

                vh = new ProgressViewHolder(v);
            }*/
            return vh;
        }

        private String getDiscountInPercent(double msp, double mrp) {
      /*  Discount = Marked Price - Selling Price

        Discount% = Discount/Marked Price × 100%.*/

            double dis = mrp - msp;
            double discount = (dis / mrp) * 100;

            DecimalFormat value = new DecimalFormat("#.#");
            value.format(discount);

            return String.valueOf(Math.round(discount));
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            // if (holder instanceof AllProductsHolder) {
            AllProductsHolder allProductsHolder = (AllProductsHolder) holder;
            allProductsHolder.productNameTV.setText(allProducts.get(position).getBrand()
                    + " " + allProducts.get(position).getTitle());
            String msp = allProducts.get(position).getMsp().replace(".00", "");
            String mrp = allProducts.get(position).getMrp().replace(".00", "");

            int someNumber = Integer.parseInt(msp);
            NumberFormat nf = NumberFormat.getInstance();
            String mspFormatted = nf.format(someNumber);

            int mrpNum = Integer.parseInt(mrp);
            String mrpFormatted = nf.format(mrpNum);

            Log.e("MSP: ", mspFormatted);

            allProductsHolder.sellingPriceTV.setText(mspFormatted);
            allProductsHolder.originalPriceTV.setPaintFlags(allProductsHolder.originalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            allProductsHolder.originalPriceTV.setText(mrpFormatted);
            allProductsHolder.pvTV.setText("CBP: " + allProducts.get(position).getPv());

            String rating = allProducts.get(position).getRate();

            if (rating == null)
                rating = "0";

            Double ratingInDouble = Double.valueOf(rating);

            allProductsHolder.ratingsTV.setText(CommonUtilsEcommerce.formatToSingleDecimalFraction(rating));

            Log.e("Actual rating: ", String.valueOf(ratingInDouble));

            if (ratingInDouble <= 1.0 && ratingInDouble > 0) {
                allProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                allProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_red_bg);
            } else if (ratingInDouble < 2 && ratingInDouble > 1.0) {
                allProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                allProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_yellow_bg);
            } else if (ratingInDouble < 3.0 && ratingInDouble >= 2.0) {
                allProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                allProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_orange_bg);
            } else if (ratingInDouble <= 5.0 && ratingInDouble >= 3.0) {
                allProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_green_bg);
                allProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
            } else {
                allProductsHolder.ratingsTV.setVisibility(View.GONE);
            }

            allProductsHolder.discountTV.setText(getDiscountInPercent(Double.parseDouble(msp), Double.parseDouble(mrp)) + "% off");
            //allProductsHolder.ratingBar.setNumStars(allProducts.get(position));

            if (allProducts.get(position).getQuantity().equals("0")) {
                allProductsHolder.discountTV.setVisibility(View.GONE);
                allProductsHolder.outOfStockTV.setVisibility(View.VISIBLE);
            } else {
                allProductsHolder.outOfStockTV.setVisibility(View.GONE);
                allProductsHolder.discountTV.setVisibility(View.VISIBLE);
            }

            if (allProducts.get(position).isHasImage()) {
                Glide.with(ecommerceCustomActivity)
                        .load(new GlideUrl(allProducts.get(position).getImage()))
                        .placeholder(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(allProductsHolder.productIV);
            }

            allProductsHolder.productItemMainCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment productOverViewFragment = new OverviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("CAT_ID_FOR_OVERVIEW", Integer.parseInt(categoryId));
                    // Log.e("CAT_ID ALL PROD: ", "" + Integer.parseInt(allProducts.get(position).getCategoryId()));
                    bundle.putInt("SUB_CATEGORY_ID_FOR_OVERVIEW", Integer.parseInt(subCategoryId));
                    bundle.putString("PRODUCT_ID", allProducts.get(position).getId());
                    bundle.putString("PRODUCT_CODE", allProducts.get(position).getProductCode());
                    bundle.putString("FOR", "LIVE");
                    productOverViewFragment.setArguments(bundle);
                    ecommerceCustomActivity.moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
                }
            });

           /* } else {
                ProgressViewHolder progressViewHolder = (ProgressViewHolder) holder;
                ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
            }*/
        }

        public void setLoaded() {
            loading = false;
        }

        @Override
        public int getItemCount() {
            return allProducts.size();
        }

        public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
            this.onLoadMoreListener = onLoadMoreListener;
        }
    }

    private class AllProductsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView sellingPriceTV, originalPriceTV, productNameTV, discountTV, pvTV, ratingsTV, outOfStockTV;
        private CardView productItemMainCV;
        private AppCompatImageView productIV;
        private RatingBar ratingBar;

        AllProductsHolder(View itemView) {
            super(itemView);
            sellingPriceTV = itemView.findViewById(R.id.selling_price_tv);
            originalPriceTV = itemView.findViewById(R.id.purchase_pice_tv);
            productNameTV = itemView.findViewById(R.id.product_name_tv);
            sellingPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian, 0, 0, 0);
            //  originalPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian_grey, 0, 0, 0);
            productItemMainCV = itemView.findViewById(R.id.product_item_main_view);
            productIV = itemView.findViewById(R.id.product_iv);
            discountTV = itemView.findViewById(R.id.discount_tv);
            pvTV = itemView.findViewById(R.id.pv_tv_item);
            ratingBar = itemView.findViewById(R.id.rating_bar_product_item);
            ratingsTV = itemView.findViewById(R.id.rating_tv_product_item);
            outOfStockTV = itemView.findViewById(R.id.out_of_stock_tv);
            outOfStockTV.setVisibility(View.GONE);
            ratingsTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star, 0);
            if (CAT_ID.equals("41")) {
                ratingsTV.setVisibility(View.GONE);
            }
        }
    }

    private void applyLazyLoading() {
        if (isForList) {
            scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    System.out.println("-------------RECYCLER SCROLL PAGE: " + page);
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list

                    if (CommonUtils.isThereInternet(ecommerceCustomActivity))

                    {
                        loadAllProducts(subCategoryId,
                                page, 5, filterPostJson);
                    } else

                    {
                        toggleView(2);
                        DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                    }
                }
            };
            // Adds the scroll listener to RecyclerView
            allProductsRV.addOnScrollListener(scrollListener);
        } else {
            scrollListener = new EndlessRecyclerViewScrollListener((GridLayoutManager) layoutManager) {
                @Override
                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                    System.out.println("-------------RECYCLER SCROLL PAGE: " + page);
                    // Triggered only when new data needs to be appended to the list
                    // Add whatever code is needed to append new items to the bottom of the list

                    if (CommonUtils.isThereInternet(ecommerceCustomActivity))

                    {
                        loadAllProducts(subCategoryId,
                                page, 5, filterPostJson);
                    } else

                    {
                        toggleView(2);
                        DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                    }
                }
            };
            // Adds the scroll listener to RecyclerView
            allProductsRV.addOnScrollListener(scrollListener);
        }
    }

}
