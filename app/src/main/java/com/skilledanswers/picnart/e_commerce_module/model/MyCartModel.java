package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 18-05-2016.
 */
public class MyCartModel {
    private String _Name = null;
    private String _shortDesc = null;
    private String _price = null;
    private int _quantity = 0;
    private String _delevery = null;
    private String _deleveryDetails = null;
    private String _sellar = null;
    private int _image = 0;
    private int _rating = 0;

    public int get_rating() {
        return _rating;
    }

    public void set_rating(int _rating) {
        this._rating = _rating;
    }

    public String get_Name() {
        return _Name;
    }

    public void set_Name(String _Name) {
        this._Name = _Name;
    }

    public String get_shortDesc() {
        return _shortDesc;
    }

    public void set_shortDesc(String _shortDesc) {
        this._shortDesc = _shortDesc;
    }

    public String get_price() {
        return _price;
    }

    public void set_price(String _price) {
        this._price = _price;
    }

    public int get_quantity() {
        return _quantity;
    }

    public void set_quantity(int _quantity) {
        this._quantity = _quantity;
    }

    public String get_delevery() {
        return _delevery;
    }

    public void set_delevery(String _delevery) {
        this._delevery = _delevery;
    }

    public String get_deleveryDetails() {
        return _deleveryDetails;
    }

    public void set_deleveryDetails(String _deleveryDetails) {
        this._deleveryDetails = _deleveryDetails;
    }

    public String get_sellar() {
        return _sellar;
    }

    public void set_sellar(String _sellar) {
        this._sellar = _sellar;
    }

    public int get_image() {
        return _image;
    }

    public void set_image(int _image) {
        this._image = _image;
    }
}
