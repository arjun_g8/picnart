package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.CommonUtilsEcommerce;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 15-11-2017.
 */

public class UserProfileFragment extends Fragment implements View.OnClickListener {
    private EcomPrefUtil prefUtil;
    private View profileView;
    private AppCompatTextView nameTV, emailTV, mobTV, logoutTV, designationTV, memberTypeTV;
    private RelativeLayout beforeLoadView;
    private LinearLayout afterLoadView;
    private ProgressBar pBar;
    private EcommerceCustomActivity activity;
    private PrefUtil appPrefUtil;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        profileView = inflater.inflate(R.layout.user_profile_fragment, container, false);
        setHasOptionsMenu(true);
        activity = (EcommerceCustomActivity) getActivity();
        activity.setToggle(false);
        prefUtil = new EcomPrefUtil(getActivity());
        appPrefUtil = new PrefUtil(activity);
        initProfileView();

        System.out.println("---------- PROFILE ON CREATE");

        return profileView;
    }

    private void initProfileView() {
        nameTV = profileView.findViewById(R.id.user_name_tv_profile);
        emailTV = profileView.findViewById(R.id.email_tv_profile);
        mobTV = profileView.findViewById(R.id.mob_tv_profile);
        logoutTV = profileView.findViewById(R.id.logout_tv_profile);
        designationTV = profileView.findViewById(R.id.designation_tv_profile);
        memberTypeTV = profileView.findViewById(R.id.member_type_tv_profile);
        profileView.findViewById(R.id.view_all_addr_tv_profile).setOnClickListener(this);
        logoutTV.setOnClickListener(this);
        profileView.findViewById(R.id.view_all_orders_profile).setOnClickListener(this);
        profileView.findViewById(R.id.view_all_daily_report_profile).setOnClickListener(this);
        profileView.findViewById(R.id.view_all_weekly_report_profile).setOnClickListener(this);
        profileView.findViewById(R.id.gen_epin_profile).setOnClickListener(this);
        profileView.findViewById(R.id.view_all_my_group_profile).setOnClickListener(this);
        profileView.findViewById(R.id.view_my_profit_card_profile).setOnClickListener(this);

        logoutTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_logout_button, 0, 0, 0);

        beforeLoadView = profileView.findViewById(R.id.before_load_view);
        afterLoadView = profileView.findViewById(R.id.after_load_view_profile);
        pBar = profileView.findViewById(R.id.progress_bar_profile);

        toggleView(1);

        if (CommonUtils.isThereInternet(getActivity()))
            loadProfileInfo();
        else {
            DialogUtils.showNoInternetDialog(getActivity());
            pBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("---------- PROFILE RESUME");
        activity = (EcommerceCustomActivity) getActivity();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("My Profile");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
    }

    private void updateView(String name, String mob, String email) {
        nameTV.setText(name);
        mobTV.setText(mob);
        emailTV.setText(email);
    }

    private void loadProfileInfo() {
        // With out calling api
        updateView(appPrefUtil.getFirstName(), appPrefUtil.getMob(), appPrefUtil.getEmail());
        getDashBoardDetails();
    }

    private void getDashBoardDetails() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getDashBoardDetailsForProfile("getDashboardDetails", appPrefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String dashBoardDetailsStringResponse = new String(response.body().bytes());
                            JSONObject responseJsonObj = new JSONObject(dashBoardDetailsStringResponse);

                            Log.e("DASHBOARD ", "STRING RESPONSE: " + dashBoardDetailsStringResponse);

                            if (responseJsonObj.getBoolean("status")) {
                                JSONObject dashBoardDataObj = responseJsonObj.getJSONObject("data");
                                JSONObject dashboardDetails = dashBoardDataObj.getJSONObject("dashboard_details");
                                String designation = dashboardDetails.getString("designation");
                                String accountType = dashboardDetails.getString("account");
                                String accountTypeFormatted = accountType.substring(0, 1).toUpperCase() + accountType.substring(1).toLowerCase();
                                memberTypeTV.setText("BluepayMax " + accountTypeFormatted + " Member");
                                designationTV.setText("Designation: " + designation);
                            } else {
                                if (!activity.isFinishing()) {
                                    DialogUtils.showReLoginPopUp(activity);
                                    Toast.makeText(activity, "" + responseJsonObj.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /*menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_cart).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);*/
        menu.clear();
    }

    private void toggleView(int v) {
        switch (v) {
            case 0:
                afterLoadView.setVisibility(View.GONE);
                beforeLoadView.setVisibility(View.VISIBLE);
                pBar.setVisibility(View.VISIBLE);
                break;
            case 1:
                pBar.setVisibility(View.GONE);
                beforeLoadView.setVisibility(View.GONE);
                afterLoadView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_all_addr_tv_profile:
                Bundle bundle = new Bundle();
                bundle.putString("VIEW_TYPE", "View all");
                bundle.putBoolean("ORDER_PLACE", false);
                activity.moveToFragment(new DeliveryAddressFragment(), "DELIVERY_ADDRESS", bundle);
                break;

            case R.id.logout_tv_profile:
                logoutPopUp(activity);
                break;

            case R.id.view_all_orders_profile:
                if (CommonUtilsEcommerce.isThereInternet(activity))
                    activity.moveToFragment(new MyOrders(), "MY_ORDERS_FRAGMENT");
                else
                    DialogUtils.showNoInternetDialog(activity);
                break;
        }
    }

    private void logoutPopUp(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.logout_dialog);

        dialog.findViewById(R.id.yes_logout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                activity.logoutOfThisApp();
            }
        });

        dialog.findViewById(R.id.no_logout_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }
}
