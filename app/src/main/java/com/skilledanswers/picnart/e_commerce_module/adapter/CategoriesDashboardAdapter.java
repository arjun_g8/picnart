package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.RecyclerViewOnClick;
import bluepaymax.skilledanswers.picnart.bus.models.CategorisModel;

public class CategoriesDashboardAdapter extends RecyclerView.Adapter<CategoriesDashboardAdapter.MyViewHolder> {

    private List<CategorisModel> categorisModelList;
    private RecyclerViewOnClick recyclerViewOnClick;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView categoryName;
        public AppCompatImageView categoryImage;
        private RelativeLayout dashboardItemMain;

        public MyViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.categoryName);
            categoryImage = (AppCompatImageView) view.findViewById(R.id.categoryImage);
            dashboardItemMain = view.findViewById(R.id.dash_board_item);
        }
    }


    public CategoriesDashboardAdapter(List<CategorisModel> categorisModelList, RecyclerViewOnClick recyclerViewOnClick) {
        this.categorisModelList = categorisModelList;
        this.recyclerViewOnClick = recyclerViewOnClick;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        CategorisModel categorisModel = categorisModelList.get(position);
        holder.categoryName.setText(categorisModel.getCategoryName());
        holder.categoryImage.setBackgroundResource(categorisModel.getCategoryImage());
        holder.dashboardItemMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerViewOnClick.onClick(position);
                recyclerViewOnClick.onClick(categorisModelList.get(position).getCategoryName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return categorisModelList.size();
    }
}