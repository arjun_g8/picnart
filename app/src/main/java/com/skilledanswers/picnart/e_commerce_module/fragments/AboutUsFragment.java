package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;

/**
 * Created by SkilledAnswers-D1 on 18-01-2018.
 */

public class AboutUsFragment extends Fragment {
    private EcommerceCustomActivity ecommerceCustomActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.about_us_fragment, container, false);
        setHasOptionsMenu(true);
        ecommerceCustomActivity.setToggle(false);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        /*menu.findItem(R.id.action_search).setVisible(false);
        menu.findItem(R.id.action_cart).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);*/
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("About us");
    }
}
