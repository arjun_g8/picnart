package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 23-12-2017.
 */

public class ProductDescriptionResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private ProductDescriptionResponse.ProductData data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public ProductData getData() {
        return data;
    }

    public void setData(ProductData data) {
        this.data = data;
    }

    public class Rating {

        @SerializedName("rate")
        @Expose
        private Object rate;
        @SerializedName("count")
        @Expose
        private String count;
        @SerializedName("avg")
        @Expose
        private String avg;
        @SerializedName("totalvote")
        @Expose
        private String totalvote;

        public Object getRate() {
            return rate;
        }

        public void setRate(Object rate) {
            this.rate = rate;
        }

        public String getCount() {
            return count;
        }

        public void setCount(String count) {
            this.count = count;
        }

        public String getAvg() {
            return avg;
        }

        public void setAvg(String avg) {
            this.avg = avg;
        }

        public String getTotalvote() {
            return totalvote;
        }

        public void setTotalvote(String totalvote) {
            this.totalvote = totalvote;
        }

    }

    public class Info {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("sellerId")
        @Expose
        private String sellerId;
        @SerializedName("sku")
        @Expose
        private String sku;
        @SerializedName("productId")
        @Expose
        private String productId;
        @SerializedName("msp")
        @Expose
        private String msp;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("shippingCharge")
        @Expose
        private String shippingCharge;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSellerId() {
            return sellerId;
        }

        public void setSellerId(String sellerId) {
            this.sellerId = sellerId;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getMsp() {
            return msp;
        }

        public void setMsp(String msp) {
            this.msp = msp;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getShippingCharge() {
            return shippingCharge;
        }

        public void setShippingCharge(String shippingCharge) {
            this.shippingCharge = shippingCharge;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

    public class ProductData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("categoryId")
        @Expose
        private String categoryId;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("isLive")
        @Expose
        private String isLive;
        @SerializedName("adminId")
        @Expose
        private String adminId;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("rejection")
        @Expose
        private String rejection;
        @SerializedName("imageURL")
        @Expose
        private List<String> imageURL = null;
        @SerializedName("attributes")
        @Expose
        private List<ProductAttribute> attributes = null;
        @SerializedName("info")
        @Expose
        private List<Info> info = null;
        @SerializedName("rating")
        @Expose
        private Rating rating;
        @SerializedName("reviewCount")
        @Expose
        private String reviewCount;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getIsLive() {
            return isLive;
        }

        public void setIsLive(String isLive) {
            this.isLive = isLive;
        }

        public String getAdminId() {
            return adminId;
        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getRejection() {
            return rejection;
        }

        public void setRejection(String rejection) {
            this.rejection = rejection;
        }

        public List<String> getImageURL() {
            return imageURL;
        }

        public void setImageURL(List<String> imageURL) {
            this.imageURL = imageURL;
        }

        public List<ProductAttribute> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<ProductAttribute> attributes) {
            this.attributes = attributes;
        }

        public List<Info> getInfo() {
            return info;
        }

        public void setInfo(List<Info> info) {
            this.info = info;
        }

        public Rating getRating() {
            return rating;
        }

        public void setRating(Rating rating) {
            this.rating = rating;
        }

        public String getReviewCount() {
            return reviewCount;
        }

        public void setReviewCount(String reviewCount) {
            this.reviewCount = reviewCount;
        }

    }

    public class ProductAttribute {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("attribute_id")
        @Expose
        private String attributeId;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("is_mandatory")
        @Expose
        private String isMandatory;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(String attributeId) {
            this.attributeId = attributeId;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getIsMandatory() {
            return isMandatory;
        }

        public void setIsMandatory(String isMandatory) {
            this.isMandatory = isMandatory;
        }

    }

}
