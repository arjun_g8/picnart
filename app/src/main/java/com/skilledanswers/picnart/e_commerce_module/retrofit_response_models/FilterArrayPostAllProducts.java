package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 23-12-2017.
 */

public class FilterArrayPostAllProducts {

    @SerializedName("filter")
    @Expose
    private List<Filter> filter = null;
    @SerializedName("priceRange")
    @Expose
    private PriceRange priceRange;
    @SerializedName("pvRange")
    @Expose
    private PvRange pvRange;
    @SerializedName("sort")
    @Expose
    private String sort;

    public List<Filter> getFilter() {
        return filter;
    }

    public void setFilter(List<Filter> filter) {
        this.filter = filter;
    }

    public PriceRange getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(PriceRange priceRange) {
        this.priceRange = priceRange;
    }

    public PvRange getPvRange() {
        return pvRange;
    }

    public void setPvRange(PvRange pvRange) {
        this.pvRange = pvRange;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "FilterArrayPostAllProducts{" +
                "filter=" + filter +
                ", priceRange=" + priceRange +
                ", pvRange=" + pvRange +
                ", sort='" + sort + '\'' +
                '}';
    }

    public static class Filter {

        @SerializedName("attributeId")
        @Expose
        private String attributeId;
        @SerializedName("value")
        @Expose
        private List<String> value = null;

        public String getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(String attributeId) {
            this.attributeId = attributeId;
        }

        public List<String> getValue() {
            return value;
        }

        public void setValue(List<String> value) {
            this.value = value;
        }
    }

    public static class PriceRange {

        @SerializedName("min")
        @Expose
        private String min;
        @SerializedName("max")
        @Expose
        private String max;

        public String getMin() {
            return min;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }

    }

    public static class PvRange {

        @SerializedName("min")
        @Expose
        private String min;
        @SerializedName("max")
        @Expose
        private String max;

        public String getMin() {
            return min;
        }

        public void setMin(String min) {
            this.min = min;
        }

        public String getMax() {
            return max;
        }

        public void setMax(String max) {
            this.max = max;
        }

    }

}