package com.skilledanswers.picnart.e_commerce_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 16-11-2017.
 */


public class AddressResponse {

    @SerializedName("data")
    @Expose
    private List<AddressData> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<AddressData> getData() {
        return data;
    }

    public void setData(List<AddressData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public class AddressData {

        @SerializedName("addressId")
        @Expose
        private Integer addressId;
        @SerializedName("address")
        @Expose
        private Object address;
       /* @SerializedName("created")
        @Expose
        private BigInteger created;*/
        @SerializedName("pincode")
        @Expose
        private Integer pincode;
       /* @SerializedName("updated")
        @Expose
        private BigInteger updated;*/
        @SerializedName("city")
        @Expose
        private Object city;

        public Integer getAddressId() {
            return addressId;
        }

        public void setAddressId(Integer addressId) {
            this.addressId = addressId;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

       /* public BigInteger getCreated() {
            return created;
        }

        public void setCreated(BigInteger created) {
            this.created = created;
        }*/

        public Integer getPincode() {
            return pincode;
        }

        public void setPincode(Integer pincode) {
            this.pincode = pincode;
        }

        /*public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

        public Object getCity() {
            return city;
        }

        public void setCity(Object city) {
            this.city = city;
        }

    }

}
