package bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;
import bluepaymax.skilledanswers.picnart.e_commerce_module.CommonUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.DialogUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfitCardDetailsActivity extends AppCompatActivity {
    private AppCompatTextView historyIdTV, pinNumTV, amtTV;
    private ProgressBar loadingBar;
    private CardView profitCardCV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profit_card_details);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profit Card Details");

        loadingBar = findViewById(R.id.profit_card_progress);
        profitCardCV = findViewById(R.id.profit_card_cv);
        profitCardCV.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);
        historyIdTV = findViewById(R.id.history_id_tv);
        pinNumTV = findViewById(R.id.pin_num_tv);
        amtTV = findViewById(R.id.profit_card_amt_tv);
        //  amtTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian,0,0,0);

        if (CommonUtils.isThereInternet(ProfitCardDetailsActivity.this)) {
            getProfitCardDetails();
        } else {
            loadingBar.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(ProfitCardDetailsActivity.this);
        }

    }

    private void getProfitCardDetails() {
        PrefUtil prefUtil = new PrefUtil(ProfitCardDetailsActivity.this);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getProfitCardDetails("getProfitCardDetails", prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String jsonString = new String(response.body().bytes());

                            Log.e("PROFIT CARD STRING ", " RESPONSE: " + jsonString);

                            JSONObject jsonObject = new JSONObject(jsonString);

                            boolean status = jsonObject.getBoolean("status");
                            if (status) {
                                JSONObject dataObj = jsonObject.getJSONObject("data");
                                if (dataObj != null) {
                                    pinNumTV.setText(dataObj.getString("pin_no"));
                                    historyIdTV.setText(dataObj.getString("history_id"));
                                    amtTV.setText("Rs. " + dataObj.getString("amount"));
                                    loadingBar.setVisibility(View.GONE);
                                    profitCardCV.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Toast.makeText(ProfitCardDetailsActivity.this, "Token expired Please Login Again", Toast.LENGTH_SHORT).show();
                                DialogUtils.showReLoginPopUp(ProfitCardDetailsActivity.this);
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                            loadingBar.setVisibility(View.GONE);
                            profitCardCV.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            loadingBar.setVisibility(View.GONE);
                            profitCardCV.setVisibility(View.GONE);
                        }
                    }
                } else {
                    loadingBar.setVisibility(View.GONE);
                    profitCardCV.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                profitCardCV.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
