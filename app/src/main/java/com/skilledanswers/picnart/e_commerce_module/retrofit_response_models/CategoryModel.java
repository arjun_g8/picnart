package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 03-11-2017.
 */

public class CategoryModel {
    @SerializedName("categoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    /*@SerializedName("createdAt")
    @Expose
    private BigInteger createdAt;*/
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("image")
    @Expose
    private List<String> image = null;
   /* @SerializedName("updated")
    @Expose
    private Object updated;*/
    @SerializedName("attributes")
    @Expose
    private List<Attribute> attributes = null;
    @SerializedName("products")
    @Expose
    private List<Object> products = null;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

   /* public BigInteger getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(BigInteger createdAt) {
        this.createdAt = createdAt;
    }*/

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImage() {
        return image;
    }

    public void setImage(List<String> image) {
        this.image = image;
    }

    /* public Object getUpdated() {
         return updated;
     }

     public void setUpdated(Object updated) {
         this.updated = updated;
     }
 */
    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Object> getProducts() {
        return products;
    }

    public void setProducts(List<Object> products) {
        this.products = products;
    }

    private class Attribute {
        @SerializedName("attributeId")
        @Expose
        private Integer attributeId;
        @SerializedName("created")
        @Expose
        private BigInteger created;
        @SerializedName("isMandatory")
        @Expose
        private Integer isMandatory;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("updated")
        @Expose
        private Object updated;

        public Integer getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(Integer attributeId) {
            this.attributeId = attributeId;
        }

        public BigInteger getCreated() {
            return created;
        }

        public void setCreated(BigInteger created) {
            this.created = created;
        }

        public Integer getIsMandatory() {
            return isMandatory;
        }

        public void setIsMandatory(Integer isMandatory) {
            this.isMandatory = isMandatory;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getUpdated() {
            return updated;
        }

        public void setUpdated(Object updated) {
            this.updated = updated;
        }
    }

}
