package com.skilledanswers.picnart.e_commerce_module.payment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.gson.Gson;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.flow_activities.EcomMainActivity;
import com.skilledanswers.picnart.e_commerce_module.place_order.PaymentStatusFragment;

import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place.EbsFormData;

public class EbsPaymentActivity extends AppCompatActivity {
    private EbsFormData formData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ebs_payment);

        isFromRecharge = getIntent().getBooleanExtra("IS_FROM_RECHARGE", false);

        formData = getIntent().getParcelableExtra("FORM_DATA");

        final WebView webView = findViewById(R.id.webView_ebs);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);

        if (Build.VERSION.SDK_INT >= 21) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

        String ebsFormData;

        ebsFormData = "account_id=" + formData.getAccountId() + "&" +
                "address=" + formData.getAddress() + "&" +
                "amount=" + formData.getAmount() + "&" +
                "channel=" + formData.getChannel() + "&" +
                "city=" + formData.getCity() + "&" +
                "country=" + formData.getCountry() + "&" +
                "currency=" + formData.getCurrency() + "&" +
                "description=" + formData.getDescription() + "&" +
                "display_currency=" + formData.getDisplayCurrency() + "&" +
                "email=" + formData.getEmail() + "&" +
                "mode=" + formData.getMode() + "&" +
                "name=" + formData.getName() + "&" +
                "phone=" + formData.getPhone() + "&" +
                "postal_code=" + formData.getPostalCode() + "&" +
                "reference_no=" + formData.getReferenceNo() + "&" +
                "return_url=" + formData.getReturnUrl() + "&" +
                "state=" + formData.getState() + "&" +
                "payment_option=" + formData.getPaymentOption() + "&" +
                "secure_hash=" + formData.getSecureHash();

        webView.setWebViewClient(new WebViewClient());

        System.out.println("----------PostData before: " + ebsFormData);

        webView.postUrl(CommonUtils.PAYMENT_GATEWAY_URL, ebsFormData.getBytes());

        webView.addJavascriptInterface(new EbsPaymentActivity.MyJavaScriptInterface(this), "Android");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                showProgressDialog();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                dismissProgressDialog();
                view.loadUrl("javascript:HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");

            }

        });
    }

    public class MyJavaScriptInterface {
        Context mContext;

        /**
         * Instantiate the interface and set the context
         */
        MyJavaScriptInterface(Context c) {
            mContext = c;
        }

        /**
         * Show a toast from the web page
         */
        @JavascriptInterface
        public void getPaymentResponse(String jsonResponse) {
            System.out.println("ddddddddd-----getPaymentResponse------------" + jsonResponse);
            String reg1 = "\"[\"";
            String reg2 = "\"]\"";

            String paymentData = jsonResponse.replace(reg1, "[\"");
            paymentData = paymentData.replace(reg2, "\"]");

            Fragment fragment;
            Bundle bundle = new Bundle();

            PaymentGatewayResponse paymentGatewayResponse = new Gson().fromJson(paymentData, PaymentGatewayResponse.class);

            System.out.println("ddddddddd-----paymentGatewayData------------" + new Gson().toJson(paymentGatewayResponse));

            boolean status = paymentGatewayResponse.getStatus();
            String msg = paymentGatewayResponse.getMessage();
            String paymentStatus = paymentGatewayResponse.getPaymentStatus();
            String transactionNum = paymentGatewayResponse.getTransactionNo();
            String orderNum = String.valueOf(paymentGatewayResponse.getOrderNo());

            double amt = 0;
            double pv = 0;
            for (int i = 0; i < paymentGatewayResponse.getProductDetails().getResult().size(); i++) {
                amt += Double.parseDouble(paymentGatewayResponse.getProductDetails().getResult().get(i).getPrice());
                pv += Double.parseDouble(paymentGatewayResponse.getProductDetails().getResult().get(i).getPv());
            }

            fragment = new PaymentStatusFragment();

            bundle.putString("MESSAGE", msg);
            bundle.putString("TRANS_NUM", transactionNum);
            bundle.putString("ORDER_NUM", orderNum);
            bundle.putString("PAYMENT_STATUS", paymentStatus);
            bundle.putString("AMT", String.valueOf(amt));
            bundle.putString("CBP", String.valueOf(pv));
            bundle.putBoolean("STATUS", status);
            bundle.putBoolean("IS_FROM_RECHARGE", isFromRecharge);

            //dismissProgressDialog();

            fragment.setArguments(bundle);
            moveToFragment(fragment, "PAY_STATUS_FRAG");

        }

    }

    private ProgressDialog pDialog;

    private void showProgressDialog() {
        if (pDialog == null) {
            pDialog = new ProgressDialog(EbsPaymentActivity.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);

        }
        pDialog.show();
    }

    private void dismissProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    private boolean isFromRecharge;

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setTitle("")
                .setMessage("Pressing back will cancel payment and closes all previous activities")
                .setCancelable(true)
                .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent;

                        intent = new Intent(EbsPaymentActivity.this, EcomMainActivity.class);
                        EcomPrefUtil ecomPrefUtil = new EcomPrefUtil(EbsPaymentActivity.this);
                        ecomPrefUtil.setIsClosedFromPayment(true);

                        startActivity(intent);
                        finishAffinity();
                    }

                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }

    public void moveToFragment(Fragment toFragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            replaceFragment(toFragment);
            System.out.println("*** ALREADY there ****");
        } else {
            transaction.replace(R.id.ebs_view, toFragment, TAG);
            transaction.addToBackStack(null).commit();
        }

    }

    public void replaceFragment(Fragment frag) {
        FragmentManager manager = getSupportFragmentManager();
        if (manager != null) {
            FragmentTransaction t = manager.beginTransaction();
            Fragment currentFrag = manager.findFragmentById(R.id.relative);

            //Check if the new Fragment is the same
            //If it is, don't add to the back stack
            if (currentFrag != null && currentFrag.getClass().equals(frag.getClass())) {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            } else {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            }
        }
    }
}
