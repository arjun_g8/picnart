package bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;
import bluepaymax.skilledanswers.picnart.e_commerce_module.CommonUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.DialogUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retrofit_response_models.OrderHistoryResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderHistoryActivity extends AppCompatActivity {
    private List<OrderHistoryResponse.Order> orderHisList = new ArrayList<>();
    private RecyclerView orderHisRV;
    private ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Order History");

        loadingBar = findViewById(R.id.prog_bar_order_history);
        orderHisRV = findViewById(R.id.order_history_rv);
        orderHisRV.setLayoutManager(new LinearLayoutManager(OrderHistoryActivity.this));
        orderHisRV.setHasFixedSize(true);

        loadingBar.setVisibility(View.VISIBLE);
        orderHisRV.setVisibility(View.GONE);

        if (CommonUtils.isThereInternet(OrderHistoryActivity.this)) {
            getAllOrderHistory();
        } else {
            loadingBar.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(OrderHistoryActivity.this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getAllOrderHistory() {
        PrefUtil prefUtil = new PrefUtil(OrderHistoryActivity.this);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getOrderHistory("getOrderHistory", prefUtil.getToken()).enqueue(new Callback<OrderHistoryResponse>() {
            @Override
            public void onResponse(Call<OrderHistoryResponse> call, Response<OrderHistoryResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            if (response.body().getData().getOrders() != null) {
                                orderHisList = response.body().getData().getOrders();
                                if (orderHisList.size() > 0) {
                                    orderHisRV.setAdapter(new OrderHistoryAdapter(orderHisList));
                                    orderHisRV.getAdapter().notifyDataSetChanged();
                                } else
                                    Toast.makeText(OrderHistoryActivity.this, "No Order History", Toast.LENGTH_SHORT).show();
                            }
                            loadingBar.setVisibility(View.GONE);
                            orderHisRV.setVisibility(View.VISIBLE);
                        } else {
                            loadingBar.setVisibility(View.GONE);
                            orderHisRV.setVisibility(View.GONE);
                            if (!isFinishing())
                                DialogUtils.showReLoginPopUp(OrderHistoryActivity.this);
                        }
                    } else {
                        loadingBar.setVisibility(View.GONE);
                        orderHisRV.setVisibility(View.GONE);
                    }
                } else {
                    loadingBar.setVisibility(View.GONE);
                    orderHisRV.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<OrderHistoryResponse> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                orderHisRV.setVisibility(View.GONE);
            }
        });
    }

    private class OrderHistoryAdapter extends RecyclerView.Adapter {
        private List<OrderHistoryResponse.Order> orderHistoryList;

        OrderHistoryAdapter(List<OrderHistoryResponse.Order> orderHistoryList) {
            this.orderHistoryList = orderHistoryList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(OrderHistoryActivity.this).inflate(R.layout.order_history_list_item, parent, false);
            return new OrderHistoryHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            OrderHistoryHolder orderHistoryHolder = (OrderHistoryHolder) holder;
            orderHistoryHolder.orderNumTV.setText(orderHistoryList.get(position).getOrderNo());
            orderHistoryHolder.orderDateTV.setText(orderHistoryList.get(position).getOrderDate());
            orderHistoryHolder.invoiceNumTV.setText(orderHistoryList.get(position).getInvoiceNo());
            orderHistoryHolder.totalAmtTV.setText("Rs. " + orderHistoryList.get(position).getTotalAmount());
            orderHistoryHolder.pvTV.setText("" + orderHistoryList.get(position).getPv());
            orderHistoryHolder.qtyTV.setText(orderHistoryList.get(position).getProductQuantity());
            if (orderHistoryList.get(position).getOrderStatus().equalsIgnoreCase("success"))
                orderHistoryHolder.statusTV.setTextColor(getResources().getColor(R.color.dark_green));
            else
                orderHistoryHolder.statusTV.setTextColor(getResources().getColor(R.color.dark_red));

            orderHistoryHolder.statusTV.setText(orderHistoryList.get(position).getOrderStatus());

        }

        @Override
        public int getItemCount() {
            return orderHistoryList.size();
        }
    }

    private class OrderHistoryHolder extends RecyclerView.ViewHolder {
        AppCompatTextView orderNumTV, orderDateTV, invoiceNumTV, totalAmtTV,
                pvTV, qtyTV, statusTV;

        OrderHistoryHolder(View itemView) {
            super(itemView);
            orderNumTV = itemView.findViewById(R.id.order_num_tv);
            orderDateTV = itemView.findViewById(R.id.order_date_tv_list_item);
            invoiceNumTV = itemView.findViewById(R.id.invoice_num_tv);
            totalAmtTV = itemView.findViewById(R.id.total_amt_tv);
            pvTV = itemView.findViewById(R.id.pv_tv_order_history);
            qtyTV = itemView.findViewById(R.id.qty_tv_list_item);
            statusTV = itemView.findViewById(R.id.status_tv_list_item);
        }
    }
}
