package com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.ShippingAndBillingAddressData;

import java.util.List;

public class GetDeliveryAddressResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<ShippingAndBillingAddressData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<ShippingAndBillingAddressData> getData() {
        return data;
    }

    public void setData(List<ShippingAndBillingAddressData> data) {
        this.data = data;
    }

}
