package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingAddressData  implements Parcelable{

    transient private boolean isSelected;

    public ShippingAddressData() {
    }

    private ShippingAddressData(Parcel in) {
        shippingId = in.readString();
        id = in.readString();
        regNo = in.readString();
        name = in.readString();
        address = in.readString();
        city = in.readString();
        state = in.readString();
        pin = in.readString();
        landmark = in.readString();
        mobile = in.readString();
        created = in.readString();
        updated = in.readString();
        district = in.readString();
        company = in.readString();
        email = in.readString();
    }

    public static final Creator<ShippingAddressData> CREATOR = new Creator<ShippingAddressData>() {
        @Override
        public ShippingAddressData createFromParcel(Parcel in) {
            return new ShippingAddressData(in);
        }

        @Override
        public ShippingAddressData[] newArray(int size) {
            return new ShippingAddressData[size];
        }
    };

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @SerializedName("shippingId")
    @Expose
    private String shippingId;

    public String getShippingId() {
        return shippingId;
    }

    public void setShippingId(String shippingId) {
        this.shippingId = shippingId;
    }

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("regNo")
    @Expose
    private String regNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("pin")
    @Expose
    private String pin;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;

    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("email")
    @Expose
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shippingId);
        dest.writeString(id);
        dest.writeString(regNo);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(state);
        dest.writeString(pin);
        dest.writeString(landmark);
        dest.writeString(mobile);
        dest.writeString(created);
        dest.writeString(updated);
        dest.writeString(district);
        dest.writeString(company);
        dest.writeString(email);
    }
}