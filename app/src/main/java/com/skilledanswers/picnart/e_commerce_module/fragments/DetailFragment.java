package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.SectionsPagerAdapter;

import java.util.ArrayList;

public class DetailFragment extends CustomFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    EcommerceCustomActivity activity;
    private OnFragmentInteractionListener mListener;

    public DetailFragment() {
        activity = (EcommerceCustomActivity) getActivity();
    }

    // TODO: Rename and change types and number of parameters
    public static DetailFragment newInstance() {

        DetailFragment fragment = new DetailFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_detail, container, false);
        activity = (EcommerceCustomActivity) getActivity();
        activity.setToggle(false);
        setHasOptionsMenu(true);
        Bundle bundleForOverViewFragment = null;
        if (getArguments() != null) {
           /* if (getArguments().getString("PRODUCT_ID") != null)
                System.out.println("--- FROM details: " + getArguments().getString("PRODUCT_ID"));*/
            bundleForOverViewFragment = getArguments();
        }

        try {
            final ViewPager viewPager = view.findViewById(R.id.view_pager);
            viewPager.setCurrentItem(0);
            ArrayList<Fragment> fragments = new ArrayList<>();
            Fragment fragment = new OverviewFragment();
            fragment.setArguments(bundleForOverViewFragment);
            fragments.add(fragment);
            // fragments.add(new Compare());
            Fragment featureFragment = new FeatureFragment();
            featureFragment.setArguments(bundleForOverViewFragment);
            fragments.add(featureFragment);
            // fragments.add(new ReviewFragment());
            TabLayout tabLayout = view.findViewById(R.id.tabLayout);
            SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager(), fragments);
            viewPager.setAdapter(sectionsPagerAdapter);
            tabLayout.setupWithViewPager(viewPager);

            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    viewPager.setCurrentItem(tab.getPosition());
                    /*if (tab.getPosition() == 1) {
                        viewPager.setClipToPadding(false);
                        viewPager.setPadding(16, 0, 16, 0);
                        viewPager.setPageMargin(8);
                        *//*viewPager.setPageTransformer(false, new ViewPager.PageTransformer() {
                            @Override public void transformPage(View page, float position) {
                                if (viewPager.getCurrentItem() == 1) {
                                    page.setTranslationX(-120);
                                }
                            }
                        });*//*
                    }*/
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("---- " + e.toString());
        }


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.e("ON ATTACH", "");
        if (context instanceof OnFragmentInteractionListener) {

            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        activity = (EcommerceCustomActivity) getActivity();

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
         menu.findItem(R.id.action_search).setVisible(true);
        //menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("");

        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

        //activity.hideToolBarLogo();
        Log.e("ON RESUME", "");
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
