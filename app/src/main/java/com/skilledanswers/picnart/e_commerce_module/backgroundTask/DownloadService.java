package com.skilledanswers.picnart.e_commerce_module.backgroundTask;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadService extends IntentService {


    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private static final String TAG = "DownloadService";
    SharedPreferences sharedPreferences;


    public DownloadService() {
        super(DownloadService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e(TAG, "Service Started!");

       /* final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        String url = intent.getStringExtra("url");
        HashMap<String, String> action = (HashMap) intent.getSerializableExtra("action");

        Bundle bundle = new Bundle();
        ConnectionDetector cd = new ConnectionDetector(this);
        if (!TextUtils.isEmpty(url)) {
            *//* Update UI: Download Service is Running *//*
            try {

                if (!cd.isConnectingToInternet()) {

                    receiver.send(STATUS_ERROR, Bundle.EMPTY);

                    this.stopSelf();


                } else {
                    receiver.send(STATUS_RUNNING, Bundle.EMPTY);
                    String results = performPostCall(url, action);
                    bundle.putString("result", results);
                    receiver.send(STATUS_FINISHED, bundle);
                }


                *//* Sending result back to activity *//*
         *//*if (null != results && results.length > 0) {
                    bundle.putStringArray("result", results);
                    receiver.send(STATUS_FINISHED, bundle);
                }*//*
            } catch (Exception e) {

                *//* Sending error message back to activity *//*
                bundle.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, bundle);
            }
        }
        Log.e(TAG, "Service Stopping!");
        this.stopSelf();*/
    }

  /*  public String performPostCall(String requestURL,
                                  HashMap<String, String> postDataParams) {
        sharedPreferences = getSharedPreferences("cookie", 0);
        boolean check_cookie = false;
        URL url;
        String response = "";
        HttpURLConnection conn = null;
        String cookieValue = null;
        String[] fields;
        SessionManager sessionManager = new SessionManager(getApplication());

        try {


            url = new URL(requestURL);

            //HttpURLConnection conn = Config.getConnection(url);
            conn = (HttpURLConnection) url.openConnection();
            //  getCookie(conn);

            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            String coo = sharedPreferences.getString("cookieValue", null);
            if (coo != sessionManager.getKeyCookie()) {
                conn.setRequestProperty("Cookie", sessionManager.getKeyCookie());
            }


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                Map<String, List<String>> headerFields = conn.getHeaderFields();

                Set<String> headerFieldsSet = headerFields.keySet();
                Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();

                while (hearerFieldsIter.hasNext()) {

                    String headerFieldKey = hearerFieldsIter.next();

                    if ("Set-Cookie".equalsIgnoreCase(headerFieldKey)) {

                        List<String> headerFieldValue = headerFields.get(headerFieldKey);

                        for (String headerValue : headerFieldValue) {


                            fields = headerValue.split(";/s*");

                            cookieValue = fields[0];
                        }

                    }

                }
                if (sessionManager.getKeyCookie() == null) {
                    sessionManager.setCookies(cookieValue);
                }


                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }

            } else {
                response = "Server Not Responding";
                // throw new HttpException(responseCode + "");
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            conn.disconnect();

        }

        Log.e("response ", response);
        return response;
    }*/

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        Log.e("url", result.toString());
        return result.toString();
    }

    public void getCookie(HttpURLConnection conn) {
        Map<String, List<String>> headerFields = conn.getHeaderFields();

        Set<String> headerFieldsSet = headerFields.keySet();
        Iterator<String> hearerFieldsIter = headerFieldsSet.iterator();

        while (hearerFieldsIter.hasNext()) {

            String headerFieldKey = hearerFieldsIter.next();

            if ("Set-Cookie".equalsIgnoreCase(headerFieldKey)) {

                List<String> headerFieldValue = headerFields.get(headerFieldKey);

                for (String headerValue : headerFieldValue) {

                    System.out.println("Cookie Found...");

                    String[] fields = headerValue.split(";/s*");

                    String cookieValue = fields[0];
                    String expires = null;
                    String path = null;
                    String domain = null;
                    boolean secure = false;

                    // Parse each field
                    for (int j = 1; j < fields.length; j++) {
                        if ("secure".equalsIgnoreCase(fields[j])) {
                            secure = true;
                        } else if (fields[j].indexOf('=') > 0) {
                            String[] f = fields[j].split("=");
                            if ("expires".equalsIgnoreCase(f[0])) {
                                expires = f[1];
                            } else if ("domain".equalsIgnoreCase(f[0])) {
                                domain = f[1];
                            } else if ("path".equalsIgnoreCase(f[0])) {
                                path = f[1];
                            }
                        }

                    }

                    System.out.println("cookieValue:" + cookieValue);
                    System.out.println("expires:" + expires);
                    System.out.println("path:" + path);
                    System.out.println("domain:" + domain);
                    System.out.println("secure:" + secure);

                    System.out.println("*****************************************");
                    sharedPreferences = getSharedPreferences("cookie", 0);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("cookieValue", cookieValue);
                    editor.apply();


                }

            }

        }
        conn.disconnect();

    }
}