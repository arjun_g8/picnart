package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by subh on 5/12/2016.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {

    ArrayList<Fragment> fragments;


    public SectionsPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        /*switch (position){
            case 0:return ((OverviewFragment) fragments.get(position)).newInstance();
            case 1:return ((Compare)fragments.get(position)).newInstance();
            case 1:return ((FeatureFragment)fragments.get(position)).newInstance();
            case 2:return ((ReviewFragment) fragments.get(position)).newInstance();
            default:return ((OverviewFragment) fragments.get(position)).newInstance();
        }*/
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Overview";
            case 1:
                return "Specifications";
            case 2:
                return "Compare";
            case 3:
                return "Review";
        }
        return null;
    }


}
