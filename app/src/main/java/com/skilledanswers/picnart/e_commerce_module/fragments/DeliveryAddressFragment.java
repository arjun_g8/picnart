package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.MyAddressAdapter;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.GetDeliveryAddressResponse;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.ShippingAndBillingAddressData;
import com.skilledanswers.picnart.e_commerce_module.place_order.OrderProceedForFragment;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;

import java.util.ArrayList;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.model.BillingDataPost;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.ShippingAddressData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 16-11-2017.
 */

public class DeliveryAddressFragment extends Fragment implements View.OnClickListener {
    private AppCompatTextView addAddressTV;
    private RecyclerView addressRV;
    private LinearLayout addressMainLayout;
    private EcomPrefUtil prefUtil;
    private PrefUtil appPrefUtil;
    private EcommerceCustomActivity activity;
    private List<ShippingAndBillingAddressData> allMyAddressListForDelivery = new ArrayList<>();
    private AppCompatTextView deliverHereButton;
    private Bundle bundle;
    private boolean isForOrder;
    private ShippingAddressData shippingAddress;
    private BillingDataPost billingAddress;
    private ProgressBar progressBar;
    private boolean isForPay;

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // menu.findItem(R.id.action_search).setVisible(true);
        //menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.delivery_address_fragment, container, false);
        setHasOptionsMenu(true);
        prefUtil = new EcomPrefUtil(getActivity());
        activity = (EcommerceCustomActivity) getActivity();
        appPrefUtil = new PrefUtil(activity);
        bundle = getArguments();
        progressBar = view.findViewById(R.id.progress_delivery_address);
        addressMainLayout = view.findViewById(R.id.addresses_main_layout);
        addAddressTV = view.findViewById(R.id.add_new_address_tv);
        addressRV = view.findViewById(R.id.address_rec_view);
        addressRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        addressRV.setHasFixedSize(true);

        // Drawable drawable = ContextCompat.getDrawable(activity, R.drawable.ic_plus);
        addAddressTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_plus, 0, 0, 0);
        // addAddressTV.setCompoundDrawables(drawable, null, null, null);

        addAddressTV.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goToNewAddressScreen();
                    }
                });

        deliverHereButton = view.findViewById(R.id.deliver_here_btn_in_delivery_address);
        deliverHereButton.setOnClickListener(this);

        addressMainLayout.setVisibility(View.GONE);
        deliverHereButton.setVisibility(View.GONE);

        if (CommonUtils.isThereInternet(getActivity())) {
            deliverHereButton.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            getAllMyAddressForDelivery();
        } else {
            progressBar.setVisibility(View.GONE);
            deliverHereButton.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(getActivity());
        }

        if (bundle != null) {
            if (bundle.getString("VIEW_TYPE") != null)
                if (bundle.getString("VIEW_TYPE", "").equalsIgnoreCase("View all")) {
                    //   deliverHereButton.setVisibility(View.GONE);
                }
            isForOrder = bundle.getBoolean("ORDER_PLACE", false);

            Log.e("IS ORDER: ", "" + isForOrder);
        }
       /* else
            deliverHereButton.setVisibility(View.VISIBLE);*/

       /* if (!isForOrder)
            addAddressTV.setVisibility(View.GONE);
        else*/
        addAddressTV.setVisibility(View.VISIBLE);

        return view;
    }

    private void goToNewAddressScreen() {
        System.out.println("------ GO TO ADDRESS");
        Bundle bundle = new Bundle();
        ShippingBillingAddressFragment myAddressEdit = new ShippingBillingAddressFragment();
        bundle.putString("ADDRESS_ACTION", "add_address");
        myAddressEdit.setArguments(bundle);
        activity.moveToFragment(myAddressEdit, "DELIVERY_ADD_ADDRESS_FRAGMENT");
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("Delivery Address");

        /*activity.hideToolBarLogo();
        activity.setAppToolBarTitle("Delivery Address");*/
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
        assert activity != null;
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("Delivery Address");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(false);
        //menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void getAllMyAddressForDelivery() {
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);

        apiInterface.getAllDeliveryAddress("getDeliveryAddress", appPrefUtil.getRegNo(), appPrefUtil.getToken()).enqueue(new Callback<GetDeliveryAddressResponse>() {
            @Override
            public void onResponse(Call<GetDeliveryAddressResponse> call, Response<GetDeliveryAddressResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        allMyAddressListForDelivery.clear();
                        allMyAddressListForDelivery.addAll(response.body().getData());

                        selectedShippingAddressID = allMyAddressListForDelivery.get(0).getId();

                        System.out.println("----------- ALL DELIVERY ADDRESS LIST SIZE: " + allMyAddressListForDelivery.size());

                        if (allMyAddressListForDelivery.size() > 0) {
                            addressRV.setAdapter(new MyAddressAdapter(allMyAddressListForDelivery, activity, isForOrder, new MyAddressAdapter.OnAddressSelected() {
                                @Override
                                public void setAddressData(ShippingAddressData shippingAddressData, BillingDataPost billingDataPost) {
                                    shippingAddress = shippingAddressData;
                                    billingAddress = billingDataPost;
                                }
                            }));
                            if (isForOrder)
                                deliverHereButton.setVisibility(View.VISIBLE);
                            else
                                deliverHereButton.setVisibility(View.GONE);
                            addressMainLayout.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                        } else {
                            Toast.makeText(activity, "Sorry There were no address found!", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                            addressMainLayout.setVisibility(View.VISIBLE);
                        }
                    } else {
                        progressBar.setVisibility(View.GONE);
                        addressMainLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetDeliveryAddressResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.deliver_here_btn_in_delivery_address:
                if (shippingAddress != null && billingAddress != null) {
                    if (CommonUtils.isThereInternet(activity))
                        //proceedToForPayment();
                        verifyMyOrders();
                    else
                        DialogUtils.showNoInternetDialog(activity);
                } else
                    Toast.makeText(activity, "Please Select one Address", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void verifyMyOrders() {
        Fragment fragment = new OrderProceedForFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("SHIPPING_DATA", shippingAddress);
        bundle.putParcelable("BILLING_DATA", billingAddress);
        fragment.setArguments(bundle);

        activity.addFragment(fragment, "ORDER_VERIFY");
    }

    private int selectedShippingAddressID;

   /* @Override
    public void onChangeAddress(int addressID) {
        selectedShippingAddressID = addressID;
        Log.e("SELECTED", "Shipping Address")
    }*/
}
