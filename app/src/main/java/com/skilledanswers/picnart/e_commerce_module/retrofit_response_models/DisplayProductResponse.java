package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 02-01-2018.
 */

public class DisplayProductResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Attribute {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("attribute_id")
        @Expose
        private Integer attributeId;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("is_mandatory")
        @Expose
        private Integer isMandatory;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public Integer getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(Integer attributeId) {
            this.attributeId = attributeId;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getIsMandatory() {
            return isMandatory;
        }

        public void setIsMandatory(Integer isMandatory) {
            this.isMandatory = isMandatory;
        }

    }

    public class Data {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("categoryId")
        @Expose
        private Integer categoryId;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("isLive")
        @Expose
        private Integer isLive;
        @SerializedName("couponEnable")
        @Expose
        private Integer couponEnable;
        @SerializedName("adminId")
        @Expose
        private Integer adminId;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("rejection")
        @Expose
        private String rejection;
        @SerializedName("limitFlag")
        @Expose
        private Integer limitFlag;
        @SerializedName("cardFlag")
        @Expose
        private Integer cardFlag;
        @SerializedName("imageURL")
        @Expose
        private List<String> imageURL = null;
        @SerializedName("attributes")
        @Expose
        private List<Attribute> attributes = null;
        @SerializedName("info")
        @Expose
        private List<Info> info = null;
        @SerializedName("rating")
        @Expose
        private Rating rating;
        @SerializedName("reviewCount")
        @Expose
        private Integer reviewCount;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getIsLive() {
            return isLive;
        }

        public void setIsLive(Integer isLive) {
            this.isLive = isLive;
        }

        public Integer getCouponEnable() {
            return couponEnable;
        }

        public void setCouponEnable(Integer couponEnable) {
            this.couponEnable = couponEnable;
        }

        public Integer getAdminId() {
            return adminId;
        }

        public void setAdminId(Integer adminId) {
            this.adminId = adminId;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getRejection() {
            return rejection;
        }

        public void setRejection(String rejection) {
            this.rejection = rejection;
        }

        public Integer getLimitFlag() {
            return limitFlag;
        }

        public void setLimitFlag(Integer limitFlag) {
            this.limitFlag = limitFlag;
        }

        public Integer getCardFlag() {
            return cardFlag;
        }

        public void setCardFlag(Integer cardFlag) {
            this.cardFlag = cardFlag;
        }

        public List<String> getImageURL() {
            return imageURL;
        }

        public void setImageURL(List<String> imageURL) {
            this.imageURL = imageURL;
        }

        public List<Attribute> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<Attribute> attributes) {
            this.attributes = attributes;
        }

        public List<Info> getInfo() {
            return info;
        }

        public void setInfo(List<Info> info) {
            this.info = info;
        }

        public Rating getRating() {
            return rating;
        }

        public void setRating(Rating rating) {
            this.rating = rating;
        }

        public Integer getReviewCount() {
            return reviewCount;
        }

        public void setReviewCount(Integer reviewCount) {
            this.reviewCount = reviewCount;
        }

    }


    public class Info {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("sellerId")
        @Expose
        private Integer sellerId;
        @SerializedName("sku")
        @Expose
        private String sku;
        @SerializedName("productId")
        @Expose
        private Integer productId;
        @SerializedName("varriant")
        @Expose
        private String varriant;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("msp")
        @Expose
        private String msp;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("shippingCharge")
        @Expose
        private String shippingCharge;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;

        private boolean isSelected;

        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getSellerId() {
            return sellerId;
        }

        public void setSellerId(Integer sellerId) {
            this.sellerId = sellerId;
        }

        public String getSku() {
            return sku;
        }

        public void setSku(String sku) {
            this.sku = sku;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getVarriant() {
            return varriant;
        }

        public void setVarriant(String varriant) {
            this.varriant = varriant;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public String getMsp() {
            return msp;
        }

        public void setMsp(String msp) {
            this.msp = msp;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getShippingCharge() {
            return shippingCharge;
        }

        public void setShippingCharge(String shippingCharge) {
            this.shippingCharge = shippingCharge;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

    }

    public class Rating {

        @SerializedName("rate")
        @Expose
        private String rate;
        @SerializedName("count")
        @Expose
        private Integer count;
        @SerializedName("avg")
        @Expose
        private Double avg;
        @SerializedName("totalvote")
        @Expose
        private Integer totalvote;

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public Integer getCount() {
            return count;
        }

        public void setCount(Integer count) {
            this.count = count;
        }

        public Double getAvg() {
            return avg;
        }

        public void setAvg(Double avg) {
            this.avg = avg;
        }

        public Integer getTotalvote() {
            return totalvote;
        }

        public void setTotalvote(Integer totalvote) {
            this.totalvote = totalvote;
        }
    }
}