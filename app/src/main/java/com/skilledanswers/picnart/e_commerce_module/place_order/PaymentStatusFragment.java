package com.skilledanswers.picnart.e_commerce_module.place_order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities.EcomMainActivity;

/**
 * Created by SkilledAnswers-D1 on 12-01-2018.
 */

public class PaymentStatusFragment extends Fragment implements View.OnClickListener {
    private Bundle transactionBundle;
    private boolean isFromRecharge;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_status_frag, container, false);

        AppCompatTextView messageTV = view.findViewById(R.id.msg_tv_pay_frag);
        AppCompatTextView message1TV = view.findViewById(R.id.msg1_tv_pay_frag);
        AppCompatImageView statusIV = view.findViewById(R.id.status_iv_pay_frag);
        AppCompatTextView transIdTV = view.findViewById(R.id.trans_num_tv);
        AppCompatTextView orderIdTV = view.findViewById(R.id.order_id_tv_pay_frag);
        AppCompatTextView amtTV = view.findViewById(R.id.amt_tv_pay_frag);
        AppCompatTextView cbpTV = view.findViewById(R.id.pv_tv_pay_frag);

        view.findViewById(R.id.ok_btn_pay_frag).setOnClickListener(this);

        transactionBundle = getArguments();
        if (transactionBundle != null) {
            boolean status = transactionBundle.getBoolean("STATUS");
            isFromRecharge = transactionBundle.getBoolean("IS_FROM_RECHARGE");

            String msg = transactionBundle.getString("PAYMENT_STATUS");
            String transID = transactionBundle.getString("TRANS_NUM");
            String orderID = transactionBundle.getString("ORDER_NUM");

            String amt = getString(R.string.rs_symbol) + " " + transactionBundle.getString("AMT");
            String cbp = transactionBundle.getString("CBP");

            if (status) {
                statusIV.setImageResource(R.drawable.ic_success_check);
                if (!isFromRecharge) {
                    messageTV.setText("Thank you for shopping with Bluepaymax!");
                    message1TV.setText("Your order was successfully placed");
                } else {
                    messageTV.setText("Thank you for recharging with Bluepaymax!");
                    message1TV.setText("Your recharge successful");
                }

            } else {
                statusIV.setImageResource(R.drawable.ic_error);
                messageTV.setText("Your transaction was unsuccessful");

                if (!isFromRecharge) {
                    message1TV.setText("Sorry!! Failed to place your order");
                } else {
                    message1TV.setText("Sorry!! Failed to recharge now");
                }
            }
            transIdTV.setText("Transaction ID: " + transID);
            orderIdTV.setText("Order ID: " + orderID);
            amtTV.setText("Amount: " + amt);
            cbpTV.setText("CBP: " + cbp);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok_btn_pay_frag:
                //Toast.makeText(getActivity(), "Exit", Toast.LENGTH_SHORT).show();
                EcomPrefUtil ecomPrefUtil = new EcomPrefUtil(getActivity());
                ecomPrefUtil.setIsClosedFromPayment(false);
                Intent intent = new Intent(getActivity(), EcomMainActivity.class);
                startActivity(intent);
                getActivity().finishAffinity();
                break;
        }
    }
}
