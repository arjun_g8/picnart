package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.DisplayProductResponse;

import java.util.List;


public class VariantAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<DisplayProductResponse.Info> variants;
    private VariantSelectable variantSelectable;

    public VariantAdapter(Context context, List<DisplayProductResponse.Info> variants, VariantSelectable variantSelectable) {
        this.context = context;
        this.variants = variants;
        this.variantSelectable = variantSelectable;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.variant_item, parent, false);
        return new VariantHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        VariantHolder variantHolder = (VariantHolder) holder;
        final int pos = variantHolder.getAdapterPosition();

        variantHolder.nameTV.setText(variants.get(pos).getVarriant());
        variantHolder.variantItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearSelection();
                variants.get(pos).setSelected(true);
                notifyDataSetChanged();
                variantSelectable.onVariantSelected(variants.get(pos).getProductCode());
            }
        });

        if (variants.get(pos).isSelected()) {
            variantHolder.variantItem.setBackground(ContextCompat.getDrawable(context, R.drawable.dark_blue_rect_bg));
        } else {
            variantHolder.variantItem.setBackground(null);
        }
    }

    private void clearSelection() {
        for (DisplayProductResponse.Info variant : variants) {
            variant.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return variants.size();
    }

    private class VariantHolder extends RecyclerView.ViewHolder {
        private FrameLayout variantItem;
        private AppCompatTextView nameTV;

        private VariantHolder(View itemView) {
            super(itemView);
            variantItem = itemView.findViewById(R.id.variant_item);
            nameTV = itemView.findViewById(R.id.variant_type_tv);
        }
    }

    public interface VariantSelectable {
        void onVariantSelected(String productCode);
    }
}
