package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.VariantAdapter;
import com.skilledanswers.picnart.e_commerce_module.retofit.BasicAuthorization;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.AllReviewsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.DisplayProductResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.LoadCartItemsModel;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.SimilarProductsResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities.ProductViewActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.PreviewImgModel;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.WishListItemsResponseObject;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.skilledanswers.picnart.Utils.CommonUtilsEcommerce.formatToSingleDecimalFraction;

/**
 * A simple {@link Fragment} subclass.
 */
public class OverviewFragment extends CustomFragment implements View.OnClickListener {

    HashMap<String, ArrayList<String>> listMap;
    private AppCompatImageView productMainIV;
    private EcommerceCustomActivity activity;
    private View productOverView;
    private int previewImages[];
    private int curPrevPos = 0;
    private RecyclerView productImagesRV, reviewsRV, similarProductsRV;
    private boolean isChecked = false;
    private int selectedItemPos = 0;
    private ArrayList<PreviewImgModel> previewImgModelArrayList = new ArrayList<>();
    private List<DisplayProductResponse.Attribute> productAttributes = new ArrayList<>();
    private TextView prodNameTV;
    private String urlForProductImg, prodName;
    private int productId, productPrice, regId, productOriginalPrice;
    private DetailFragment.OnFragmentInteractionListener mListener;
    private PrefUtil prefUtil;
    AppCompatTextView addToCartTV;
    List<String> previewImagesStringList = new ArrayList<>(), productShortDescriptionList = new ArrayList<>();
    private List<DisplayProductResponse.Attribute> productAttributeList = new ArrayList<>();
    private List<SimilarProductsResponse.SimilarProductData> similarProductsList = new ArrayList<>();
    private AppCompatTextView productPriceTV, ramSizeTV, osTypeTV, productOriginalPriceTV,
            batteryTV, avgRatingsTV, totalRatingReviewTV, CBPTV, aboutTV, shippingChargeTV, availMsgTV;
    private AppCompatImageButton wishListBtn;
    private AppCompatEditText pincodeEd;
    private TextInputLayout pincodeTxtIp;
    private List<WishListItemsResponseObject.WishListDataGet> wishListItems = new ArrayList<>();
    private List<LoadCartItemsModel.CartItemData> cartItemsList = new ArrayList<>();
    private boolean isAddedToWishList, isAddedToCart;
    private int count, prodImg, CATEGORY_ID;
    private boolean isForDummy;
    private Bundle bundleForNew, bundle;
    private String name, avgRating, totalRatingAndReview, prodCode, catIdForOverview;
    private EcomPrefUtil ecomPrefUtil;
    private List<AllReviewsResponse.ReviewData> productReviewList = new ArrayList<>();
    private CardView quickDescriptionCV, checkAvailabiltyCV;
    private NestedScrollView mainScrollView;
    private ProgressBar loadingBar, pinCheckProgressBar;
    private LinearLayout bottomViewForButtons;
    private CoordinatorLayout ratingReviewView;
    private AppCompatButton goPincodeBtn;
    private boolean isAvailableAtLocation = false;
    private boolean isGrocery;
    private boolean isAttachedToWindow = true;
    private String savedPincode;
//    private String[] internalSpaceSplit,ramSplit,batterySplit;

    public OverviewFragment() {
        // Required empty public constructor
        activity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        productOverView = inflater.inflate(R.layout.fragment_overview, container, false);
        activity = (EcommerceCustomActivity) getActivity();
        ecomPrefUtil = new EcomPrefUtil(activity);

        addToCartTV = productOverView.findViewById(R.id.rent_now);

        if (ecomPrefUtil.getSavedPincode() != null) {
            savedPincode = ecomPrefUtil.getSavedPincode();
        }

        variantsCV = productOverView.findViewById(R.id.variant_cv);
        variantsCV.setVisibility(View.GONE);

        checkAvailabiltyCV = productOverView.findViewById(R.id.check_availablity_cv);
        pincodeEd = productOverView.findViewById(R.id.pincode_ed_overview);
        pincodeEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String pinCode = charSequence.toString();
                pincodeTxtIp.setErrorEnabled(false);
               /* if (pinCode.length() == 6) {
                    checkIfAvailableAtYourLocation(pinCode);
                }*/
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String pincodeEntered = editable.toString();
                if (pincodeEntered.equals("")) {
                    requestFocus(pincodeEd);
                    pincodeTxtIp.setErrorEnabled(true);
                    pincodeTxtIp.setError("Enter Pincode");

                } else if (pincodeEntered.length() < 6) {
                    requestFocus(pincodeEd);
                    pincodeTxtIp.setErrorEnabled(true);
                    pincodeTxtIp.setError("Pincode must be atleast 6 digits");
                } else {
                    if (CommonUtils.isThereInternet(activity)) {
                        productOverView.findViewById(R.id.check_availbilty_btn).setVisibility(View.GONE);
                        pinCheckProgressBar.setVisibility(View.VISIBLE);
                        checkIfAvailableAtYourLocation(pincodeEntered);

                    } else
                        DialogUtils.showNoInternetDialog(activity);
                }
            }
        });
        pincodeTxtIp = productOverView.findViewById(R.id.pincode_txt_ip_overview);
        goPincodeBtn = productOverView.findViewById(R.id.check_availbilty_btn);
        goPincodeBtn.setOnClickListener(this);
        pinCheckProgressBar = productOverView.findViewById(R.id.pincode_check_p_bar);
        pinCheckProgressBar.setVisibility(View.GONE);

        availMsgTV = productOverView.findViewById(R.id.avail_msg_tv);
        availMsgTV.setVisibility(View.GONE);

        mainScrollView = productOverView.findViewById(R.id.main_overview_scroll_id);
        loadingBar = productOverView.findViewById(R.id.loading_bar_overview_fragment);
        bottomViewForButtons = productOverView.findViewById(R.id.bottom_view_overview_id);

        ratingReviewView = productOverView.findViewById(R.id.rating_review_view);

        productOverView.findViewById(R.id.full_details_tv).setOnClickListener(this);

        mainScrollView.setVisibility(View.GONE);
        bottomViewForButtons.setVisibility(View.GONE);
        loadingBar.setVisibility(View.VISIBLE);

        similarProductsRV = productOverView.findViewById(R.id.similar_products_rv);
        similarProductsRV.setLayoutManager(new GridLayoutManager(activity, 2));
        similarProductsRV.setNestedScrollingEnabled(false);
        similarProductsRV.setHasFixedSize(true);

        if (getArguments() != null) {
            if (getArguments().getString("PRODUCT_CODE") != null) {
                prodCode = getArguments().getString("PRODUCT_CODE");
                catIdForOverview = String.valueOf(getArguments().getInt("CAT_ID_FOR_OVERVIEW"));

                isGrocery = getArguments().getBoolean("IS_GROCERY", false);
                if (catIdForOverview.equals("41") || isGrocery) {
                    ratingReviewView.setVisibility(View.GONE);
                    checkAvailabiltyCV.setVisibility(View.VISIBLE);
                } else {
                    checkAvailabiltyCV.setVisibility(View.GONE);
                }

                System.out.println("------------- PRODUCT CODE IN OVERVIEW: " + prodCode);
                System.out.println("------------- CAT ID IN OVERVIEW: " + catIdForOverview);
            }
        }

        activity.setToggle(false);
        setHasOptionsMenu(true);
        prefUtil = new PrefUtil(activity);
        System.out.println("--- ON CREATE");

        reviewsRV = productOverView.findViewById(R.id.reviews_rv);
        reviewsRV.setLayoutManager(new LinearLayoutManager(activity));
        reviewsRV.setNestedScrollingEnabled(false);
        reviewsRV.setHasFixedSize(true);

        performThisWhileStart();
        return productOverView;
    }

    @Override
    public void onDestroy() {
        isAttachedToWindow = false;
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        isAttachedToWindow = false;
        super.onDetach();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // menu.clear();
    }

    int catID;

    private CardView variantsCV;

    private void setUCBPariantView(List<DisplayProductResponse.Info> variants) {
        RecyclerView variantRV = productOverView.findViewById(R.id.variants_rv);
        variantRV.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        variantRV.setHasFixedSize(true);

        variantsCV.setVisibility(View.VISIBLE);
        variantRV.setVisibility(View.VISIBLE);

        variantRV.setAdapter(new VariantAdapter(activity, variants, new VariantAdapter.VariantSelectable() {
            @Override
            public void onVariantSelected(String productCode) {
                prodCode = productCode;
                fetchProductCompleteInfo();
            }
        }));
    }

    private void fetchProductCompleteInfo() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getProductDetailsUsingCode("loadDisplayProduct", prodCode).enqueue(new Callback<DisplayProductResponse>() {
            @Override
            public void onResponse(Call<DisplayProductResponse> call, Response<DisplayProductResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        int catId = response.body().getData().getCategoryId();
                        catID = catId;
                        prodName = response.body().getData().getTitle();
                        productId = response.body().getData().getId();

                        getAllReviewsForTheProduct();

                        System.out.println("-----------CATEGORY ID OVERVIEW: " + catID);

                        System.out.println("------------OVERVIEW RESPONSE PROD CODE: " + prodCode);

                        previewImagesStringList.clear();
                        previewImagesStringList = response.body().getData().getImageURL();
                        productAttributeList.clear();
                        productAttributeList = response.body().getData().getAttributes();
                        previewImgModelArrayList.clear();
                        for (int i = 0; i < previewImagesStringList.size(); i++) {
                            if (i == 0)
                                previewImgModelArrayList.add(new PreviewImgModel(previewImagesStringList.get(i), true));
                            else
                                previewImgModelArrayList.add(new PreviewImgModel(previewImagesStringList.get(i), false));
                        }

//                        prodCode = response.body().getData().getInfo().get(0).getProductCode();

                        productShortDescriptionList.clear();

                        for (int i = 0; i < productAttributeList.size(); i++) {
                            if (productAttributeList.get(i).getName().equalsIgnoreCase("Internal Memory"))
                                productShortDescriptionList.add("Internal Storage_" + productAttributeList.get(i).getValue());
                            if (productAttributeList.get(i).getName().equalsIgnoreCase("RAM"))
                                productShortDescriptionList.add("RAM_" + productAttributeList.get(i).getValue());
                            if (productAttributeList.get(i).getName().equalsIgnoreCase("Battery Capacity"))
                                productShortDescriptionList.add("Battery Capacity_" + productAttributeList.get(i).getValue());
                        }

                        DisplayProductResponse.Info currentVariant = response.body().getData().getInfo().get(0);

                        if (response.body().getData().getInfo() != null) {
                            if (response.body().getData().getInfo().size() > 0) {
                                List<DisplayProductResponse.Info> variants = new ArrayList<>();
                                variants.addAll(response.body().getData().getInfo());
                                for (DisplayProductResponse.Info variant : variants) {
                                    if (variant.getProductCode().equalsIgnoreCase(prodCode)) {
                                        variant.setSelected(true);
                                        currentVariant = variant;
                                        break;
                                    } else {
                                        variant.setSelected(false);
                                    }
                                }
                                setUCBPariantView(variants);
                            } else {
                                variantsCV.setVisibility(View.GONE);
                            }
                        } else
                            variantsCV.setVisibility(View.GONE);

                        String mrp = currentVariant.getMrp().replace(".00", "");
                        String msp = currentVariant.getMsp().replace(".00", "");
                        productPrice = Integer.parseInt(msp);
                        productOriginalPrice = Integer.parseInt(mrp);

                        String reviewCount = String.valueOf(response.body().getData().getReviewCount());
                        String ratingsTotal = String.valueOf(response.body().getData().getRating().getCount());

                        if (response.body().getData().getRating().getAvg() != null)
                            avgRating = formatToSingleDecimalFraction(String.valueOf(response.body().getData().getRating().getAvg()));
                        else
                            avgRating = "0";

                        totalRatingAndReview = ratingsTotal + " ratings and " + reviewCount + " reviews";

                        System.out.println("------------ RESPONSE PROD IMG SIZE: " + previewImagesStringList.size());

                        initProductOverView();

                        AppCompatTextView outOfStockTV = productOverView.findViewById(R.id.out_of_stock_tv);

                        if (currentVariant.getQuantity() == 0) {
                            outOfStockTV.setVisibility(View.VISIBLE);
                        } else {
                            outOfStockTV.setVisibility(View.GONE);
                        }

                        String productBrand = response.body().getData().getBrand();
                        String productName = response.body().getData().getTitle();

                        if (response.body().getData().getInfo().size() > 1) {
                            variantsCV.setVisibility(View.VISIBLE);
                            prodNameTV.setText(productBrand + " " + productName);
                        } else {
                            prodNameTV.setText(productBrand + " " + productName + "\n" + currentVariant.getVarriant());
                            variantsCV.setVisibility(View.GONE);
                        }
                        if (savedPincode != null) {
                            pincodeEd.setText(savedPincode);
                            pincodeEd.setSelection(pincodeEd.getText().toString().length());
                            checkIfAvailableAtYourLocation(savedPincode);
                        }

                        quickDescriptionCV = productOverView.findViewById(R.id.quick_desc_cv_overview);

                        if (catId == 14) {
                            quickDescriptionCV.setVisibility(View.VISIBLE);
                        } else
                            quickDescriptionCV.setVisibility(View.GONE);

                        shippingChargeTV.setText("Rs. " + currentVariant.getShippingCharge());

                        aboutTV.setText(response.body().getData().getDescription());

                        CBPTV.setText("CBP: " + currentVariant.getPv());

                        loadAllSimilarProducts();

                        loadingBar.setVisibility(View.GONE);
                        mainScrollView.setVisibility(View.VISIBLE);
                        bottomViewForButtons.setVisibility(View.VISIBLE);

                        // initOverView(prodName, productPrice, productOriginalPrice);
                    } else {
                        System.out.println("-------- IN overview: " + response.body().getStatus());
                    }
                } else {
                    System.out.println("-------- IN overview: RESPONSE ERROR");
                }
            }

            @Override
            public void onFailure(Call<DisplayProductResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("-----------  IN overview:" + t.toString());
            }
        });
    }


    private void performThisWhileStart() {
        activity = (EcommerceCustomActivity) getActivity();
        prefUtil = new PrefUtil(activity);

        if (getArguments() != null) {
            if (getArguments().getString("PRODUCT_ID") != null)
                System.out.println("--------- FROM DEtails to overview: " + getArguments().getString("PRODUCT_ID"));
            bundle = getArguments();

            if (bundle.getString("FOR") != null) {
                if (bundle.getString("FOR").equalsIgnoreCase("DUMMY")) {
                    productId = bundle.getInt("CATEGORY_ID");
                    productPrice = bundle.getInt("PRODUCT_PRICE");
                    prodImg = bundle.getInt("DUMMY_IMG");
                    // prodName = bundle.getString("PRODUCT_NAME");
                    productOriginalPrice = bundle.getInt("PRODUCT_ORIGINAL_PRICE");
                    isForDummy = true;
                    System.out.println("----------DUMMY SET");
                    initProductOverView();
                } else {
                    if (bundle.getString("PRODUCT_ID") != null)
                        productId = Integer.parseInt(bundle.getString("PRODUCT_ID"));
                    prodCode = bundle.getString("PRODUCT_CODE");
                    isForDummy = false;
                    if (CommonUtils.isThereInternet(activity)) {
                        loadAllCartItems();
                        fetchProductCompleteInfo();
                    }

                    System.out.println("------------ ppp PROD NAME: " + prodName);
                    //  prodNameTV.setText(prodName);
                }
            }

            System.out.println("---- IMAGES: " + previewImagesStringList.size());

            System.out.println("----------- IS ADDED TO CART: " + isAddedToCart);
        }
    }

    private void loadAllSimilarProducts() {
        System.out.println("------------- OVERVIEW CATEGORY ID RELATED PRODUCTS: " + catID);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getAllRelatedProducts("loadRelatedProducts", String.valueOf(catID)).enqueue(new Callback<SimilarProductsResponse>() {
            @Override
            public void onResponse(Call<SimilarProductsResponse> call, Response<SimilarProductsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        similarProductsList.clear();
                        similarProductsList = response.body().getData();

                        if (similarProductsList.size() > 0) {
                            similarProductsRV.setAdapter(new SimilarProductsAdapter(similarProductsList));
                            similarProductsRV.getAdapter().notifyDataSetChanged();
                        }
                    } else {
                        Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<SimilarProductsResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        activity = (EcommerceCustomActivity) getActivity();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("");

        System.out.println("--------------------- ON RESUME OVERVIEW");

        hideSoftKeyBoard();

        if (CommonUtils.isThereInternet(activity))
            loadAllCartItems();

        //activity.setAppToolBarTitle(prodName);
    }

    private void hideSoftKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getActivity().getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Check whether if view there or not
         */
        if (focusedView != null) {
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
        isAttachedToWindow = true;
    }

    private void initProductOverView() {
        batteryTV = productOverView.findViewById(R.id.battery_cap_tv_overview);
        osTypeTV = productOverView.findViewById(R.id.os_type_tv_overview);
        ramSizeTV = productOverView.findViewById(R.id.ram_size_tv_overview);
        productPriceTV = productOverView.findViewById(R.id.overview_product_price_tv);
        avgRatingsTV = productOverView.findViewById(R.id.avg_rating_tv);
        totalRatingReviewTV = productOverView.findViewById(R.id.ratings_reviews_total_count_tv);
        productOriginalPriceTV = productOverView.findViewById(R.id.overview_product_original_price_tv);
        aboutTV = productOverView.findViewById(R.id.about_product_tv);
        CBPTV = productOverView.findViewById(R.id.overview_product_pv_tv);
        shippingChargeTV = productOverView.findViewById(R.id.shipping_charge_tv_overview);

        productOverView.findViewById(R.id.rate_product_btn).setOnClickListener(this);
        productOverView.findViewById(R.id.add_review_btn).setOnClickListener(this);

        avgRatingsTV.setText(avgRating);
        totalRatingReviewTV.setText(totalRatingAndReview);

        //  Drawable drawable = ContextCompat.getDrawable(getActivity(), R.drawable.ic_rupee_indian);
        productPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian, 0, 0, 0);

        wishListBtn = productOverView.findViewById(R.id.wishlist_img_btn_in_product_list);

        System.out.println("---- Is added to wish list: " + isAddedToWishList);

        if (productShortDescriptionList != null) {

            if (catID == 14) {
                try {
                    if (productShortDescriptionList.size() > 0) {
                        System.out.println("---product ttri size: " + productShortDescriptionList.size());
                        String[] internalSpaceSplit = productShortDescriptionList.get(1).split("_");
                        String[] ramSplit = productShortDescriptionList.get(0).split("_");
                        String[] batterySplit = productShortDescriptionList.get(2).split("_");

                        System.out.println("----- SHORT descr:  " + productShortDescriptionList.toString());

                        osTypeTV.setText(internalSpaceSplit[0] + ": " + internalSpaceSplit[1]);
                        ramSizeTV.setText(ramSplit[0] + ": " + ramSplit[1]);
                        batteryTV.setText(batterySplit[0] + ": " + batterySplit[1]);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        prodNameTV = productOverView.findViewById(R.id.product_name_tv_in_overview);

        if (isForDummy) {
            prodNameTV.setText(prodName);
        } else
            prodNameTV.setText(prodName);

        productImagesRV = productOverView.findViewById(R.id.item_previews_rec_view);
        productImagesRV.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, true));
        productImagesRV.setHasFixedSize(true);
        productImagesRV.setAdapter(new PreviewImgAdapter());
        productImagesRV.getAdapter().notifyDataSetChanged();

        productMainIV = productOverView.findViewById(R.id.product_main_iv);

        productMainIV.setOnClickListener(this);


        if (isForDummy) {
            productMainIV.setImageResource(prodImg);
        } else {
            if (previewImagesStringList != null)
                if (previewImagesStringList.size() > 0) {
                   /* if (previewImagesStringList.size() == 1)
                        urlForProductImg = previewImagesStringList.get(0);
                    else*/
                    urlForProductImg = previewImagesStringList.get(0);
                }

            if (urlForProductImg == null || urlForProductImg.equals("")) {
                urlForProductImg = "http://google.com/";
                productMainIV.setImageResource(R.drawable.ic_def_pic);
            } else {
                if (isAttachedToWindow) {
                    Glide.with(activity)
                            .load(new GlideUrl(urlForProductImg))
                            .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
//                        e.printStackTrace();
                                    //   System.out.println("---- glide ex: " + e.toString());
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .dontAnimate()
                            .into(productMainIV);
                }
            }
        }

        System.out.println("--------- URL IS: " + urlForProductImg);

        addToCartTV = productOverView.findViewById(R.id.rent_now);
        AppCompatTextView buy_now = productOverView.findViewById(R.id.buy_now);
        addToCartTV.setOnClickListener(this);
        buy_now.setOnClickListener(this);

        if (isForDummy) {
            System.out.println("-----------DUMMY");
            addToCartTV.setClickable(false);
            productOriginalPriceTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            productOriginalPriceTV.setText("");
        } else {
            System.out.println("------Product name: " + prodName);
            System.out.println("------Product msp: " + productPrice);
            System.out.println("------Product mrp: " + productOriginalPrice);
            if (productOriginalPrice == productPrice) {
                productOriginalPriceTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                productOriginalPriceTV.setText("");
            } else {
                productOriginalPriceTV.setPaintFlags(productOriginalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                productOriginalPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian_grey, 0, 0, 0);
                productOriginalPriceTV.setText("" + CommonUtils.formatPriceByComma(String.valueOf(productOriginalPrice)));
            }
        }
        productPriceTV.setText("" + CommonUtils.formatPriceByComma(String.valueOf(productPrice)));
//System.out.pri
        System.out.println("-------------- FROM DEtails to overview resume: " + productId);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listMap = new HashMap<>();
        ArrayList<String> list = new ArrayList<>();
        list.add("VIEW ALL");
        list.add("VIEW ALL");
        list.add("VIEW ALL");
        listMap.put("Relevant products", list);
        listMap.put("Accessories", list);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.product_main_iv:
                if (!isForDummy)
                    goToProductFullView();
                break;

            case R.id.rent_now:
                System.out.println("--- ADD to cart");
                if (CommonUtils.isThereInternet(activity)) {
                    if (addToCartTV.getText().toString().equalsIgnoreCase("Add to Cart")) {
                        if (catIdForOverview.equals("41") || isGrocery) {
                            if (ecomPrefUtil.getSavedPincode() != null) {
                                Log.e("PINCODE FROM", " SESSION: " + ecomPrefUtil.getSavedPincode());
                                pincodeEd.setText(ecomPrefUtil.getSavedPincode().trim());
                                pincodeEd.setSelection(pincodeEd.getText().toString().length());
                                Log.e("PRODUCT CODE", " : " + prodCode);
                                Log.e("PRODUCT ID", " : " + productId);
                                addCart(Integer.parseInt(prefUtil.getRegNo()), productId, 1, prodCode);
                            } else {
                                Toast.makeText(activity, "Please check availability at your location", Toast.LENGTH_SHORT).show();
                            }
                          /*  if (isAvailableAtLocation)
                                addCart(Integer.parseInt(prefUtil.getRegNo()), productId, 1, prodCode);
                            else
                                Toast.makeText(activity, "Please check availability at your location", Toast.LENGTH_SHORT).show();*/
                        } else {
                            addCart(Integer.parseInt(prefUtil.getRegNo()), productId, 1, prodCode);
                        }

                    } else {
                        MyCart myCart = new MyCart();
                     /*   bundle.putInt("CATEGORY_ID", bundle.getInt("CATEGORY_ID"));
                        myCart.setArguments(bundle);*/
                        if (bundle != null)
                            activity.moveToFragment(myCart, "MY_CART_FRAGMENT");
                    }
                } else {
                    DialogUtils.showNoInternetDialog(activity);
                }
                break;

            case R.id.buy_now:
                DeliveryAddressFragment myCart = new DeliveryAddressFragment();
                activity.moveToFragment(myCart, "DELIVERY_ADDRESS_FRAGMENT");
                break;

            case R.id.add_review_btn:
                showAddReviewPopUp(String.valueOf(productId));
                break;

            case R.id.rate_product_btn:
                showRatingDialog();
                break;
            case R.id.check_availbilty_btn:
                pincodeEd.setText("");

                /*String pincodeEntered = pincodeEd.getText().toString();
                if (pincodeEntered.equals("")) {
                    requestFocus(pincodeEd);
                    pincodeTxtIp.setErrorEnabled(true);
                    pincodeTxtIp.setError("Enter Pincode");

                } else if (pincodeEntered.length() < 6) {
                    requestFocus(pincodeEd);
                    pincodeTxtIp.setErrorEnabled(true);
                    pincodeTxtIp.setError("Pincode must be atleast 6 digits");
                } else {
                    if (CommonUtils.isThereInternet(activity)) {
                        productOverView.findViewById(R.id.check_availbilty_btn).setVisibility(View.GONE);
                        pinCheckProgressBar.setVisibility(View.VISIBLE);
                        checkIfAvailableAtYourLocation(pincodeEntered);

                    } else
                        DialogUtils.showNoInternetDialog(activity);
                }*/
                break;

            case R.id.full_details_tv:
                goToFullDetails();
                break;
        }
    }

    private void goToFullDetails() {
        Fragment fragment = new FeatureFragment();
        fragment.setArguments(bundle);
        activity.moveToFragment(fragment, "DETAIL_FRAGMENT");
    }

    private void requestFocus(View view) {
        view.requestFocus();
    }

    private void checkIfAvailableAtYourLocation(final String pinCode) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.isAvailableAtLocation("checkGroceryDeliverability", pinCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String availStringResponse = new String(response.body().bytes());
                            JSONObject availResponseObject = new JSONObject(availStringResponse);
                            boolean status = availResponseObject.getBoolean("status");
                            String msg = availResponseObject.getString("message");

                            isAvailableAtLocation = status;

                            pinCheckProgressBar.setVisibility(View.GONE);
                            goPincodeBtn.setVisibility(View.VISIBLE);

                            availMsgTV.setVisibility(View.VISIBLE);
                            availMsgTV.setText(msg);
                            if (isAvailableAtLocation) {
                                ecomPrefUtil.savePincodeForGrocey(pinCode);
                                availMsgTV.setTextColor(ContextCompat.getColor(activity, R.color.green));
                                goPincodeBtn.setText("Change");
                            } else {
                                availMsgTV.setTextColor(ContextCompat.getColor(activity, R.color.dark_red));
                                goPincodeBtn.setText("Clear");
                            }

                            // Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    hideSoftKeyBoard();

                } else {
                    pinCheckProgressBar.setVisibility(View.GONE);
                    goPincodeBtn.setVisibility(View.VISIBLE);
                    availMsgTV.setVisibility(View.GONE);
                    hideSoftKeyBoard();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                pinCheckProgressBar.setVisibility(View.GONE);
                goPincodeBtn.setVisibility(View.VISIBLE);
                hideSoftKeyBoard();
            }
        });
    }

    private void addRatingForTheProduct(String prodId, final String rating) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.saveRating("saveRating", prodId, prefUtil.getRegNo(), rating).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String ratingResponseString = new String(response.body().bytes());
                        JSONObject jsonObject = new JSONObject(ratingResponseString);
                        boolean status;
                        String msg;

                        status = jsonObject.getBoolean("status");
                        msg = jsonObject.getString("message");

                        if (status) {
                            Toast.makeText(activity, "Thanks For rating!", Toast.LENGTH_SHORT).show();
                            ratingDialog.dismiss();
                            ratingDialog.cancel();
                            fragmentTransition(new OverviewFragment(), bundleForNew);
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                            getCartCount();
                        } else
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                System.out.println("--------- " + t.toString());
            }
        });
    }

    private void addReview(String reviewText, String prodId) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.saveReview("addReview", prodId, prefUtil.getRegNo(), prefUtil.getToken(), reviewText).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String ratingResponseString = new String(response.body().bytes());
                        JSONObject jsonObject = new JSONObject(ratingResponseString);
                        boolean status;
                        String msg;

                        status = jsonObject.getBoolean("status");
                        msg = jsonObject.getString("message");

                        if (status) {
                            Toast.makeText(activity, "Thanks For reviewing!", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            dialog.cancel();
                            fragmentTransition(new OverviewFragment(), bundleForNew);
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                            getCartCount();
                        } else
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private Dialog dialog;
    private Dialog ratingDialog;

    private void showRatingDialog() {
        ratingDialog = new Dialog(activity);
        ratingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        ratingDialog.setContentView(R.layout.add_rating_popup);
        ratingDialog.setCancelable(true);

        AppCompatRatingBar ratingBar = ratingDialog.findViewById(R.id.rating_bar_overview);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                System.out.println("------------- RATING: " + v);
                addRatingForTheProduct(String.valueOf(productId), String.valueOf(v));
            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(ratingDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ratingDialog.show();
        ratingDialog.getWindow().setAttributes(lp);
    }


    private void showAddReviewPopUp(final String prodId) {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_review_dialog_layout);
        dialog.setCancelable(true);
        final AppCompatEditText reviewEd = dialog.findViewById(R.id.review_ed_dialog);
        final TextInputLayout reviewTxtIp = dialog.findViewById(R.id.review_txt_ip_dialog);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.findViewById(R.id.submit_review_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String review = reviewEd.getText().toString();
                if (TextUtils.isEmpty(review)) {
                    reviewTxtIp.setErrorEnabled(true);
                    reviewTxtIp.setError("Please add something for review");
                } else {
                    reviewTxtIp.setErrorEnabled(false);
                    if (CommonUtils.isThereInternet(activity))
                        addReview(review, prodId);
                    else
                        DialogUtils.showNoInternetDialog(activity);
                }
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }


    private void getCartCount() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getCartCount("countCartItems", prefUtil.getRegNo(), prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String cartCountResponseString = new String(response.body().bytes());
                        System.out.println("--------------- CART COUNT: " + cartCountResponseString);
                        JSONObject cartCountJsonObject = new JSONObject(cartCountResponseString);
                        boolean status = cartCountJsonObject.getBoolean("status");

                        if (status) {
                            String countOfCart = cartCountJsonObject.getString("count");
                            Log.e("CART COUNT: ", countOfCart);

                            count = Integer.parseInt(countOfCart);
                            ecomPrefUtil.setCartCount(count);
                            System.out.println("--- CURRENT COUNT: " + count);
                            activity.updateCartCount(count);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void loadAllCartItems() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.loadCartItems("loadCartItems", prefUtil.getToken()).enqueue(new Callback<LoadCartItemsModel>() {
            @Override
            public void onResponse(Call<LoadCartItemsModel> call, Response<LoadCartItemsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        cartItemsList.clear();
                        cartItemsList = response.body().getData();
                        isAlreadyInCart(prodCode);
                        System.out.println("------------ PRODUCT IN CART: " + inCart);
                    } else {
                        isAddedToCart = false;
                    }
                }
            }

            @Override
            public void onFailure(Call<LoadCartItemsModel> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addCart(int regId, int productId, int qty, String prodCode) {
        System.out.println("---- PRODUCT ID for cart: " + productId);

        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.addToCart("addtocart", regId, prefUtil.getToken(), productId, qty, prodCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String cartAddResponseString = new String(response.body().bytes());
                        System.out.println("--------------- ADD TO CART: " + cartAddResponseString);
                        JSONObject cartAddJsonObject = new JSONObject(cartAddResponseString);
                        String msg = cartAddJsonObject.getString("message");
                        boolean status = cartAddJsonObject.getBoolean("status");

                        if (status) {
                            count = cartItemsList.size();
                            isAddedToCart = true;
                            activity.updateCartCount(count);
                            toggleCartView(isAddedToCart);
                            fragmentTransition(new OverviewFragment(), bundleForNew);
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                            getCartCount();
                        } else {
                            Toast.makeText(activity, "" + msg, Toast.LENGTH_SHORT).show();
                            isAddedToCart = false;
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                        isAddedToCart = false;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        isAddedToCart = false;
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                isAddedToCart = false;
            }
        });


    }

    private boolean isAnyImgSelected;

    private void goToProductFullView() {
        Intent productFullViewIntent = new Intent(activity, ProductViewActivity.class);
        previewImagesStringList.clear();
        for (int i = 0; i < previewImgModelArrayList.size(); i++) {
            previewImagesStringList.add(previewImgModelArrayList.get(i).getImgUrl());
        }
        productFullViewIntent.putStringArrayListExtra("PREVIEW_IMAGES_FULL_LIST_FROM_OVERVIEW", (ArrayList<String>) previewImagesStringList);

        if (!isAnyImgSelected)
            curPrevPos = previewImagesStringList.size() - 1;

        productFullViewIntent.putExtra("PREV_IMG_POS", curPrevPos);

        if (Build.VERSION.SDK_INT >= 21) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(activity, productMainIV, "product_image_for_transition");
            startActivity(productFullViewIntent, options.toBundle());
        } else
            startActivity(productFullViewIntent);

    }

    private class PreviewImgAdapter extends RecyclerView.Adapter {

        PreviewImgAdapter() {
            Collections.reverse(previewImgModelArrayList);
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.preview_img_item, parent, false);
            return new PreviewImageHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final PreviewImageHolder previewImageHolder = (PreviewImageHolder) holder;
//curPrevPos = position;
            LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                    .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                    .build();

            String url = previewImgModelArrayList.get(position).getImgUrl();

            if (isAttachedToWindow) {
                Glide.with(activity)
                        .load(new GlideUrl(url))
                        .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(previewImageHolder.previewIV);
            }

            //   previewImageHolder.previewIV.setImageResource(previewImages[position]);
            previewImageHolder.previewIV.setSelected(selectedItemPos == position);
            System.out.println("--- SELECTED POSITION: " + selectedItemPos);
            selectedItemPos = curPrevPos;
            System.out.println("--- CUR POSITION: " + curPrevPos);
            final Drawable drawable = ContextCompat.getDrawable(activity, R.drawable.rect_corner_blue_bg);
            if (previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).isSelected()) {
                previewImageHolder.previewLayout.setBackgroundDrawable(drawable);
            } else {
                previewImageHolder.previewLayout.setBackground(null);
            }
            previewImageHolder.previewIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    isAnyImgSelected = true;

                    LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                            .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                            .build();


                    String url = previewImgModelArrayList.get(position).getImgUrl();

                    if (isAttachedToWindow) {
                        Glide.with(activity)
                                .load(new GlideUrl(url, auth))
                                .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                                .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                                .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        return false;
                                    }
                                })
                                .dontAnimate()
                                .into(productMainIV);
                    }

                    // load preview images here
                    //  productMainIV.setImageResource(previewImgModelArrayList.get(position).getImgUrl());
                    curPrevPos = position;
                    //   clearAllChecks();
                    if (isChecked) {
                        clearAllChecks();
                        previewImageHolder.previewLayout.setBackground(null);
                        //  previewImageHolder.previewIV.setBackground(null);
                        previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).setSelected(false);
                    }

                    if (!previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).isSelected()) {
                        clearAllChecks();
                        isChecked = false;
                        previewImageHolder.previewLayout.setBackground(drawable);
                        //   previewImageHolder.previewIV.setBackgroundDrawable(drawable);
                        previewImgModelArrayList.get(previewImageHolder.getAdapterPosition()).setSelected(true);
                    }
                    notifyDataSetChanged();
                }
            });
        }

        private void clearAllChecks() {
            for (int i = 0; i < previewImgModelArrayList.size(); i++) {
                previewImgModelArrayList.get(i).setSelected(false);
            }
        }

        @Override
        public int getItemCount() {
            return previewImgModelArrayList.size();
        }
    }

    private class PreviewImageHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView previewIV;
        private RelativeLayout previewLayout;

        PreviewImageHolder(View itemView) {
            super(itemView);
            previewIV = itemView.findViewById(R.id.list_item_prev_iv);
            previewLayout = itemView.findViewById(R.id.prev_img_parent_view_id);
        }

    }

    private void toggleCartView(boolean cartStatus) {
        if (cartStatus)
            addToCartTV.setText("Go to Cart");
        else
            addToCartTV.setText("Add to Cart");
    }

    /*public void checkIfAlreadyAddedToCart(int prodId) {
        if (cartItemsList.size() > 0) {
            for (int i = 0; i < cartItemsList.size(); i++) {
                if (cartItemsList.get(i).getProduct().getId().equals(String.valueOf(prodId))) {
                    System.out.println("---- yES in cart");
                    isAddedToCart = true;
                    break;
                } else
                    isAddedToCart = false;
                if (Integer.parseInt(cartItemsList.get(i).getProduct().getId()) == prodId) {
                    System.out.println("---- yES ");
                    isAddedToCart = true;
                    break;
                } else
                    isAddedToCart = false;
            }
        } else
            isAddedToCart = false;
    }*/

    public void fragmentTransition(Fragment fragment, Bundle bundle) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragment.setArguments(bundle);
        // transaction.setCustomAnimations(R.animator.in,R.animator.out);
        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.replace(R.id.relative, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        activity.getSupportFragmentManager().popBackStack();

    }

    private class ReviewsAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.reviews_list_item, parent, false);
            return new ReviewsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            ReviewsHolder reviewsHolder = (ReviewsHolder) holder;

            if (productReviewList.get(position).getRate() == null || productReviewList.get(position).getRate().equals("0.0")
                    || productReviewList.get(position).getRate().equals("")) {
                reviewsHolder.ratingsTV.setVisibility(View.GONE);
            } else {
                reviewsHolder.ratingsTV.setVisibility(View.VISIBLE);
                reviewsHolder.ratingsTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star, 0);
                reviewsHolder.ratingsTV.setText(formatToSingleDecimalFraction(productReviewList.get(position).getRate()));
            }

            reviewsHolder.certifiedBuyerTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_check_circle, 0);
            String reviewTitle[] = productReviewList.get(position).getReview().split(" ");
            reviewsHolder.reviewTitleTV.setText(reviewTitle[0]);
            reviewsHolder.reviewDescTV.setText(productReviewList.get(position).getReview());
            reviewsHolder.reviewDateTV.setText(productReviewList.get(position).getUpdated());
        }

        @Override
        public int getItemCount() {
            return productReviewList.size();
        }
    }

    private class ReviewsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView ratingsTV, userReviewedTV, reviewDateTV, reviewDescTV, reviewTitleTV, certifiedBuyerTV;

        ReviewsHolder(View itemView) {
            super(itemView);
            ratingsTV = itemView.findViewById(R.id.rating_tv_review_item);
            reviewDescTV = itemView.findViewById(R.id.product_review_item_desc_tv);
            reviewTitleTV = itemView.findViewById(R.id.product_review_item_title_tv);
            userReviewedTV = itemView.findViewById(R.id.product_review_item_user_name_tv);
            reviewDateTV = itemView.findViewById(R.id.product_review_item_date_tv);
            certifiedBuyerTV = itemView.findViewById(R.id.certified_buyer_review_list_tv);
            userReviewedTV.setVisibility(View.GONE);
        }
    }

    private void getAllReviewsForTheProduct() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getReviewForTheProduct("getProductReviews", String.valueOf(productId), 0, 10000).enqueue(new Callback<AllReviewsResponse>() {
            @Override
            public void onResponse(Call<AllReviewsResponse> call, Response<AllReviewsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        productReviewList.clear();
                        productReviewList = response.body().getData();
                        if (productReviewList.size() > 0) {
                            Log.e("REVIEWS TOTAL: ", "" + productReviewList.size());
                            reviewsRV.setAdapter(new ReviewsAdapter());
                        } else
                            reviewsRV.setVisibility(View.GONE);
                    } else {
                        Log.e("FALSE", "");
                        // Toast.makeText(activity, "Please login again to use this app", Toast.LENGTH_SHORT).show();
                        reviewsRV.setVisibility(View.GONE);
                    }
                } else {
                    Log.e("UN SUCCESS", "");
                    reviewsRV.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<AllReviewsResponse> call, Throwable t) {
                reviewsRV.setVisibility(View.GONE);
            }
        });
    }

    private boolean inCart;

    private void isAlreadyInCart(String prodCode) {
        boolean isInCart = false;
        for (int i = 0; i < cartItemsList.size(); i++) {
            if (cartItemsList.get(i).getProductCode().equals(prodCode)) {
                inCart = true;
                isInCart = true;
                break;
            }
        }
        toggleCartView(isInCart);
    }

    private class SimilarProductsAdapter extends RecyclerView.Adapter {
        private List<SimilarProductsResponse.SimilarProductData> similarList;

        SimilarProductsAdapter(List<SimilarProductsResponse.SimilarProductData> similarList) {
            this.similarList = similarList;
            for (int i = 0; i < similarList.size(); i++) {
                if (similarList.get(i).getImage() != null) {
                    similarList.get(i).setHasImage(true);
                } else
                    similarList.get(i).setHasImage(false);
            }
            similarProductsList = similarList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity).inflate(R.layout.similar_product_list_item, parent, false);
            return new SimilarProductsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final SimilarProductsHolder similarProductsHolder = (SimilarProductsHolder) holder;
            similarProductsHolder.productNameTV.setText(similarProductsList.get(position).getBrand()
                    + " " + similarProductsList.get(position).getTitle());

            String msp = similarProductsList.get(position).getMsp().replace(".00", "");

            similarProductsHolder.originalPriceTV.setPaintFlags(similarProductsHolder.originalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            String mrp = similarProductsList.get(position).getMrp().replace(".00", "");

            int someNumber = Integer.parseInt(msp);
            NumberFormat nf = NumberFormat.getInstance();
            String mspFormatted = nf.format(someNumber);

            int mrpNum = Integer.parseInt(mrp);
            String mrpFormatted = nf.format(mrpNum);

            similarProductsHolder.sellingPriceTV.setText(mspFormatted);
            similarProductsHolder.originalPriceTV.setText(mrpFormatted);

            String CBP = similarProductsList.get(position).getPv();
            similarProductsHolder.CBPTV.setText("CBP: " + CBP);

            String rating = similarProductsList.get(position).getRating();

            if (rating == null)
                rating = "0";

            Double ratingInDouble = Double.valueOf(rating);

            similarProductsHolder.ratingsTV.setText(formatToSingleDecimalFraction(rating));

            Log.e("Actual rating: ", String.valueOf(ratingInDouble));

           /* if (ratingInDouble <= 1.0 && ratingInDouble > 0) {
                similarProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_red_bg);
            } else if (ratingInDouble < 2 && ratingInDouble > 1.0) {
                similarProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_yellow_bg);
            } else if (ratingInDouble < 3.0 && ratingInDouble >= 2.0) {
                similarProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_orange_bg);
            } else if (ratingInDouble <= 5.0 && ratingInDouble >= 3.0) {
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_green_bg);
                similarProductsHolder.ratingsTV.setVisibility(View.VISIBLE);
            } else {
                similarProductsHolder.ratingsTV.setVisibility(View.GONE);
            }*/

            similarProductsHolder.discountTV.setText(getDiscountInPercent(Double.parseDouble(msp), Double.parseDouble(mrp)) + "% off");

            /*if (similarProductsList.get(position).getQuantity().equals("0")) {
                similarProductsHolder.discountTV.setVisibility(View.GONE);
                similarProductsHolder.outOfStockTV.setVisibility(View.VISIBLE);
            } else {
                similarProductsHolder.outOfStockTV.setVisibility(View.GONE);
                similarProductsHolder.discountTV.setVisibility(View.VISIBLE);
            }*/

            if (similarProductsList.get(position).isHasImage()) {
                if (isAttachedToWindow) {
                    Glide.with(activity)
                            .load(new GlideUrl(similarProductsList.get(position).getImage()))
                            .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .dontAnimate()
                            .into(similarProductsHolder.productIV);
                }
            }

            similarProductsHolder.productItemMainCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment productOverViewFragment = new OverviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("PRODUCT_ID", similarProductsList.get(position).getId());
                    bundle.putString("PRODUCT_CODE", similarProductsList.get(position).getProductCode());
                    Log.e("CAT ID SIMILAR SENT: ", "" + catIdForOverview);
                    bundle.putBoolean("IS_GROCERY", isGrocery);
                    bundle.putInt("CAT_ID_FOR_OVERVIEW", Integer.parseInt(catIdForOverview));
                    bundle.putString("FOR", "LIVE");
                    productOverViewFragment.setArguments(bundle);
                    activity.moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
                }
            });
        }

        @Override
        public int getItemCount() {
//            if (similarProductsList.size() < 2)
            return similarProductsList.size();
          /*  else if (similarProductsList.size() < 4)
                return 2;
            else
                return 4;*/
        }
    }


    private String getDiscountInPercent(double msp, double mrp) {
        double dis = mrp - msp;
        double discount = (dis / mrp) * 100;

        DecimalFormat value = new DecimalFormat("#.#");
        value.format(discount);

        return String.valueOf(Math.round(discount));
    }

    private class SimilarProductsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView sellingPriceTV, originalPriceTV, productNameTV, discountTV, CBPTV, ratingsTV, outOfStockTV;
        private CardView productItemMainCV;
        private AppCompatImageView productIV;
        private RatingBar ratingBar;

        SimilarProductsHolder(View itemView) {
            super(itemView);
            sellingPriceTV = itemView.findViewById(R.id.selling_price_tv);
            originalPriceTV = itemView.findViewById(R.id.purchase_pice_tv);
            productNameTV = itemView.findViewById(R.id.product_name_tv);
            sellingPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian, 0, 0, 0);
            //  originalPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian_grey, 0, 0, 0);
            productItemMainCV = itemView.findViewById(R.id.product_item_main_view);
            productIV = itemView.findViewById(R.id.product_iv);
            discountTV = itemView.findViewById(R.id.discount_tv);
            ratingsTV = itemView.findViewById(R.id.rating_tv_product_item);
            ratingsTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star, 0);
            ratingsTV.setVisibility(View.GONE);
            CBPTV = itemView.findViewById(R.id.pv_tv_item);
            ratingBar = itemView.findViewById(R.id.rating_bar_product_item);
            outOfStockTV = itemView.findViewById(R.id.out_of_stock_tv);
            outOfStockTV.setVisibility(View.GONE);
            // CBPTV.setVisibility(View.GONE);
            //discountTV.setVisibility(View.GONE);
            //originalPriceTV.setVisibility(View.GONE);
        }
    }
}
