package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.MyCartAdapter;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.LoadCartItemsModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 17-05-2016.
 */
public class MyCart extends Fragment {
    EcommerceCustomActivity activity;
    private EcomPrefUtil prefUtil;
    private PrefUtil prefUtilApp;
    private List<LoadCartItemsModel.CartItemData> cartItemsList = new ArrayList<>();
    private RecyclerView recyclerView = null;
    private static TextView amountTV, shippingChargeTV;
    private RelativeLayout cartMainView;
    private LinearLayout cartBottomView;
    private View noItemsView;
    private Bundle bundleForCart;
    private ProgressBar loadingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        activity = (EcommerceCustomActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.setToggle(false);

        prefUtil = new EcomPrefUtil(activity);
        prefUtilApp = new PrefUtil(activity);

        bundleForCart = getArguments();
        if (bundleForCart != null)
            System.out.println("-------- BUNDLE FOR CART: " + bundleForCart.getInt("CATEGORY_ID"));
        View view = inflater.inflate(R.layout.fragment_my_cart, container, false);
        loadingBar = view.findViewById(R.id.my_cart_prog_bar);
        loadingBar.setVisibility(View.VISIBLE);
        recyclerView = view.findViewById(R.id.cart_recycleviewID);
        cartMainView = view.findViewById(R.id.cart_main_view);
        cartBottomView = view.findViewById(R.id.cart_bottom_view);
        noItemsView = view.findViewById(R.id.no_item_view_id);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        shippingChargeTV = view.findViewById(R.id.shipping_charge_cart_tv);

        amountTV = view.findViewById(R.id.amount);
        AppCompatButton pay = view.findViewById(R.id.pay);
        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EcommerceCustomActivity activity = (EcommerceCustomActivity) getActivity();
                DeliveryAddressFragment addresses = new DeliveryAddressFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("ORDER_PLACE", true);
                addresses.setArguments(bundle);
                activity.moveToFragment(addresses, "DELIVERY_ADDRESS");
            }
        });

        shippingChargeTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showShippingInfoPopup();
            }
        });


        cartMainView.setVisibility(View.GONE);
        //getAllCartItems();
        if (CommonUtils.isThereInternet(activity))
            loadAllCartItems();
        else {
            cartMainView.setVisibility(View.GONE);
            loadingBar.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(activity);
        }

        return view;
    }

    private void showShippingInfoPopup() {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.for_no_shipping_charge_popup);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.ok_btn_in_no_shipping_popup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                /*Toast.makeText(activity, "Please Login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(activity, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);*/
                dialog.cancel();
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void loadAllCartItems() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.loadCartItems("loadCartItems", prefUtilApp.getToken()).enqueue(new Callback<LoadCartItemsModel>() {
            @Override
            public void onResponse(Call<LoadCartItemsModel> call, Response<LoadCartItemsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        cartItemsList.clear();
                        cartItemsList = response.body().getData();

                        activity.updateCartCount(cartItemsList.size());
                        int amt = 0;
                        String am = "0";

                        double totalAmt = 0, shippingCharge = 0, amount = 0;

                        if (cartItemsList.size() > 0) {
                            for (int i = 0; i < cartItemsList.size(); i++) {
                                String msp = cartItemsList.get(i).getStock().getMsp().replace(".00", "");
                                amt += Integer.parseInt(msp);
                                am = msp;
                                shippingCharge += Double.parseDouble(cartItemsList.get(i).getStock().getShippingCharge());
                                amount += Double.parseDouble(cartItemsList.get(i).getStock().getMsp());

                                totalAmt = shippingCharge + amount;
                            }

                            System.out.println("------------ TOTAL AMOUNT CART: " + totalAmt);

                            getCartTotal();

                            MyCartAdapter adapter = new MyCartAdapter(activity, cartItemsList, new MyCartAdapter.NoItemsInCartInterface() {
                                @Override
                                public void noItems(boolean status) {
                                    if (status) {
                                        DialogUtils.showCustomToast(activity,"No Items in cart");
                                        //Toast.makeText(activity, "No Items in cart", Toast.LENGTH_SHORT).show();
                                        cartMainView.setVisibility(View.GONE);
                                        noItemsView.setVisibility(View.VISIBLE);
                                    }
                                }
                            }, new MyCartAdapter.CartUpdatable() {
                                @Override
                                public void onCartUpdate() {
                                    System.out.println("--------------ON  CART UPDATED:");
                                    getCartTotal();
                                }

                                @Override
                                public void onCartRemove() {
                                    System.out.println("--------------ON  CART UPDATED:");
                                    getCartTotal();
                                }
                            });
                            recyclerView.setAdapter(adapter);
                            cartMainView.setVisibility(View.VISIBLE);
                           // cartBottomView.setVisibility(View.GONE);
                            loadingBar.setVisibility(View.GONE);

                        } else {
                            // Toast.makeText(activity, "response.body().getMessage()", Toast.LENGTH_SHORT).show();
                            noItemsView.setVisibility(View.VISIBLE);
                            cartMainView.setVisibility(View.GONE);
                            loadingBar.setVisibility(View.GONE);
                        }
                    } else {
                        if (response.body().getMessage().equalsIgnoreCase("Cart Empty")) {
                            noItemsView.setVisibility(View.VISIBLE);
                            cartMainView.setVisibility(View.GONE);
                            loadingBar.setVisibility(View.GONE);
                            DialogUtils.showCustomToast(activity,response.body().getMessage());
                          //  Toast.makeText(activity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            noItemsView.setVisibility(View.VISIBLE);
                            cartMainView.setVisibility(View.GONE);
                            loadingBar.setVisibility(View.GONE);
                            DialogUtils.showReLoginPopUp(getActivity());
                        }
                    }
                }
                else {
                    cartMainView.setVisibility(View.GONE);
                    loadingBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<LoadCartItemsModel> call, Throwable t) {
                cartMainView.setVisibility(View.GONE);
                loadingBar.setVisibility(View.GONE);
            }
        });
    }

    private void getCartTotal() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getCartTotal("calculateCartTotal", prefUtilApp.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String jsonResponseString = new String(response.body().bytes());
                            System.out.println("-----------CART TOTAL JSON: " + jsonResponseString);
                            JSONObject jsonObject = new JSONObject(jsonResponseString);

                            try {
                                String cartTotalPrice = jsonObject.getString("cartTotal");
                                String shippingCharge = jsonObject.getString("shippingPrice");
                                String itemPrice = jsonObject.getString("itemPrice");
                                if (cartTotalPrice == null)
                                    DialogUtils.showCustomToast(activity,"Session Expired..Please login again!");
                                 //   Toast.makeText(activity, "Session Expired..Please login again!", Toast.LENGTH_SHORT).show();
                                else {
                                    System.out.println("------------CART TOTAL API: " + cartTotalPrice);
                                    MyCart.setTotalAmount(Integer.parseInt(itemPrice), Integer.parseInt(shippingCharge));
                                    MyCart.setShippingAmount(shippingCharge);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public static void setTotalAmount(int itemPrice, int shippingPrice) {
        String total = String.valueOf(itemPrice + shippingPrice);

        int someNumber = Integer.parseInt(total);
        NumberFormat nf = NumberFormat.getInstance();
        String totalFormatted = nf.format(someNumber);

        String shippingAndItemPrice = "";

        if (shippingPrice == 0)
            shippingAndItemPrice = "";
        else
            shippingAndItemPrice = itemPrice + " + " + shippingPrice + " = ";

        amountTV.setText("Rs. " + shippingAndItemPrice + totalFormatted);
    }

    public static void setShippingAmount(String amt) {
        /*int someNumber = Integer.parseInt(amt);
        NumberFormat nf = NumberFormat.getInstance();
        String amtFormatted = nf.format(someNumber);*/

        shippingChargeTV.setText("Rs. " + amt);

        /*int totalAmt = Integer.parseInt(amountTV.getText().toString().replace("Rs. ", ""));
        System.out.println("--------- TOTAL AMT: " + totalAmt);
        System.out.println("--------- SHIPPING AMT: " + amt);

        Log.e("TOTAL AMT: ", "" + totalAmt);*/

        if (Integer.parseInt(amt) > 0) {
            shippingChargeTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_info, 0);
            shippingChargeTV.setClickable(true);
        } else {
            shippingChargeTV.setText("Free");
            shippingChargeTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            shippingChargeTV.setClickable(false);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("My Cart");

        /*activity.hideToolBarLogo();
        activity.setAppToolBarTitle("My Cart");*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.hideToolBarTitleAndShowLogo();
    }

}
