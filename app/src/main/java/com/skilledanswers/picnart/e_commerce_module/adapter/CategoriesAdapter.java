package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.EcommerceCategoriesResponse;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.SubCategoryFragment;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by SkilledAnswers-D1 on 03-11-2017.
 */

public class CategoriesAdapter extends RecyclerView.Adapter {
    private Context context;
    private List<EcommerceCategoriesResponse.CategoryData> categoryList;
    private EcommerceCustomActivity activity;
    private int view = -1;

    public CategoriesAdapter(Context context, List<EcommerceCategoriesResponse.CategoryData> catList, int view) {
        this.context = context;
        activity = (EcommerceCustomActivity) context;

        try {
            if (catList.size() > 0) {
                for (int i = 0; i < catList.size(); i++) {
                    if (catList.get(i).getImage() != null) {
                        catList.get(i).setHasImage(true);
                    } else
                        catList.get(i).setHasImage(false);

                    if (catList.get(i).getSubCategories() != null && catList.get(i).getSubCategories().size() > 0) {
                        catList.get(i).setDoesItHasSubCategory(true);
                    } else
                        catList.get(i).setDoesItHasSubCategory(false);

                    if (catList.get(i).getName().equalsIgnoreCase("Apparels"))
                        catList.remove(i);
                }

                EcommerceCategoriesResponse.CategoryData groceryCategory = catList.get(catList.size() - 1);
                EcommerceCategoriesResponse.CategoryData defCategory = catList.get(2);
                catList.set(2, groceryCategory);
                catList.set(catList.size() - 1, defCategory);
                this.categoryList = catList;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.view = view;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View categoriesView = null;
        if (view == 0)
            categoriesView = LayoutInflater.from(context).inflate(R.layout.new_categories_list_item_view, parent, false);
        else if (view == 1)
            categoriesView = LayoutInflater.from(context).inflate(R.layout.categories_list_item, parent, false);
        else if (view == 2)
            categoriesView = LayoutInflater.from(context).inflate(R.layout.categories_list_item_for_main, parent, false);
        return new CategoriesHolder(categoriesView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        CategoriesHolder categoriesHolder = (CategoriesHolder) holder;

        categoriesHolder.categoryTV.setText(categoryList.get(position).getName());

        System.out.println("------------ IMAGE URL: " + categoryList.get(position).getImageURL());

        String url = "http://google.com/";
        if (categoryList.get(position).getImage() != null)
            if (categoryList.get(position).isHasImage()) {
                url = categoryList.get(position).getImageURL();
                System.out.println("---- IMAGE URL: " + url);
            }

        Glide.with(context)
                .load(new GlideUrl(url))
                /*.placeholder(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                .error(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))*/
                .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (e != null)
                            System.out.println("------ " + e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .dontAnimate()
                .into(categoriesHolder.categoryIV);

        categoriesHolder.categoryView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (categoryList.size() > 0) {
                    String categoryId = String.valueOf(categoryList.get(position).getId());
                    System.out.println("----- CATEGORY: " + categoryId);

                    Fragment fragment = new SubCategoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("ECOM_CATEGORY_ID", Integer.parseInt(categoryId));
                    bundle.putString("ECOM_CATEGORY_NAME", categoryList.get(position).getName());
                    fragment.setArguments(bundle);
                    activity.moveToFragment(fragment, "ECOMMERCE_SUB_CATEGORY_FRAGMENT");

                    //moveToProductsByCategoryScreen(categoryId);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (categoryList != null)
            return categoryList.size();
        else
            return 0;
    }

    private class CategoriesHolder extends RecyclerView.ViewHolder {
        private CircleImageView categoryIV;
        private AppCompatTextView categoryTV;
        private LinearLayout categoryView;

        CategoriesHolder(View itemView) {
            super(itemView);
            categoryIV = itemView.findViewById(R.id.category_item_iv);
            categoryTV = itemView.findViewById(R.id.category_item_tv);
            categoryView = itemView.findViewById(R.id.category_view);
        }
    }

}
