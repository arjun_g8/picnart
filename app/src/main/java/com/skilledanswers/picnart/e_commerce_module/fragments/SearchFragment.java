package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.SearchSuggestionProvider;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.SearchResponse;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 08-01-2018.
 */

public class SearchFragment extends Fragment {
    private List<SearchResponse.SearchData> searchResultList = new ArrayList<>();
    private RecyclerView searchRV;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private SearchView search;
    private AppCompatImageButton clearBtn;
    PrefUtil prefUtil;
    private AppCompatTextView noSearchItemsTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View searchView = inflater.inflate(R.layout.search_fragment, container, false);

        setHasOptionsMenu(true);
        ecommerceCustomActivity.setToggle(false);
        prefUtil = new PrefUtil(ecommerceCustomActivity);

        noSearchItemsTV = searchView.findViewById(R.id.no_search_item_found_tv);

        searchRV = searchView.findViewById(R.id.search_rv);
        searchRV.setLayoutManager(new WrapContentLinearLayoutManager(ecommerceCustomActivity, LinearLayoutManager.VERTICAL, false));
        // searchRV.setHasFixedSize(true);

        return searchView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("Search");
        prefUtil.putIsForSearch(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        prefUtil.putIsForSearch(false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.search, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setIconified(false);
        searchView.setSubmitButtonEnabled(true);
        SearchManager searchManager = (SearchManager) ecommerceCustomActivity.getSystemService(Context.SEARCH_SERVICE);
        assert searchManager != null;
        searchView.setSearchableInfo(searchManager.getSearchableInfo(ecommerceCustomActivity.getComponentName()));

        final SearchRecentSuggestions suggestions = new SearchRecentSuggestions(ecommerceCustomActivity,
                SearchSuggestionProvider.AUTHORITY, SearchSuggestionProvider.MODE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                noSearchItemsTV.setVisibility(View.GONE);
                System.out.println("----------- Entered text for search: " + newText + "---------- SIZE: " + newText.length());
                if (newText.length() == 0) {
                    searchResultList.clear();
                    searchRV.setAdapter(new SearchAdapter(searchResultList));
                    searchRV.getAdapter().notifyDataSetChanged();
                } else {
                    if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
                        //  suggestions.saveRecentQuery(newText, null);
                        searchResultList.clear();
                        searchForProduct(newText);
                    } else
                        DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                }
                return true;
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == android.R.id.home || super.onOptionsItemSelected(item);
    }

    private void searchForProduct(final String searchTerm) {
        Log.e("SEARCH TERM: ", searchTerm);
        if (!searchTerm.equals("") && searchTerm.length() > 1) {
            EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
            ecommerceAPIInterface.searchProduct("searchProduct", searchTerm).enqueue(new Callback<SearchResponse>() {
                @Override
                public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            searchResultList.clear();
                            searchResultList = response.body().getData();

                            Log.e("SEARCH TERM: ", searchTerm);
                            System.out.println("------- SEARCH TERM: " + searchTerm);

                            System.out.println("------- FILTER SEARCH FROM SERVER TOTAL : " + searchResultList.size());

                            if (searchResultList.size() > 0) {
                                noSearchItemsTV.setVisibility(View.GONE);
                                searchRV.setAdapter(new SearchAdapter(searchResultList));
                                searchRV.getAdapter().notifyDataSetChanged();
                            } else {
                                noSearchItemsTV.setVisibility(View.VISIBLE);
                                searchRV.setAdapter(new SearchAdapter(searchResultList));
                                searchRV.getAdapter().notifyDataSetChanged();
                            }

                        } else {
                            noSearchItemsTV.setVisibility(View.VISIBLE);
                            searchResultList.clear();
                            searchRV.setAdapter(new SearchAdapter(searchResultList));
                            searchRV.getAdapter().notifyDataSetChanged();
                            // Toast.makeText(SearchActivity.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<SearchResponse> call, Throwable t) {

                }
            });
        } else {
            noSearchItemsTV.setVisibility(View.VISIBLE);
            System.out.println("------------------- AFTER CLEAR");
            searchRV.setAdapter(new SearchAdapter(searchResultList));
            searchRV.getAdapter().notifyDataSetChanged();
        }
    }


    private class SearchAdapter extends RecyclerView.Adapter {
        private List<SearchResponse.SearchData> filterList = new ArrayList<>();
        private List<SearchResponse.SearchData> searchDataList = new ArrayList<>();

        SearchAdapter(List<SearchResponse.SearchData> searchDataList) {
            this.searchDataList = searchDataList;
            this.filterList = searchDataList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View searchResultItemView = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.search_result_item, parent, false);
            return new SearchHolder(searchResultItemView);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            SearchHolder searchHolder = (SearchHolder) holder;

            String searchedProduct = filterList.get(position).getBrand() + " " + filterList.get(position).getTitle();
            String inName = " in " + filterList.get(position).getName();

            searchHolder.searchResultTV.setText(searchedProduct + inName, TextView.BufferType.SPANNABLE);
            Spannable s = (Spannable) searchHolder.searchResultTV.getText();
            int start = searchedProduct.length();
            int end = start + inName.length();
            s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            // searchHolder.searchResultTV.setText(searchedProduct);
            searchHolder.searchItemMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment productOverViewFragment = new OverviewFragment();
                    Bundle bundle = new Bundle();
                    System.out.println("------- PRODUCT code after search: " + filterList.get(position).getProductCode());
                    bundle.putString("PRODUCT_CODE", filterList.get(position).getProductCode());
                    bundle.putInt("CAT_ID_FOR_OVERVIEW", Integer.parseInt(filterList.get(position).getCategoryId()));
                    bundle.putBoolean("IS_GROCERY", filterList.get(position).getIsGrocery());
                    bundle.putString("FOR", "LIVE");
                    productOverViewFragment.setArguments(bundle);
                    ecommerceCustomActivity.getSupportFragmentManager().popBackStack();
                    ecommerceCustomActivity.moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
                }
            });
        }

        @Override
        public int getItemCount() {
          /*  System.out.println("------- FILTER SEARCH RESULT TOTAL : " + filterList.size());
            System.out.println("------- SEARCH RESULT TOTAL : " + searchDataList.size());*/
            return filterList.size();
        }

        /*@Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        System.out.println("------- SEARCH EMPTY : ");
                        filterList = searchDataList;
                    } else {
                        List<SearchResponse.SearchData> filteredList = new ArrayList<>();
                        for (SearchResponse.SearchData row : searchDataList) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getBrand().toLowerCase().contains(charString.toLowerCase()) || row.getTitle().contains(charSequence)) {
                                filteredList.add(row);
                            }
                        }

                        System.out.println("------- SEARCH NOT EMPTY : " + filteredList.size());

                        filterList = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filterList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    filterList = (List<SearchResponse.SearchData>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }*/

        private class SearchHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView searchResultTV;
            private LinearLayout searchItemMain;

            SearchHolder(View itemView) {
                super(itemView);
                searchItemMain = itemView.findViewById(R.id.filter_item_main_view_id);
                searchResultTV = itemView.findViewById(R.id.filter_item_title_tv);
                searchResultTV.setTextSize(16f);
            }
        }

        public void moveToFragment(Fragment toFragment, String TAG) {
            FragmentTransaction transaction = ecommerceCustomActivity.getSupportFragmentManager().beginTransaction();

            Fragment fragment = ecommerceCustomActivity.getSupportFragmentManager().findFragmentByTag(TAG);
            if (fragment != null && fragment.isVisible()) {
                // replaceFragment(toFragment);
                System.out.println("*** ALREADY there ****");
            } else {
           /* if (TAG.equals("APPLY_FILTER_FRAGMENT")) {
                transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                transaction.replace(R.id.relative, toFragment, TAG);
                transaction.commit();
            } else {*/
                transaction.setCustomAnimations(R.anim.left_to_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                transaction.replace(R.id.relative, toFragment, TAG);
                transaction.addToBackStack(null).commit();
                //  }
            }
        }
    }

    public class WrapContentLinearLayoutManager extends LinearLayoutManager {
        public WrapContentLinearLayoutManager(Context context) {
            super(context);
        }

        public WrapContentLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }

        public WrapContentLinearLayoutManager(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
            super(context, attrs, defStyleAttr, defStyleRes);
        }

        //... constructor
        @Override
        public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
            try {
                super.onLayoutChildren(recycler, state);
            } catch (IndexOutOfBoundsException e) {
                Log.e("Error", "IndexOutOfBoundsException in RecyclerView happens");
            }
        }
    }
}
