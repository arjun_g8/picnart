package bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.api_pojo;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GroceryCardData implements Parcelable{
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("regNo")
    @Expose
    private Integer regNo;
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("cardAmount")
    @Expose
    private String cardAmount;
    @SerializedName("balance")
    @Expose
    private String balance;
    @SerializedName("redeemAllowance")
    @Expose
    private String redeemAllowance;
    @SerializedName("activation")
    @Expose
    private String activation;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("currentAvailable")
    @Expose
    private String currentAvailable;

    transient private boolean isSelected;

    protected GroceryCardData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            regNo = null;
        } else {
            regNo = in.readInt();
        }
        if (in.readByte() == 0) {
            orderId = null;
        } else {
            orderId = in.readInt();
        }
        if (in.readByte() == 0) {
            productId = null;
        } else {
            productId = in.readInt();
        }
        if (in.readByte() == 0) {
            type = null;
        } else {
            type = in.readInt();
        }
        cardAmount = in.readString();
        balance = in.readString();
        redeemAllowance = in.readString();
        activation = in.readString();
        if (in.readByte() == 0) {
            status = null;
        } else {
            status = in.readInt();
        }
        created = in.readString();
        updated = in.readString();
        currentAvailable = in.readString();
    }

    public static final Creator<GroceryCardData> CREATOR = new Creator<GroceryCardData>() {
        @Override
        public GroceryCardData createFromParcel(Parcel in) {
            return new GroceryCardData(in);
        }

        @Override
        public GroceryCardData[] newArray(int size) {
            return new GroceryCardData[size];
        }
    };

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegNo() {
        return regNo;
    }

    public void setRegNo(Integer regNo) {
        this.regNo = regNo;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getCardAmount() {
        return cardAmount;
    }

    public void setCardAmount(String cardAmount) {
        this.cardAmount = cardAmount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRedeemAllowance() {
        return redeemAllowance;
    }

    public void setRedeemAllowance(String redeemAllowance) {
        this.redeemAllowance = redeemAllowance;
    }

    public String getActivation() {
        return activation;
    }

    public void setActivation(String activation) {
        this.activation = activation;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getCurrentAvailable() {
        return currentAvailable;
    }

    public void setCurrentAvailable(String currentAvailable) {
        this.currentAvailable = currentAvailable;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (regNo == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(regNo);
        }
        if (orderId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderId);
        }
        if (productId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(productId);
        }
        if (type == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(type);
        }
        dest.writeString(cardAmount);
        dest.writeString(balance);
        dest.writeString(redeemAllowance);
        dest.writeString(activation);
        if (status == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(status);
        }
        dest.writeString(created);
        dest.writeString(updated);
        dest.writeString(currentAvailable);
    }
}
