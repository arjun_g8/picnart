package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SkilledAnswers-D1 on 29-12-2017.
 */

public class SaveAddressForOrderPlaceResponse implements Parcelable{

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("transactions")
    @Expose
    private Transactions transactions;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("message")
    @Expose
    private String message;

    private SaveAddressForOrderPlaceResponse(Parcel in) {
        byte tmpStatus = in.readByte();
        status = tmpStatus == 0 ? null : tmpStatus == 1;
        message = in.readString();
    }

    public static final Creator<SaveAddressForOrderPlaceResponse> CREATOR = new Creator<SaveAddressForOrderPlaceResponse>() {
        @Override
        public SaveAddressForOrderPlaceResponse createFromParcel(Parcel in) {
            return new SaveAddressForOrderPlaceResponse(in);
        }

        @Override
        public SaveAddressForOrderPlaceResponse[] newArray(int size) {
            return new SaveAddressForOrderPlaceResponse[size];
        }
    };

    @Override
    public String toString() {
        return "SaveAddressForOrderPlaceResponse{" +
                "status=" + status +
                ", transactions=" + transactions +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }



    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte((byte) (status == null ? 0 : status ? 1 : 2));
        parcel.writeString(message);
        parcel.writeValue(Transactions.class);
        parcel.writeValue(Data.class);
    }

    public class Transactions implements Parcelable{

        @SerializedName("orderId")
        @Expose
        private String orderId;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("udf1")
        @Expose
        private String udf1;

        protected Transactions(Parcel in) {
            orderId = in.readString();
            amount = in.readString();
            udf1 = in.readString();
        }

        public final Creator<Transactions> CREATOR = new Creator<Transactions>() {
            @Override
            public Transactions createFromParcel(Parcel in) {
                return new Transactions(in);
            }

            @Override
            public Transactions[] newArray(int size) {
                return new Transactions[size];
            }
        };

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getUdf1() {
            return udf1;
        }

        public void setUdf1(String udf1) {
            this.udf1 = udf1;
        }

        @Override
        public String toString() {
            return "Transactions{" +
                    "orderId='" + orderId + '\'' +
                    ", amount='" + amount + '\'' +
                    ", udf1='" + udf1 + '\'' +
                    '}';
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(orderId);
            parcel.writeString(amount);
            parcel.writeString(udf1);
        }
    }

    public class FormData implements Parcelable{

        @SerializedName("address_line_1")
        @Expose
        private String addressLine1;
        @SerializedName("address_line_2")
        @Expose
        private String addressLine2;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("api_key")
        @Expose
        private String apiKey;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("email")
        @Expose
        private Object email;
        @SerializedName("mode")
        @Expose
        private String mode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("return_url")
        @Expose
        private String returnUrl;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("udf1")
        @Expose
        private String udf1;
        @SerializedName("udf2")
        @Expose
        private String udf2;
        @SerializedName("udf3")
        @Expose
        private String udf3;
        @SerializedName("udf4")
        @Expose
        private String udf4;
        @SerializedName("udf5")
        @Expose
        private String udf5;
        @SerializedName("zip_code")
        @Expose
        private String zipCode;
        @SerializedName("hash")
        @Expose
        private String hash;

        protected FormData(Parcel in) {
            addressLine1 = in.readString();
            addressLine2 = in.readString();
            amount = in.readString();
            apiKey = in.readString();
            city = in.readString();
            country = in.readString();
            currency = in.readString();
            description = in.readString();
            mode = in.readString();
            name = in.readString();
            orderId = in.readString();
            phone = in.readString();
            returnUrl = in.readString();
            state = in.readString();
            udf1 = in.readString();
            udf2 = in.readString();
            udf3 = in.readString();
            udf4 = in.readString();
            udf5 = in.readString();
            zipCode = in.readString();
            hash = in.readString();
        }

        public final Creator<FormData> CREATOR = new Creator<FormData>() {
            @Override
            public FormData createFromParcel(Parcel in) {
                return new FormData(in);
            }

            @Override
            public FormData[] newArray(int size) {
                return new FormData[size];
            }
        };

        public String getAddressLine1() {
            return addressLine1;
        }

        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        public String getAddressLine2() {
            return addressLine2;
        }

        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Object getEmail() {
            return email;
        }

        public void setEmail(Object email) {
            this.email = email;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getReturnUrl() {
            return returnUrl;
        }

        public void setReturnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getUdf1() {
            return udf1;
        }

        public void setUdf1(String udf1) {
            this.udf1 = udf1;
        }

        public String getUdf2() {
            return udf2;
        }

        public void setUdf2(String udf2) {
            this.udf2 = udf2;
        }

        public String getUdf3() {
            return udf3;
        }

        public void setUdf3(String udf3) {
            this.udf3 = udf3;
        }

        public String getUdf4() {
            return udf4;
        }

        public void setUdf4(String udf4) {
            this.udf4 = udf4;
        }

        public String getUdf5() {
            return udf5;
        }

        public void setUdf5(String udf5) {
            this.udf5 = udf5;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(addressLine1);
            parcel.writeString(addressLine2);
            parcel.writeString(amount);
            parcel.writeString(apiKey);
            parcel.writeString(city);
            parcel.writeString(country);
            parcel.writeString(currency);
            parcel.writeString(description);
            parcel.writeString(mode);
            parcel.writeString(name);
            parcel.writeString(orderId);
            parcel.writeString(phone);
            parcel.writeString(returnUrl);
            parcel.writeString(state);
            parcel.writeString(udf1);
            parcel.writeString(udf2);
            parcel.writeString(udf3);
            parcel.writeString(udf4);
            parcel.writeString(udf5);
            parcel.writeString(zipCode);
            parcel.writeString(hash);
        }
    }

    public class Form implements Parcelable{

        @SerializedName("address_line_1")
        @Expose
        private String addressLine1;
        @SerializedName("address_line_2")
        @Expose
        private String addressLine2;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("api_key")
        @Expose
        private String apiKey;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("currency")
        @Expose
        private String currency;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mode")
        @Expose
        private String mode;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("return_url")
        @Expose
        private String returnUrl;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("udf1")
        @Expose
        private String udf1;
        @SerializedName("udf2")
        @Expose
        private String udf2;
        @SerializedName("udf3")
        @Expose
        private String udf3;
        @SerializedName("udf4")
        @Expose
        private String udf4;
        @SerializedName("udf5")
        @Expose
        private String udf5;
        @SerializedName("zip_code")
        @Expose
        private String zipCode;
        @SerializedName("hash")
        @Expose
        private String hash;

        protected Form(Parcel in) {
            addressLine1 = in.readString();
            addressLine2 = in.readString();
            amount = in.readString();
            apiKey = in.readString();
            city = in.readString();
            country = in.readString();
            currency = in.readString();
            description = in.readString();
            email = in.readString();
            mode = in.readString();
            name = in.readString();
            orderId = in.readString();
            phone = in.readString();
            returnUrl = in.readString();
            state = in.readString();
            udf1 = in.readString();
            udf2 = in.readString();
            udf3 = in.readString();
            udf4 = in.readString();
            udf5 = in.readString();
            zipCode = in.readString();
            hash = in.readString();
        }

        public final Creator<Form> CREATOR = new Creator<Form>() {
            @Override
            public Form createFromParcel(Parcel in) {
                return new Form(in);
            }

            @Override
            public Form[] newArray(int size) {
                return new Form[size];
            }
        };

        public String getAddressLine1() {
            return addressLine1;
        }

        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        public String getAddressLine2() {
            return addressLine2;
        }

        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getApiKey() {
            return apiKey;
        }

        public void setApiKey(String apiKey) {
            this.apiKey = apiKey;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMode() {
            return mode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getReturnUrl() {
            return returnUrl;
        }

        public void setReturnUrl(String returnUrl) {
            this.returnUrl = returnUrl;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getUdf1() {
            return udf1;
        }

        public void setUdf1(String udf1) {
            this.udf1 = udf1;
        }

        public String getUdf2() {
            return udf2;
        }

        public void setUdf2(String udf2) {
            this.udf2 = udf2;
        }

        public String getUdf3() {
            return udf3;
        }

        public void setUdf3(String udf3) {
            this.udf3 = udf3;
        }

        public String getUdf4() {
            return udf4;
        }

        public void setUdf4(String udf4) {
            this.udf4 = udf4;
        }

        public String getUdf5() {
            return udf5;
        }

        public void setUdf5(String udf5) {
            this.udf5 = udf5;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(addressLine1);
            parcel.writeString(addressLine2);
            parcel.writeString(amount);
            parcel.writeString(apiKey);
            parcel.writeString(city);
            parcel.writeString(country);
            parcel.writeString(currency);
            parcel.writeString(description);
            parcel.writeString(email);
            parcel.writeString(mode);
            parcel.writeString(name);
            parcel.writeString(orderId);
            parcel.writeString(phone);
            parcel.writeString(returnUrl);
            parcel.writeString(state);
            parcel.writeString(udf1);
            parcel.writeString(udf2);
            parcel.writeString(udf3);
            parcel.writeString(udf4);
            parcel.writeString(udf5);
            parcel.writeString(zipCode);
            parcel.writeString(hash);
        }
    }


    public class Data implements Parcelable{
        @SerializedName("form")
        @Expose
        private Form form;
        @SerializedName("formURL")
        @Expose
        private String formURL;
        @SerializedName("formData")
        @Expose
        private FormData formData;

        protected Data(Parcel in) {
            form = in.readParcelable(Form.class.getClassLoader());
            formURL = in.readString();
            formData = in.readParcelable(FormData.class.getClassLoader());
        }

        public final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public Form getForm() {
            return form;
        }

        public void setForm(Form form) {
            this.form = form;
        }

        public String getFormURL() {
            return formURL;
        }

        public void setFormURL(String formURL) {
            this.formURL = formURL;
        }

        public FormData getFormData() {
            return formData;
        }

        public void setFormData(FormData formData) {
            this.formData = formData;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(form, i);
            parcel.writeString(formURL);
            parcel.writeParcelable(formData, i);
        }
    }

}
