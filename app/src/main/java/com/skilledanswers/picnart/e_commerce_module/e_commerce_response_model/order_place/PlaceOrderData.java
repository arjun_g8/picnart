package bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderData {
    @SerializedName("form")
    @Expose
    private Form form;
    @SerializedName("formURL")
    @Expose
    private String formURL;
    @SerializedName("formData")
    @Expose
    private FormData formData;
    @SerializedName("formType")
    @Expose
    private String formType;

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public String getFormURL() {
        return formURL;
    }

    public void setFormURL(String formURL) {
        this.formURL = formURL;
    }

    public FormData getFormData() {
        return formData;
    }

    public void setFormData(FormData formData) {
        this.formData = formData;
    }

    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

}
