package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.backgroundTask.DownloadResultReceiver;
import com.skilledanswers.picnart.e_commerce_module.backgroundTask.DownloadService;

import java.util.HashMap;

/**
 * The Class CustomFragment is the base Fragment class. You can extend your
 * Fragment classes with this class in case you want to apply common set of
 * rules for those Fragments.
 */
public class CustomFragment extends Fragment implements DownloadResultReceiver.Receiver, OnClickListener {

    /* (non-Javadoc)
     * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
     */

    DownloadResultReceiver receiver;
    private HashMap<String, String> action;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    /**
     * Set the touch and click listener for a View.
     *
     * @param v the view
     * @return the same view
     */
    public View setTouchNClick(View v) {

        v.setOnClickListener(this);
      //  v.setOnTouchListener(EcommerceCustomActivity.TOUCH);
        return v;
    }

    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {

    }

    public void setAction(HashMap<String, String> action) {
        this.action = action;
        receiver = new DownloadResultReceiver(new android.os.Handler());
        receiver.setReceiver(this);
       /* if (getActivity() != null) {
            Intent intent = new Intent(getActivity(), DownloadService.class);
            intent.putExtra("url", Config.PICNARTHOME);
            intent.putExtra("action", action);
            intent.putExtra("receiver", receiver);
            intent.putExtra("requestId", 101);

            getActivity().startService(intent);

        }*/

    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_RUNNING:
              /*  progressDialog.setMessage("Loading...");
                progressDialog.show();*/
                break;
            case DownloadService.STATUS_FINISHED:
                /* Hide progress & extract result from bundle */
                //progressDialog.dismiss();
                displayDatas(resultData);
                break;
            case DownloadService.STATUS_ERROR:
                /* Handle the error */
                //progressDialog.dismiss();
                String error = resultData.getString(Intent.EXTRA_TEXT);
//				Log.e("error report", error);
                Toast.makeText(getActivity(), "No internet conection try later", Toast.LENGTH_LONG).show();
                // Toast.makeText(this, error, Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void displayDatas(Bundle resultData) {

    }
}
