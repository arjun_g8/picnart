package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SkilledAnswers-D1 on 27-12-2017.
 */

public class UpdateCartPostResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private UpdateCartPostResponse.UpdatedCartData data;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("totals")
    @Expose
    private Totals totals;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public UpdatedCartData getData() {
        return data;
    }

    public void setData(UpdatedCartData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Totals getTotals() {
        return totals;
    }

    public void setTotals(Totals totals) {
        this.totals = totals;
    }

    public class Totals {

        @SerializedName("itemPrice")
        @Expose
        private Integer itemPrice;
        @SerializedName("shippingPrice")
        @Expose
        private Integer shippingPrice;
        @SerializedName("cartTotal")
        @Expose
        private Integer cartTotal;

        public Integer getItemPrice() {
            return itemPrice;
        }

        public void setItemPrice(Integer itemPrice) {
            this.itemPrice = itemPrice;
        }

        public Integer getShippingPrice() {
            return shippingPrice;
        }

        public void setShippingPrice(Integer shippingPrice) {
            this.shippingPrice = shippingPrice;
        }

        public Integer getCartTotal() {
            return cartTotal;
        }

        public void setCartTotal(Integer cartTotal) {
            this.cartTotal = cartTotal;
        }

    }

    public class UpdatedCartData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("regNo")
        @Expose
        private String regNo;
        @SerializedName("productId")
        @Expose
        private String productId;
        @SerializedName("productCode")
        @Expose
        private String productCode;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("totalPrice")
        @Expose
        private Integer totalPrice;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getRegNo() {
            return regNo;
        }

        public void setRegNo(String regNo) {
            this.regNo = regNo;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public Integer getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(Integer totalPrice) {
            this.totalPrice = totalPrice;
        }

    }

}
