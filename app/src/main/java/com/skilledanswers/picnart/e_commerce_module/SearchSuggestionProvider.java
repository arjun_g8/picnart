package com.skilledanswers.picnart.e_commerce_module;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by subh on 5/31/2016.
 */
public class SearchSuggestionProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "picnart.com.picnart.model.SearchSuggestionProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
