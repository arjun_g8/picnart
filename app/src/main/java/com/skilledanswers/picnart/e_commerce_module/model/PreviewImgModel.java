package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 29-09-2017.
 */

public class PreviewImgModel {
    private String imgUrl;
    private boolean isSelected;

    private boolean hasImage;

    public boolean isHasImage() {
        return hasImage;
    }

    public void setHasImage(boolean hasImage) {
        this.hasImage = hasImage;
    }


    public PreviewImgModel(String imgUrl, boolean isSelected) {
        this.imgUrl = imgUrl;
        this.isSelected = isSelected;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public boolean isSelected() {
        return isSelected;
    }


    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
