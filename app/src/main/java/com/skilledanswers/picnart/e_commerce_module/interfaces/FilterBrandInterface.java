package com.skilledanswers.picnart.e_commerce_module.interfaces;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 26-12-2017.
 */

public interface FilterBrandInterface {
    void brandsSelected(List<String> brandsSelected);
}
