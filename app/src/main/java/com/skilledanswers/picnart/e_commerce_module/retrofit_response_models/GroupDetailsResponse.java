package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SkilledAnswers-D1 on 18-01-2018.
 */

public class GroupDetailsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("bvplid")
        @Expose
        private String bvplid;
        @SerializedName("sponsor_bvplid")
        @Expose
        private String sponsorBvplid;
        @SerializedName("account")
        @Expose
        private String account;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("group1_details")
        @Expose
        private Group1Details group1Details;
        @SerializedName("group2_details")
        @Expose
        private Group2Details group2Details;

        public String getBvplid() {
            return bvplid;
        }

        public void setBvplid(String bvplid) {
            this.bvplid = bvplid;
        }

        public String getSponsorBvplid() {
            return sponsorBvplid;
        }

        public void setSponsorBvplid(String sponsorBvplid) {
            this.sponsorBvplid = sponsorBvplid;
        }

        public String getAccount() {
            return account;
        }

        public void setAccount(String account) {
            this.account = account;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Group1Details getGroup1Details() {
            return group1Details;
        }

        public void setGroup1Details(Group1Details group1Details) {
            this.group1Details = group1Details;
        }

        public Group2Details getGroup2Details() {
            return group2Details;
        }

        public void setGroup2Details(Group2Details group2Details) {
            this.group2Details = group2Details;
        }

    }

    public class Group1Details {

        @SerializedName("previous_registered_members")
        @Expose
        private String previousRegisteredMembers;
        @SerializedName("previous_acivated_members")
        @Expose
        private String previousAcivatedMembers;
        @SerializedName("previous_pv")
        @Expose
        private String previousPv;
        @SerializedName("current_registered_members")
        @Expose
        private String currentRegisteredMembers;
        @SerializedName("current_acivated_members")
        @Expose
        private String currentAcivatedMembers;
        @SerializedName("current_pv")
        @Expose
        private String currentPv;
        @SerializedName("total_registered_members")
        @Expose
        private String totalRegisteredMembers;
        @SerializedName("total_acivated_members")
        @Expose
        private String totalAcivatedMembers;
        @SerializedName("total_pv")
        @Expose
        private String totalPv;

        public String getPreviousRegisteredMembers() {
            return previousRegisteredMembers;
        }

        public void setPreviousRegisteredMembers(String previousRegisteredMembers) {
            this.previousRegisteredMembers = previousRegisteredMembers;
        }

        public String getPreviousAcivatedMembers() {
            return previousAcivatedMembers;
        }

        public void setPreviousAcivatedMembers(String previousAcivatedMembers) {
            this.previousAcivatedMembers = previousAcivatedMembers;
        }

        public String getPreviousPv() {
            return previousPv;
        }

        public void setPreviousPv(String previousPv) {
            this.previousPv = previousPv;
        }

        public String getCurrentRegisteredMembers() {
            return currentRegisteredMembers;
        }

        public void setCurrentRegisteredMembers(String currentRegisteredMembers) {
            this.currentRegisteredMembers = currentRegisteredMembers;
        }

        public String getCurrentAcivatedMembers() {
            return currentAcivatedMembers;
        }

        public void setCurrentAcivatedMembers(String currentAcivatedMembers) {
            this.currentAcivatedMembers = currentAcivatedMembers;
        }

        public String getCurrentPv() {
            return currentPv;
        }

        public void setCurrentPv(String currentPv) {
            this.currentPv = currentPv;
        }

        public String getTotalRegisteredMembers() {
            return totalRegisteredMembers;
        }

        public void setTotalRegisteredMembers(String totalRegisteredMembers) {
            this.totalRegisteredMembers = totalRegisteredMembers;
        }

        public String getTotalAcivatedMembers() {
            return totalAcivatedMembers;
        }

        public void setTotalAcivatedMembers(String totalAcivatedMembers) {
            this.totalAcivatedMembers = totalAcivatedMembers;
        }

        public String getTotalPv() {
            return totalPv;
        }

        public void setTotalPv(String totalPv) {
            this.totalPv = totalPv;
        }

    }

    public class Group2Details {

        @SerializedName("previous_registered_members")
        @Expose
        private String previousRegisteredMembers;
        @SerializedName("previous_acivated_members")
        @Expose
        private String previousAcivatedMembers;
        @SerializedName("previous_pv")
        @Expose
        private String previousPv;
        @SerializedName("current_registered_members")
        @Expose
        private String currentRegisteredMembers;
        @SerializedName("current_acivated_members")
        @Expose
        private String currentAcivatedMembers;
        @SerializedName("current_pv")
        @Expose
        private String currentPv;
        @SerializedName("total_registered_members")
        @Expose
        private String totalRegisteredMembers;
        @SerializedName("total_acivated_members")
        @Expose
        private String totalAcivatedMembers;
        @SerializedName("total_pv")
        @Expose
        private String totalPv;

        public String getPreviousRegisteredMembers() {
            return previousRegisteredMembers;
        }

        public void setPreviousRegisteredMembers(String previousRegisteredMembers) {
            this.previousRegisteredMembers = previousRegisteredMembers;
        }

        public String getPreviousAcivatedMembers() {
            return previousAcivatedMembers;
        }

        public void setPreviousAcivatedMembers(String previousAcivatedMembers) {
            this.previousAcivatedMembers = previousAcivatedMembers;
        }

        public String getPreviousPv() {
            return previousPv;
        }

        public void setPreviousPv(String previousPv) {
            this.previousPv = previousPv;
        }

        public String getCurrentRegisteredMembers() {
            return currentRegisteredMembers;
        }

        public void setCurrentRegisteredMembers(String currentRegisteredMembers) {
            this.currentRegisteredMembers = currentRegisteredMembers;
        }

        public String getCurrentAcivatedMembers() {
            return currentAcivatedMembers;
        }

        public void setCurrentAcivatedMembers(String currentAcivatedMembers) {
            this.currentAcivatedMembers = currentAcivatedMembers;
        }

        public String getCurrentPv() {
            return currentPv;
        }

        public void setCurrentPv(String currentPv) {
            this.currentPv = currentPv;
        }

        public String getTotalRegisteredMembers() {
            return totalRegisteredMembers;
        }

        public void setTotalRegisteredMembers(String totalRegisteredMembers) {
            this.totalRegisteredMembers = totalRegisteredMembers;
        }

        public String getTotalAcivatedMembers() {
            return totalAcivatedMembers;
        }

        public void setTotalAcivatedMembers(String totalAcivatedMembers) {
            this.totalAcivatedMembers = totalAcivatedMembers;
        }

        public String getTotalPv() {
            return totalPv;
        }

        public void setTotalPv(String totalPv) {
            this.totalPv = totalPv;
        }

    }

}
