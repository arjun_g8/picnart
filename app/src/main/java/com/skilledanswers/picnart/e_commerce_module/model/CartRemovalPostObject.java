package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 10-11-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartRemovalPostObject {

    @SerializedName("cartId")
    @Expose
    private Integer cartId;

    public Integer getCartId() {
        return cartId;
    }

    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

}