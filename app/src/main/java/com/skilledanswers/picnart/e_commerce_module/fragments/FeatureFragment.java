package com.skilledanswers.picnart.e_commerce_module.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.DisplayProductResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class FeatureFragment extends CustomFragment {
    private Bundle specificationsBundle;
    private int productId;
    private String prodCode;
    private RecyclerView specificationsRV;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private List<DisplayProductResponse.Attribute> productAttributes = new ArrayList<>();

    public FeatureFragment() {
    }

    public static FeatureFragment newInstance() {
        FeatureFragment fragment = new FeatureFragment();
        return fragment;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        // menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("Full Details");

        // ecommerceCustomActivity.hideToolBarLogo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.full_specifications_fragment, container, false);
        setHasOptionsMenu(true);
        specificationsRV = view.findViewById(R.id.full_specifications_rv);
        specificationsRV.setLayoutManager(new LinearLayoutManager(ecommerceCustomActivity));
        specificationsRV.setHasFixedSize(true);
        specificationsBundle = getArguments();
        if (specificationsBundle.getString("PRODUCT_ID") != null)
            productId = Integer.parseInt(specificationsBundle.getString("PRODUCT_ID"));
        prodCode = specificationsBundle.getString("PRODUCT_CODE");

        if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
            fetchProductCompleteInfo();
        } else {
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
        }

        return view;
    }

    private void fetchProductCompleteInfo() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getProductDetailsUsingCode("loadDisplayProduct", prodCode).enqueue(new Callback<DisplayProductResponse>() {
            @Override
            public void onResponse(Call<DisplayProductResponse> call, Response<DisplayProductResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        productAttributes.clear();
                        productAttributes = response.body().getData().getAttributes();

                        System.out.println("-----------PRODUCT ATTR SIZE: " + productAttributes.size());

                        if (productAttributes.size() > 0) {
                            specificationsRV.setAdapter(new SpecificationAdapter());
                            specificationsRV.getAdapter().notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<DisplayProductResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("----------- " + t.toString());
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();

    }

    private class SpecificationAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.specification_list_item, parent, false);
            return new SpecificationsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            SpecificationsHolder specificationsHolder = (SpecificationsHolder) holder;
            specificationsHolder.featureNameTV.setText(productAttributes.get(position).getName());
            specificationsHolder.featureValueTV.setText(productAttributes.get(position).getValue());
        }

        @Override
        public int getItemCount() {
            return productAttributes.size();
        }
    }

    private class SpecificationsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView featureNameTV, featureValueTV;

        SpecificationsHolder(View itemView) {
            super(itemView);
            featureNameTV = itemView.findViewById(R.id.specification_item_title_tv);
            featureValueTV = itemView.findViewById(R.id.specification_item_value_tv);
        }
    }
}
