package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SkilledAnswers-D1 on 15-11-2017.
 */

public class PostAddressModel {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private City city;
    @SerializedName("pincode")
    @Expose
    private Integer pincode;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Integer getPincode() {
        return pincode;
    }

    public void setPincode(Integer pincode) {
        this.pincode = pincode;
    }

    public static class User {

        @SerializedName("userId")
        @Expose
        private Integer userId;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

    }

    public static class City {

        @SerializedName("cityId")
        @Expose
        private Integer cityId;

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

    }
}