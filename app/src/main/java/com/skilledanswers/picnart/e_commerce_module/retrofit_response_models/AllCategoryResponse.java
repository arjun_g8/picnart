package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 03-11-2017.
 */

public class AllCategoryResponse {

    @SerializedName("data")
    @Expose
    private List<CategoryModel> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<CategoryModel> getData() {
        return data;
    }

    public void setData(List<CategoryModel> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public class Attribute {

        @SerializedName("attributeId")
        @Expose
        private Integer attributeId;
      /*  @SerializedName("created")
        @Expose
        private BigInteger created;*/
        @SerializedName("isMandatory")
        @Expose
        private Integer isMandatory;
        @SerializedName("name")
        @Expose
        private String name;
       /* @SerializedName("updated")
        @Expose
        private BigInteger updated;*/

        public Integer getAttributeId() {
            return attributeId;
        }

        public void setAttributeId(Integer attributeId) {
            this.attributeId = attributeId;
        }

       /* public BigInteger getCreated() {
            return created;
        }

        public void setCreated(BigInteger created) {
            this.created = created;
        }*/

        public Integer getIsMandatory() {
            return isMandatory;
        }

        public void setIsMandatory(Integer isMandatory) {
            this.isMandatory = isMandatory;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

      /*  public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

    }

    public class Brand {

        @SerializedName("brandId")
        @Expose
        private Integer brandId;
        @SerializedName("active")
        @Expose
        private Integer active;
       /* @SerializedName("createdAt")
        @Expose
        private String createdAt;*/
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("name")
        @Expose
        private String name;
        /*@SerializedName("updated")
        @Expose
        private BigInteger updated;*/

        public Integer getBrandId() {
            return brandId;
        }

        public void setBrandId(Integer brandId) {
            this.brandId = brandId;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

      /*  public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }*/

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

       /* public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

    }

    public class CategoryModel {

        @SerializedName("categoryId")
        @Expose
        private Integer categoryId;
        @SerializedName("active")
        @Expose
        private Object active;
        @SerializedName("categoryName")
        @Expose
        private String categoryName;
       /* @SerializedName("createdAt")
        @Expose
        private String createdAt;*/
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
       /* @SerializedName("updated")
        @Expose
        private BigInteger updated;*/
        @SerializedName("attributes")
        @Expose
        private List<Attribute> attributes = null;
        @SerializedName("brands")
        @Expose
        private List<Brand> brands = null;

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public Object getActive() {
            return active;
        }

        public void setActive(Object active) {
            this.active = active;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

      /*  public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }*/

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

       /* public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

        public List<Attribute> getAttributes() {
            return attributes;
        }

        public void setAttributes(List<Attribute> attributes) {
            this.attributes = attributes;
        }

        public List<Brand> getBrands() {
            return brands;
        }

        public void setBrands(List<Brand> brands) {
            this.brands = brands;
        }
    }

}
