package com.skilledanswers.picnart.e_commerce_module.retofit;


import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.EcommerceCategoriesResponse;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.GetDeliveryAddressResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.AllProductsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.AllReviewsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.BannersResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.DisplayProductResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.FilterResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.GroupDetailsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.LoadCartItemsModel;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.MyOrdersResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.OrderHistoryResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.ProductDescriptionResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.SearchResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.SimilarProductsResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.StateResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.UpdateCartPostResponse;

import org.json.JSONObject;

import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place.PlaceOrderResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface EcommerceAPIInterface {
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> checkForAppUpdates(@Field("action") String action, @Field("version") String versionName);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> performLogin(@Field("action") String action, @Field("bvplID") String login_id,
                                    @Field("password") String login_password);

    @POST(".")
    @FormUrlEncoded
    Call<EcommerceCategoriesResponse> getEcommerceCategories(@Field("action") String actionForCategoryLoad);

    @POST(".")
    @FormUrlEncoded
    Call<AllProductsResponse> getAllProducts(@Field("action") String action, @Field("categoryId") int categoryId,
                                             @Field("offset") int offset,
                                             @Field("requestLimit") int maxReq,
                                             @Field("filterArray") JSONObject filter);

    @POST(".")
    @FormUrlEncoded
    Call<ProductDescriptionResponse> getProductFullDetails(@Field("action") String action, @Field("ProductID") int productId);

    @POST(".")
    @FormUrlEncoded
    Call<DisplayProductResponse> getProductDetailsUsingCode(@Field("action") String action, @Field("productCode") String productCode);

    @POST(".")
    @FormUrlEncoded
    Call<FilterResponse> fetchAllFilters(@Field("action") String action, @Field("categoryId") int categoryId);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> addToCart(@Field("action") String action, @Field("regNo") int regNum, @Field("token") String token, @Field("ProductID") int productId,
                                 @Field("quantity") int qty, @Field("productCode") String prodCode);

    @POST(".")
    @FormUrlEncoded
    Call<LoadCartItemsModel> loadCartItems(@Field("action") String action, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> getCartCount(@Field("action") String action, @Field("regNo") String regNum, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> removeProductFromCart(@Field("action") String action, @Field("cartId") String cartId, @Field("token") String token);


    @POST(".")
    @FormUrlEncoded
    Call<UpdateCartPostResponse> updateCart(@Field("action") String action, @Field("cartId") String cartId, @Field("quantity") String qty,
                                            @Field("regNo") String regNum,
                                            @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<GetDeliveryAddressResponse> getAllDeliveryAddress(@Field("action") String action,
                                                           @Field("regNo") String regNum,
                                                           @Field("token") String token);

    /*@POST(".")
    @FormUrlEncoded
    Call<PlaceOrderResponse> placeMyOrders(@Field("action") String action,
                                           @Field("regNo") String regNum,
                                           @Field("requestFrom") String reFrom,
                                           @Field("token") String token,
                                           @Field("shippingData") String shippingData,
                                           @Field("billingData") String billingData,
                                           @Field("redeem") String redeem,
                                           @Field("paymentMethod") String paymentMethod);*/

    @POST(".")
    @FormUrlEncoded
    Call<PlaceOrderResponse> placeMyOrders(@Field("action") String action,
                                           @Field("regNo") String regNum,
                                           @Field("requestFrom") String reFrom,
                                           @Field("token") String token,
                                           @Field("shippingData") String shippingData,
                                           @Field("billingData") String billingData,
                                           @Field("redeem") String redeem,
                                           @Field("groceryCard") String groceryCard,
                                           @Field("specialWallet") String specialWallet,
                                           @Field("cartType") String cartType,
                                           @Field("pinCodeCheckFlag") boolean pinCodeCheckFlag,
                                           @Field("paymentMethod") String paymentMethod);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> saveToMyAddresses(@Field("action") String action,
                                         @Field("regNo") String regNum,
                                         @Field("token") String token,
                                         @Field("shippingData") String shippingData,
                                         @Field("billingData") String billingData);

    @POST(".")
    @FormUrlEncoded
    Call<MyOrdersResponse> getMyOrders(@Field("action") String action,
                                       @Field("regNo") String regNum,
                                       @Field("token") String token, @Field("offset") int offset,
                                       @Field("requestLimit") int reqLimit, @Field("orderStatus") String orderStatus);


    @POST(".")
    @FormUrlEncoded
    Call<AllReviewsResponse> getReviewForTheProduct(@Field("action") String action,
                                                    @Field("productID") String prodId,
                                                    @Field("offset") int offset,
                                                    @Field("requestLimit") int reqLimit);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> saveRating(@Field("action") String action,
                                  @Field("productId") String prodId,
                                  @Field("regNo") String regNum,
                                  @Field("vote") String vote);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> saveReview(@Field("action") String action,
                                  @Field("ProductID") String prodId,
                                  @Field("regNo") String regNum, @Field("token") String token,
                                  @Field("review") String vote);

    @POST(".")
    @FormUrlEncoded
    Call<SearchResponse> searchProduct(@Field("action") String action,
                                       @Field("searchTerm") String prodId);


    @POST(".")
    @FormUrlEncoded
    Call<SimilarProductsResponse> getAllRelatedProducts(@Field("action") String action,
                                                        @Field("relatedProductID") String catId);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> getCartTotal(@Field("action") String action, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> cancelPlacedOrder(@Field("action") String action, @Field("token") String token,
                                         @Field("request") String reqFor, @Field("reason") String reason,
                                         @Field("subOrderId") String subOrderId, @Field("otherReason") String otherReason);

    @POST(".")
    @FormUrlEncoded
    Call<AllProductsResponse> getTopRatedProducts(@Field("action") String action, @Field("categoryId") String catId);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> handlePostPayment(@Field("action") String action, @Field("token") String token,
                                         @Field("regNo") String regNum,
                                         @Field("paymentGatewayData") JSONObject jsonObject, @Field("response_code") String resCode);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> isAvailableAtLocation(@Field("action") String action, @Field("pin") String pinCode);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> getWalletAmt(@Field("action") String action, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> removeUserAddress(@Field("action") String action, @Field("token") String token,
                                         @Field("addressId") String addressId);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> getDashBoardDetailsForProfile(@Field("action") String action, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<GroupDetailsResponse> getMyGroupsForProfile(@Field("action") String action, @Field("token") String token,
                                                     @Field("regno") String regNum);

    @POST(".")
    @FormUrlEncoded
    Call<BannersResponse> getBannersForEcommerce(@Field("action") String action, @Field("type") String indexType);

    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> getProfitCardDetails(@Field("action") String action, @Field("token") String token);

    @POST(".")
    @FormUrlEncoded
    Call<OrderHistoryResponse> getOrderHistory(@Field("action") String action, @Field("token") String token);

    //added by Jasmini on 29th dec 2017
    @FormUrlEncoded
    @POST(".")
    Call<ResponseBody> getDailyReports(@Field("action") String action, @Field("token") String token);


    //    Fetch all states
    @FormUrlEncoded
    @POST(".")
    Call<StateResponse> getAllStates(@Field("action") String action);

    //Fetch districts by state

    @FormUrlEncoded
    @POST(".")
    Call<ResponseBody> getDistrictsOfState(@Field("action") String action,
                                           @Field("state_code") String stateCode);
}
