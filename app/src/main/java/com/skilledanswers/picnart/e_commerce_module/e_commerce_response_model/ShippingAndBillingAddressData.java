package com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShippingAndBillingAddressData implements Parcelable{
    transient private boolean isSelected;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public static Creator<ShippingAndBillingAddressData> getCREATOR() {
        return CREATOR;
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("regNo")
    @Expose
    private String regNo;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("district")
    @Expose
    private String district;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("pin")
    @Expose
    private Integer pin;
    @SerializedName("landmark")
    @Expose
    private String landmark;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("billingName")
    @Expose
    private String billingName;
    @SerializedName("billingEmail")
    @Expose
    private String billingEmail;
    @SerializedName("billingAddress")
    @Expose
    private String billingAddress;
    @SerializedName("billingCity")
    @Expose
    private String billingCity;
    @SerializedName("billingDistrict")
    @Expose
    private String billingDistrict;
    @SerializedName("billingState")
    @Expose
    private String billingState;
    @SerializedName("billingPin")
    @Expose
    private Integer billingPin;
    @SerializedName("billingLandmark")
    @Expose
    private String billingLandmark;
    @SerializedName("billingMobile")
    @Expose
    private String billingMobile;
    @SerializedName("billingCompany")
    @Expose
    private String billingCompany;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("displayFlag")
    @Expose
    private String displayFlag;

    protected ShippingAndBillingAddressData(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        regNo = in.readString();
        name = in.readString();
        email = in.readString();
        address = in.readString();
        city = in.readString();
        district = in.readString();
        state = in.readString();
        if (in.readByte() == 0) {
            pin = null;
        } else {
            pin = in.readInt();
        }
        landmark = in.readString();
        mobile = in.readString();
        company = in.readString();
        billingName = in.readString();
        billingEmail = in.readString();
        billingAddress = in.readString();
        billingCity = in.readString();
        billingDistrict = in.readString();
        billingState = in.readString();
        if (in.readByte() == 0) {
            billingPin = null;
        } else {
            billingPin = in.readInt();
        }
        billingLandmark = in.readString();
        billingMobile = in.readString();
        billingCompany = in.readString();
        created = in.readString();
        updated = in.readString();
        displayFlag = in.readString();
    }

    public static final Creator<ShippingAndBillingAddressData> CREATOR = new Creator<ShippingAndBillingAddressData>() {
        @Override
        public ShippingAndBillingAddressData createFromParcel(Parcel in) {
            return new ShippingAndBillingAddressData(in);
        }

        @Override
        public ShippingAndBillingAddressData[] newArray(int size) {
            return new ShippingAndBillingAddressData[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getBillingName() {
        return billingName;
    }

    public void setBillingName(String billingName) {
        this.billingName = billingName;
    }

    public String getBillingEmail() {
        return billingEmail;
    }

    public void setBillingEmail(String billingEmail) {
        this.billingEmail = billingEmail;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBillingCity() {
        return billingCity;
    }

    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    public String getBillingDistrict() {
        return billingDistrict;
    }

    public void setBillingDistrict(String billingDistrict) {
        this.billingDistrict = billingDistrict;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public Integer getBillingPin() {
        return billingPin;
    }

    public void setBillingPin(Integer billingPin) {
        this.billingPin = billingPin;
    }

    public String getBillingLandmark() {
        return billingLandmark;
    }

    public void setBillingLandmark(String billingLandmark) {
        this.billingLandmark = billingLandmark;
    }

    public String getBillingMobile() {
        return billingMobile;
    }

    public void setBillingMobile(String billingMobile) {
        this.billingMobile = billingMobile;
    }

    public String getBillingCompany() {
        return billingCompany;
    }

    public void setBillingCompany(String billingCompany) {
        this.billingCompany = billingCompany;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public String getDisplayFlag() {
        return displayFlag;
    }

    public void setDisplayFlag(String displayFlag) {
        this.displayFlag = displayFlag;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(regNo);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(district);
        dest.writeString(state);
        if (pin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pin);
        }
        dest.writeString(landmark);
        dest.writeString(mobile);
        dest.writeString(company);
        dest.writeString(billingName);
        dest.writeString(billingEmail);
        dest.writeString(billingAddress);
        dest.writeString(billingCity);
        dest.writeString(billingDistrict);
        dest.writeString(billingState);
        if (billingPin == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(billingPin);
        }
        dest.writeString(billingLandmark);
        dest.writeString(billingMobile);
        dest.writeString(billingCompany);
        dest.writeString(created);
        dest.writeString(updated);
        dest.writeString(displayFlag);
    }
}
