package bluepaymax.skilledanswers.picnart.e_commerce_module.place_order;

public interface BackPressedFromProceedOrder {
    void onChangeAddress(int addressID);
}
