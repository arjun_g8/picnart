package com.skilledanswers.picnart.e_commerce_module;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import bluepaymax.skilledanswers.picnart.LoginActivity;
import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.CommonUtilsEcommerce;
import bluepaymax.skilledanswers.picnart.Utils.DialogUtils;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;
import bluepaymax.skilledanswers.picnart.Utils.TouchEffect;
import bluepaymax.skilledanswers.picnart.e_commerce_module.backgroundTask.DownloadResultReceiver;
import bluepaymax.skilledanswers.picnart.e_commerce_module.backgroundTask.DownloadService;
import bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities.EcomMainActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.flow_activities.NoInternetActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.AboutUsFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.DetailFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.MyCart;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.MyOrders;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.SearchFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.ShopByCategoryFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.UserProfileFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EcommerceCustomActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener,
        DownloadResultReceiver.Receiver, DetailFragment.OnFragmentInteractionListener {

    public static final TouchEffect TOUCH = new TouchEffect();
    DownloadResultReceiver receiver;
    ActionBarDrawerToggle toggle;
    FragmentManager fragmentManager;
    private boolean settoggle = true;
    private HashMap<String, String> action;
    private CoordinatorLayout container;
    protected Toolbar toolbar;
    private DrawerLayout drawer;
    public int count;
    private AlertDialog myalertDialog = null;
    private TextView cart_count_text, notification_count_text;
    public boolean isShowMenuItem = false;
    View cart;
    public MenuItem cartCountMenuItem, walletMenuItem, searchMenuItem, notificationItem;
    private PrefUtil prefUtil;
    private EcomPrefUtil ecomPrefUtil;
    // private List<CartItemsResponseModel.CartProductData> cartItemsList = new ArrayList<>();
    private AppCompatTextView titleTV, searchTV;
    private AppCompatImageView toolbarLogoIV;
    public LinearLayout searchLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("------ ON CRAETE ECOM");

        setContentView(R.layout.activity_custom_ecommerce);
        container = findViewById(R.id.relative);
        toolbar = findViewById(R.id.toolbar_custom);
        setSupportActionBar(toolbar);
        assert toolbar != null;
        toolbarLogoIV = toolbar.findViewById(R.id.home_logo);

        searchLayout = findViewById(R.id.search_bar_view);

        /*toolbar.findViewById(R.id.home_logo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                Intent intent = new Intent(getApplicationContext(), EcomMainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.putExtra("EXIT", true);
                startActivity(intent);


            }
        });*/

        titleTV = toolbar.findViewById(R.id.title_tv);
        searchTV = findViewById(R.id.search_tv_app_bar);
        searchTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_gray, 0, 0, 0);
        searchTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Intent intent = new Intent(EcommerceCustomActivity.this, SearchActivity.class);
                startActivity(intent);
                finish();*/
                moveToFragmentNoStack(new SearchFragment(), "NEW_SEARCH");
            }
        });

        prefUtil = new PrefUtil(this);
        ecomPrefUtil = new bluepaymax.skilledanswers.picnart.e_commerce_module.EcomPrefUtil(this);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setUpNavigationCustomDrawer();
    }

    private void setUpNavigationCustomDrawer() {
        drawer = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.nav_view);
        navigationView.findViewById(R.id.home_nav_tv).setOnClickListener(this);
        navigationView.findViewById(R.id.shop_by_category_nav_tv).setOnClickListener(this);
        navigationView.findViewById(R.id.my_orders_nav_tv).setOnClickListener(this);
        navigationView.findViewById(R.id.about_us_nav_tv).setOnClickListener(this);

        AppCompatTextView homeTV = navigationView.findViewById(R.id.home_nav_tv);
        AppCompatTextView shopByCategoryTV = navigationView.findViewById(R.id.shop_by_category_nav_tv);
        AppCompatTextView myOrdersTV = navigationView.findViewById(R.id.my_orders_nav_tv);
        AppCompatTextView aboutUsTV = navigationView.findViewById(R.id.about_us_nav_tv);

        homeTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_home, 0, 0, 0);
        shopByCategoryTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_bag, 0, 0, 0);
        myOrdersTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_order, 0, 0, 0);
        aboutUsTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_about, 0, 0, 0);

        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        setToggle(true);
    }

    private NavigationView navigationView;

    public void moveToFragmentNoStack(Fragment toFragment, String TAG) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            // replaceFragment(toFragment);
            System.out.println("*** ALREADY there ****");
        } else {
            transaction.setCustomAnimations(R.anim.left_to_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            transaction.add(R.id.relative, toFragment, TAG);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }

    public void setAppToolBarTitle(String title) {
        titleTV = toolbar.findViewById(R.id.title_tv);
        titleTV.setText(title);
        titleTV.setVisibility(View.VISIBLE);
        titleTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
    }

    public void hideToolBarLogo() {
        toolbarLogoIV = toolbar.findViewById(R.id.home_logo);
        toolbarLogoIV.setVisibility(View.GONE);
        titleTV.setVisibility(View.GONE);

        System.out.println("------------ IS LOGO VISIBLE: " + toolbarLogoIV.isShown());
    }

    public void hideToolBarTitleAndShowLogo() {
        titleTV.setVisibility(View.VISIBLE);
        // toolbarLogoIV.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        /*hideToolBarTitleAndShowLogo();
        showSearchBar();

        System.out.println("------ ON RESUME FRAG ECOM");

        setAppToolBarTitle("Bluepaymax");*/
    }

    public void hideSearchBar() {
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
        toolbar.setLayoutParams(params);
        searchLayout.setVisibility(View.GONE);
    }

    public void showSearchBar() {
        AppBarLayout.LayoutParams params =
                (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL
                | AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS);
        toolbar.setLayoutParams(params);
        searchLayout = findViewById(R.id.search_bar_view);
        searchLayout.setVisibility(View.VISIBLE);
    }

    public void setToggle(
            boolean isEnabled) {

        if (isEnabled) {

            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            hideToolBarTitleAndShowLogo();
            toggle.setDrawerIndicatorEnabled(true);
            toggle.syncState();

        } else {
            // drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);
            toggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                    // toolbar.setLogo(R.mipmap.sangeethaluncher);
                }
            });
            toggle.syncState();
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            System.out.println("----------------COUNT BACK STACK: " + getSupportFragmentManager().getBackStackEntryCount());
            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                System.out.println("---------------- NO PREV FRAG");
                showSearchBar();
                hideToolBarTitleAndShowLogo();
                // titleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_only_logo, 0, 0, 0);
                setAppToolBarTitle("BLUEPAY MAX");
                //titleTV.setTypeface(titleTV.getTypeface(), Typeface.BOLD_ITALIC);
                /*Typeface.createFromAsset(getApplicationContext().getAssets(),
                        "font/xerox_font.ttf");*/
                hideToolBarTitleAndShowLogo();
                if (prefUtil.getSearchStatus()) {
                    getSupportFragmentManager().popBackStack();
                } else {

                }
                showSearchBar();
                hideToolBarTitleAndShowLogo();
                setToggle(true);

                setAppToolBarTitle("BLUEPAY MAX");
                //titleTV.setTypeface(titleTV.getTypeface(), Typeface.BOLD_ITALIC);
                getSupportFragmentManager().popBackStack();

                hideSoftKeyBoard();

            } else {
               /* boolean isClosedFromPay = ecomPrefUtil.getCloseStatus();

                Log.e("IS_FROM_PAY", "status: " + isClosedFromPay);

                if (isClosedFromPay)*/
                super.onBackPressed();
         /*       else {
                    startActivity(new Intent(this, LandingScreenActivity.class));
                    finish();
                }*/
            }

        }
    }

    private void hideSoftKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Check whether if view there or not
         */
        if (focusedView != null) {
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public void updateCartCount(int count) {
        //getAllCartItems();
//        this.count = count;
        // prefUtil.setCartCount(count);
        Log.e("CART COUNT: ", "" + count);
        cart_count_text.setText(String.valueOf(count));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_ecommerce, menu);

        MenuItem notification = menu.findItem(R.id.action_notification);
        notificationItem = notification;
//        MenuItem cart_count = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(notification, R.layout.action_bar_notification_ecommerce);


        notification.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  moveToFragment(new Notifications(), "NOTIFICATION_FRAGMENT");
            }
        });

        notification.setVisible(false);


        View notification_count = notification.getActionView();
        notification_count_text = notification_count.findViewById(R.id.hotlist_notificationtext);

        cartCountMenuItem = menu.findItem(R.id.action_cart);
        MenuItemCompat.setActionView(cartCountMenuItem, R.layout.cart_count);
        cart = cartCountMenuItem.getActionView();
        cart_count_text = cart.findViewById(R.id.cart_count_text);

        if (CommonUtilsEcommerce.isThereInternet(this)) {
            getCartCount();
            //updateCartCount(cartItemsList.size());
        } else DialogUtils.showNoInternetDialog(this);


        cartCountMenuItem.getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CommonUtils.isThereInternet(EcommerceCustomActivity.this)) {
                    moveToFragment(new MyCart(), "CART_FRAGMENT");
                } else {
                    Intent intent = new Intent(EcommerceCustomActivity.this, NoInternetActivity.class);
                    startActivity(intent);
                }

            }
        });

        walletMenuItem = menu.findItem(R.id.action_notification);
        searchMenuItem = menu.findItem(R.id.action_search);

        if (isShowMenuItem)
            setActionItemForE_Commerce(true);
        else
            setActionItemForE_Commerce(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_myaccount) {
            // Handle the camera action
            if (CommonUtilsEcommerce.isThereInternet(this)) {
                moveToFragment(new UserProfileFragment(), "PROFILE_FRAGMENT");
            } else
                DialogUtils.showNoInternetDialog(this);
            return true;
        }
/*        else if (id == R.id.action_myorders) {
            fragmentTransition(new MyOrders());
            return true;
        } else if (id == R.id.action_wishlist) {
            if (CommonUtilsEcommerce.isThereInternet(this))
                fragmentTransition(new Wishlist());
            else
                DialogUtils.showNoInternetDialog(this);
            return true;

        }*/
        else if (id == R.id.action_rateapp) {
            return true;

        } else if (id == R.id.action_help_center) {
            return true;

        } else if (id == R.id.share_drawer) {
            // fragmentTransition(new MyOrders());
            return true;
        }

        /*else if (id == R.id.action_legal) {
            return true;

        }*/
       /* else if (id == R.id.action_cart) {
           // Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_LONG).show();
            moveToFragment(new MyCart(), "MY_CART_FRAGMENT");
            return true;
        }*/
        /*else if (id == R.id.action_notification) {
            return true;

        }*/
        else if (id == R.id.action_search) {
          /*  Intent intent = new Intent(EcommerceCustomActivity.this, SearchActivity.class);
            startActivity(intent);
            finish();*/

            moveToFragmentNoStack(new SearchFragment(), "NEW_SEARCH");

            // moveToFragment(new SearchFragment(), "NEW_SEARCH");
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void refreshHomeScreen() {
        if (CommonUtils.isThereInternet(this)) {
            Intent intent = new Intent(getApplicationContext(), EcomMainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            Intent intent = new Intent(EcommerceCustomActivity.this, NoInternetActivity.class);
            startActivity(intent);
        }
    }

    private void gotoMyOrders() {
        if (CommonUtils.isThereInternet(this)) {
            moveToFragment(new MyOrders(), "MY_ORDERS_FRAGMENT");
        } else {
            Intent intent = new Intent(EcommerceCustomActivity.this, NoInternetActivity.class);
            startActivity(intent);
        }
    }

    private void showAboutUs() {
        moveToFragment(new AboutUsFragment(), "ABOUT_US_FRAGMENT");
    }

    private void goForShopByCategory() {
        Bundle bundle = new Bundle();

        if (CommonUtils.isThereInternet(this)) {
            bundle.putString("CLICKED_FOR", "CATEGORY");
            moveToFragment(new ShopByCategoryFragment(), "SHOP_BY_CATEGORY_FRAGMENT", bundle);
        } else {
            Intent intent = new Intent(EcommerceCustomActivity.this, NoInternetActivity.class);
            startActivity(intent);
        }

    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        return true;
    }

    public void closeNavDrawer() {
        drawer.closeDrawer(GravityCompat.START);
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
       /* if (v.getId() == R.id.fabCart) {
            Toast.makeText(getApplicationContext(), "added to cart", Toast.LENGTH_LONG).show();
        }*/
        switch (v.getId()) {
            case R.id.home_nav_tv:
                closeNavDrawer();
                refreshHomeScreen();
                break;
            case R.id.my_orders_nav_tv:
                closeNavDrawer();
                gotoMyOrders();
                break;
            case R.id.about_us_nav_tv:
                closeNavDrawer();
                showAboutUs();
                break;
            case R.id.shop_by_category_nav_tv:
                closeNavDrawer();
                goForShopByCategory();
                break;
        }

    }

    /**
     * Sets the touch and click listeners for a view..
     *
     * @param id the id of View
     * @return the view
     */
    public View setTouchNClick(int id) {

        View v = setClick(id);
        // v.setOnTouchListener(TOUCH);
        return v;
    }

    /**
     * Sets the click listener for a view.
     *
     * @param id the id of View
     * @return the view
     */
    public View setClick(int id) {

        View v = findViewById(id);
        v.setOnClickListener(this);
        return v;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        switch (resultCode) {
            case DownloadService.STATUS_RUNNING:
              /*  progressDialog.setMessage("Loading...");
                progressDialog.show();*/
                break;
            case DownloadService.STATUS_FINISHED:
                /* Hide progress & extract result from bundle */
                //progressDialog.dismiss();
                displayDatas(resultData);
                break;
            case DownloadService.STATUS_ERROR:
                /* Handle the error */
                //progressDialog.dismiss();
                String error = resultData.getString(Intent.EXTRA_TEXT);
                //Log.e("error report",error);
                Toast.makeText(getApplicationContext(), "No internet conection try later", Toast.LENGTH_LONG).show();
                // Toast.makeText(this, error, Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void displayDatas(Bundle resultData) {

    }

    public void moveToFragment(Fragment toFragment, String TAG) {
        hideSearchBar();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            replaceFragment(toFragment);
            System.out.println("*** ALREADY there ****");
        } else {
           /* if (TAG.equals("APPLY_FILTER_FRAGMENT")) {
                transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                transaction.replace(R.id.relative, toFragment, TAG);
                transaction.commit();
            } else {*/

//           , R.anim.enter_from_right, R.anim.exit_to_left

            transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
            transaction.replace(R.id.relative, toFragment, TAG);
            transaction.addToBackStack(null).commit();
            //  }
        }

    }

    public void addFragment(Fragment toFragment, String TAG) {
        hideSearchBar();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            System.out.println("*** ALREADY there ****");
        } else {
            transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
            transaction.add(R.id.relative, toFragment, TAG);
            transaction.addToBackStack(null).commit();
        }
    }

    public void replaceFragment(Fragment frag) {
        hideSearchBar();
        FragmentManager manager = getSupportFragmentManager();
        if (manager != null) {
            FragmentTransaction t = manager.beginTransaction();
            Fragment currentFrag = manager.findFragmentById(R.id.relative);

            //Check if the new Fragment is the same
            //If it is, don't add to the back stack
            if (currentFrag != null && currentFrag.getClass().equals(frag.getClass())) {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            } else {
                t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            }
        }
    }

    public interface OnBackPressedListener {

        /**
         * Callback, which is called if the Back Button is pressed.
         * Fragments that extend MainFragment can/should override this Method.
         *
         * @return true if the App can be closed, false otherwise
         */
        boolean onBackPressed();
    }


    public void moveToFragment(Fragment toFragment, String TAG, Bundle bundle) {
        hideSearchBar();
        System.out.println("------- INSIDE MOVE TO FRAGMENT");
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        toFragment.setArguments(bundle);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            System.out.println("*** ALREADY there ****");
        } else {
            transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);
            //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.replace(R.id.relative, toFragment, TAG);
            transaction.addToBackStack(null).commit();
        }
        System.out.println("-------- DONE MOVE TO FRAGMENT");
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void setActionItemForE_Commerce(boolean action) {
        walletMenuItem.setVisible(action);
        cartCountMenuItem.setVisible(action);
        notificationItem.setVisible(false);
        //searchMenuItem.setVisible(action);
    }

    private void getCartCount() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getCartCount("countCartItems", prefUtil.getRegNo(), prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String cartCountResponseString = new String(response.body().bytes());

                        JSONObject cartCountJsonObject = new JSONObject(cartCountResponseString);
                        boolean status = cartCountJsonObject.getBoolean("status");

                        if (status) {
                            String countOfCart = cartCountJsonObject.getString("count");
                            Log.e("CART COUNT: ", countOfCart);

                            count = Integer.parseInt(countOfCart);
                            ecomPrefUtil.setCartCount(count);
                            System.out.println("--- CURRENT COUNT: " + count);
                            updateCartCount(count);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        showSearchBar();
        hideToolBarTitleAndShowLogo();
        setAppToolBarTitle("BLUEPAY MAX");
        // titleTV.setTypeface(titleTV.getTypeface(), Typeface.BOLD_ITALIC);

        System.out.println("------ ON RESUME ECOM");
    }

    public void logoutOfThisApp() {
        prefUtil.clearPref();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
