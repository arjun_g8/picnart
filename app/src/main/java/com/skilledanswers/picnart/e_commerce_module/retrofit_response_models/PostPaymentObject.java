package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

/**
 * Created by SkilledAnswers-D1 on 12-01-2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostPaymentObject {

    @SerializedName("address_line_1")
    @Expose
    private String addressLine1;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("return_url")
    @Expose
    private String returnUrl;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("udf1")
    @Expose
    private String udf1;
    @SerializedName("zip_code")
    @Expose
    private String zipCode;
    @SerializedName("payment_datetime")
    @Expose
    private String paymentDatetime;
    @SerializedName("response_message")
    @Expose
    private String responseMessage;
    @SerializedName("response_code")
    @Expose
    private String responseCode;
    @SerializedName("transaction_id")
    @Expose
    private String transactionId;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("hash")
    @Expose
    private String hash;

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPaymentDatetime() {
        return paymentDatetime;
    }

    public void setPaymentDatetime(String paymentDatetime) {
        this.paymentDatetime = paymentDatetime;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return "PostPaymentObject{" +
                "addressLine1='" + addressLine1 + '\'' +
                ", amount='" + amount + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", currency='" + currency + '\'' +
                ", description='" + description + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", orderId='" + orderId + '\'' +
                ", phone='" + phone + '\'' +
                ", returnUrl='" + returnUrl + '\'' +
                ", state='" + state + '\'' +
                ", udf1='" + udf1 + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", paymentDatetime='" + paymentDatetime + '\'' +
                ", responseMessage='" + responseMessage + '\'' +
                ", responseCode='" + responseCode + '\'' +
                ", transactionId='" + transactionId + '\'' +
                ", paymentMethod='" + paymentMethod + '\'' +
                ", hash='" + hash + '\'' +
                '}';
    }
}
