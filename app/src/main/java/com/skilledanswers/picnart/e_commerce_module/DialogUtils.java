package com.skilledanswers.picnart.e_commerce_module;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import bluepaymax.skilledanswers.picnart.LoginActivity;
import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;


/**
 * Created by SkilledAnswers-D1 on 24-10-2017.
 */

public class DialogUtils {
    public static void showNoInternetDialog(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.no_internet_dialog);
        dialog.findViewById(R.id.go_to_settings_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                context.startActivity(intent);
                dialog.dismiss();
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void showEnableGpsDialog(final Context context) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("GPS");
        alertDialog.setMessage("Enable GPS to get location updates");
        alertDialog.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(intent);
            }

        });
        alertDialog.show();
    }

    public static void showCustomPDialog(Dialog customPDialog, String msg) {
        try {
            customPDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customPDialog.setContentView(R.layout.custom_progress_dialog);
            customPDialog.setTitle(msg);

            AppCompatTextView text = customPDialog.findViewById(R.id.pd_text);
            text.setText(msg);
            ProgressBar prog = customPDialog.findViewById(R.id.pd_p_bar);
            prog.setIndeterminate(true);
            customPDialog.setCancelable(false);

            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            lp.copyFrom(customPDialog.getWindow().getAttributes());
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            customPDialog.show();
            customPDialog.getWindow().setAttributes(lp);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showReLoginPopUp(final Context context) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.re_login_popup);
        dialog.setCancelable(false);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();

       /* WindowManager.LayoutParams newLp = dialog.getWindow().getAttributes();
        if (newLp != null)*/
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.findViewById(R.id.login_btn_in_popup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Toast.makeText(context, "Please Login", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        PrefUtil prefUtil = new PrefUtil(context);
        prefUtil.clearPref();

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    public static void dismissCustomPDialog(Dialog progressDialog) {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public static void showCustomToast(Context context, String msg) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert layoutInflater != null;
        View toastView = layoutInflater.inflate(R.layout.custom_toast_view, null);

        AppCompatTextView toastTV = toastView.findViewById(R.id.toast_tv);
        toastTV.setText(msg);

        final Toast toast = new Toast(context.getApplicationContext());

        toast.setView(toastView);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 1000);
    }

}
