package com.skilledanswers.picnart.e_commerce_module;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by SkilledAnswers-D1 on 24-10-2017.
 */

public class CommonUtils {
    //**** SOME CONSTANTS HERE ***

   //This URL is now in LIVE
    public static final String BASE_URL = "https://www.bluepaymax.com/newsite/androidRequest.php/";

    //EBS now
    public static final String PAYMENT_GATEWAY_URL = "https://secure.ebs.in/pg/ma/payment/request";

    //OLD URL
//    "https://www.bluepaymax.com/new/androidRequest.php/"

    public static boolean isThereInternet(Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    public static boolean isGPSEnabled(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    public static boolean isLocationAccessPermissionGiven(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static String formatPriceByComma(String actualNum) {
        String formattedString;
        try {
             formattedString = actualNum.replace(".00", "");
        }
        catch (Exception e){
             formattedString = actualNum.replace(".0", "");
        }

        int someNumber = Integer.parseInt(formattedString);
        NumberFormat nf = NumberFormat.getInstance();
        return nf.format(someNumber);
    }

}
