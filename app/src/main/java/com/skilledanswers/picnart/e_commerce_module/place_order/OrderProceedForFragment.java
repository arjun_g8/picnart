package com.skilledanswers.picnart.e_commerce_module.place_order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import bluepaymax.skilledanswers.picnart.R;
import bluepaymax.skilledanswers.picnart.Utils.PrefUtil;
import bluepaymax.skilledanswers.picnart.e_commerce_module.CommonUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.DialogUtils;
import bluepaymax.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.adapter.GroceryCardAdapter;
import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place.EbsFormData;
import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place.FormData;
import bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place.PlaceOrderResponse;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.BillingDataPost;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.ShippingAddressData;
import bluepaymax.skilledanswers.picnart.e_commerce_module.payment.EbsPaymentActivity;
import bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.api_pojo.GroceryCardApplyResponse;
import bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.api_pojo.GroceryCardData;
import bluepaymax.skilledanswers.picnart.e_commerce_module.place_order.api_pojo.GroceryCardResponse;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retrofit_response_models.LoadCartItemsModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 12-01-2018.
 */

public class OrderProceedForFragment extends Fragment implements View.OnClickListener {
    private RecyclerView ordersForPayRV;
    private AppCompatTextView nameTv, mobTv, addressTv, payTv, walletAmtTV, discountTV, redeemMsgTV;
    private AppCompatCheckBox redeemFromWalletChBox;
    private ProgressBar redeemProgress;
    private List<LoadCartItemsModel.CartItemData> cartItemsList = new ArrayList<>();
    private PrefUtil prefUtil;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private String totalAmtToPay,
            actualWalletAmt = "", actualAmtToPay = "", actualDiscount = "0", redeemStatus = "0";
    private Bundle bundle;
    private ShippingAddressData shippingAddress;
    private BillingDataPost billingAddress;

    private RecyclerView groceryCardsRV;
    private View groceryCardView;
    private AppCompatTextView noGroceryCardTV, groceryCardApplyStatusTV;
    private ProgressBar groceryCardsLoading;
    private String rupeesSymbol;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.order_proceed_for_pay_view, container, false);

        setHasOptionsMenu(true);

        prefUtil = new PrefUtil(ecommerceCustomActivity);

        rupeesSymbol = getString(R.string.rs_symbol);

        groceryCardView = view.findViewById(R.id.grocery_card_view_id);

        groceryCardsRV = groceryCardView.findViewById(R.id.grocery_cards_rv);
        groceryCardsRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        groceryCardsRV.setHasFixedSize(true);

        groceryCardsLoading = groceryCardView.findViewById(R.id.grocery_card_loading_bar);
        noGroceryCardTV = groceryCardView.findViewById(R.id.no_grocery_card_tv);
        groceryCardApplyStatusTV = groceryCardView.findViewById(R.id.grocery_card_apply_tv);

        groceryCardsBeforeLoadView();

        redeemProgress = view.findViewById(R.id.redeem_p_bar);
        nameTv = view.findViewById(R.id.name_in_address_list_item);
        mobTv = view.findViewById(R.id.mob_in_address_list_item);
        addressTv = view.findViewById(R.id.address_in_address_list_item);
        walletAmtTV = view.findViewById(R.id.wallet_amount_tv_o_proceed);
        payTv = view.findViewById(R.id.pay_amount_tv_o_proceed);
        discountTV = view.findViewById(R.id.discount_tv_o_proceed);
        redeemMsgTV = view.findViewById(R.id.redeem_msg_tv);
        payTv.setOnClickListener(this);

        redeemFromWalletChBox = view.findViewById(R.id.redeem_from_wallet_ch_box);

        view.findViewById(R.id.change_addr_tv_o_proceed).setOnClickListener(this);
        view.findViewById(R.id.proceed_pay_tv).setOnClickListener(this);

        ordersForPayRV = view.findViewById(R.id.order_proceed_rv);
        ordersForPayRV.setLayoutManager(new LinearLayoutManager(ecommerceCustomActivity));
        ordersForPayRV.setHasFixedSize(true);

        bundle = getArguments();

        redeemFromWalletChBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
                        redeemProgress.setVisibility(View.VISIBLE);
                        redeemFromWallet();
                    } else {
                        DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                    }

                } else {
                    //Nothing to do

                    discountTV.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(actualDiscount));
                    payTv.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(actualAmtToPay));
                    walletAmtTV.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(actualWalletAmt));
                    redeemProgress.setVisibility(View.GONE);
                    redeemMsgTV.setVisibility(View.GONE);
                    redeemStatus = "0";
                }
            }
        });

        if (bundle != null) {
            shippingAddress = bundle.getParcelable("SHIPPING_DATA");
            billingAddress = bundle.getParcelable("BILLING_DATA");

            Log.e("SHIPPING_ID GOT: ", shippingAddress.getShippingId());

            nameTv.setText(shippingAddress.getName());
            mobTv.setText(shippingAddress.getMobile());
            addressTv.setText(shippingAddress.getAddress());
        }

        if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
            loadAllCartItems();
            getCartTotal();
            getWalletAmount();
        } else {
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
        }

        return view;
    }

    private boolean checkIfAnyGroceryItemsInCart() {
        pinCodeChecked = false;
        System.out.println("-------------- CART LIST: " + new Gson().toJson(cartItemsList));
        boolean isThereGroceryItem = false;
        for (int i = 0; i < cartItemsList.size(); i++) {
            if (cartItemsList.get(i).getProduct().getParentCategoryID() == 41) {
                isThereGroceryItem = true;
                pinCodeChecked = true;
                break;
            }
        }
        return isThereGroceryItem;
    }

    private void groceryCardsBeforeLoadView() {
        noGroceryCardTV.setVisibility(View.GONE);
        groceryCardsLoading.setVisibility(View.VISIBLE);
        groceryCardsRV.setVisibility(View.GONE);
        groceryCardApplyStatusTV.setVisibility(View.GONE);
    }

    private void groceryCardsAfterLoadView() {
        noGroceryCardTV.setVisibility(View.GONE);
        groceryCardsLoading.setVisibility(View.GONE);
        groceryCardsRV.setVisibility(View.VISIBLE);
        groceryCardApplyStatusTV.setVisibility(View.GONE);
    }

    private void noGroceryCardsView() {
        noGroceryCardTV.setVisibility(View.VISIBLE);
        groceryCardsLoading.setVisibility(View.GONE);
        groceryCardsRV.setVisibility(View.GONE);
        groceryCardApplyStatusTV.setVisibility(View.GONE);
    }

    private void loadGroceryCards() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getGroceryCards("getMyGroceryCards", prefUtil.getToken(), prefUtil.getRegNo()).enqueue(new Callback<GroceryCardResponse>() {
            @Override
            public void onResponse(Call<GroceryCardResponse> call, Response<GroceryCardResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            if (response.body().getData() != null) {
                                if (response.body().getData().size() > 0) {
                                    groceryCardsRV.setAdapter(new GroceryCardAdapter(getActivity(), response.body().getData(), new GroceryCardAdapter.GroceryCardSelectable() {
                                        @Override
                                        public void onGroceryCardSelected(GroceryCardData groceryCardData) {
                                            if (groceryCardData == null) {
                                                groceryCardApplyStatusTV.setVisibility(View.GONE);
                                                groceryCardID = "0";
                                            } else {
                                                groceryCardID = String.valueOf(groceryCardData.getId());
                                                applyGroceryCard(redeemStatus);
                                            }
                                            groceryCardsRV.getAdapter().notifyDataSetChanged();
                                        }
                                    }));
                                    groceryCardsAfterLoadView();
                                } else {
                                    noGroceryCardsView();
                                }
                            } else
                                noGroceryCardsView();

                        } else {
                            noGroceryCardsView();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GroceryCardResponse> call, Throwable t) {
                noGroceryCardsView();
            }
        });
    }

    private void applyGroceryCard(String walletRedeem) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);

        ecommerceAPIInterface.redeemFromGroceryCard("redeemFromGroceryCard", prefUtil.getToken(), prefUtil.getRegNo(),
                groceryCardID, "1", walletRedeem).enqueue(new Callback<GroceryCardApplyResponse>() {
            @Override
            public void onResponse(Call<GroceryCardApplyResponse> call, Response<GroceryCardApplyResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            String msg = String.valueOf(Html.fromHtml(response.body().getMessage()));
                            groceryCardApplyStatusTV.setText(msg);
                            groceryCardApplyStatusTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.green));
                            groceryCardApplyStatusTV.setVisibility(View.VISIBLE);
                            groceryCardsRV.getAdapter().notifyDataSetChanged();

                            String amountToPay = String.valueOf(Double.parseDouble(actualAmtToPay) - response.body().getDiscount());

                            payTv.setText(rupeesSymbol + " " + amountToPay);
                            discountTV.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(response.body().getDiscount().toString()));
                            redeemProgress.setVisibility(View.GONE);
                            redeemMsgTV.setVisibility(View.VISIBLE);
                            redeemMsgTV.setTextColor(ContextCompat.getColor(ecommerceCustomActivity, R.color.green));
                            redeemMsgTV.setText("Available for redeem from wallet");
                            redeemStatus = "1";
                        } else {
                            String msg = String.valueOf(Html.fromHtml(response.body().getMessage()));
                            groceryCardApplyStatusTV.setText(msg);
                            groceryCardApplyStatusTV.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_light));
                            groceryCardApplyStatusTV.setVisibility(View.VISIBLE);
                            groceryCardsRV.getAdapter().notifyDataSetChanged();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GroceryCardApplyResponse> call, Throwable t) {
                groceryCardApplyStatusTV.setVisibility(View.GONE);
            }
        });
    }

    private void getWalletAmount() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getWalletAmt("getWalletAmount", prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String walletResponseString = new String(response.body().bytes());
                            Log.e("Get Wallet: ", walletResponseString);
                            JSONObject responseObject = new JSONObject(walletResponseString);

                            if (responseObject.getBoolean("status")) {
                                JSONObject dataObj = responseObject.getJSONObject("data");
                                String walletAmt = dataObj.getString("wallet_amount");

                                int someNumber = Integer.parseInt(walletAmt);
                                NumberFormat nf = NumberFormat.getInstance();
                                String walletFormatted = nf.format(someNumber);
                                walletAmtTV.setText("Rs. " + walletFormatted);
                                actualWalletAmt = walletAmt;

                            } else {
                                Toast.makeText(ecommerceCustomActivity, "" + responseObject.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void redeemFromWallet() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);

        //Redeem new
        ecommerceAPIInterface.applyRedeem("walletRedeem", prefUtil.getToken(), prefUtil.getRegNo(), "1", groceryCardID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.e("Redeem Wallet: ", response.raw().toString());
                    if (response.body() != null) {
                        try {
                            String redeemWalletResponseString = new String(response.body().bytes());
                            JSONObject responseObject = new JSONObject(redeemWalletResponseString);
                            Log.e("Redeem Wallet String: ", redeemWalletResponseString);
                            if (responseObject.getBoolean("status")) {
                                JSONObject dataObj = responseObject.getJSONObject("data");
                                String walletAmt = dataObj.getString("walletAmount");
                                String amtToPay = dataObj.getString("payable");
                                String discount = dataObj.getString("discount");

                                walletAmtTV.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(walletAmt));
                                payTv.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(amtToPay));
                                discountTV.setText(rupeesSymbol + " " + CommonUtils.formatPriceByComma(discount));
                                redeemProgress.setVisibility(View.GONE);
                                redeemMsgTV.setVisibility(View.VISIBLE);
                                redeemMsgTV.setTextColor(ContextCompat.getColor(ecommerceCustomActivity, R.color.green));
                                redeemMsgTV.setText("Available for redeem from wallet");
                                redeemStatus = "1";

                            } else {
                                String msg = String.valueOf(Html.fromHtml(responseObject.getString("message")));
                                redeemMsgTV.setVisibility(View.VISIBLE);
                                redeemMsgTV.setTextColor(ContextCompat.getColor(ecommerceCustomActivity, R.color.dark_red));
                                redeemMsgTV.setText(msg);
                                redeemProgress.setVisibility(View.GONE);
                                redeemStatus = "0";
                                redeemFromWalletChBox.setChecked(false);
                                Toast.makeText(ecommerceCustomActivity, msg, Toast.LENGTH_LONG).show();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    redeemProgress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                redeemProgress.setVisibility(View.GONE);
                redeemMsgTV.setVisibility(View.GONE);
            }
        });
    }

    private String groceryCardID = "0";


    private void getCartTotal() {
        final EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getCartTotal("calculateCartTotal", prefUtil.getToken()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            String jsonResponseString = new String(response.body().bytes());
                            System.out.println("-----------CART TOTAL JSON: " + jsonResponseString);
                            JSONObject jsonObject = new JSONObject(jsonResponseString);

                            try {
                                String cartTotalPrice = jsonObject.getString("cartTotal");
                                if (cartTotalPrice == null)
                                    Toast.makeText(ecommerceCustomActivity, "Session Expired..Please login again!", Toast.LENGTH_SHORT).show();
                                else {
                                    totalAmtToPay = cartTotalPrice;
                                    actualAmtToPay = totalAmtToPay;

                                    payTv.setText("Rs. " + CommonUtils.formatPriceByComma(totalAmtToPay));
                                    System.out.println("------------CART TOTAL ODER VERIFY: " + cartTotalPrice);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.change_addr_tv_o_proceed:
                ecommerceCustomActivity.getSupportFragmentManager().popBackStack();
                break;
            case R.id.proceed_pay_tv:
                proceedToPayment();
                break;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.setToggle(false);
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("Verify Your Orders");
    }

    private void loadAllCartItems() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.loadCartItems("loadCartItems", prefUtil.getToken()).enqueue(new Callback<LoadCartItemsModel>() {
            @Override
            public void onResponse(Call<LoadCartItemsModel> call, Response<LoadCartItemsModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        cartItemsList.clear();
                        cartItemsList = response.body().getData();

                        ecommerceCustomActivity.updateCartCount(cartItemsList.size());
                        double totalAmt = 0, shippingCharge = 0, amount = 0;

                        if (cartItemsList.size() > 0) {
                            for (int i = 0; i < cartItemsList.size(); i++) {
                                shippingCharge += Double.parseDouble(cartItemsList.get(i).getStock().getShippingCharge());
                                amount += Double.parseDouble(cartItemsList.get(i).getStock().getMsp());

                                totalAmt = shippingCharge + amount;

                                ordersForPayRV.setAdapter(new OrderProceedProductsAdapter());
                                ordersForPayRV.getAdapter().notifyDataSetChanged();

                            }

                            if (checkIfAnyGroceryItemsInCart()) {
                                loadGroceryCards();
                            } else {
                                noGroceryCardsView();
                                noGroceryCardTV.setText("Grocery cards not applicable if there is no grocery items.");
                            }

                            System.out.println("------------ TOTAL AMOUNT CART: " + totalAmt);

                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<LoadCartItemsModel> call, Throwable t) {

            }
        });
    }

    private class OrderProceedProductsAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.oder_item_proceed, parent, false);
            return new OrderProceedProductsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            OrderProceedProductsHolder orderProceedProductsHolder = (OrderProceedProductsHolder) holder;

            Glide.with(ecommerceCustomActivity)
                    .load(new GlideUrl(cartItemsList.get(position).getProduct().getImage()))
                    .placeholder(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                    .error(ContextCompat.getDrawable(ecommerceCustomActivity, R.drawable.ic_def_pic))
                    .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .dontAnimate()
                    .into(orderProceedProductsHolder.proIV);

            orderProceedProductsHolder.proNameTV.setText(cartItemsList.get(position).getProduct().getBrand() +
                    " " +
                    cartItemsList.get(position).getProduct().getTitle());

            String msp = cartItemsList.get(position).getStock().getMsp().replace(".00", "");

            orderProceedProductsHolder.priceTV.setText("Rs. " + CommonUtils.formatPriceByComma(msp));
            orderProceedProductsHolder.qtyTV.setText("Qty: " + cartItemsList.get(position).getQuantity());
        }

        @Override
        public int getItemCount() {
            return cartItemsList.size();
        }
    }

    private class OrderProceedProductsHolder extends RecyclerView.ViewHolder {
        private AppCompatImageView proIV;
        private AppCompatTextView proNameTV, priceTV, qtyTV;

        OrderProceedProductsHolder(View itemView) {
            super(itemView);
            proIV = itemView.findViewById(R.id.img_order_item_proceed);
            proNameTV = itemView.findViewById(R.id.product_name_tv_order_item_proceed);
            priceTV = itemView.findViewById(R.id.order_item_price_tv);
            qtyTV = itemView.findViewById(R.id.order_item_qty_tv);
        }
    }

    private boolean pinCodeChecked;

    private void proceedToPayment() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.placeMyOrders("placeFinalOrder", prefUtil.getRegNo(),
                "app", prefUtil.getToken(), new Gson().toJson(shippingAddress), new Gson().toJson(billingAddress),
                redeemStatus, groceryCardID, "", "", pinCodeChecked, "ebs").enqueue(new Callback<PlaceOrderResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderResponse> call, Response<PlaceOrderResponse> response) {
                Log.e("POST_ADDRESS", response.raw().toString());
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        /*try {
                            String responseJson = new String(response.body().bytes());
                            JSONObject jsonObject = new JSONObject(responseJson);

                            System.out.println("-----------PLACE ORDER response: " + jsonObject.toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }*/

                        if (response.body().getStatus()) {
                            Toast.makeText(ecommerceCustomActivity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                            try {
                                FormData formData = response.body().getData().getFormData();
                                EbsFormData ebsFormData = new EbsFormData();
                                ebsFormData.setName(formData.getName());
                                ebsFormData.setPhone(formData.getPhone());
                                ebsFormData.setEmail(formData.getEmail());
                                ebsFormData.setAccountId(formData.getAccountId());
                                ebsFormData.setAddress(formData.getAddress());

                                ebsFormData.setChannel(formData.getChannel());
                                ebsFormData.setCountry(formData.getCountry());
                                ebsFormData.setCity(formData.getCity());
                                ebsFormData.setDescription(formData.getDescription());
                                ebsFormData.setDisplayCurrency(formData.getDisplayCurrency());

                                ebsFormData.setAmount(formData.getAmount());
                                ebsFormData.setMode(formData.getMode());
                                ebsFormData.setReferenceNo(formData.getReferenceNo());
                                ebsFormData.setReturnUrl(formData.getReturnUrl());
                                ebsFormData.setPaymentOption(formData.getPaymentOption());

                                ebsFormData.setSecureHash(formData.getSecureHash());
                                ebsFormData.setState(formData.getState());
                                ebsFormData.setPostalCode(formData.getPostalCode());
                                ebsFormData.setCurrency(formData.getCurrency());

                                System.out.println("--------ORDER FORM DATA POST: " + new Gson().toJson(ebsFormData));

                                Intent intent = new Intent(getActivity(), EbsPaymentActivity.class);
                                intent.putExtra("FORM_DATA", ebsFormData);
                                intent.putExtra("IS_FROM_RECHARGE", false);
                                startActivity(intent);
                            } catch (Exception e) {
                                e.printStackTrace();
                                System.out.println("-------Exc: " + e.toString());
                            }

                        } else {
                            Toast.makeText(ecommerceCustomActivity, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    System.out.println("--------UN SUCCESS ");
                }
            }

            @Override
            public void onFailure(Call<PlaceOrderResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("--------EXC: " + t.toString());
            }
        });
    }
}
