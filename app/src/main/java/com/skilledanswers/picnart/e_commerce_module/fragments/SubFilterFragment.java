package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.interfaces.FilterBrandInterface;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.FilterResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by SkilledAnswers-D1 on 26-12-2017.
 */

public class SubFilterFragment extends Fragment {
    private EcommerceCustomActivity ecommerceCustomActivity;
    private View applyFilterView;
    private RecyclerView filterRV;
    private List<FilterResponse.FilterData> filterItemList = new ArrayList<>();
    private List<FilterResponse.Value> filterValues = new ArrayList<>();
    private String attributeId = "", attributeName = "";
    private int pos, subCatId, catId;
    private ArrayList<String> subFilterList = new ArrayList<>();
    private String subCategoryName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        applyFilterView = inflater.inflate(R.layout.sub_filter_fragment, container, false);
        setHasOptionsMenu(true);

        attributeId = getArguments().getString("ATTRIBUTE_ID_FILTER");
        attributeName = getArguments().getString("ATTRIBUTE_NAME_FILTER");
        pos = getArguments().getInt("POSITION_SUB_FILTER");

        catId = getArguments().getInt("CATEGORY_ID_SUB_FILTER");
        subCatId = getArguments().getInt("SUB_CATEGORY_ID_SUB_FILTER");
        subCategoryName = getArguments().getString("SUB_CATEGORY_NAME_SUB_FILTER");

        filterRV = applyFilterView.findViewById(R.id.sub_filters_rv);
        filterRV.setLayoutManager(new LinearLayoutManager(ecommerceCustomActivity));
        filterRV.setHasFixedSize(true);

        applyFilterView.findViewById(R.id.done_btn_sub_filter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ecommerceCustomActivity, ApplyFilterFragment.class);
                intent.putStringArrayListExtra("SUB_FILTER_LIST", subFilterList);
                getTargetFragment().onActivityResult(getTargetRequestCode(), RESULT_OK, intent);
                getActivity().getSupportFragmentManager().popBackStack();
                //getFragmentManager().popBackStack();
            }
        });

        if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
            loadAllFiltersAvailable(subCatId);
        } else
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);

        return applyFilterView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();

        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle(attributeName);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(false);
        //menu.findItem(R.id.action_settings).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void loadAllFiltersAvailable(int categoryId) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.fetchAllFilters("loadFilters", categoryId).enqueue(new Callback<FilterResponse>() {
            @Override
            public void onResponse(Call<FilterResponse> call, Response<FilterResponse> response) {
                if (response.isSuccessful()) {

                    if (response.body().getStatus()) {
                        filterItemList.clear();
                        filterItemList = response.body().getData();

                        filterValues.clear();

                        filterValues.addAll(filterItemList.get(pos).getValue());

                        if (filterValues.size() > 0) {
                            filterRV.setAdapter(new FilterItemsAdapter());
                            //  System.out.println("-------sub filter Value: " + filterValues.get(0).getValue());
                        }
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<FilterResponse> call, Throwable t) {

            }
        });
    }

    private class FilterItemsAdapter extends RecyclerView.Adapter {
        FilterBrandInterface filterBrandInterface;

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.sub_filter_recycler_view_item, parent, false);
            return new FilterItemsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final FilterItemsHolder filterItemsHolder = (FilterItemsHolder) holder;
            filterItemsHolder.filterItemSelectedCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        filterValues.get(position).setChecked(true);
                        subFilterList.add(filterValues.get(position).getValue());
                    } else {
                        filterValues.get(position).setChecked(false);
                        try {
                            subFilterList.remove(filterValues.get(position).getValue());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            filterItemsHolder.filterItemSelectedCB.setChecked(filterValues.get(position).isChecked());

            filterItemsHolder.filterItemSelectedCB.setText(filterValues.get(position).getValue());
            // filterItemsHolder.filterItemTitleTV.setText(filterItemList.get(position).getName());
        }

        @Override
        public int getItemCount() {
            return filterValues.size();
        }
    }

    private class FilterItemsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView filterItemTitleTV;
        private LinearLayoutCompat filterItemMainView;
        private AppCompatCheckBox filterItemSelectedCB;

        FilterItemsHolder(View itemView) {
            super(itemView);
            // filterItemMainView = itemView.findViewById(R.id.sub_filter_item_main_id);
            //  filterItemTitleTV = itemView.findViewById(R.id.sub_filter_item_tv);
            filterItemSelectedCB = itemView.findViewById(R.id.sub_filter_item_cb);
            //  filterItemSelectedTV = (AppCompatTextView) itemView.findViewById(R.id.selected_filter_tv);
        }
    }

}
