package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.MyOrdersAdapter;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.MyOrdersResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 07-05-2016.
 */
public class MyOrders extends Fragment {
    EcommerceCustomActivity activity;
    private RecyclerView recyclerView = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private List<MyOrdersResponse.OrderedData> myOrderList = new ArrayList<>();
    private PrefUtil appUtil;
    private ProgressBar loadingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        activity = (EcommerceCustomActivity) getActivity();
        appUtil = new PrefUtil(activity);
        activity.setToggle(false);
        View view = inflater.inflate(R.layout.my_orders_fragment, container, false);
        loadingBar = view.findViewById(R.id.my_orders_prog_bar);
        loadingBar.setVisibility(View.VISIBLE);
        recyclerView = view.findViewById(R.id.my_orders_rv);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        if (CommonUtils.isThereInternet(activity)) {
            recyclerView.setVisibility(View.GONE);
            getAllOrders(null);
        } else {
            recyclerView.setVisibility(View.GONE);
            loadingBar.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(activity);
        }

        // Toolbar toolbar = (Toolbar)getActivity().findViewById(R.id.toolbar);
        //toolbar.setTitle("My Orders");
/*        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                OrderDetails orderDetails=new OrderDetails();
               activity.fragmentTransition(orderDetails);
            }
        }));*/
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();

    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity.hideToolBarTitleAndShowLogo();
    }

    private void getAllOrders(final String status) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getMyOrders("loadOrderedItems", appUtil.getRegNo(), appUtil.getToken(), 0, 1000, status).enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        myOrderList.clear();
                        myOrderList = response.body().getData();

                        if (myOrderList.size() > 0) {
                            System.out.println("----------- MY order list: " + myOrderList.size());
                            recyclerView.setAdapter(new MyOrdersAdapter(activity, myOrderList, new MyOrdersAdapter.OrderCancellable() {
                                @Override
                                public void onOrderCancelled() {
                                    getAllOrders(null);
                                }

                                @Override
                                public void onOrderItemClicked(MyOrdersResponse.OrderedData orderData) {
                                    moveToProductDetails(orderData);
                                }
                            }));
                            recyclerView.getAdapter().notifyDataSetChanged();
                        } else {
                            Toast.makeText(getActivity(), "No orders", Toast.LENGTH_SHORT).show();
                        }
                        // getAllOrders("0");

                        recyclerView.setVisibility(View.VISIBLE);
                        loadingBar.setVisibility(View.GONE);

                    } else {
                        recyclerView.setVisibility(View.GONE);
                        loadingBar.setVisibility(View.GONE);
                        if (!activity.isFinishing()) {
                            DialogUtils.showReLoginPopUp(getActivity());
                            Toast.makeText(getActivity(), "Something went wrong! Please login again", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    loadingBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
                recyclerView.setVisibility(View.GONE);
                loadingBar.setVisibility(View.GONE);
            }
        });

    }

    private void moveToProductDetails(MyOrdersResponse.OrderedData orderedData) {
        Fragment productOverViewFragment = new OverviewFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("SUB_CATEGORY_ID_FOR_OVERVIEW", orderedData.getCategoryId());
        bundle.putString("PRODUCT_ID", String.valueOf(orderedData.getProductId()));
        bundle.putString("PRODUCT_CODE", orderedData.getProductCode());
        bundle.putString("FOR", "LIVE");
        productOverViewFragment.setArguments(bundle);
        activity.moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideSearchBar();
        activity.hideToolBarLogo();
        activity.setAppToolBarTitle("My Orders");
    }
}
