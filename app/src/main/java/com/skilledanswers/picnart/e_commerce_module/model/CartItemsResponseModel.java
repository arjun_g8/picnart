package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 09-11-2017.
 */

public class CartItemsResponseModel {
    @SerializedName("data")
    @Expose
    private List<CartProductData> data = null;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;

    public List<CartProductData> getData() {
        return data;
    }

    public void setData(List<CartProductData> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }


    public class CartProductData {

        @SerializedName("cartId")
        @Expose
        private Integer cartId;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("productBean")
        @Expose
        private ProductBeanCart productBean;

        public Integer getCartId() {
            return cartId;
        }

        public void setCartId(Integer cartId) {
            this.cartId = cartId;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public ProductBeanCart getProductBean() {
            return productBean;
        }

        public void setProductBean(ProductBeanCart productBean) {
            this.productBean = productBean;
        }

    }


    public class ProductBeanCart {

        @SerializedName("productId")
        @Expose
        private Integer productId;
       /* @SerializedName("created")
        @Expose
        private BigInteger created;*/
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("image")
        @Expose
        private List<String> image = null;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("purchasePrice")
        @Expose
        private Integer purchasePrice;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("salePrice")
        @Expose
        private Integer salePrice;
       /* @SerializedName("updated")
        @Expose
        private BigInteger updated;*/
        @SerializedName("offers")
        @Expose
        private List<Object> offers = null;
        @SerializedName("productAttributs")
        @Expose
        private List<Object> productAttributs = null;

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        /*public BigInteger getCreated() {
            return created;
        }

        public void setCreated(BigInteger created) {
            this.created = created;
        }*/

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getImage() {
            return image;
        }

        public void setImage(List<String> image) {
            this.image = image;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getPurchasePrice() {
            return purchasePrice;
        }

        public void setPurchasePrice(Integer purchasePrice) {
            this.purchasePrice = purchasePrice;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public Integer getSalePrice() {
            return salePrice;
        }

        public void setSalePrice(Integer salePrice) {
            this.salePrice = salePrice;
        }

        /*public BigInteger getUpdated() {
            return updated;
        }

        public void setUpdated(BigInteger updated) {
            this.updated = updated;
        }*/

        public List<Object> getOffers() {
            return offers;
        }

        public void setOffers(List<Object> offers) {
            this.offers = offers;
        }

        public List<Object> getProductAttributs() {
            return productAttributs;
        }

        public void setProductAttributs(List<Object> productAttributs) {
            this.productAttributs = productAttributs;
        }

    }
}
