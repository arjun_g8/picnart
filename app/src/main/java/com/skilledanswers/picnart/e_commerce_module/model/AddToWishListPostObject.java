package com.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 10-11-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddToWishListPostObject {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("productBean")
    @Expose
    private ProductBeanPostWishList productBean;
    @SerializedName("user")
    @Expose
    private UserObjectPostWishList user;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductBeanPostWishList getProductBean() {
        return productBean;
    }

    public void setProductBean(ProductBeanPostWishList productBean) {
        this.productBean = productBean;
    }

    public UserObjectPostWishList getUser() {
        return user;
    }

    public void setUser(UserObjectPostWishList user) {
        this.user = user;
    }

    public static class ProductBeanPostWishList {

        @SerializedName("productId")
        @Expose
        private Integer productId;

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

    }

    public static class UserObjectPostWishList {

        @SerializedName("userId")
        @Expose
        private Integer userId;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

    }

}