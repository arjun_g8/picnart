package com.skilledanswers.picnart.e_commerce_module.flow_activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.adapter.BannerAdapter;
import com.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.EcommerceCategoriesResponse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.fragments.OverviewFragment;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retrofit_response_models.AllProductsResponse;
import bluepaymax.skilledanswers.picnart.e_commerce_module.retrofit_response_models.BannersResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EcomMainActivity extends EcommerceCustomActivity {

    ActionBar actionBar;
    int mCurrentPosition;
    int lastPageIndex = 6;

    private int delay = 3000; //milliseconds
    private ViewPager viewPager;
    private BannerAdapter bannerAdapter;
    List<EcommerceCategoriesResponse.CategoryData> categoriesList = new ArrayList<>();
    private EcommerceCustomActivity customActivity;
    private LinearLayoutCompat homeAppliancesTopView, kitchenAppliancesTopView, mobilesTopView, computersTopView,
            groceriesTopView;

    private LinearLayoutManager horizontalLayoutManager;

    private Handler handler;
    Runnable runnable;

    private RecyclerView categoriesRV, topRatedMobilesRV, topRatedLaptopsRV, topRatedAcRV, topRatedTVRV;
    private ProgressBar loadingPBarForCategory, loadingPBarForLaptopsTopRated, loadingBarMobilesTopRated,
            loadingBarACTopRated, loadingBarTVTopRated;

    private String prodCode;
    private boolean isAfterSearch;

    private List<AllProductsResponse.AllProductsData> topRatedProductMobilesList = new ArrayList<>();
    private List<AllProductsResponse.AllProductsData> topRatedProductLaptopsList = new ArrayList<>();
    private List<AllProductsResponse.AllProductsData> topRatedProductAcList = new ArrayList<>();
    private List<AllProductsResponse.AllProductsData> topRatedProductTvList = new ArrayList<>();

    private View topRatedMobilesDefaultView, topRatedAcDefaultView, topRatedTvDefaultView, topRatedLaptopsDefaultView;
    private CardView topRatedMobilesCV, topRatedAcCV, topRatedTvCV, topRatedLaptopsCV;

    private AppCompatImageView bannerDefaultView;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        CoordinatorLayout relativeLayout = findViewById(R.id.relative);
        getLayoutInflater().inflate(R.layout.activity_main_ecommerce, relativeLayout);

        handler = new Handler();

        isShowMenuItem = true;
        // setContentView(R.layout.activity_main_ecommerce);

        customActivity = new EcommerceCustomActivity();

        if (getIntent() != null) {
            isAfterSearch = getIntent().getBooleanExtra("IS_AFTER_SEARCH", false);
            prodCode = getIntent().getStringExtra("PRODUCT_CODE");

            if (isAfterSearch) {
                if (prodCode != null) {
                    Fragment productOverViewFragment = new OverviewFragment();
                    Bundle bundle = new Bundle();
                    System.out.println("------- PRODUCT code after search: " + prodCode);
                    // bundle.putString("PRODUCT_ID", allProducts.get(position).getId());
                    bundle.putString("PRODUCT_CODE", prodCode);
                    bundle.putString("FOR", "LIVE");
                    productOverViewFragment.setArguments(bundle);
                    moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
                }
            }
        }

        loadingBarACTopRated = findViewById(R.id.top_rated_ac_p_bar);
        loadingBarMobilesTopRated = findViewById(R.id.top_rated_mobiles_p_bar);
        loadingPBarForLaptopsTopRated = findViewById(R.id.top_rated_laptops_p_bar);
        loadingBarTVTopRated = findViewById(R.id.top_rated_tv_p_bar);

        viewPager = findViewById(R.id.pager_main);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                mCurrentPosition = position; // Declare mCurrentPosition as a global variable to track the current position of the item in the ViewPager
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // For going from the first item to the last item, when the 1st A goes to 1st C on the left, again we let the ViewPager do it's job until the movement is completed, we then set the current item to the 2nd C.
                // Set the current item to the item before the last item if the current position is 0
                //if (mCurrentPosition == 0)
                // viewPager.setCurrentItem(lastPageIndex - 1, false); // lastPageIndex is the index of the last item, in this case is pointing to the 2nd A on the list. This variable should be declared and initialzed as a global variable

                // For going from the last item to the first item, when the 2nd C goes to the 2nd A on the right, we let the ViewPager do it's job for us, once the movement is completed, we set the current item to the 1st A.
                // Set the current item to the second item if the current position is on the last
                //if (mCurrentPosition == lastPageIndex)
                //  viewPager.setCurrentItem(3, false);

            }
        });


        final FragmentManager manager = getSupportFragmentManager();
        manager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                //Toast.makeText(EcomMainActivity.this, "count "+manager.getBackStackEntryCount(), Toast.LENGTH_SHORT).show();
                if (manager.getBackStackEntryCount() == 0) {
                    try {
                        setActionbar(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        setActionbar(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        initMainView();
    }

    //********* Banners loading here

    private void loadAllBanners() {
        beforeLoadingBannerView();
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getBannersForEcommerce("loadBanners", "index").enqueue(new Callback<BannersResponse>() {
            @Override
            public void onResponse(Call<BannersResponse> call, Response<BannersResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            String[] banners = new String[response.body().getData().size()];

                            for (int i = 0; i < response.body().getData().size(); i++) {
                                banners[i] = response.body().getData().get(i).getUrl();
                            }
                            bannerAdapter = new BannerAdapter(EcomMainActivity.this, banners);
                            viewPager.setAdapter(bannerAdapter);

                            afterLoadingBannersView();

                            runnable = new Runnable() {
                                public void run() {
                                    if (bannerAdapter.getCount() == mCurrentPosition) {
                                        mCurrentPosition = 0;
                                    } else {
                                        mCurrentPosition++;
                                    }
                                    viewPager.setCurrentItem(mCurrentPosition, true);
                                    handler.postDelayed(this, delay);
                                }
                            };

                        } else {
                            Toast.makeText(customActivity, "Failed to load Banners", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BannersResponse> call, Throwable t) {

            }
        });
    }

    private FrameLayout afterLoadingCategoriesView;
    private View beforeCategoriesLoad;

    private void beforeLoadView(CardView view, View defaultView) {
        view.setVisibility(View.GONE);
        defaultView.setVisibility(View.VISIBLE);
    }

    private void afterLoadView(CardView view, View defaultView) {
        view.setVisibility(View.VISIBLE);
        defaultView.setVisibility(View.GONE);
    }

    private void beforeLoadingCategories() {
        beforeCategoriesLoad.setVisibility(View.VISIBLE);
        afterLoadingCategoriesView.setVisibility(View.GONE);
    }

    private void afterLoadingCategories() {
        beforeCategoriesLoad.setVisibility(View.GONE);
        afterLoadingCategoriesView.setVisibility(View.VISIBLE);
    }

    private void beforeLoadingBannerView(){
        bannerDefaultView.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.GONE);
    }

    private void afterLoadingBannersView(){
        bannerDefaultView.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
    }

    private void initMainView() {
        categoriesRV = findViewById(R.id.categories_rv_main_screen);
        topRatedMobilesRV = findViewById(R.id.top_rated_mobiles_rv);
        topRatedLaptopsRV = findViewById(R.id.top_rated_laptops_rv);
        topRatedAcRV = findViewById(R.id.top_rated_ac_rv);
        topRatedTVRV = findViewById(R.id.top_rated_tv_rv);

        bannerDefaultView = findViewById(R.id.banners_default_view);
        afterLoadingCategoriesView = findViewById(R.id.categories_after_load_view);
        beforeCategoriesLoad = findViewById(R.id.categories_load_before_view);

        topRatedAcCV = findViewById(R.id.top_rated_ac_cv);
        topRatedMobilesCV = findViewById(R.id.top_rated_mobiles_cv);
        topRatedLaptopsCV = findViewById(R.id.top_rated_laptops_cv);
        topRatedTvCV = findViewById(R.id.top_rated_tv_cv);

        topRatedAcDefaultView = findViewById(R.id.top_rated_ac_default_load_view);
        topRatedTvDefaultView = findViewById(R.id.top_rated_tv_default_load_view);
        topRatedMobilesDefaultView = findViewById(R.id.top_rated_mobiles_default_load_view);
        topRatedLaptopsDefaultView = findViewById(R.id.top_rated_laptops_default_load_view);

        horizontalLayoutManager
                = new LinearLayoutManager(EcomMainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager horizontalLayoutManager1
                = new LinearLayoutManager(EcomMainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager horizontalLayoutManager3
                = new LinearLayoutManager(EcomMainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager horizontalLayoutManager2
                = new LinearLayoutManager(EcomMainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        LinearLayoutManager horizontalLayoutManager4
                = new LinearLayoutManager(EcomMainActivity.this, LinearLayoutManager.HORIZONTAL, false);

        categoriesRV.setLayoutManager(horizontalLayoutManager);
        categoriesRV.setHasFixedSize(true);

        topRatedMobilesRV.setLayoutManager(horizontalLayoutManager1);
        topRatedMobilesRV.setHasFixedSize(true);

        topRatedLaptopsRV.setLayoutManager(horizontalLayoutManager2);
        topRatedLaptopsRV.setHasFixedSize(true);

        topRatedTVRV.setLayoutManager(horizontalLayoutManager4);
        topRatedTVRV.setHasFixedSize(true);

        topRatedAcRV.setLayoutManager(horizontalLayoutManager3);
        topRatedAcRV.setHasFixedSize(true);

        loadingPBarForCategory = findViewById(R.id.loading_p_bar_main_categories);
        loadingPBarForLaptopsTopRated = findViewById(R.id.top_rated_laptops_p_bar);
        loadingBarMobilesTopRated = findViewById(R.id.top_rated_mobiles_p_bar);
        loadingBarACTopRated = findViewById(R.id.top_rated_ac_p_bar);
        loadingBarTVTopRated = findViewById(R.id.top_rated_tv_p_bar);

        categoriesRV.setVisibility(View.INVISIBLE);
        topRatedMobilesRV.setVisibility(View.INVISIBLE);
        topRatedLaptopsRV.setVisibility(View.INVISIBLE);
        topRatedAcRV.setVisibility(View.INVISIBLE);

        if (CommonUtils.isThereInternet(EcomMainActivity.this)) {
            /*loadingPBarForCategory.setIndeterminate(true);
            loadingPBarForCategory.setVisibility(View.VISIBLE);
            loadingBarMobilesTopRated.setIndeterminate(true);
            loadingBarMobilesTopRated.setVisibility(View.VISIBLE);
            loadingPBarForLaptopsTopRated.setIndeterminate(true);
            loadingPBarForLaptopsTopRated.setVisibility(View.VISIBLE);
            loadingBarACTopRated.setIndeterminate(true);
            loadingBarACTopRated.setVisibility(View.VISIBLE);
            loadingBarTVTopRated.setVisibility(View.VISIBLE);*/

            loadAllBanners();
            loadAllCategories();
            getAllTopRatedMobiles("14"); //Mobiles
            getAllTopRatedLaptops("21"); //Laptops
            getAllTopRatedAC("126"); // Ac's
            getAllTopRatedTV("174"); // Tv's
        } else {
            Intent intent = new Intent(EcomMainActivity.this, NoInternetActivity.class);
            startActivity(intent);
            // DialogUtils.showNoInternetDialog(EcomMainActivity.this);
            /*RecyclerView.Adapter categoryAdapter = new CategoriesDashboardAdapter(EcomMainActivity.this, categoriesList, 2);
            categoriesRV.setAdapter(categoryAdapter);
            categoryAdapter.notifyDataSetChanged();*/
        }
    }

    private void getAllTopRatedTV(String catId) {
        beforeLoadView(topRatedTvCV, topRatedTvDefaultView);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getTopRatedProducts("topRatingProducts", catId).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        if (response.body().getData() != null) {

                            topRatedProductTvList.clear();
                            topRatedProductTvList = response.body().getData();

                            if (topRatedProductTvList.size() > 0)
                                topRatedTVRV.setAdapter(new TopRatedProductsAdapter(topRatedProductTvList));

                            afterLoadView(topRatedTvCV, topRatedTvDefaultView);
                        }
                    } else {
                        // Toast.makeText(customActivity, "" , Toast.LENGTH_SHORT).show();
                    }
                }
                loadingBarTVTopRated.setVisibility(View.GONE);
                topRatedTVRV.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                loadingBarTVTopRated.setVisibility(View.GONE);
                topRatedTVRV.setVisibility(View.VISIBLE);
            }
        });
    }

    public void setActionbar(boolean actionbar) {
        // getSupportActionBar().setDisplayShowHomeEnabled(actionbar);
        if (actionbar) {
            actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            // actionBar.setLogo(R.drawable.sangeethaactionlogo);
        } else {

            actionBar.setDisplayHomeAsUpEnabled(false);
            actionBar.setDisplayShowHomeEnabled(false);
            //actionBar.setLogo(R.drawable.sangeethaactionlogo);
            setToggle(true);
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (runnable != null)
            handler.removeCallbacks(runnable);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (runnable != null)
            handler.postDelayed(runnable, delay);
        showSearchBar();
        hideToolBarLogo();
        setAppToolBarTitle("BLUEPAY MAX");
        System.out.println("------ ON RESUME MAIN");

        hideSoftKeyBoard();
    }

    private void hideSoftKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();

        if (focusedView != null) {
            assert inputManager != null;
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void loadAllCategories() {
        beforeLoadingCategories();
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiInterface.getEcommerceCategories("loadAllCategories").enqueue(new Callback<EcommerceCategoriesResponse>() {
            @Override
            public void onResponse(Call<EcommerceCategoriesResponse> call, Response<EcommerceCategoriesResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        categoriesList.clear();
                        categoriesList = response.body().getData();
                        if (categoriesList.size() > 0) {
                            categoriesRV.setAdapter(new CategoriesAdapter(EcomMainActivity.this, categoriesList, 2));
                            categoriesRV.getAdapter().notifyDataSetChanged();
                            loadingPBarForCategory.setVisibility(View.GONE);
                            afterLoadingCategories();
                        }
                        categoriesRV.setVisibility(View.VISIBLE);
                    } else {
                        categoriesRV.setVisibility(View.VISIBLE);
                    }
                } else {
                    loadingPBarForCategory.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<EcommerceCategoriesResponse> call, Throwable t) {
                t.printStackTrace();
                System.out.println("-------- categories ex: " + t.toString());
                loadingPBarForCategory.setVisibility(View.GONE);
            }
        });
    }

    private void getAllTopRatedMobiles(final String catId) {
        beforeLoadView(topRatedMobilesCV, topRatedMobilesDefaultView);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getTopRatedProducts("topRatingProducts", catId).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        if (response.body().getData() != null) {

                            topRatedProductMobilesList.clear();
                            topRatedProductMobilesList = response.body().getData();

                            if (topRatedProductMobilesList.size() > 0)
                                topRatedMobilesRV.setAdapter(new TopRatedProductsAdapter(topRatedProductMobilesList));

                            afterLoadView(topRatedMobilesCV, topRatedMobilesDefaultView);
                        }
                    } else {
                        // Toast.makeText(customActivity, "" , Toast.LENGTH_SHORT).show();
                    }
                }
                loadingBarMobilesTopRated.setVisibility(View.GONE);
                topRatedMobilesRV.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                loadingBarMobilesTopRated.setVisibility(View.GONE);
                topRatedMobilesRV.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getAllTopRatedLaptops(final String catId) {
        beforeLoadView(topRatedLaptopsCV, topRatedLaptopsDefaultView);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getTopRatedProducts("topRatingProducts", catId).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        if (response.body().getData() != null) {
                            topRatedProductLaptopsList.clear();
                            topRatedProductLaptopsList = response.body().getData();

                            if (topRatedProductLaptopsList.size() > 0)
                                topRatedLaptopsRV.setAdapter(new TopRatedProductsAdapter(topRatedProductLaptopsList));
                            topRatedLaptopsRV.getAdapter().notifyDataSetChanged();

                            afterLoadView(topRatedLaptopsCV, topRatedLaptopsDefaultView);
                        }
                    } else {
                        // Toast.makeText(customActivity, "" , Toast.LENGTH_SHORT).show();
                    }
                }
                loadingPBarForLaptopsTopRated.setVisibility(View.GONE);
                topRatedLaptopsRV.setVisibility(View.VISIBLE);
            }


            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                loadingPBarForLaptopsTopRated.setVisibility(View.GONE);
                topRatedLaptopsRV.setVisibility(View.VISIBLE);
            }
        });
    }

    private void getAllTopRatedAC(final String catId) {
        beforeLoadView(topRatedAcCV, topRatedAcDefaultView);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getTopRatedProducts("topRatingProducts", catId).enqueue(new Callback<AllProductsResponse>() {
            @Override
            public void onResponse(Call<AllProductsResponse> call, Response<AllProductsResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        if (response.body().getData() != null)
                            topRatedProductAcList.clear();
                        topRatedProductAcList = response.body().getData();
                        if (topRatedProductAcList.size() > 0) {
                            topRatedAcRV.setAdapter(new TopRatedProductsAdapter(topRatedProductAcList));
                            topRatedAcRV.getAdapter().notifyDataSetChanged();

                            afterLoadView(topRatedAcCV, topRatedAcDefaultView);
                        }
                    }
                    loadingBarACTopRated.setVisibility(View.GONE);
                    topRatedAcRV.setVisibility(View.VISIBLE);
                } else {
                    // Toast.makeText(customActivity, "" , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AllProductsResponse> call, Throwable t) {
                loadingBarACTopRated.setVisibility(View.GONE);
                topRatedAcRV.setVisibility(View.VISIBLE);
            }
        });
    }

    //Note : CAT ID 21 (Laptops) 126 (AC)

    private class TopRatedProductsAdapter extends RecyclerView.Adapter {
        private List<AllProductsResponse.AllProductsData> topProductsList;

        TopRatedProductsAdapter(List<AllProductsResponse.AllProductsData> topRatedProductsList) {
            this.topProductsList = topRatedProductsList;
            for (int i = 0; i < topRatedProductsList.size(); i++) {
                if (topRatedProductsList.get(i).getImage() != null) {
                    topRatedProductsList.get(i).setHasImage(true);
                } else
                    topRatedProductsList.get(i).setHasImage(false);
            }
            topProductsList = topRatedProductsList;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(EcomMainActivity.this).inflate(R.layout.top_rated_product_view, parent, false);
            return new TopRatedProductsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final TopRatedProductsHolder similarProductsHolder = (TopRatedProductsHolder) holder;
            similarProductsHolder.productNameTV.setText(topProductsList.get(position).getBrand()
                    + " " + topProductsList.get(position).getTitle());

            String msp = topProductsList.get(position).getMsp().replace(".00", "");

            similarProductsHolder.originalPriceTV.setPaintFlags(similarProductsHolder.originalPriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            String mrp = topProductsList.get(position).getMrp().replace(".00", "");


            int someNumber = Integer.parseInt(msp);
            NumberFormat nf = NumberFormat.getInstance();
            String mspFormatted = nf.format(someNumber);
            similarProductsHolder.sellingPriceTV.setText(mspFormatted);

            int mrpNum = Integer.parseInt(mrp);
            String mrpFormatted = nf.format(mrpNum);
            similarProductsHolder.originalPriceTV.setText(mrpFormatted);

            String pv = topProductsList.get(position).getPv();
            similarProductsHolder.pvTV.setText("CBP: " + pv);

            String rating = topProductsList.get(position).getRate();

            if (rating == null)
                rating = "0";

            Double ratingInDouble = Double.valueOf(rating);

            similarProductsHolder.ratingsTV.setText(CommonUtilsEcommerce.formatToSingleDecimalFraction(rating));

            Log.e("Actual rating: ", String.valueOf(ratingInDouble));

            if (ratingInDouble <= 1.0 && ratingInDouble > 0) {
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_red_bg);
            } else if (ratingInDouble <= 2 && ratingInDouble > 1.0) {
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.rect_rating_yellow_bg);
            } else if (ratingInDouble <= 3.0 && ratingInDouble > 2.0) {
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_orange_bg);
            } else if (ratingInDouble <= 5.0 && ratingInDouble > 3.0) {
                similarProductsHolder.ratingsTV.setBackgroundResource(R.drawable.ratings_item_green_bg);
            } else {
                similarProductsHolder.ratingsTV.setVisibility(View.GONE);
            }

            similarProductsHolder.discountTV.setText(getDiscountInPercent(Double.parseDouble(msp), Double.parseDouble(mrp)) + "% off");

            /*if (similarProductsList.get(position).getQuantity().equals("0")) {
                similarProductsHolder.discountTV.setVisibility(View.GONE);
                similarProductsHolder.outOfStockTV.setVisibility(View.VISIBLE);
            } else {
                similarProductsHolder.outOfStockTV.setVisibility(View.GONE);
                similarProductsHolder.discountTV.setVisibility(View.VISIBLE);
            }*/

            if (topProductsList.get(position).isHasImage()) {
                Glide.with(EcomMainActivity.this)
                        .load(new GlideUrl(topProductsList.get(position).getImage()))
                        .placeholder(ContextCompat.getDrawable(EcomMainActivity.this, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(EcomMainActivity.this, R.drawable.ic_def_pic))
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(similarProductsHolder.productIV);
            }

            similarProductsHolder.productItemMainCV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Fragment productOverViewFragment = new OverviewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("PRODUCT_ID", topProductsList.get(position).getId());
                    bundle.putString("PRODUCT_CODE", topProductsList.get(position).getProductCode());
                    bundle.putString("FOR", "LIVE");
                    productOverViewFragment.setArguments(bundle);
                    moveToFragment(productOverViewFragment, "OVERVIEW_FRAGMENT");
                }
            });
        }

        @Override
        public int getItemCount() {
            return topProductsList.size();
        }
    }

    private String getDiscountInPercent(double msp, double mrp) {
      /*  Discount = Marked Price - Selling Price

        Discount% = Discount/Marked Price × 100%.*/

        double dis = mrp - msp;
        double discount = (dis / mrp) * 100;

        DecimalFormat value = new DecimalFormat("#.#");
        value.format(discount);

        return String.valueOf(Math.round(discount));
    }

    private class TopRatedProductsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView sellingPriceTV, originalPriceTV, productNameTV, discountTV, pvTV, ratingsTV, outOfStockTV;
        private CardView productItemMainCV;
        private AppCompatImageView productIV;
        private RatingBar ratingBar;

        TopRatedProductsHolder(View itemView) {
            super(itemView);
            sellingPriceTV = itemView.findViewById(R.id.selling_price_tv);
            originalPriceTV = itemView.findViewById(R.id.purchase_pice_tv);
            productNameTV = itemView.findViewById(R.id.product_name_tv);
            sellingPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian, 0, 0, 0);
            //  originalPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian_grey, 0, 0, 0);
            productItemMainCV = itemView.findViewById(R.id.product_item_main_view);
            productIV = itemView.findViewById(R.id.product_iv);
            discountTV = itemView.findViewById(R.id.discount_tv);
            ratingsTV = itemView.findViewById(R.id.rating_tv_product_item);
            ratingsTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_star, 0);
            // ratingsTV.setVisibility(View.GONE);
            pvTV = itemView.findViewById(R.id.pv_tv_item);
            ratingBar = itemView.findViewById(R.id.rating_bar_product_item);
            outOfStockTV = itemView.findViewById(R.id.out_of_stock_tv);
            outOfStockTV.setVisibility(View.GONE);
            // pvTV.setVisibility(View.GONE);
            //discountTV.setVisibility(View.GONE);
            //originalPriceTV.setVisibility(View.GONE);
        }
    }

}
