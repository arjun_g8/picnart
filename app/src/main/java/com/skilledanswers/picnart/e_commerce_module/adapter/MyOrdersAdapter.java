package com.skilledanswers.picnart.e_commerce_module.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.MyOrdersResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by SkilledAnswers-D1 on 09-05-2016.
 */
public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.Holder> {
    private Context context = null;
    private List<MyOrdersResponse.OrderedData> myOrderModels = null;
    private EcommerceCustomActivity activity;
    private PrefUtil appUtil;
    private OrderCancellable orderCancellable;
    private boolean isForRating;

    public interface OrderCancellable {
        void onOrderCancelled();

        void onOrderItemClicked(MyOrdersResponse.OrderedData orderData);
    }


    public MyOrdersAdapter(Context context, List<MyOrdersResponse.OrderedData> myOrderModels, OrderCancellable orderCancellable) {
        this.context = context;
        this.myOrderModels = myOrderModels;
        activity = (EcommerceCustomActivity) context;
        appUtil = new PrefUtil(activity);
        this.orderCancellable = orderCancellable;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.my_orders_list_item, parent, false);
        return new Holder(view);
    }

    private String reasonSelected = "";
    private String actionSelected = "I want to Cancel";

    private void showOrderCancelPopUp(final String orderId, String orderProductName, String prodPrice, String qty, String imgUrl) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.order_cancel_popup);
        dialog.setCancelable(true);
        final TextInputEditText reasonED = dialog.findViewById(R.id.order_cancel_reason_ed);
        final TextInputLayout reasonIp = dialog.findViewById(R.id.reason_for_cancelling_tip);
        AppCompatTextView productNameTV = dialog.findViewById(R.id.prod_name_order_cancel);
        AppCompatTextView productPriceTV = dialog.findViewById(R.id.prod_price_order_cancel);
        AppCompatTextView productQtyTV = dialog.findViewById(R.id.prod_qty_order_cancel);
        AppCompatImageView prodImgV = dialog.findViewById(R.id.prod_img_order_cancel);
        RadioGroup radioGroup = dialog.findViewById(R.id.order_cancel_rg);

        Glide.with(context)
                .load(new GlideUrl(imgUrl))
                .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                .error(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .dontAnimate()
                .into(prodImgV);
        productNameTV.setText(orderProductName);
        productPriceTV.setText("Rs. " + prodPrice);
        productQtyTV.setText("Qty: " + qty);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int id = radioGroup.getCheckedRadioButtonId();
                if (id == R.id.yes_for_cancel_rbtn) {
                    actionSelected = "I want to Cancel";
                } else {
                    actionSelected = "I want to Replace";
                }

            }
        });


        AppCompatSpinner reasonSelectSpinner = dialog.findViewById(R.id.reason_spinner_order_cancel);

        final String[] reasons = new String[]{
                "I do not want this", "Mistakely Ordered", "My Unavailability", "Other"
        };

        reasonSelectSpinner.setAdapter(new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, reasons));
        reasonSelectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                reasonSelected = reasons[i];
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.findViewById(R.id.cancel_order_confirm_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (CommonUtils.isThereInternet(activity))
                    cancelOrder(actionSelected, orderId, reasonED.getText().toString());
                else
                    DialogUtils.showNoInternetDialog(activity);
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {
        MyOrdersResponse.OrderedData model = myOrderModels.get(position);
        //holder.orderedDate.setText("" + model.ge);
        //  holder.orderID.setText("" + model.getOrderId());
        //  holder.productImage.setImageResource(model.get_productImage());
        holder.productName.setText("" + model.getTitle());
        String statusIs = model.getOrderStatus();
        try {
            if (statusIs != null)
                statusIs = statusIs.substring(0, 1).toUpperCase() + statusIs.substring(1);
        } catch (Exception e) {
            statusIs = "";
        }

        holder.status.setText(statusIs);
        holder.orderedDateTV.setText("Ordered On " + model.getCreated());


        /*if (!model.getFlag()) {
            holder.bottomLayout.setVisibility(View.GONE);
            holder.ratedAlreadyTV.setVisibility(View.GONE);
        } else {
            */
        assert statusIs != null;
            if (statusIs.equalsIgnoreCase("failed")|| statusIs.equalsIgnoreCase("cancelled"))
                holder.bottomLayout.setVisibility(View.GONE);
            else
                holder.bottomLayout.setVisibility(View.VISIBLE);
      //  }


        String urlForProductImg = "";

        if (myOrderModels != null)
            if (myOrderModels.size() > 0) {
                if (myOrderModels.get(position).getImage() != null)
                    urlForProductImg = myOrderModels.get(position).getImage();

                Glide.with(context)
                        .load(new GlideUrl(urlForProductImg))
                        .placeholder(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                        .error(ContextCompat.getDrawable(context, R.drawable.ic_def_pic))
                        .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .dontAnimate()
                        .into(holder.productImage);
            }
        holder.ratingBar.setVisibility(View.GONE);
        holder.alreadyRatedView.setVisibility(View.GONE);

        //  }

        holder.addReviewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddReviewPopUp(String.valueOf(myOrderModels.get(position).getProductId()));
            }
        });

        holder.cancelOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showOrderCancelPopUp(String.valueOf(myOrderModels.get(position).getOrderDetailId()), myOrderModels.get(position).getTitle(),
                        myOrderModels.get(position).getPrice(), String.valueOf(myOrderModels.get(position).getQuantity()), myOrderModels.get(position).getImage());
            }
        });

        holder.ordered_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderCancellable.onOrderItemClicked(myOrderModels.get(position));
            }
        });

    }


    private void cancelOrder(final String reqFor, final String orderId, final String otherReason) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.cancelPlacedOrder("returnRequest", appUtil.getToken(), reqFor, reasonSelected, orderId, otherReason).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String jsonStringResponse = new String(response.body().bytes());

                    System.out.println("----- REQ FOR: " + reqFor);
                    System.out.println("----- REQ Reason: " + reasonSelected);
                    System.out.println("----- REQ ACTION: " + actionSelected);
                    System.out.println("----- REQ OTHER REASON: " + otherReason);
                    System.out.println("----- REQ SUB ORDER ID: " + orderId);

                    System.out.println("----------- ORDER CANCEL JSON STRING: " + jsonStringResponse);

                    JSONObject jsonObject = new JSONObject(jsonStringResponse);
                    boolean status;
                    String msg = jsonObject.getString("message");
                    status = jsonObject.getBoolean("status");

                    if (status) {
                        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
                        orderCancellable.onOrderCancelled();
                    } else {
                        // if (msg.equalsIgnoreCase("This order has not found"))
                        Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();
                        /*else
                            DialogUtils.showReLoginPopUp(activity);*/
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private Dialog dialog;

    private void showAddReviewPopUp(final String prodId) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.add_review_dialog_layout);
        dialog.setCancelable(true);
        final AppCompatEditText reviewEd = dialog.findViewById(R.id.review_ed_dialog);
        final TextInputLayout reviewTxtIp = dialog.findViewById(R.id.review_txt_ip_dialog);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;

        dialog.findViewById(R.id.submit_review_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String review = reviewEd.getText().toString();
                if (TextUtils.isEmpty(review)) {
                    reviewTxtIp.setErrorEnabled(true);
                    reviewTxtIp.setError("Please add something for review");
                } else {
                    reviewTxtIp.setErrorEnabled(false);
                    if (CommonUtils.isThereInternet(activity))
                        addReview(review, prodId);
                    else
                        DialogUtils.showNoInternetDialog(activity);
                }
            }
        });
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void addReview(String reviewText, String prodId) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.saveReview("addReview", prodId, appUtil.getRegNo(), appUtil.getToken(), reviewText).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String ratingResponseString = new String(response.body().bytes());
                        JSONObject jsonObject = new JSONObject(ratingResponseString);
                        boolean status;
                        String msg;

                        status = jsonObject.getBoolean("status");
                        msg = jsonObject.getString("message");

                        if (status) {
                            Toast.makeText(context, "Thanks For reviewing!", Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                            dialog.cancel();
                        } else
                            Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void addRatingForTheProduct(String prodId, final String rating) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.saveRating("saveRating", prodId, appUtil.getRegNo(), rating).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        String ratingResponseString = new String(response.body().bytes());
                        JSONObject jsonObject = new JSONObject(ratingResponseString);
                        boolean status;
                        String msg;

                        status = jsonObject.getBoolean("status");
                        msg = jsonObject.getString("message");

                        if (status) {
                            Toast.makeText(context, "Thanks For rating!", Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(context, "" + msg, Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                System.out.println("--------- " + t.toString());
            }
        });
    }

    @Override
    public int getItemCount() {
        return myOrderModels.size();
    }

    protected class Holder extends RecyclerView.ViewHolder {
        protected TextView orderedDate = null;
        protected TextView orderID = null;
        AppCompatImageView productImage;
        AppCompatTextView productName;
        AppCompatTextView status, orderedDateTV, ratedAlreadyTV;
        protected TextView statusdesc = null;
        protected AppCompatButton get_more;
        CardView ordered_item;
        private RatingBar ratingBar;
        private AppCompatButton addReviewBtn, cancelOrderBtn;
        private RelativeLayout bottomLayout;
        private LinearLayout alreadyRatedView;

        Holder(View itemView) {
            super(itemView);
            ordered_item = itemView.findViewById(R.id.order_item_cv);

         /*   this.orderedDate = (TextView) itemView.findViewById(R.id.my_orderRow_OrderDateID);
            this.orderID = (TextView) itemView.findViewById(R.id.my_orderRow_OrderIdID);*/
            this.productImage = itemView.findViewById(R.id.prod_img_order_list);
            this.productName = itemView.findViewById(R.id.product_name_tv_order_list);
            this.status = itemView.findViewById(R.id.orders_status_and_date_tv);
            this.orderedDateTV = itemView.findViewById(R.id.orders_date_tv);
            this.addReviewBtn = itemView.findViewById(R.id.add_review_btn_order_item);
            this.ratingBar = itemView.findViewById(R.id.rating_bar_order_item);
            this.cancelOrderBtn = itemView.findViewById(R.id.cancel_order_btn);
            this.bottomLayout = itemView.findViewById(R.id.bottom_layout_order_item);
            this.alreadyRatedView = itemView.findViewById(R.id.already_rated_view);
            this.ratedAlreadyTV = itemView.findViewById(R.id.order_already_rate_tv);
            this.alreadyRatedView.setVisibility(View.GONE);

           /* this.statusdesc = (TextView) itemView.findViewById(R.id.my_orderRow_deleverydescID);

            get_more = (AppCompatButton) itemView.findViewById(R.id.get_more);
            ordered_item = (CardView) itemView.findViewById(R.id.ordered_item);*/
        }
    }

}
