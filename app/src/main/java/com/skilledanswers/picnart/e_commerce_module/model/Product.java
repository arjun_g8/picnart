/*
 * Copyright (c) 2016. Subhash Kumar
 */

package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by subh on 1/22/2016.
 */
public class Product implements Parcelable {

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };
    private int product_id;
    private String product_name;
    private String product_img_url;
    private String mrp;
    private String discount;
    private String discounted_amount;
    private String likes;
    private String share;
    private String store_name;
    private String store_logo;

    protected Product(Parcel in) {
        product_id = in.readInt();
        product_name = in.readString();
        product_img_url = in.readString();
        mrp = in.readString();
        discount = in.readString();
        discounted_amount = in.readString();
        likes = in.readString();
        share = in.readString();
        store_name = in.readString();
        store_logo = in.readString();
    }

    public Product() {
    }

    public Product(int product_id, String product_name, String product_img_url) {
        this.product_id = product_id;
        this.product_name = product_name;
        this.product_img_url = product_img_url;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getDiscounted_amount() {
        return discounted_amount;
    }

    public void setDiscounted_amount(String discounted_amount) {
        this.discounted_amount = discounted_amount;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getStore_logo() {
        return store_logo;
    }

    public void setStore_logo(String store_logo) {
        this.store_logo = store_logo;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_img_url() {
        return product_img_url;
    }

    public void setProduct_img_url(String product_img_url) {
        this.product_img_url = product_img_url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(product_id);
        dest.writeString(product_name);
        dest.writeString(product_img_url);
        dest.writeString(mrp);
        dest.writeString(discount);
        dest.writeString(discounted_amount);
        dest.writeString(likes);
        dest.writeString(share);
        dest.writeString(store_name);
        dest.writeString(store_logo);
    }
}
