package bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PlaceOrderResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("internalPayment")
    @Expose
    private Boolean internalPayment;
    @SerializedName("transactions")
    @Expose
    private Transactions transactions;
    @SerializedName("neft")
    @Expose
    private Boolean neft;
    @SerializedName("data")
    @Expose
    private PlaceOrderData data;
    @SerializedName("message")
    @Expose
    private String message;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getInternalPayment() {
        return internalPayment;
    }

    public void setInternalPayment(Boolean internalPayment) {
        this.internalPayment = internalPayment;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void setTransactions(Transactions transactions) {
        this.transactions = transactions;
    }

    public Boolean getNeft() {
        return neft;
    }

    public void setNeft(Boolean neft) {
        this.neft = neft;
    }

    public PlaceOrderData getData() {
        return data;
    }

    public void setData(PlaceOrderData data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
