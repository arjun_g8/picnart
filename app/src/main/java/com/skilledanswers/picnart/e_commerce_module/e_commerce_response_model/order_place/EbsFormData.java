package bluepaymax.skilledanswers.picnart.e_commerce_module.e_commerce_response_model.order_place;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EbsFormData implements Parcelable{

    @SerializedName("account_id")
    @Expose
    private String accountId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("display_currency")
    @Expose
    private String displayCurrency;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("payment_option")
    @Expose
    private String paymentOption;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("reference_no")
    @Expose
    private String referenceNo;
    @SerializedName("return_url")
    @Expose
    private String returnUrl;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("secure_hash")
    @Expose
    private String secureHash;

    public EbsFormData() {
    }

    private EbsFormData(Parcel in) {
        accountId = in.readString();
        address = in.readString();
        amount = in.readString();
        channel = in.readString();
        city = in.readString();
        country = in.readString();
        currency = in.readString();
        description = in.readString();
        displayCurrency = in.readString();
        email = in.readString();
        mode = in.readString();
        name = in.readString();
        paymentOption = in.readString();
        phone = in.readString();
        postalCode = in.readString();
        referenceNo = in.readString();
        returnUrl = in.readString();
        state = in.readString();
        secureHash = in.readString();
    }

    public static final Creator<EbsFormData> CREATOR = new Creator<EbsFormData>() {
        @Override
        public EbsFormData createFromParcel(Parcel in) {
            return new EbsFormData(in);
        }

        @Override
        public EbsFormData[] newArray(int size) {
            return new EbsFormData[size];
        }
    };

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayCurrency() {
        return displayCurrency;
    }

    public void setDisplayCurrency(String displayCurrency) {
        this.displayCurrency = displayCurrency;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSecureHash() {
        return secureHash;
    }

    public void setSecureHash(String secureHash) {
        this.secureHash = secureHash;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accountId);
        dest.writeString(address);
        dest.writeString(amount);
        dest.writeString(channel);
        dest.writeString(city);
        dest.writeString(country);
        dest.writeString(currency);
        dest.writeString(description);
        dest.writeString(displayCurrency);
        dest.writeString(email);
        dest.writeString(mode);
        dest.writeString(name);
        dest.writeString(paymentOption);
        dest.writeString(phone);
        dest.writeString(postalCode);
        dest.writeString(referenceNo);
        dest.writeString(returnUrl);
        dest.writeString(state);
        dest.writeString(secureHash);
    }
}
