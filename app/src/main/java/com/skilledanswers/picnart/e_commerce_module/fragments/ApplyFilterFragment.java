package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.CommonUtilsEcommerce;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.FilterResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by SkilledAnswers-D1 on 26-12-2017.
 */

public class ApplyFilterFragment extends Fragment implements View.OnClickListener {
    private EcommerceCustomActivity ecommerceCustomActivity;
    private NestedScrollView applyFilterLoadedView;
    private View applyFilterView;
    private RecyclerView filterRV;
    private List<FilterResponse.FilterData> filterItemList = new ArrayList<>();
    private static final int SUB_FILTER_REQ_CODE = 85;
    private ProgressBar loadingBar;
    private LinearLayoutCompat bottomView;
    private HashMap<String, ArrayList<String>> filterMap = new HashMap<>();
    private String filterKey;
    private String priceRange;
    private String pvRange;
    private String subCategoryName;
    private int subCatId, catId, resultsCount, subLayerId;
    private String minPrice, maxPrice, minPV, maxPV, defMinPrice, defMaxPrice, defMinPV, defMaxPV;
    private CrystalRangeSeekbar priceRangeSeekBar, pvRangeSeekBar;
    private AppCompatTextView minPriceTV, maxPriceTV, minPvTV, maxPvTV;
    private boolean isFilterCleared;
    private Bundle filterBundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        applyFilterView = inflater.inflate(R.layout.apply_filter_fragment, container, false);
        applyFilterLoadedView = applyFilterView.findViewById(R.id.apply_filter_loaded_view);
        loadingBar = applyFilterView.findViewById(R.id.loading_p_bar_apply_filter);
        bottomView = applyFilterView.findViewById(R.id.bottom_view_in_apply_filter);

        priceRangeSeekBar = applyFilterView.findViewById(R.id.price_seek_bar);
        pvRangeSeekBar = applyFilterView.findViewById(R.id.pv_seek_bar);
        minPvTV = applyFilterView.findViewById(R.id.min_pv_tv);
        maxPvTV = applyFilterView.findViewById(R.id.max_pv_tv);
        minPriceTV = applyFilterView.findViewById(R.id.min_price_tv);
        maxPriceTV = applyFilterView.findViewById(R.id.max_price_tv);
        //filterResultCountTV = applyFilterView.findViewById(R.id.num_of_filter_results_found_tv);

        applyFilterView.findViewById(R.id.apply_filter_btn).setOnClickListener(this);
        applyFilterView.findViewById(R.id.clear_filter_btn).setOnClickListener(this);
        setHasOptionsMenu(true);
        filterRV = applyFilterView.findViewById(R.id.filters_rv);
        filterRV.setLayoutManager(new LinearLayoutManager(ecommerceCustomActivity));
        filterRV.setHasFixedSize(true);

        if (getArguments() != null) {
            filterBundle = getArguments();
            catId = getArguments().getInt("CATEGORY_ID_FILTER");
            subCatId = getArguments().getInt("SUB_CATEGORY_ID_FILTER");
            subCategoryName = getArguments().getString("SUB_CATEGORY_NAME_FILTER");

            //  minPrice = getArguments().getString("MIN_PRICE_FILTER");
            minPrice = "0";
            maxPrice = getArguments().getString("MAX_PRICE_FILTER");
            // minPV = getArguments().getString("MIN_PV_FILTER");
            minPV = "0";
            maxPV = getArguments().getString("MAX_PV_FILTER");

            String maxPriceFormatted = maxPrice.replace(".00", "");
            String maxPvFormatted = maxPV.replace(".00", "");

            maxPriceFormatted = Double.toString(Math.floor(Double.parseDouble(maxPriceFormatted)));
            maxPvFormatted = Double.toString(Math.floor(Double.parseDouble(maxPvFormatted)));

            maxPriceFormatted = maxPriceFormatted.replace(".0", "");
            maxPvFormatted = maxPvFormatted.replace(".0", "");

            maxPrice = String.valueOf(CommonUtilsEcommerce.roundNumberToNearestHundred(Integer.valueOf(maxPriceFormatted), 100));
            maxPV = String.valueOf(CommonUtilsEcommerce.roundNumberToNearestHundred(Integer.valueOf(maxPvFormatted), 100));

            defMinPrice = minPrice;
            defMaxPrice = maxPrice;
            defMinPV = minPV;
            defMaxPV = maxPV;

            System.out.println("---- MIN PV: " + minPV + "\t MAX PV: " + maxPV);
            System.out.println("---- MIN PRICE: " + minPrice + "\t MAX PRICE: " + maxPrice);

            System.out.println("---- START DEF MIN PV: " + defMinPV + "\t MAX PV: " + defMaxPV);
            System.out.println("---- START DEF MIN PRICE: " + defMinPrice + "\t MAX PRICE: " + defMaxPrice);

            setUpRangeSeekBar();

            minPvTV.setText("Min: " + minPV);
            maxPvTV.setText("Max: " + maxPV);
        }

        if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
            loadAllFiltersAvailable(subCatId);
        } else {
            loadingBar.setVisibility(View.GONE);
            applyFilterLoadedView.setVisibility(View.GONE);
            bottomView.setVisibility(View.GONE);
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
        }

        return applyFilterView;
    }

    private void setUpRangeSeekBar() {
        priceRangeSeekBar.setBarColor(ContextCompat.getColor(ecommerceCustomActivity, R.color.gray));
        pvRangeSeekBar.setBarColor(ContextCompat.getColor(ecommerceCustomActivity, R.color.gray));
        //String maxPriceFormatted = maxPrice.replace(".00", "");
        String maxPriceFormatted = maxPrice;

        int priceInterval = 5;

        if (Integer.valueOf(maxPriceFormatted) >= 100 && maxPriceFormatted.length() < 5) {
            int interval;
            switch (maxPriceFormatted.length()) {
                case 3:
                    interval = 10;
                    break;
                case 4:
                    interval = 100;
                    break;
                default:
                    interval = 5;
                    break;
            }
            priceInterval = interval;
        } else if (Integer.valueOf(maxPriceFormatted) >= 100 && maxPriceFormatted.length() >= 5) {
            int interval;
            switch (maxPriceFormatted.length()) {
                case 5:
                    interval = 1000;
                    break;
                case 6:
                    interval = 10000;
                    break;
                case 7:
                    interval = 10000;
                    break;
                case 8:
                    interval = 10000;
                    break;
                default:
                    interval = 1000;
                    break;
            }
            priceInterval = interval;
        }

        int pvInterval = 5;

        String maxPvFormatted = maxPV;

        if (Integer.valueOf(maxPvFormatted) >= 100 && maxPvFormatted.length() <= 6) {
            int interval;
            switch (maxPvFormatted.length()) {
                case 3:
                    interval = 10;
                    break;
                case 4:
                    interval = 100;
                    break;
                case 5:
                    interval = 1000;
                    break;
                case 6:
                    interval = 1000;
                    break;
                default:
                    interval = 5;
                    break;
            }
            pvInterval = interval;
        }

        priceRangeSeekBar.setSteps(priceInterval);
        priceRangeSeekBar.setMinValue(Float.parseFloat(minPrice));
        priceRangeSeekBar.setMaxValue(Float.parseFloat(maxPrice));
        pvRangeSeekBar.setSteps(pvInterval);
        pvRangeSeekBar.setMinValue(Float.parseFloat(minPV));
        pvRangeSeekBar.setMaxValue(Float.parseFloat(maxPV));

        Log.e("PRICE INTERVAL: ", "" + priceInterval);

        System.out.println("------------ MIN PV: " + minPV);
        System.out.println("------------ MAX PV: " + maxPV);

        priceRangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                minPrice = String.valueOf(minValue);
                maxPrice = String.valueOf(maxValue);
                minPriceTV.setText("Min: " + minPrice);
                maxPriceTV.setText("Max: " + maxPrice);
            }
        });

// set final value listener
        pvRangeSeekBar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                minPV = String.valueOf(minValue);
                maxPV = String.valueOf(maxValue);
                Log.e("MIN PV: ", minPV);
                Log.e("MAX PV: ", maxPV);
                minPvTV.setText("Min: " + minPV);
                maxPvTV.setText("Max: " + maxPV);
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.setToggle(false);
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("Filter");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_search).setVisible(false);
        //menu.findItem(R.id.action_settings).setVisible(false);
        // menu.findItem(R.id.action_notification).setVisible(false);
    }

    private void loadAllFiltersAvailable(final int categoryId) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.fetchAllFilters("loadFilters", categoryId).enqueue(new Callback<FilterResponse>() {
            @Override
            public void onResponse(Call<FilterResponse> call, Response<FilterResponse> response) {
                System.out.println("------------- CAT ID FOR FILTER: " + categoryId);
                if (response.isSuccessful()) {

                    if (response.body().getStatus()) {
                        filterItemList.clear();
                        filterItemList = response.body().getData();

                        if (filterItemList.size() > 0) {
                            filterRV.setAdapter(new FilterItemsAdapter());
                            filterRV.getAdapter().notifyDataSetChanged();
                            loadingBar.setVisibility(View.GONE);
                            applyFilterLoadedView.setVisibility(View.VISIBLE);
                            bottomView.setVisibility(View.VISIBLE);
                            // System.out.println("-------Value: " + filterItemList.get(0).getValue().get(0).getValue());
                        }
                    } else {
                        System.out.println("----------------- NO FILTERS");
                        loadingBar.setVisibility(View.GONE);
                        filterRV.setVisibility(View.GONE);
                        bottomView.setVisibility(View.VISIBLE);
                        applyFilterLoadedView.setVisibility(View.VISIBLE);
                    }

                } else {
                    loadingBar.setVisibility(View.GONE);
                    applyFilterLoadedView.setVisibility(View.GONE);
                    bottomView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FilterResponse> call, Throwable t) {
                loadingBar.setVisibility(View.GONE);
                applyFilterLoadedView.setVisibility(View.GONE);
                bottomView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.apply_filter_btn:
                getSelectedFilters();
                break;
            case R.id.clear_filter_btn:
                filterMap.clear();
                minPV = defMinPV;
                maxPV = defMaxPV;
                minPrice = defMinPrice;
                maxPrice = defMaxPrice;
                System.out.println("---- DEF MIN PV: " + defMinPV + "\t MAX PV: " + defMaxPV);
                System.out.println("---- DEF MIN PRICE: " + defMinPrice + "\t MAX PRICE: " + defMaxPrice);
                setUpRangeSeekBar();
                isFilterCleared = true;
                filterRV.setAdapter(new FilterItemsAdapter());
                filterRV.getAdapter().notifyDataSetChanged();
                Toast.makeText(ecommerceCustomActivity, "Cleared", Toast.LENGTH_SHORT).show();
                fragmentTransition(new ApplyFilterFragment(), filterBundle);
                break;
        }
    }

    public void fragmentTransition(Fragment fragment, Bundle bundle) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragment.setArguments(bundle);
        // transaction.setCustomAnimations(R.animator.in,R.animator.out);
        // transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.replace(R.id.relative, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
        getActivity().getSupportFragmentManager().popBackStack();

    }

    private void restartTheFragment(Fragment toFragment, String TAG) {
        ecommerceCustomActivity.hideSearchBar();
        System.out.println("------- INSIDE MOVE TO FRAGMENT");
        FragmentTransaction transaction = ecommerceCustomActivity.getSupportFragmentManager().beginTransaction();
        Fragment fragment = ecommerceCustomActivity.getSupportFragmentManager().findFragmentByTag(TAG);
        if (fragment != null && fragment.isVisible()) {
            System.out.println("*** ALREADY there ****");
            replaceFragment(toFragment);
        } else {
          /*  transaction.setCustomAnimations(R.anim.enter_anim,
                    R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);*/
            //  transaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            transaction.replace(R.id.relative, toFragment, TAG);
            transaction.addToBackStack(null).commit();
        }
        System.out.println("-------- DONE MOVE TO FRAGMENT");
    }

    private void replaceFragment(Fragment frag) {
        ecommerceCustomActivity.hideSearchBar();
        FragmentManager manager = ecommerceCustomActivity.getSupportFragmentManager();
        if (manager != null) {
            FragmentTransaction t = manager.beginTransaction();
            Fragment currentFrag = manager.findFragmentById(R.id.relative);

            //Check if the new Fragment is the same
            //If it is, don't add to the back stack
            if (currentFrag != null && currentFrag.getClass().equals(frag.getClass())) {
              /*  t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);*/
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            } else {
               /* t.setCustomAnimations(R.anim.enter_anim,
                        R.anim.stay_anim, R.anim.stay_anim, R.anim.exit_anim);*/
                t.replace(R.id.relative, frag).addToBackStack(null).commit();
            }
        }
    }

    private void getSelectedFilters() {
        System.out.println("\n\n");
        for (Map.Entry m : filterMap.entrySet()) {
            System.out.println("---------- " + m.getKey() + "\t" + m.getValue());
        }
        Bundle bundle = new Bundle();
        bundle.putSerializable("FILTER_HASHMAP", filterMap);
        bundle.putInt("CATEGORY_ID_FOR_ALL_PRODUCTS", catId);
        bundle.putInt("SUB_CATEGORY_ID_FOR_ALL_PRODUCTS", subCatId);
        bundle.putString("SUB_CATEGORY_NAME_FOR_ALL_PRODUCTS", subCategoryName);
        bundle.putString("MIN_PRICE_FILTER", minPrice);
        bundle.putString("MAX_PRICE_FILTER", maxPrice);
        bundle.putString("MIN_PV_FILTER", minPV);
        bundle.putString("MAX_PV_FILTER", maxPV);
        //bundle.putBundle("APPLY_FILTER",bundle);
        Fragment allProductsFragment = new AllProductsFragment();
        allProductsFragment.setArguments(bundle);
        ecommerceCustomActivity.getSupportFragmentManager().popBackStack();
        ecommerceCustomActivity.moveToFragment(allProductsFragment, "ALL_PRODUCTS_FRAGMENT");
    }

    private class FilterItemsAdapter extends RecyclerView.Adapter {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(ecommerceCustomActivity).inflate(R.layout.filter_item_view, parent, false);
            return new FilterItemsHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            final FilterItemsHolder filterItemsHolder = (FilterItemsHolder) holder;
            filterItemsHolder.filterItemMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isFilterCleared = false;
                    Bundle bundle = new Bundle();
                    bundle.putString("ATTRIBUTE_ID_FILTER", filterItemList.get(position).getAttributeId());
                    bundle.putString("ATTRIBUTE_NAME_FILTER", filterItemList.get(position).getName());
                    bundle.putInt("CATEGORY_ID_SUB_FILTER", catId);
                    bundle.putInt("SUB_CATEGORY_ID_SUB_FILTER", subCatId);
                    bundle.putString("SUB_CATEGORY_NAME_SUB_FILTER", subCategoryName);
                    filterKey = filterItemList.get(position).getAttributeId();
                    bundle.putInt("POSITION_SUB_FILTER", position);
                    Fragment fragment = new SubFilterFragment();
                    fragment.setArguments(bundle);
                    fragment.setTargetFragment(ApplyFilterFragment.this, SUB_FILTER_REQ_CODE);
                    //    ecommerceCustomActivity.getSupportFragmentManager().popBackStack();
                    ecommerceCustomActivity.moveToFragment(fragment, "SUB_FILTER_FRAGMENT");
                }
            });

            filterItemsHolder.filterItemTitleTV.setText(filterItemList.get(position).getName());

            ArrayList<String> selectedList = new ArrayList<>();
            //selectedList = filterMap.get(filterKey);

            for (Map.Entry m : filterMap.entrySet()) {
                if (m.getKey().equals(filterItemList.get(position).getAttributeId())) {
                    System.out.println("---------- IN " + m.getKey() + "\t" + m.getValue());
                    String val = m.getValue().toString().replace("[", "");
                    val = val.replace("]", "");
                    selectedList.add(val);
                    break;
                }
            }

            System.out.println("------------- SELECTED LIST TO STRING: " + selectedList.toString());

            StringBuilder stringBuilder = new StringBuilder("");

            for (int s = 0; s < selectedList.size(); s++) {
                if (s == 0)
                    stringBuilder.append(selectedList.get(s));
                else if (s == selectedList.size() - 1)
                    stringBuilder.append(selectedList.get(s));
                else
                    stringBuilder.append(selectedList.get(s)).append(",");
            }

            System.out.println("------------- SELECTED FILTER: " + stringBuilder.toString());

            if (!isFilterCleared) {
                if (!TextUtils.isEmpty(stringBuilder.toString())) {
                    filterItemsHolder.filterItemSelectedTV.setVisibility(View.VISIBLE);
                    filterItemsHolder.filterItemSelectedTV.setText(stringBuilder.toString());
                } else
                    filterItemsHolder.filterItemSelectedTV.setVisibility(View.GONE);
            } else {
                filterItemsHolder.filterItemSelectedTV.setVisibility(View.GONE);
            }

        }

        @Override
        public int getItemCount() {
            return filterItemList.size();
        }
    }

    private class FilterItemsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView filterItemTitleTV, filterItemSelectedTV;
        private LinearLayout filterItemMainView;

        FilterItemsHolder(View itemView) {
            super(itemView);
            filterItemMainView = itemView.findViewById(R.id.filter_item_main_view_id);
            filterItemTitleTV = itemView.findViewById(R.id.filter_item_title_tv);
            filterItemSelectedTV = itemView.findViewById(R.id.selected_filter_tv);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SUB_FILTER_REQ_CODE && resultCode == RESULT_OK) {
            //brandsFilterSelList.clear();
            ArrayList<String> filter_list = data.getStringArrayListExtra("SUB_FILTER_LIST");
            System.out.println("----- data selected size: " + filter_list.size());
            //System.out.println("----- data selected value: " + filter_list.get(0));
            filterMap.put(filterKey, filter_list);
        }
    }

}
