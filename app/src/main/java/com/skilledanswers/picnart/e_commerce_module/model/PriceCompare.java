package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by subh on 5/25/2016.
 */
public class PriceCompare {

    private int image_id;
    private int price;
    private String store_url;

    public PriceCompare() {
    }

    public int getImage_id() {
        return image_id;
    }

    public void setImage_id(int image_id) {
        this.image_id = image_id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getStore_url() {
        return store_url;
    }

    public void setStore_url(String store_url) {
        this.store_url = store_url;
    }
}
