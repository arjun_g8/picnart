package com.skilledanswers.picnart.e_commerce_module;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skilledanswers.picnart.R;

import bluepaymax.skilledanswers.picnart.R;


public class ViewPagerAdapter extends PagerAdapter {

    // Declare Variables
    Context context;
    String[] description;
    int[] images;
    LayoutInflater inflater;
    private Context mContext;

    public ViewPagerAdapter(Context context, String[] description,
                            int[] flag) {
        this.context = context;

        this.description = description;

        this.images = flag;
    }


    //constructor
    public ViewPagerAdapter(Context c) {
        mContext = c;
    }


    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        // Declare Variables

        TextView txtcountry;

        ImageView imgflag;

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);

        // Locate the TextViews in viewpager_item.xml

        txtcountry = itemView.findViewById(R.id.country);

        // Capture position and set to the TextViews

        txtcountry.setText(description[position]);


        // Locate the ImageView in viewpager_item.xml
        imgflag = itemView.findViewById(R.id.flag);
        // Capture position and set to the ImageView
        imgflag.setImageResource(images[position]);

        // Add viewpager_item.xml to ViewPager
        container.addView(itemView);

        return itemView;


    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // Remove viewpager_item.xml from ViewPager
        container.removeView((RelativeLayout) object);

    }
}
