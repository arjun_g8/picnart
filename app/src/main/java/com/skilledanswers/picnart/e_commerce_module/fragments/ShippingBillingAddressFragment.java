package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.Utils.PrefUtil;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.DistrictResponse;
import com.skilledanswers.picnart.e_commerce_module.retrofit_response_models.StateResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.model.BillingDataPost;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.ShippingAddressData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SkilledAnswers-D1 on 29-12-2017.
 */

public class ShippingBillingAddressFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private PrefUtil appUtil;
    private EcommerceCustomActivity ecommerceCustomActivity;
    private TextInputLayout nameTxtIp, mobTxtIp, addressTxtIp, landMarkTxtIp,
            pinCodeTxtIp, cityTxtIp,
            nameTxtIpBilling, mobTxtIpBilling, addressTxtIpBilling,
            landMarkTxtIpBilling,
            pinCodeTxtIpBilling, cityTxtIpBilling, stateTxtIpBilling, emailTxtIpBilling,
            districtBillingTxtIp;
    private TextInputEditText nameEd, mobEd, addressEd, landMarkEd, pinCodeEd, cityEd,
            nameEdBilling, mobEdBilling, addressEdBilling, landMarkEdBilling, pinCodeEdBilling, cityEdBilling,
            stateEdBilling, emailEdBilling, districtEdBilling;
    private AppCompatCheckBox checkBox;
    private String name = "", address = "", city = "", state = "", phNum = "", landMark = "", pinCode = "", email = "",
            nameBilling, addressBilling, cityBilling, stateBilling, phNumBilling, landMarkBilling = "", pinCodeBilling, emailBilling = "";

    private AppCompatSpinner stateSpinner, districtSpinner;
    private ProgressBar loadingBar;
    private NestedScrollView addAddressView;

    private String stateCodeBilling, stateCodeShipping, districtCodeShipping, districtCodeBilling;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.shipping_billing_ragment, container, false);

        appUtil = new PrefUtil(ecommerceCustomActivity);
        setHasOptionsMenu(true);

        addAddressView = view.findViewById(R.id.add_address_view);

        loadingBar = view.findViewById(R.id.progress_bar_shipping_billing);

        nameTxtIp = view.findViewById(R.id.name_txt_ip_shipping);
        mobTxtIp = view.findViewById(R.id.mob_txt_ip_shipping);
        addressTxtIp = view.findViewById(R.id.address_txt_ip_shipping);
        landMarkTxtIp = view.findViewById(R.id.landmark_txt_ip_shipping);
        cityTxtIp = view.findViewById(R.id.city_txt_ip_shipping);
        pinCodeTxtIp = view.findViewById(R.id.pincode_txt_ip_shipping);
        stateSpinner = view.findViewById(R.id.state_spinner_shipping);
        districtSpinner = view.findViewById(R.id.district_spinner_shipping_address);

        stateSpinner.setOnItemSelectedListener(this);
        districtSpinner.setOnItemSelectedListener(this);

        nameTxtIpBilling = view.findViewById(R.id.name_txt_ip_billing);
        mobTxtIpBilling = view.findViewById(R.id.mob_txt_ip_billing);
        emailTxtIpBilling = view.findViewById(R.id.email_txt_ip_billing);
        addressTxtIpBilling = view.findViewById(R.id.address_txt_ip_billing);
        landMarkTxtIpBilling = view.findViewById(R.id.land_mark_txt_ip_billing);
        cityTxtIpBilling = view.findViewById(R.id.city_txt_ip_billing);
        pinCodeTxtIpBilling = view.findViewById(R.id.pin_code_txt_ip_billing);
        stateTxtIpBilling = view.findViewById(R.id.state_txt_ip_billing);
        districtBillingTxtIp = view.findViewById(R.id.district_txt_ip_billing);

        nameEd = view.findViewById(R.id.name_ed_shipping);
        mobEd = view.findViewById(R.id.mob_ed_shipping);
        addressEd = view.findViewById(R.id.address_ed_shipping);
        landMarkEd = view.findViewById(R.id.land_mark_ed_shipping);
        cityEd = view.findViewById(R.id.city_ed_shipping);
        pinCodeEd = view.findViewById(R.id.pin_code_ed_shipping);

        nameEdBilling = view.findViewById(R.id.name_ed_billing);
        mobEdBilling = view.findViewById(R.id.mob_ed_billing);
        emailEdBilling = view.findViewById(R.id.email_billing);
        addressEdBilling = view.findViewById(R.id.address_ed_billing);
        landMarkEdBilling = view.findViewById(R.id.land_mark_ed_billing);
        cityEdBilling = view.findViewById(R.id.city_ed_billing);
        pinCodeEdBilling = view.findViewById(R.id.pin_code_ed_billing);
        stateEdBilling = view.findViewById(R.id.state_ed_billing);
        districtEdBilling = view.findViewById(R.id.district_ed_billing);

        checkBox = view.findViewById(R.id.billing_addr_same_cb);
        checkBox.setChecked(false);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    setBillingAddressWithShippingAddress();
                } else {
                    clearBillingAddress();
                }
            }
        });

        view.findViewById(R.id.save_address_btn).setOnClickListener(this);

        applyTextChangeListenerToView(nameEd, nameTxtIp);
        applyTextChangeListenerToView(mobEd, mobTxtIp);
        applyTextChangeListenerToView(addressEd, addressTxtIp);
        applyTextChangeListenerToView(cityEd, cityTxtIp);
        applyTextChangeListenerToView(pinCodeEd, pinCodeTxtIp);

        applyTextChangeListenerToView(nameEdBilling, nameTxtIpBilling);
        applyTextChangeListenerToView(mobEdBilling, mobTxtIpBilling);
        applyTextChangeListenerToView(emailEdBilling, emailTxtIpBilling);
        applyTextChangeListenerToView(addressEdBilling, addressTxtIpBilling);
        applyTextChangeListenerToView(cityEdBilling, cityTxtIpBilling);
        applyTextChangeListenerToView(pinCodeEdBilling, pinCodeTxtIpBilling);
        applyTextChangeListenerToView(stateEdBilling, stateTxtIpBilling);

        initView();

        return view;
    }

    private void initView() {
        addAddressView.setVisibility(View.GONE);
        if (CommonUtils.isThereInternet(getActivity())) {
            loadingBar.setIndeterminate(true);
            loadingBar.setVisibility(View.VISIBLE);
            fetchStates();
        } else {
            DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
        }
    }

    private void setSameStateAndDistrict() {
        stateEdBilling.setText(selectedState);
        districtEdBilling.setText(selectedDistrict);
    }

    private void fetchStates() {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getAllStates("getAllStates").enqueue(new Callback<StateResponse>() {
            @Override
            public void onResponse(Call<StateResponse> call, Response<StateResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus()) {
                            stateList = response.body().getData();

                            if (stateList != null) {

                                Collections.sort(stateList, new Comparator<StateResponse.StateData>() {
                                    @Override
                                    public int compare(StateResponse.StateData stateData, StateResponse.StateData t1) {
                                        return stateData.getStateName().
                                                compareToIgnoreCase(t1.getStateName());
                                    }
                                });

                                List<String> states = new ArrayList<>();

                                for (StateResponse.StateData State : stateList) {
                                    states.add(State.getStateName());
                                }

                                stateSpinner.setAdapter(new ArrayAdapter<>(ecommerceCustomActivity,
                                        android.R.layout.simple_selectable_list_item, states));
                            }
                            loadingBar.setVisibility(View.GONE);
                            addAddressView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<StateResponse> call, Throwable t) {

            }
        });
    }

    private void getDistrictsOfState(String stateCode) {
        Log.i("DISTRICT", "state code: " + stateCode);
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.getDistrictsOfState("getDistrictDetails",
                stateCode).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    String s = new String(response.body().bytes());

                    String jsonString = s.replaceAll("\\s+", "");

                    Log.i("DISTRICT", "response: " +
                            s + "\n json: " +
                            jsonString);

                    DistrictResponse districtResponse = new Gson().fromJson(jsonString,
                            DistrictResponse.class);

                    if (districtResponse.getStatus()) {
                        districtList = districtResponse.getData();

                        if (districtList.size() > 0) {
                            Collections.sort(districtList, new Comparator<DistrictResponse.DistrictData>() {
                                @Override
                                public int compare(DistrictResponse.DistrictData districtData, DistrictResponse.DistrictData t1) {
                                    return districtData.getDistrictName().
                                            compareToIgnoreCase(t1.getDistrictName());
                                }
                            });

                            List<String> districts = new ArrayList<>();

                            for (DistrictResponse.DistrictData district : districtList) {
                                districts.add(district.getDistrictName());
                            }

                            districtSpinner.setAdapter(new ArrayAdapter<>(ecommerceCustomActivity,
                                    android.R.layout.simple_selectable_list_item, districts));
                        }
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                Log.i("DISTRICT", "exc: " + t.toString());
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        menu.clear();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ecommerceCustomActivity = (EcommerceCustomActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ecommerceCustomActivity.hideSearchBar();
        ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("New / Update Address");
        /*ecommerceCustomActivity.hideToolBarLogo();
        ecommerceCustomActivity.setAppToolBarTitle("New / Update Address");*/
    }

    private void applyTextChangeListenerToView(final TextInputEditText view, final TextInputLayout errorView) {
        view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                errorView.setErrorEnabled(false);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void requestFocus(View view) {
        view.requestFocus();
    }

    private void showErrorMessage(String error, TextInputLayout view) {
        view.setError(error);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save_address_btn:
                if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
                    if (isAllRequiredFieldsVerified()) {
                        ShippingAddressData shippingData = new ShippingAddressData();
                        shippingData.setShippingId("");
                        shippingData.setAddress(address);
                        shippingData.setName(name);
                        shippingData.setMobile(phNum);
                        shippingData.setLandmark(landMark);
                        shippingData.setCity(city);
                        shippingData.setPin(pinCode);
                        shippingData.setState(selectedState);
                        shippingData.setDistrict(selectedDistrict);
                        shippingData.setCompany("N/A");


                        BillingDataPost billingDataPost = new BillingDataPost();
                        billingDataPost.setAddress(addressBilling);
                        billingDataPost.setName(nameBilling);
                        billingDataPost.setMobile(phNumBilling);
                        billingDataPost.setLandmark(landMarkBilling);
                        billingDataPost.setCity(cityBilling);
                        billingDataPost.setPin(pinCodeBilling);
                        billingDataPost.setState(stateBilling);
                        billingDataPost.setDistrict(districtBilling);
                        billingDataPost.setCompany("N/A");

                        PrefUtil prefUtil = new PrefUtil(ecommerceCustomActivity);

                        billingDataPost.setEmail(prefUtil.getEmail());
                        shippingData.setEmail(prefUtil.getEmail());

                        System.out.println("--------Shipping JSON OBJ: " + new Gson().toJson(shippingData));
                        System.out.println("--------Billing JSON OBJ: " + new Gson().toJson(billingDataPost));

                        /*try {
                            shippingJsonObject = new JSONObject(json);
                            billingJsonObject = new JSONObject(billingJson);


                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("--------EXC: " + e.toString());
                        }*/

                        saveAddress(new Gson().toJson(shippingData), new Gson().toJson(billingDataPost));
                    }
                } else {
                    DialogUtils.showNoInternetDialog(ecommerceCustomActivity);
                }
                break;
        }
    }

    private String districtBilling;

    private String selectedState, selectedDistrict;

    private boolean isAllRequiredFieldsVerified() {
        boolean verified = false;

        name = nameEd.getText().toString();
        address = addressEd.getText().toString();
        city = cityEd.getText().toString();
        state = selectedState;
        pinCode = pinCodeEd.getText().toString();
        phNum = mobEd.getText().toString();
        landMark = landMarkEd.getText().toString();

        nameBilling = nameEdBilling.getText().toString();
        addressBilling = addressEdBilling.getText().toString();
        cityBilling = cityEdBilling.getText().toString();
        pinCodeBilling = pinCodeEdBilling.getText().toString();
        phNumBilling = mobEdBilling.getText().toString();
        emailBilling = emailEdBilling.getText().toString();
        landMarkBilling = landMarkEdBilling.getText().toString();
        stateBilling = stateEdBilling.getText().toString();
        districtBilling = districtEdBilling.getText().toString();

        if (TextUtils.isEmpty(name)) {
            showErrorMessage("Please enter Name", nameTxtIp);
            requestFocus(nameEd);
        } else if (TextUtils.isEmpty(phNum)) {
            showErrorMessage("Please enter Phone number", mobTxtIp);
            requestFocus(mobEd);
        } else if (phNum.length() < 10) {
            showErrorMessage("Please enter 10 digit Phone number", mobTxtIp);
            requestFocus(mobEd);
        } else if (TextUtils.isEmpty(address)) {
            showErrorMessage("Please enter address", addressTxtIp);
            requestFocus(addressEd);
        } else if (address.length() < 15) {
            showErrorMessage("Address should be more than 15 characters", addressTxtIp);
            requestFocus(addressEd);
        } else if (TextUtils.isEmpty(city)) {
            showErrorMessage("Please enter City", cityTxtIp);
            requestFocus(cityEd);
        } else if (TextUtils.isEmpty(pinCode)) {
            showErrorMessage("Please enter Pincode", pinCodeTxtIp);
            requestFocus(pinCodeEd);
        } else if (pinCode.length() < 6) {
            showErrorMessage("Please enter Valid Pincode", pinCodeTxtIp);
            requestFocus(pinCodeEd);
        } else if (TextUtils.isEmpty(nameBilling)) {
            showErrorMessage("Please enter Name", nameTxtIpBilling);
            requestFocus(nameEdBilling);
        } else if (TextUtils.isEmpty(phNumBilling)) {
            showErrorMessage("Please enter Phone number", mobTxtIpBilling);
            requestFocus(mobEdBilling);
        } else if (phNumBilling.length() < 10) {
            showErrorMessage("Please enter 10 digit Phone number", mobTxtIpBilling);
            requestFocus(mobEdBilling);
        } else if (TextUtils.isEmpty(email)) {
            showErrorMessage("Please enter Email Id", emailTxtIpBilling);
            requestFocus(emailEdBilling);
        } else if (TextUtils.isEmpty(addressBilling)) {
            showErrorMessage("Please enter address", addressTxtIpBilling);
            requestFocus(addressEdBilling);
        } else if (addressBilling.length() < 15) {
            showErrorMessage("Address should be more than 15 characters", addressTxtIpBilling);
            requestFocus(addressEdBilling);
        } else if (TextUtils.isEmpty(city)) {
            showErrorMessage("Please enter City", cityTxtIpBilling);
            requestFocus(cityEdBilling);
        } else if (TextUtils.isEmpty(stateBilling)) {
            showErrorMessage("Please enter State in Billing", stateTxtIpBilling);
            requestFocus(cityEdBilling);
        } else if (TextUtils.isEmpty(districtBilling)) {
            showErrorMessage("Please enter District in Billing", districtBillingTxtIp);
            requestFocus(cityEdBilling);
        } else if (TextUtils.isEmpty(pinCodeBilling)) {
            showErrorMessage("Please enter Pincode", pinCodeTxtIpBilling);
            requestFocus(pinCodeEdBilling);
        } else if (pinCodeBilling.length() < 6) {
            showErrorMessage("Please enter Valid Pincode", pinCodeTxtIpBilling);
            requestFocus(pinCodeEdBilling);
        } else
            verified = true;

        return verified;
    }

    private void saveAddress(String shippingData, String billingData) {
        EcommerceAPIInterface ecommerceAPIInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        ecommerceAPIInterface.saveToMyAddresses("addUserShippingAddress", appUtil.getRegNo(),
                appUtil.getToken(), shippingData, billingData).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response != null) {
                    Log.e("POST_ADDRESS", response.raw().toString());
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                String responseJson = new String(response.body().bytes());
                                JSONObject jsonObject = new JSONObject(responseJson);

                                if (jsonObject.getBoolean("status")) {
                                    if (!getActivity().isFinishing()) {
                                        Toast.makeText(getActivity(), "Your Address Saved Successfully!", Toast.LENGTH_SHORT).show();

                                        getActivity().getSupportFragmentManager().popBackStackImmediate();
                                        DeliveryAddressFragment addresses = new DeliveryAddressFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putBoolean("ORDER_PLACE", true);
                                        addresses.setArguments(bundle);
                                        ecommerceCustomActivity.moveToFragment(addresses, "DELIVERY_ADDRESS");
                                    }
                                } else {
                                    if (!getActivity().isFinishing())
                                        Toast.makeText(getActivity(), "" + jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                                }

                                System.out.println("-----------SAVE ADDRESS response: " + new Gson().toJson(jsonObject));
                            } catch (IOException e) {
                                e.printStackTrace();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            System.out.println("--------UN SUCCESS ");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                t.printStackTrace();
                System.out.println("--------EXC: " + t.toString());
            }
        });
    }

    private void setBillingAddressWithShippingAddress() {
        name = nameEd.getText().toString();
        address = addressEd.getText().toString();
        city = cityEd.getText().toString();
        state = selectedState;
        pinCode = pinCodeEd.getText().toString();
        phNum = mobEd.getText().toString();
        PrefUtil prefUtil = new PrefUtil(ecommerceCustomActivity);

        email = prefUtil.getEmail();
        landMark = landMarkEd.getText().toString();

        setSameStateAndDistrict();

        nameEdBilling.setText(name);
        mobEdBilling.setText(phNum);
        emailEdBilling.setText(email);
        landMarkEdBilling.setText(landMark);
        pinCodeEdBilling.setText(pinCode);
        addressEdBilling.setText(address);
        cityEdBilling.setText(city);
    }

    private void clearBillingAddress() {
        nameEdBilling.setText("");
        mobEdBilling.setText("");
        emailEdBilling.setText("");
        landMarkEdBilling.setText("");
        pinCodeEdBilling.setText("");
        addressEdBilling.setText("");
        cityEdBilling.setText("");
        stateEdBilling.setText("");
        districtEdBilling.setText("");
    }

    private List<StateResponse.StateData> stateList = new ArrayList<>();
    private List<DistrictResponse.DistrictData> districtList = new ArrayList<>();

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if (adapterView == stateSpinner) {
            selectedState = stateList.get(i).getStateName();
            if (CommonUtils.isThereInternet(ecommerceCustomActivity)) {
                getDistrictsOfState(stateList.get(i).getStateCode());
            }
        } else if (adapterView == districtSpinner) {
            selectedDistrict = districtList.get(i).getDistrictName();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
