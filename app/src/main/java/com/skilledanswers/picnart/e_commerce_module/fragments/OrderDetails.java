package com.skilledanswers.picnart.e_commerce_module.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;


public class OrderDetails extends Fragment {

    EcommerceCustomActivity activity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        activity = (EcommerceCustomActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.setToggle(false);
        return inflater.inflate(R.layout.order_details_fragment_view, container, false);
    }


}
