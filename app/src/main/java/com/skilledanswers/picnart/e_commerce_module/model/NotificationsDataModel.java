package bluepaymax.skilledanswers.picnart.e_commerce_module.model;

/**
 * Created by SkilledAnswers-D1 on 21-11-2017.
 */

public class NotificationsDataModel {
    private int image;
    private String status, description, receivedOn;

    public NotificationsDataModel(int image, String status, String description, String receivedOn) {
        this.image = image;
        this.status = status;
        this.description = description;
        this.receivedOn = receivedOn;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReceivedOn() {
        return receivedOn;
    }

    public void setReceivedOn(String receivedOn) {
        this.receivedOn = receivedOn;
    }
}
