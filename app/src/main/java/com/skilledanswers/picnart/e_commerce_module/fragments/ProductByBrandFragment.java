package com.skilledanswers.picnart.e_commerce_module.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.skilledanswers.picnart.R;
import com.skilledanswers.picnart.Utils.DialogUtils;
import com.skilledanswers.picnart.e_commerce_module.CommonUtils;
import com.skilledanswers.picnart.e_commerce_module.EcomPrefUtil;
import com.skilledanswers.picnart.e_commerce_module.EcommerceCustomActivity;
import com.skilledanswers.picnart.e_commerce_module.retofit.BasicAuthorization;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIClient;
import com.skilledanswers.picnart.e_commerce_module.retofit.EcommerceAPIInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import bluepaymax.skilledanswers.picnart.e_commerce_module.model.ProductByBrandModel;
import bluepaymax.skilledanswers.picnart.e_commerce_module.model.WishListItemsResponseObject;

/**
 * Created by SkilledAnswers-D1 on 09-11-2017.
 */

public class ProductByBrandFragment extends CustomFragment {
    EcommerceCustomActivity activity;
    RecyclerView recList;
    private String listFor, categoryId;
    private List<WishListItemsResponseObject.WishListDataGet> wishListItems = new ArrayList<>();
    private List<ProductByBrandModel.Datum> productByBrandsList;
    ProductByBrandHolder prHolder;
    private EcomPrefUtil prefUtil;
    private int userId;
    //  private AppCompatTextView noItemsTV;
    private View noItemView;
    private String urlForProductImg = "www.google.com";
    private FloatingActionButton sortFAB;
    private View mainView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = (EcommerceCustomActivity) getActivity();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setDisplayShowHomeEnabled(true);
        activity.setToggle(false);
        if (mainView == null)
            mainView = inflater.inflate(R.layout.main_container, container, false);
        noItemView = mainView.findViewById(R.id.no_item_view_id);

        prefUtil = new EcomPrefUtil(activity);
        //userId = Integer.parseInt(prefUtil.getUserDetails().getUserId());
        userId = 27;

        sortFAB = mainView.findViewById(R.id.sort_btn_in_products_list);

        // noItemsTV = (AppCompatTextView) v.findViewById(R.id.no_items_txt);
        recList = mainView.findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        // GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recList.setLayoutManager(linearLayoutManager);

        listFor = getArguments().getString("LIST_FOR", "");
        categoryId = getArguments().getString("CATEGORY_ID", "");
        System.out.println("--- CATEGORY ID: " + categoryId);

        sortFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSortBottomSheetDialog();
                // showDialogForSorting();
            }
        });

        if (CommonUtils.isThereInternet(activity)) {
            loadAllWishListItems();
            loadProductByBrand(categoryId);
        } else
            DialogUtils.showNoInternetDialog(activity);

        return mainView;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("---- ON RESUME");
       /* if (CommonUtilsEcommerce.isThereInternet(activity)) {
            loadAllWishListItems();
            loadProductByBrand(categoryId);
        } else
            DialogUtils.showNoInternetDialog(activity);*/
    }

    private void showSortBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(getActivity());
        View sheetView = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_for_sort, null);
        mBottomSheetDialog.setContentView(sheetView);

        final RadioGroup timeRadioGroup = sheetView.findViewById(R.id.radio_group_for_sort);

        timeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.low_to_high:
                        sortProducts("LOW TO HIGH");
                        Toast.makeText(getActivity(), "Low to high", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.high_to_low:
                        sortProducts("HIGH TO LOW");
                        Toast.makeText(getActivity(), "High to Low", Toast.LENGTH_SHORT).show();
                        break;
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBottomSheetDialog.dismiss();

                    }
                }, 250);

            }
        });

        mBottomSheetDialog.show();
    }


    private void showDialogForSorting() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sort_dialog_layout);

        final RadioGroup timeRadioGroup = dialog.findViewById(R.id.radio_group_for_sort);

        timeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.low_to_high:
                        sortProducts("LOW TO HIGH");
                        Toast.makeText(getActivity(), "Low to high", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.high_to_low:
                        sortProducts("HIGH TO LOW");
                        Toast.makeText(getActivity(), "High to Low", Toast.LENGTH_SHORT).show();
                        break;
                }
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();

                    }
                }, 250);

            }
        });

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void sortProducts(String sortBy) {
        System.out.println("**** PRICE BEFORE SORT: ");

        for (int i = 0; i < productByBrandsList.size(); i++) {
            System.out.println("**** Rs. " + productByBrandsList.get(i).getSalePrice());
        }

        switch (sortBy) {
            case "HIGH TO LOW":
                Collections.sort(productByBrandsList, new Comparator<ProductByBrandModel.Datum>() {
                    @Override
                    public int compare(final ProductByBrandModel.Datum object1, final ProductByBrandModel.Datum object2) {
                        int obj1, obj2;
                        obj1 = object1.getSalePrice();
                        obj2 = object2.getSalePrice();
                        return obj2 - obj1;
                    }
                });

                System.out.println("\n\n**** PRICE AFTER SORT: ");

                for (int i = 0; i < productByBrandsList.size(); i++) {
                    System.out.println("**** Rs. " + productByBrandsList.get(i).getSalePrice());
                }

                break;
            case "LOW TO HIGH":
                Collections.sort(productByBrandsList, new Comparator<ProductByBrandModel.Datum>() {
                    @Override
                    public int compare(final ProductByBrandModel.Datum object1, final ProductByBrandModel.Datum object2) {
                        int obj1, obj2;
                        obj1 = object1.getSalePrice();
                        obj2 = object2.getSalePrice();
                        return obj1 - obj2;
                    }
                });
                break;
        }


        if (productByBrandsList.size() > 0) {
            System.out.println("------- PRODUCT IMAGES1: " + String.valueOf(productByBrandsList.get(0).getImage()));
            //  System.out.println("------- PRODUCT IMAGES2: " + String.valueOf(productByBrandsList.get(0).getImage().get(0)));
            RecyclerView.Adapter adapter = new ProductByBrandAdapter();
            recList.setAdapter(adapter);
            //adapter.notifyDataSetChanged();
        } else {
            recList.setVisibility(View.GONE);
            noItemView.setVisibility(View.VISIBLE);
        }
    }

    public static ProductByBrandFragment newInstance() {
        return new ProductByBrandFragment();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (EcommerceCustomActivity) getActivity();
    }

    private void loadProductByBrand(String categoryId) {
        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        String url = CommonUtils.BASE_URL + "operator/productbybrand/" + categoryId;
        Log.e("BRAND URL: ", url);
    }

    private class ProductByBrandAdapter extends RecyclerView.Adapter {

        public ProductByBrandAdapter() {
            for (int i = 0; i < productByBrandsList.size(); i++) {
                if (productByBrandsList.get(i).getImage() != null) {
                    productByBrandsList.get(i).setHasImage(true);
                } else
                    productByBrandsList.get(i).setHasImage(false);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(activity)
                    .inflate(R.layout.new_product_item_layout, parent, false);
            return new ProductByBrandHolder(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            prHolder = (ProductByBrandHolder) holder;

            if (productByBrandsList.get(position).getImage() != null) {
                urlForProductImg = CommonUtils.BASE_URL + "operator/image/" + productByBrandsList.get(position).getImage().get(0);
                Log.e("PRODUCT BY CATEGORY: ", "" + urlForProductImg);
            }

        /*    if (productByBrandsList.get(position).getImage() != null) {
                if (productByBrandsList.get(position).getImage().size() > 0)
                    urlForProductImg = CommonUtilsEcommerce.BASE_URL + "operator/image/" + productByBrandsList.get(prHolder.getAdapterPosition()).getImage().get(0);
            }*/

            int sellingPrice = productByBrandsList.get(position).getSalePrice();
            int purchasePrice = productByBrandsList.get(position).getPurchasePrice();

            if (purchasePrice == sellingPrice) {
                prHolder.purchasePriceTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                prHolder.purchasePriceTV.setText("");
            } else {
                prHolder.purchasePriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian_grey, 0, 0, 0);
                prHolder.purchasePriceTV.setPaintFlags(prHolder.purchasePriceTV.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                prHolder.purchasePriceTV.setText(String.valueOf(productByBrandsList.get(position).getPurchasePrice()));
            }

            prHolder.sellPriceTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_rupee_indian, 0, 0, 0);

            prHolder.productNameTV.setText(productByBrandsList.get(position).getName());
            prHolder.sellPriceTV.setText("" + productByBrandsList.get(position).getSalePrice());

            LazyHeaders auth = new LazyHeaders.Builder() // can be cached in a field and reused
                    .addHeader("Authorization", new BasicAuthorization("admin", "1234"))
                    .build();

            if (productByBrandsList.get(position).isHasImage()) {
                if (urlForProductImg != null) {
                    Glide.with(activity)
                            .load(new GlideUrl(urlForProductImg, auth))
                            .placeholder(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .error(ContextCompat.getDrawable(activity, R.drawable.ic_def_pic))
                            .listener(new RequestListener<GlideUrl, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, GlideUrl model, Target<GlideDrawable> target, boolean isFirstResource) {
                                   /* e.printStackTrace();
                                    System.out.println("---- glide ex: " + e.toString());*/
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, GlideUrl model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    return false;
                                }
                            })
                            .dontAnimate()
                            .into(prHolder.productIV);
                } else {
                    prHolder.productIV.setImageResource(R.drawable.ic_def_pic);
                }
            } else
                prHolder.productIV.setImageResource(R.drawable.ic_def_pic);


            prHolder.productView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ArrayList<String> productAttributesList = new ArrayList<>();

                    if (productByBrandsList.get(position).getProductAttributs() != null) {
                        if (productByBrandsList.get(position).getProductAttributs().size() > 0) {
                            for (int i = 0; i < productByBrandsList.get(position).getProductAttributs().size(); i++) {
                                productAttributesList.add(productByBrandsList.get(position).getProductAttributs().get(i).getAttributeName() + "_" +
                                        (productByBrandsList.get(position).getProductAttributs().get(i).getValue()));
                            }
                        }
                    }

                    // notifyDataSetChanged();
                    System.out.println("*********** URL for product image: " + urlForProductImg);
                    moveToNextFragment(productByBrandsList.get(position).getProductId(), productByBrandsList.get(position).getSalePrice(),
                            productByBrandsList.get(position).getPurchasePrice(),
                            urlForProductImg, productByBrandsList.get(position).getName(), productByBrandsList.get(position).getImage(),
                            productAttributesList);
                }
            });

            prHolder.wishListBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CommonUtils.isThereInternet(activity)) {
                        if (productByBrandsList.size() > 0) {
                            if (checkIfAlreadyAddedToWishList(productByBrandsList.get(position).getProductId())) {
                                // prHolder.wishListBtn.setImageResource(R.drawable.ic_wishlist_heart_red);
                                removeFromWishList(position);
                            } else {
                                addToWishList(productByBrandsList.get(position).getName(), productByBrandsList.get(position).getProductId(), position);
                                // prHolder.productByBrandsList.setImageResource(R.drawable.ic_wishlist_heart);
                            }
                            // notifyDataSetChanged();
                        }
                    } else
                        DialogUtils.showNoInternetDialog(activity);


                }
            });

            if (productByBrandsList.size() > 0) {
                if (checkIfAlreadyAddedToWishList(productByBrandsList.get(position).getProductId())) {
                    prHolder.wishListBtn.setImageResource(R.drawable.ic_wishlist_heart_red);
                } else {
                    prHolder.wishListBtn.setImageResource(R.drawable.ic_wishlist_heart);
                }
            }

        }

        @Override
        public int getItemCount() {
            return productByBrandsList.size();
        }
    }

    private void moveToNextFragment(int productId, int productPrice, int originalPrice, String imgUrlForMainProductIV,
                                    String productName, List<String> imgList
            , ArrayList<String> productAttributesList) {
        Bundle bundle = new Bundle();
        bundle.putInt("CATEGORY_ID", productId);
        //  bundle.putString("LIST_FOR", "Product by category");
        bundle.putInt("PRODUCT_PRICE", productPrice);
        bundle.putInt("PRODUCT_ORIGINAL_PRICE", originalPrice);
        bundle.putString("PRODUCT_IMG_URL", imgUrlForMainProductIV);
        bundle.putString("PRODUCT_NAME", productName);
        bundle.putStringArrayList("IMG_ARRAY_LIST", (ArrayList<String>) imgList);
        bundle.putStringArrayList("PRODUCT_ATTRIBUTES_LIST", productAttributesList);
        Fragment fragment = new DetailFragment();

        FragmentManager fragmentManager = activity.getSupportFragmentManager();

        fragment.setArguments(bundle);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // transaction.setCustomAnimations(R.animator.in,R.animator.out);
        transaction.setCustomAnimations(R.anim.left_to_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        transaction.replace(R.id.relative, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private class ProductByBrandHolder extends RecyclerView.ViewHolder {
        private LinearLayout productView;
        private AppCompatTextView sellPriceTV, purchasePriceTV, productNameTV;
        private AppCompatImageView productIV;
        private AppCompatImageButton wishListBtn;
//        private LinearLayout

        ProductByBrandHolder(View itemView) {
            super(itemView);
            productView = itemView.findViewById(R.id.product_item_m_view);
            sellPriceTV = itemView.findViewById(R.id.selling_price_tv);
            purchasePriceTV = itemView.findViewById(R.id.purchase_pice_tv);
            productNameTV = itemView.findViewById(R.id.product_name_tv);
            productIV = itemView.findViewById(R.id.product_iv);
            wishListBtn = itemView.findViewById(R.id.wishlist_img_btn_in_product_list);
        }
    }

    private void addToWishList(String prodName, int prodId, final int position) {
       /* EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        final AddToWishListPostObject addToWishListPostObject = new AddToWishListPostObject();
        addToWishListPostObject.setName(prodName);
        AddToWishListPostObject.ProductBeanPostWishList productBean = new AddToWishListPostObject.ProductBeanPostWishList();
        productBean.setProductId(prodId);
        AddToWishListPostObject.UserObjectPostWishList userObjectPostWishList = new AddToWishListPostObject.UserObjectPostWishList();
        userObjectPostWishList.setUserId(userId);
        addToWishListPostObject.setProductBean(productBean);
        addToWishListPostObject.setUser(userObjectPostWishList);
        apiInterface.addToWishList(addToWishListPostObject).enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                Log.e("ADD_TO_WISHLIST: ", response.raw().toString());
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        productByBrandsList.get(position).setAddedToWishList(true);

                        Log.e("Added to wish list:", "" + productByBrandsList.get(position).isAddedToWishList());
                        recList.getAdapter().notifyItemRangeChanged(position, wishListItems.size());
                        Toast.makeText(activity, "Added to wish list", Toast.LENGTH_SHORT).show();

                        if (CommonUtilsEcommerce.isThereInternet(activity)) {
                            loadAllWishListItems();
                            loadProductByBrand(categoryId);
                        } else
                            DialogUtils.showNoInternetDialog(activity);
                    }
                } else
                    Toast.makeText(activity, "Try later", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private void loadAllWishListItems() {
        /*EcomPrefUtil prefUtil = new EcomPrefUtil(getActivity());
        int userId = Integer.parseInt(prefUtil.getUserDetails().getUserId());
        String urlForWishListItems = CommonUtilsEcommerce.BASE_URL + "user/wishlist/" + userId;
        Log.e("URL for Wishlist Get: ", urlForWishListItems);

        EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        apiInterface.getAllWishListItems(urlForWishListItems).enqueue(new Callback<WishListItemsResponseObject>() {
            @Override
            public void onResponse(Call<WishListItemsResponseObject> call, Response<WishListItemsResponseObject> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus()) {
                        wishListItems = response.body().getData();
                    }
                } else
                    Toast.makeText(activity, "Something went wrong! Please try later", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<WishListItemsResponseObject> call, Throwable t) {
                Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });*/
    }

    private boolean checkIfAlreadyAddedToWishList(int prodId) {
        boolean exists = false;
        if (wishListItems.size() > 0) {
            for (int i = 0; i < wishListItems.size(); i++) {
                if (wishListItems.get(i).getProductBean().getProductId() == prodId) {
                    exists = true;
                    break;
                }
            }
        }
        return exists;
    }

    private void removeFromWishList(final int pos) {
       /* EcommerceAPIInterface apiInterface = EcommerceAPIClient.getClient().create(EcommerceAPIInterface.class);
        try {
            WishListRemovePostObject wishListRemovePostObject = new WishListRemovePostObject();
            wishListRemovePostObject.setWishListId(wishListItems.get(pos).getWishListId());
            apiInterface.removeFromWishList(wishListRemovePostObject).enqueue(new Callback<GeneralResponse>() {
                @Override
                public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getStatus()) {
                            productByBrandsList.get(pos).setAddedToWishList(false);
                            if (wishListItems.size() > 0) {
                                wishListItems.remove(pos);
                                recList.getAdapter().notifyItemRangeChanged(pos, wishListItems.size());
                                Toast.makeText(activity, "Removed from wish list", Toast.LENGTH_SHORT).show();

                                if (CommonUtilsEcommerce.isThereInternet(activity)) {
                                    loadAllWishListItems();
                                    loadProductByBrand(categoryId);
                                } else
                                    DialogUtils.showNoInternetDialog(activity);
                            } else
                                Toast.makeText(activity, "No items to remove", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<GeneralResponse> call, Throwable t) {
                    Toast.makeText(activity, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
}
