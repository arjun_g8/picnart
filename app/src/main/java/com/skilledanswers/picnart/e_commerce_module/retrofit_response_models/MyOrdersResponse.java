package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

/**
 * Created by SkilledAnswers-D1 on 30-12-2017.
 */

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrdersResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<OrderedData> data = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<OrderedData> getData() {
        return data;
    }

    public void setData(List<OrderedData> data) {
        this.data = data;
    }

    public class OrderedData implements Parcelable {

        @SerializedName("orderId")
        @Expose
        private Integer orderId;
        @SerializedName("orderDetailId")
        @Expose
        private Integer orderDetailId;
        @SerializedName("total_amount")
        @Expose
        private String totalAmount;
        @SerializedName("cart_type")
        @Expose
        private String cartType;
        @SerializedName("orderStatus")
        @Expose
        private String orderStatus;
        @SerializedName("order_no")
        @Expose
        private String orderNo;
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("product_code")
        @Expose
        private String productCode;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("quantity")
        @Expose
        private Integer quantity;
        @SerializedName("created")
        @Expose
        private String created;
        @SerializedName("updated")
        @Expose
        private String updated;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("card_flag")
        @Expose
        private Integer cardFlag;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("category_id")
        @Expose
        private Integer categoryId;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("new")
        @Expose
        private String _new;
        @SerializedName("after")
        @Expose
        private String after;
        @SerializedName("cancelled_reason")
        @Expose
        private String cancelledReason;
        @SerializedName("cancelled_order")
        @Expose
        private Boolean cancelledOrder;
        @SerializedName("flag")
        @Expose
        private Boolean flag;

        OrderedData(Parcel in) {
            if (in.readByte() == 0) {
                orderId = null;
            } else {
                orderId = in.readInt();
            }
            if (in.readByte() == 0) {
                orderDetailId = null;
            } else {
                orderDetailId = in.readInt();
            }
            totalAmount = in.readString();
            cartType = in.readString();
            orderStatus = in.readString();
            orderNo = in.readString();
            if (in.readByte() == 0) {
                productId = null;
            } else {
                productId = in.readInt();
            }
            productCode = in.readString();
            price = in.readString();
            if (in.readByte() == 0) {
                quantity = null;
            } else {
                quantity = in.readInt();
            }
            created = in.readString();
            updated = in.readString();
            status = in.readString();
            if (in.readByte() == 0) {
                cardFlag = null;
            } else {
                cardFlag = in.readInt();
            }
            brand = in.readString();
            if (in.readByte() == 0) {
                categoryId = null;
            } else {
                categoryId = in.readInt();
            }
            title = in.readString();
            pv = in.readString();
            image = in.readString();
            createdAt = in.readString();
            _new = in.readString();
            after = in.readString();
            cancelledReason = in.readString();
            byte tmpCancelledOrder = in.readByte();
            cancelledOrder = tmpCancelledOrder == 0 ? null : tmpCancelledOrder == 1;
            byte tmpFlag = in.readByte();
            flag = tmpFlag == 0 ? null : tmpFlag == 1;
        }

        public final Creator<OrderedData> CREATOR = new Creator<OrderedData>() {
            @Override
            public OrderedData createFromParcel(Parcel in) {
                return new OrderedData(in);
            }

            @Override
            public OrderedData[] newArray(int size) {
                return new OrderedData[size];
            }
        };

        public Integer getOrderId() {
            return orderId;
        }

        public void setOrderId(Integer orderId) {
            this.orderId = orderId;
        }

        public Integer getOrderDetailId() {
            return orderDetailId;
        }

        public void setOrderDetailId(Integer orderDetailId) {
            this.orderDetailId = orderDetailId;
        }

        public String getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(String totalAmount) {
            this.totalAmount = totalAmount;
        }

        public String getCartType() {
            return cartType;
        }

        public void setCartType(String cartType) {
            this.cartType = cartType;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public Integer getQuantity() {
            return quantity;
        }

        public void setQuantity(Integer quantity) {
            this.quantity = quantity;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getUpdated() {
            return updated;
        }

        public void setUpdated(String updated) {
            this.updated = updated;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Integer getCardFlag() {
            return cardFlag;
        }

        public void setCardFlag(Integer cardFlag) {
            this.cardFlag = cardFlag;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public Integer getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Integer categoryId) {
            this.categoryId = categoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String get_new() {
            return _new;
        }

        public void set_new(String _new) {
            this._new = _new;
        }

        public String getAfter() {
            return after;
        }

        public void setAfter(String after) {
            this.after = after;
        }

        public String getCancelledReason() {
            return cancelledReason;
        }

        public void setCancelledReason(String cancelledReason) {
            this.cancelledReason = cancelledReason;
        }

        public Boolean getCancelledOrder() {
            return cancelledOrder;
        }

        public void setCancelledOrder(Boolean cancelledOrder) {
            this.cancelledOrder = cancelledOrder;
        }

        public Boolean getFlag() {
            return flag;
        }

        public void setFlag(Boolean flag) {
            this.flag = flag;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (orderId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(orderId);
            }
            if (orderDetailId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(orderDetailId);
            }
            dest.writeString(totalAmount);
            dest.writeString(cartType);
            dest.writeString(orderStatus);
            dest.writeString(orderNo);
            if (productId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(productId);
            }
            dest.writeString(productCode);
            dest.writeString(price);
            if (quantity == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(quantity);
            }
            dest.writeString(created);
            dest.writeString(updated);
            dest.writeString(status);
            if (cardFlag == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(cardFlag);
            }
            dest.writeString(brand);
            if (categoryId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(categoryId);
            }
            dest.writeString(title);
            dest.writeString(pv);
            dest.writeString(image);
            dest.writeString(createdAt);
            dest.writeString(_new);
            dest.writeString(after);
            dest.writeString(cancelledReason);
            dest.writeByte((byte) (cancelledOrder == null ? 0 : cancelledOrder ? 1 : 2));
            dest.writeByte((byte) (flag == null ? 0 : flag ? 1 : 2));
        }
    }

}
