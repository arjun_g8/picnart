package com.skilledanswers.picnart.e_commerce_module.retrofit_response_models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by SkilledAnswers-D1 on 23-12-2017.
 */

public class AllProductsResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("data")
    @Expose
    private List<AllProductsData> data = null;
    @SerializedName("priceFilter")
    @Expose
    private PriceFilter priceFilter;
    @SerializedName("pvFilter")
    @Expose
    private PvFilter pvFilter;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<AllProductsData> getData() {
        return data;
    }

    public void setData(List<AllProductsData> data) {
        this.data = data;
    }

    public PriceFilter getPriceFilter() {
        return priceFilter;
    }

    public void setPriceFilter(PriceFilter priceFilter) {
        this.priceFilter = priceFilter;
    }

    public PvFilter getPvFilter() {
        return pvFilter;
    }

    public void setPvFilter(PvFilter pvFilter) {
        this.pvFilter = pvFilter;
    }

    public class AllProductsData {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("brand")
        @Expose
        private String brand;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("pv")
        @Expose
        private String pv;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("mrp")
        @Expose
        private String mrp;
        @SerializedName("product_code")
        @Expose
        private String productCode;
        @SerializedName("msp")
        @Expose
        private String msp;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("rate")
        @Expose
        private String rate;

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        private boolean isHasImage;

        public boolean isHasImage() {
            return isHasImage;
        }

        public void setHasImage(boolean hasImage) {
            isHasImage = hasImage;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getBrand() {
            return brand;
        }

        public void setBrand(String brand) {
            this.brand = brand;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPv() {
            return pv;
        }

        public void setPv(String pv) {
            this.pv = pv;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMrp() {
            return mrp;
        }

        public void setMrp(String mrp) {
            this.mrp = mrp;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getMsp() {
            return msp;
        }

        public void setMsp(String msp) {
            this.msp = msp;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }

    public class PriceFilter {

        @SerializedName("maxPrice")
        @Expose
        private String maxPrice;
        @SerializedName("minPrice")
        @Expose
        private String minPrice;

        public String getMaxPrice() {
            return maxPrice;
        }

        public void setMaxPrice(String maxPrice) {
            this.maxPrice = maxPrice;
        }

        public String getMinPrice() {
            return minPrice;
        }

        public void setMinPrice(String minPrice) {
            this.minPrice = minPrice;
        }

    }

    public class PvFilter {

        @SerializedName("maxpv")
        @Expose
        private String maxpv;
        @SerializedName("minpv")
        @Expose
        private String minpv;

        public String getMaxpv() {
            return maxpv;
        }

        public void setMaxpv(String maxpv) {
            this.maxpv = maxpv;
        }

        public String getMinpv() {
            return minpv;
        }

        public void setMinpv(String minpv) {
            this.minpv = minpv;
        }
    }

    @Override
    public String toString() {
        return "AllProductsResponse{" +
                "status=" + status +
                ", data=" + data +
                ", priceFilter=" + priceFilter +
                ", pvFilter=" + pvFilter +
                '}';
    }
}
