package com.skilledanswers.picnart;

import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;


public class SignUpActivity extends AppCompatActivity {
    ///////////////////////////////////////////////// widgit's
    // buttons
    private AppCompatButton buttonSignUp=null;
    private AppCompatButton buttonCancle=null;
    // imageview
    private AppCompatImageView imageViewFB=null;

    // textI/p layout
    private TextInputLayout inputLayoutFname=null;
    private TextInputLayout inputLayoutLname=null;
    private TextInputLayout inputLayoutEmail=null;
    private TextInputLayout inputLayoutmobileIndia=null;
    private TextInputLayout inputLayoutPwd=null;
    // edittext
    private AppCompatEditText editTextFname=null;
    private AppCompatEditText editTextLname=null;
    private AppCompatEditText editTextEmail=null;
    private AppCompatEditText editTextstaticIndianno=null;
    private AppCompatEditText editTextmobileNo=null;
    private AppCompatEditText editTextpwd=null;
    // textview
    private AppCompatTextView textViewlogin=null;
    ////////////////////////////////////////////////// ///////////////end


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /////////////////////////////////// hide the icon bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ////////////////////////////////////////////////////////// end
        setContentView(R.layout.activity_signup);
        /////////////////////////////// toolbar setting

       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setTitle("Sign Up");
        /////////////////////////////////////// end
        /////////////////////////////// widgits to java
        buttonSignUp = findViewById(R.id.signUpActSignUpButtonID);
        buttonCancle= findViewById(R.id.signUpCancleButtonID);
        buttonCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //

        ////////////////////

       /* buttonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SignUpActivity.this,LandingPage.class);
                startActivity(intent);
                finish();
            }
        });*/
    }

}
